

################################
#If the mod does not work, replace the .mod file with "AGOT Faces Modification.mod" in this zip(or folder).
################################

################################
#Changelog
################################
#	-1.2-
#	Added a new Male High Valyrian portrait.(So High Valyrian now basically more like a good-looking Essosi lol)
#	Added new beardstyles for High Valyrian.(Actually just Frankish beards)
#	Made a new Firstmen-Andal Hybrid portrait which more abled to show the Firstman-like appearance.
#	fixed 2.0's problem about the Relation Circles.
#	Edited more characters' DNA to stick to the book's description.
#
#	-1.1-
#	Replaced Ironborn portrait.
#	Replaced Essosi (Free Cities People) portrait.
#	Added Firstmen-Andal Hybrid portrait for Riverlanders, Former Southrons, Royce Family and Sistermen.
#	Modified Andal portrait's cheeks, eyes, chins.
################################