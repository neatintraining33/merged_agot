##############################################################################################
# This file handles Way of Life focus events.
# .100 Initial Routing Events
# .200 Hunting Focus
# .300 Intrigue Focus (PLANNED)
# .400 Carousing Focus (PLANNED)
# .500 Seduction Focus (PLANNED)
# .600 Placeholder
# .700 Placeholder
# .800 Placeholder
# .900 Placeholder
###  SEARCH "###FIXME###" to fix issues.
###  Special Thanks to Dewguru, Noxbestia, and Ngppgn for their periodic help with my coding abilities.  
##############################################################################################

namespace = TTNaughtyextra

####### Initial Routing Events #######
narrative_event = {  ### Routing event for the Rainy Day Hunter Encounter
	id = TTNaughtyextra.100
	hide_window = yes
	only_playable = yes
	min_age =  16
	capable_only = yes
	prisoner = no
	trigger = {
		has_dlc = "Way of Life"
		has_focus = focus_hunting
		in_command = no
		NOT = { has_character_flag = rainyday_hunt }
	}

	mean_time_to_happen = {
		days = 360
		
		modifier = {
			factor = 0.33
			NOT = { has_character_flag = rainyday_hunt }
		}	
		modifier = {
			factor = 1.5
			has_character_flag = rainyday_hunt
		}
		modifier = {
			factor = 1.5
			trait = content
		}
		modifier = {
			factor = 0.8
			trait = hunter
		}
		modifier = {
			factor = 2.0
			trait = slothful
		}
		modifier = {
			factor = 0.33
			trait = patient
		}
		modifier = {
			factor = 1.25
			trait = craven
		}
	}
	immediate = {
		set_character_flag = rainyday_hunt
		if = {
			limit = { # player is a woman
				is_female = yes
				}
				narrative_event = { id = TTNaughtyextra.200 } #Female Player on Male Hunter
				break = yes
				}
		if = {
			limit = { # player is a man
				is_female = no
				}
				narrative_event = { id = TTNaughtyextra.220 } #Male Player on Female Hunter
				break = yes
				}
		}
	}
		
		
		
####################### The Rainy Day Hunter Encounter ##################
####### Female player on Male Hunter #######
narrative_event = {
	id = TTNaughtyextra.200 
	desc = "TTNaughtyextra200" 
	picture = rainy_day
	title = RainyDayhunter
	
	is_triggered_only = yes 
	option = {
		name = "TTNaughtyextra200A" 
		narrative_event = { id = TTNaughtyextra.201 }
		}
}
narrative_event = {
	id = TTNaughtyextra.201
	desc = "TTNaughtyextra201" 
	picture = tavern_1
	title = RainyDayhunter
	
	is_triggered_only = yes 
	option = {
		name = "TTNaughtyextra201A" 
		narrative_event = { id = TTNaughtyextra.202 }
		}
}
narrative_event = {
	id = TTNaughtyextra.202
	desc = "TTNaughtyextra202" 
	picture = male_hunter
	title = RainyDayhunter
	
	is_triggered_only = yes 
	option = {
		name = "TTNaughtyextra202A" 
		narrative_event = { id = TTNaughtyextra.203 }
		}
	option = { # Player choice to end the event chain
		name = "TTNaughtyextra202B" 
		add_character_modifier = { name = tt_still_wanting_more duration = 10 }
		}
}
narrative_event = {
	id = TTNaughtyextra.203
	desc = "TTNaughtyextra203" 
	picture = hunter_8
	title = RainyDayhunter
	
	is_triggered_only = yes 
	option = {
		name = "TTNaughtyextra203A" 
		narrative_event = { id = TTNaughtyextra.204 }
		}
}
narrative_event = {
	id = TTNaughtyextra.204
	desc = "TTNaughtyextra204" 
	picture = hunter_3
	title = RainyDayhunter
	
	is_triggered_only = yes 
	option = {
		name = "TTNaughtyextra204A" 
		narrative_event = { id = TTNaughtyextra.205 }
		}
}
narrative_event = {
	id = TTNaughtyextra.205
	desc = "TTNaughtyextra205" 
	picture = hunter_4
	title = RainyDayhunter
	
	is_triggered_only = yes 
	option = {
		name = "TTNaughtyextra205A" 
		narrative_event = { id = TTNaughtyextra.206 }
		}
	option = {
		name = "TTNaughtyextra205B" 
		narrative_event = { id = TTNaughtyextra.206 }
		}
}
narrative_event = {
	id = TTNaughtyextra.206
	desc = "TTNaughtyextra206" 
	picture = hunter_5
	title = RainyDayhunter
	
	is_triggered_only = yes 
	option = {
		name = "TTNaughtyextra206A" 
		narrative_event = { id = TTNaughtyextra.207 }
		}
}
narrative_event = {
	id = TTNaughtyextra.207
	desc = "TTNaughtyextra207" 
	picture = hunter_7
	title = RainyDayhunter
	
	is_triggered_only = yes 
	option = {
		name = "TTNaughtyextra207A" 
		narrative_event = { id = TTNaughtyextra.208 }
		}
}
narrative_event = {
	id = TTNaughtyextra.208
	desc = "TTNaughtyextra208" 
	picture = hunter_10
	title = RainyDayhunter
	
	is_triggered_only = yes 
	option = {
		name = "TTNaughtyextra208A" 
		add_character_modifier = { name = tt_good_sex duration = 30 }
		}
	option = {
		name = "TTNaughtyextra208B" 
		add_character_modifier = { name = tt_good_sex duration = 30 }
		character_event = { id = TTVisits.9010 }
		}
}
####### Female player on Male Hunter <END> #######
####### Male player on Female Hunter #######
narrative_event = {
	id = TTNaughtyextra.220 
	desc = "TTNaughtyextra220" 
	picture = rainy_day
	title = RainyDayhunter2
	
	is_triggered_only = yes 
	option = {
		name = "TTNaughtyextra220A" 
		narrative_event = { id = TTNaughtyextra.221 }
		}
}
narrative_event = {
	id = TTNaughtyextra.221
	desc = "TTNaughtyextra221" 
	picture = tavern_1
	title = RainyDayhunter2
	
	is_triggered_only = yes 
	option = {
		name = "TTNaughtyextra221A" 
		narrative_event = { id = TTNaughtyextra.222 }
		}
}
narrative_event = {
	id = TTNaughtyextra.222
	desc = "TTNaughtyextra222" 
	picture = hunter_1
	title = RainyDayhunter2
	
	is_triggered_only = yes 
	option = {
		name = "TTNaughtyextra222A" 
		narrative_event = { id = TTNaughtyextra.223 }
		}
	option = { # Player choice to end the event chain
		name = "TTNaughtyextra222B" 
		add_character_modifier = { name = tt_still_wanting_more duration = 10 }
		}
}
narrative_event = {
	id = TTNaughtyextra.223
	desc = "TTNaughtyextra223" 
	picture = hunter_8
	title = RainyDayhunter2
	
	is_triggered_only = yes 
	option = {
		name = "TTNaughtyextra223A" 
		narrative_event = { id = TTNaughtyextra.224 }
		}
}
narrative_event = {
	id = TTNaughtyextra.224
	desc = "TTNaughtyextra224" 
	picture = hunter_3
	title = RainyDayhunter2
	
	is_triggered_only = yes 
	option = {
		name = "TTNaughtyextra224A" 
		narrative_event = { id = TTNaughtyextra.225 }
		}
}
narrative_event = {
	id = TTNaughtyextra.225
	desc = "TTNaughtyextra225" 
	picture = hunter_4
	title = RainyDayhunter2
	
	is_triggered_only = yes 
	option = {
		name = "TTNaughtyextra225A" 
		narrative_event = { id = TTNaughtyextra.226 }
		}
}
narrative_event = {
	id = TTNaughtyextra.226
	desc = "TTNaughtyextra226" 
	picture = hunter_5
	title = RainyDayhunter2
	
	is_triggered_only = yes 
	option = {
		name = "TTNaughtyextra226A" 
		narrative_event = { id = TTNaughtyextra.227 }
		}
}
narrative_event = {
	id = TTNaughtyextra.227
	desc = "TTNaughtyextra227" 
	picture = hunter_7
	title = RainyDayhunter2
	
	is_triggered_only = yes 
	option = {
		name = "TTNaughtyextra227A" 
		narrative_event = { id = TTNaughtyextra.228 }
		}
}
narrative_event = {
	id = TTNaughtyextra.228
	desc = "TTNaughtyextra228" 
	picture = hunter_11
	title = RainyDayhunter2
	
	is_triggered_only = yes 
	option = {
		name = "TTNaughtyextra228A" 
		add_character_modifier = { name = tt_good_sex duration = 30 }
		}
	option = {
		name = "TTNaughtyextra228B" 
		add_character_modifier = { name = tt_good_sex duration = 30 }
		}
}
####### Male player on Female Hunter <END> #######