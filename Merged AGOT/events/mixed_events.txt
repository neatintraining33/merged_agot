namespace = royconv
	character_event = {
		id = royconv.0
		desc = "EVTDESCROYCONV"
		picture = "GFX_valyria_dragon"
	
		is_triggered_only = yes
	
		option = { 
		name = "EVTROYCONV" 
			piety = -50
			prestige = 20
		}
}
	character_event = {
		id = royconv.1
		desc = "EVTDESCROYCONV1"
		picture = "GFX_summer_islanders"
	
		is_triggered_only = yes
	
		trigger = {
		higher_real_tier_than = BARON
		is_ruler = yes
		OR = {
		culture = summer_tyroshi
		AND = {
			culture = summer_islander
			top_liege = { culture = tyroshi }
			}
		}
		NOT = { capital_scope = { culture = summer_tyroshi } }
		}

		option = { 
		name = "EVTROYCONV1" 
			piety = -10
			any_courtier = {
				limit = {
					culture = ROOT
					employer = { character = ROOT }
					dynasty = ROOT
				}
				culture = summer_tyroshi
			}
			capital_scope = { culture = summer_tyroshi }
		}
}
	character_event = {
		id = royconv.2
		desc = "EVTDESCROYCONV2"
		picture = "GFX_dothraki_camp"
	
		is_triggered_only = yes

		trigger = {
		higher_real_tier_than = BARON
		is_ruler = yes
		OR = {
		culture = dothrak_tyroshi
		AND = {
			culture = dothraki
			top_liege = { culture = tyroshi }
			}
		}
		NOT = { capital_scope = { culture = dothrak_tyroshi } }
		}
	
		option = { 
		name = "EVTROYCONV2" 
			piety = -10
			any_courtier = {
				limit = {
					culture = ROOT
					employer = { character = ROOT }
					dynasty = ROOT
				}
				culture = dothrak_tyroshi
			}
			capital_scope = { culture = dothrak_tyroshi }
		}
}
	character_event = {
		id = royconv.3
		desc = "EVTDESCROYCONV2"
		picture = "GFX_dothraki_camp"
	
		is_triggered_only = yes
	
		option = { 
		name = "EVTROYCONV2" 
			piety = -10
			prestige = 20
		}
}
	character_event = {
		id = royconv.4
		desc = "EVTDESCROYCONV1"
		picture = "GFX_summer_islanders"
	
		is_triggered_only = yes
	
		option = { 
		name = "EVTROYCONV1" 
			piety = -10
			prestige = 20
		}
}
	character_event = {
		id = royconv.5
		desc = "EVTDESCROYCONV5"
		picture = "GFX_evt_valyria_2"
	
		is_triggered_only = yes

		trigger = {
		higher_real_tier_than = BARON
		is_ruler = yes
		OR = {
		culture = iron_valyrian
		AND = {
			culture = ironborn
			top_liege = { culture_group = valyrian }
			NOT = { top_liege = { religion_group = westerosi_religion } }
			}
		}
		NOT = { capital_scope = { culture = iron_valyrian } }
		}
	
		option = { 
		name = "EVTROYCONV5" 
			piety = 10
			any_courtier = {
				limit = {
					culture = ROOT
					employer = { character = ROOT }
					dynasty = ROOT
				}
				culture = iron_valyrian
			}
			capital_scope = { culture = iron_valyrian }
		}
}
character_event = {
	id = royconv.6
	desc = "EVTDESCROYCONV6"
	picture = GFX_evt_valyria_2
	only_playable = yes
		

	is_triggered_only = yes
		
	war = no

	trigger = {
		in_command = no
		higher_tier_than = DUKE
		prestige = 2000
		OR = {
			culture = valyrian_dothraki
			culture = valyrian_yi_ti
			culture = valyrian_lengi
			culture = valyrian_summer_islander
			culture = valyrian_ghiscari
			culture = iron_valyrian
			culture = mountain_valyrian
			culture = shadow_valyrian
			culture = valyrian_sarnori
			culture = valyrian_qartheen
		}
		NOT = { has_artifact = dragon_egg }
		NOT = {
			trait = incapable
			trait = infirm
			trait = inbred
			trait = imbecile
		}

	}

	option = { 
	name = "EVTROYCONV6" 
		prestige = 105
		add_artifact = dragon_egg 
		}
	option = { 
	name = "EVTBROYCONV6" 
		wealth = 250
		prestige = -100 
		}
}
#Convert to new religion
	character_event = {
		id = royconv.7
		desc = "EVTDESCROYCONV7"
		picture = "GFX_valyria_dragon"
	
		is_triggered_only = yes
	
		option = { 
		name = "EVTROYCONV7" 
			capital_scope = { religion = ROOT }
			piety = -150
			prestige = 20
		}
}
#High Dothraki
	character_event = {
		id = royconv.8
		desc = "EVTDESCROYCONV8"
		picture = "GFX_evt_dothraki"
	
		is_triggered_only = yes
	
		option = { 
		name = "EVTROYCONV8" 
			capital_scope = { religion = ROOT }
			piety = -150
			prestige = 20
		}
}