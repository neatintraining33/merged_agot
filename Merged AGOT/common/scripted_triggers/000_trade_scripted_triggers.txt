far_east_trade_route_post_trigger = {
	OR = {
		province_id = 585
		province_id = 595
		province_id = 606
		province_id = 638
		province_id = 648
		province_id = 659
		province_id = 660
		province_id = 663
		province_id = 674
		province_id = 678
		province_id = 680
		province_id = 709
		province_id = 717
		#province_id = 719
		province_id = 727
		province_id = 728
		province_id = 729
		province_id = 765
		province_id = 770
		province_id = 777
		province_id = 780
		province_id = 788
		province_id = 790
		province_id = 792
		province_id = 795
		province_id = 835
		province_id = 847
		province_id = 852
		province_id = 862
		province_id = 865
		province_id = 875
	}
}
central_trade_route_post_trigger = {
	OR = {
		province_id = 406
		province_id = 417
		province_id = 419
		province_id = 433
		province_id = 450
		province_id = 470
		province_id = 471
		province_id = 476
		province_id = 484
		province_id = 542
		province_id = 544
		province_id = 545
		province_id = 546
		province_id = 549
		province_id = 550
		province_id = 551
		province_id = 553
		province_id = 555
		province_id = 561
		province_id = 564
		province_id = 565
		province_id = 576
		province_id = 585
		province_id = 590
		province_id = 598
		province_id = 599
		province_id = 604
		province_id = 606
		province_id = 610
		province_id = 613
		province_id = 638
		province_id = 639
		province_id = 654
		province_id = 663
		province_id = 700
		province_id = 703
		province_id = 709
	}	
}
southern_trade_route_post_trigger = {
	OR = {
		province_id = 368
		province_id = 491
		province_id = 493
		province_id = 496
		province_id = 501
		province_id = 502
		province_id = 526
		province_id = 600
		province_id = 601
		province_id = 619
		province_id = 623
		province_id = 625
		province_id = 626
		province_id = 663
		province_id = 729
		province_id = 747
		province_id = 751
	}	
}
rhoyne_trade_route_post_trigger = {
	OR = {
		province_id = 413
		province_id = 414
		province_id = 417
		province_id = 420
		province_id = 422
		province_id = 423
		province_id = 428
		province_id = 429
		province_id = 433
		province_id = 450
		province_id = 476
		province_id = 762
	}	
}
#valyria_trade_route_post_trigger = {
#	OR = {
#		province_id = 597
#		province_id = 755
#		province_id = 756
#		province_id = 757
#		province_id = 758
#		province_id = 759
#		province_id = 760
#		province_id = 761
#		province_id = 1178
#		province_id = 1184
#	}
#}
narrowsea_route_post_trigger = {
	OR = {
		province_id = 46
		province_id = 57
		province_id = 62
		province_id = 119
		province_id = 358
		province_id = 359
		province_id = 366
		province_id = 367
		province_id = 378
		province_id = 1142
	}
}
kingsroad_route_post_trigger = {
	OR = {
		province_id = 17
		province_id = 20
		province_id = 27
		province_id = 47
		province_id = 84
		province_id = 107
		province_id = 119
		province_id = 224
		province_id = 226
		province_id = 227
		province_id = 232
		province_id = 304
		province_id = 318
		province_id = 324
		province_id = 356
		province_id = 1203
	}
}
goldroad_route_post_trigger = {
	OR = {
		province_id = 182
		province_id = 183
		province_id = 194
		province_id = 220
		province_id = 221
		province_id = 226
		province_id = 238
		province_id = 1203
	}
}
river_road_route_post_trigger = {
	OR = {
		province_id = 91
		province_id = 97
		province_id = 101
		province_id = 112
		province_id = 174
		province_id = 178
	}
}
high_road_route_post_trigger = {
	OR = {
		province_id = 136
		province_id = 149
	}
}
roseroad_route_post_trigger = {
	OR = {
		province_id = 212
		province_id = 226
		province_id = 227
		province_id = 253
		province_id = 267
		province_id = 276
		province_id = 286
		province_id = 291
		province_id = 293
		province_id = 1202
		province_id = 1203
	}
}
oceanroad_route_post_trigger = {
	OR = {
		province_id = 167
		province_id = 183
		province_id = 197
		province_id = 235
	}
}
reach_coast_route_post_trigger = {
	OR = {
		province_id = 79
		province_id = 92
		province_id = 161
		province_id = 292
	}
}
dorneroad_route_post_trigger = {
	OR = {
		province_id = 306
		province_id = 316
		province_id = 330
		province_id = 334
		province_id = 349
		province_id = 350
		province_id = 366
	}
}
princespass_route_post_trigger = {
	OR = {
		province_id = 261
		province_id = 262
		province_id = 279
		province_id = 328
		province_id = 335
	}
}