## Clawmen ##
#non-canon
174513 = {
	name="Barren"
	culture = crackclawmen
}
174514 = {
	name="Whysper"
	culture = crackclawmen
}
174516 = {
	name="Vally"
	culture = crackclawmen
}
174517 = {
	name="Bayman"
	culture = crackclawmen
}
174518 = {
	name="Point"
	culture = crackclawmen
}
174519 = {
	name="Batler"
	culture = crackclawmen
}
174520 = {
	name="Sentel"
	culture = crackclawmen
}
174521 = {
	name="Woodgarde"
	culture = crackclawmen
}
174522 = {
	name="Archyr"
	culture = crackclawmen
}
174523 = {
	name = Rackley
	culture = crackclawmen
}
174524 = {
	name = Crackshield
	culture = crackclawmen
}
174525 = {
	name = Cragby
	culture = crackclawmen
}
174526 = {
	name = Fallow
	culture = crackclawmen
}
174527 = {
	name = Knolley
	culture = crackclawmen
}
174528 = {
	name = Peary
	culture = crackclawmen
}
174529 = {
	name = Swayne
	culture = crackclawmen
}
174530 = {
	name = Towers
	culture = crackclawmen
}
174531 = {
	name = Hedge
	culture = crackclawmen
}
174532 = {
	name = Chayce
	culture = crackclawmen
}
174533 = {
	name = Dyne
	culture = crackclawmen
}
174534 = {
	name = Passland
	culture = crackclawmen
}
174535 = {
	name = Agnell
	culture = crackclawmen
}
174537 = {
	name = Hory
	culture = crackclawmen
}
174538 = {
	name = Trayce
	culture = crackclawmen
}
174539 = {
	name= Clayne
	culture = crackclawmen
}
174540 = {
	name= Humblehide
	culture = crackclawmen
}
174541 = {
	name= Farrowbone
	culture = crackclawmen
}
174542 = {
	name= Pinebone
	culture = crackclawmen
}
174543 = {
	name= Earthe
	culture = crackclawmen
}
174544 = {
	name= Hoger
	culture = crackclawmen
}
174545 = {
	name= Tuskend
	culture = crackclawmen
}
174546 = {
	name= Freewood
	culture = crackclawmen
}
174547 = {
	name= Treemore
	culture = crackclawmen
}
174548 = {
	name= Troggs
	culture = crackclawmen
}
174549 = {
	name= Squall
	culture = crackclawmen
}
174550 = {
	name= Rooke
	culture = crackclawmen
}
174551 = {
	name= Hammar
	culture = crackclawmen
}
174552 = {
	name= Dyrk
	culture = crackclawmen
}

#canon
174410 = {
	name="Boggs"
	culture = crackclawmen
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 40
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
42 = {
	name="Brune"
	culture = crackclawmen
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 21
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
174411 = {
	name="Cave"
	culture = crackclawmen
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 42
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1197 = {
	name="Crabb"
	culture = crackclawmen
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 27
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
47 = {
	name="Hardy"
	culture = crackclawmen
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 43
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
174412 = {
	name="Pyne"
	culture = crackclawmen
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 44
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}