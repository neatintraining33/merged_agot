k_pykeTK = {
	color = { 219 147 29 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_pyke }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 162 # Pyke
	creation_requires_capital = no
	culture = ironborn
}