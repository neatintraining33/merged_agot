k_duskendaleTK = {
	color = { 48 26 60 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_duskendale }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 212 # Duskendale
	creation_requires_capital = yes
	culture = crownlander

	title = "shadowking_male"
	title_female = "shadowking_female"
}

k_rosbyTK = {
	color = { 189 189 189 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_rosby }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 211 # Rosby
	creation_requires_capital = no
	culture = crownlander
}