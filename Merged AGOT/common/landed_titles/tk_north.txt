k_barrowlandsTK = {
	color = { 202 157 17 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_barrowlands }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 46 # Barrowton
	creation_requires_capital = yes
	culture = northman

	title = "barrowking_male"
	title_female = "barrowking_female"
	title_prefix = "prefix_kingdomofthe"
}

k_blackpoolTK = {
	color = { 145 145 213 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		capital_holding = { c_blackpool }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 533 # Blackpool
	creation_requires_capital = yes
	culture = northman

	title_prefix = "prefix_kingdomofthe"
}

k_dreadfortTK = {
	color = { 235 44 107 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_dreadfort }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 53 # The Dreadfort
	creation_requires_capital = no
	culture = northman

	title = "dreadking_male"
	title_female = "dreadking_female"
	title_prefix = "prefix_kingdomofthe"
}

k_flintsfingerTK = {
	color = { 215 231 230 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_flintsfinger }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 73 # Flint's Finger
	creation_requires_capital = yes
	culture = northman
}

k_hornwoodTK = {
	color = { 204 84 2 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_hornwood }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 60 # Hornwood
	creation_requires_capital = yes
	culture = northman

	title_prefix = "prefix_kingdomofthe"
}

k_karholdTK = {
	color = { 145 145 213 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_karhold }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 29 # Karhold
	creation_requires_capital = yes
	culture = northman
}

k_lasthearthTK = {
	color = { 214 19 11 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_lasthearth }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 25 # Last Hearth
	creation_requires_capital = yes
	culture = northman

	title_prefix = "prefix_kingdomofthe"
}

k_theneckTK = {
	color = { 13 122 91 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_theneck }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 71 # Greywater Watch
	creation_requires_capital = yes
	culture = crannogman

	title = "marshking_male"
	title_female = "marshking_female"
}

k_northclansTK = {
	color = { 8 96 202 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_northclans }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 22 # Arrendell
	creation_requires_capital = yes
	culture = hill_clansman

	title_prefix = "prefix_kingdomofthe"
}

k_oldcastleTK = {
	color = { 145 145 213 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_oldcastle }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 65 # Oldcastle
	creation_requires_capital = yes
	culture = northman

	title_prefix = "prefix_kingdomofthe"
}

k_rillsTK = {
	color = { 132 151 174 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_rills }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 41 # The Rills
	creation_requires_capital = yes
	culture = northman

	title_prefix = "prefix_kingdomofthe"
}

k_skagosTK = {
	color = { 121 121 121 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_skagos }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 31 # Kingshouse
	creation_requires_capital = yes
	culture = skagosi
}

k_stonyshoreTK = {
	color = { 135 165 200 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_stonyshore }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 40 #Stony Shore
	creation_requires_capital = yes
	culture = northman

	title_prefix = "prefix_kingdomofthe"
}

k_whiteharborTK = {
	color = { 230 186 60 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_whiteharbor }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 62 # White Harbor
	creation_requires_capital = yes
	culture = northman
}

k_winterfellTK = {
	color = { 225 225 225 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_winterfell }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 47 # Winterfell
	creation_requires_capital = yes
	culture = northman

	title = "winterking_male"
	title_female = "winterking_female"
}

k_wolfswoodTK = {
	color = { 95 163 91 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_wolfswood }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 39 # Deepwood
	creation_requires_capital = yes
	culture = northman

	title = "wolfking_male"
	title_female = "wolfking_female"
	title_prefix = "prefix_kingdomofthe"
}