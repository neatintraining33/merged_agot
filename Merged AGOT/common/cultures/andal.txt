andal = {
	graphical_cultures = { westerngfx } # buildings, portraits, units

	old_andal = { # Old Andals
		graphical_cultures = { normangfx } # units
		color = { 0.2 0.4 0.7 }

		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }

		male_names = {
			Addam_Adam Aethelmure Alesander Alyn Alyx Ambrose Andahar Armistead
			Andrey Androw Anguy Argrave Arlan Arwood Arwyn Andrian Arbert Adron Anson Alvyn Alard Athos Axel
			Belgrave Ben Benfrey Bennet Bennifer Bradamar Bryan Brynden Bryar Benjicot Bellos Barion Beslon Bryne Benedict Bernarr
			Cetheres Clarence Clement Cleos Colmar Cosgrove Creighton Criston Cormond Cleyson Cavan
			Dafyn Damon Danwell Dennett Denys Deremond Desmond Dickon Donnel Dudley Duncan Derrick
			Ebrose Edmund Edmure Edmyn Edwyn Ellery Elmar Elwood Emmon Enger Erreg Euson Erron Ellard 
			Eddyn Estion Ermen Elys Eddam
			Florian Franklyn Forrest
			Garrett Garse Geremy Gillam Gaston Gwayne Garibald Garyn Gorgon
			Halmon Harbert Harry Harsley Harys Hendry Hibald Hosteen Hoster Hugo Hobert Harwin Halion Harstyn Humfrey Hollis
			Illifer Igon
			Jaime Jammos_Jamos Jared Jason Jeffory John_Jon Jon_Jon Jonn_Jon Jonos Jonothor Jonthor Joseth Jorin Josua
			Karyl Kirth Kennoth Kelis Kenric Kedge
			Lennocks Leslyn Lewys Lister Lothar Lucamore Lucan Lucas Luceon Lucias Luke Lyman Lymond Lyonel Larys Leon
			Malwyn Manfred Manfryd Marq Martyn Mathis Maynard Medgar Melwys Meribald Merrett Mohor Myles Mance Madden
			Narbert Ned Norbert
			Olyvar Osmund Osmynd Oswell Otho Owen Othan Oscar Olver Orland
			Pate Patrek Perwyn Petyr Pyron
			Quentyn Quincy
			Rawney Raymond Raymun Raymund Robert Robin Roger Roland Ronald Ronel Ryger Ryman Rormund Rorran Ryan Roderick Redwyn
			Sandor Steffon Stevron Symond Simons Samwell
			Theo Theomar Thoren_Toren Tion Tommard Tristan Tristifer Tytos Thaddeus Tymond Tomas Terin Torrence Tyler Tommen
			Utherydes
			Walder Walton Waltyr Wendel Whalen Wilbert Willam Willamen Willem William Willis Willum Wyl Willard Will
			Zachery
		}
		female_names = {
			Alyce Alys Alysanne:1000 Alyssa:1000 Alyx:1000 Amabel:500 Amerei:1000 Arwyn Allise Aleria Alerie Amena Arwen Aglantine
			Arianne Ammara Aelinor Andrya Amyra Amera Alise Alessa Amane Alayne Annara
			Alianne Arysa Alle Alannis Andara Aemma Alicent Ariela Alla Amalia Annet Amarei
			Alison Ashara Agnes:1000 Anya Alynne
			Barba:1000 Barbara Becca:500 Bella_Belle:500 Bellena_Belenna Bess Bessa:500 Bethany:1000 Belarra Bea Betha:500 Brella
			Beony Barra Brienne Belandra Briony
			Carellen Cass Catelyn:1750 Cynthea:500 Carene_Cerena Carene_Carene Cyrene Carolei Cariah
			Cassana Corenna Cyrenna Cassandra Cymoril Ceryse Cersei:500 Cindra Cerelle Cerissa Cerenna
			Danelle Della:500 Darianne Dolessa Damina Darlessa Dorcas Dorna Deana Donella Desmerei Dalla
			Delena Denyse Desmera Donyse Dyna Darla:1000
			Eleanor Ellery Elyana Emberlei:500 Emphyria Ermesande Elonne  Eleyne Elyria Elenya Elinor
			Eglantine Elene Elyn Ellyn Emma Eionne Elspeth Elenora  Emmara Elanna Elyanna
			Elenei Estalia Esme Ella
			Fiona Falena Falyse Flora Falia Freya Fruella Frynne
			Genna Gallisa Gretchel Grisel Ganna Gwyn Gwyneth Gella
			Harra Hostella:500 Helya Halanna Helicent Helena Helyna
			Ilyse_Ilysa Ilyse_Ilyse Idera Idana Ismyra Irya
			Jayne:1000 Jeyne:1500 Jonquil Joyeuse:500 Jyanna:500  Jannia Janei Janna Jeyne Joanne Jocelyn Joy Joyeuse
			Jeane Jenna Johanna Janyce Jonella Jonnela Jyana Janye Jerene Jalona Jena
			Jeona Jancia Jennelyn Joanna
			Kyra:500 Keleria Kellyn Kelyce Kella
			Lacey Leana Leslyn Liane Lysa:1750 Lythene:500 Leomella Lorell Leonylla Lanna Layna Leana Leonella
			Leonette Lira Lucia Lenora Lysessa Lynessa Lia Lelia Lorea Leona Lyessa Lyra Lythene Lyssa Laena
			Lia Leyla Lynesse Lysana Lemore Lorra Lynora Leila Lollys
			Maerie Marianne Marissa:1000 Mariya:1000 Masha Mellara:1000 Merianne:500 Minisa Mordane Morya:500 Mylessa
			Meredith_Meredyth Melene Melantha:1000 Malessa Margot Melesa Mina Myrielle Myrcella Melessa Margot Meria 
			Manysa Marsella Mela Melicent Mya Myranda Mary Marya Mhaegan Moelle Mylenda Megga
			Maris Matrice Margaery Mychella Malora Melissa:1000 Maegelle:500 Marla Maia
			Orissa Osmeria Olanei Olene Olenna Obela
			Nolla
			Perra:750 Perriane:750 Pia Philena Patrice Pya Pella Peony
			Randa Ravella Rhialta Roslin:1000 Ryella:500 Riona Ronessa Rosamund Reyna Rowena Rienne Roelle Rhonda
			Rhea Rohanne Rylene Rylla Rylena
			Sallei Sarra:750 Sarya Serra:750 Sharna Shella Shirei:500 Sylwa Sylvia Syrella Sabrina Sabitha Shiera:1000 Saranella
			Sybell Sharra Sirona Sharena Serina Scolera Selyse Sabrine Senelle Shireen Shyra
			Tysane:750 Tyta Tyressa Tyiah Tilly Tiona Tya Tysha Tinessa Tyshara Theona Tarissa Teora Tyana
			Talla Tanda Tania Tanselle Tyene
			Unella Ursula
			Violet Victoria_Victaria Valiete
			Yolanda Ysila Yorena
			Walda:1000 Wynafrei Wylla
			Zhoe Zia:750
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 25
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 5
		mat_grm_name_chance = 20
		mother_name_chance = 0

		modifier = default_culture_modifier
	}


	crownlander = { # Waterman
		graphical_cultures = { crownlandergfx normangfx } # units
		color = { 0.75 0.2 0.5 }

		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }

		male_names = {
			Addam Addison Adrian Aegon Aemon Aenys Albin Allard Alliser Alton Alyn Arthor Arryk
			Baelor Balman Balon Belard Bennard Bernarr Bolen Bonifer Boros Bryen Byron Buford
			Clarence Clayton Clement
			Daemon Daeron Dale Damond Danos Daryn Davos Denys Devan Donnel Dontos Duncan Duram Durance
			Edric Edwyn Ellard Elwood Elyas Enfren Erryk Eustace
			Gilbert Godry Gormon Gregor Gwayne Gulian Guncer Gunthor Gyles
			Haric Harmon Harrold Harys Herman Holtyn Howard Hubard Humfrey Hyle
			Irwen
			Jacelyn Jaime Janos Jaremy Jarion Jarman Jarmon Jeomar Joffrey Jon Jothos Justin
			Kendel
			Laren Lothar Lothor Lucifer Lucos Luthor
			Maegor Mandon Manfrey Manly Maric Marston Mathar Matthos Merrell Morros Mortimer
			Olyvar Ormory Orryn Osmund Osney Ossifer Oswell Owain
			Perkin Perwyn
			Rayford Raymund Renfred Rennifer Rhaegar Richard Rickard Robert Robin Roger Rolland Rupert
			Samwell Sefton Simon Steffon Symon Symond
			Tarell Tommen Trebor Triston Trystane Tywin
			Qarlton Quenton
			Viserys
			Wallace Whalon Willas
			Yohnen
		}
		female_names = {
			Alarra Alerie Alona Alyce Alynne Alys Alysanne Alyssa Amyana Annara Arianne Arwyn Ashaella Ashera
			Beony Bethany
			Cassana Cassella Catelyn Cersei Colianne Cynthea
			Dorea Doryssa
			Elenora Ella Elleria Elinda Elinor Elrone Ermesande
			Falena Falyse
			Hazel Hemona Hollyse
			Idane Ismyra
			Jalona Janei Janora Jarella Jendella Jenette Jenna Jennei Jeyne Joanna Jocelyn Jomylla Jonquil
			Larra Lenylla Leria Lollys Lyra Lysa
			Maegelle Maia Margaery Marya Melianne Mella Meredyth Myrcella Myrielle
			Nella
			Obany Olenna Olianne Onna Oreyne Oriya
			Pella Pennei Priscella
			Raella Roncianne Rosamund Roslin
			Salanna Sallei Samantha Saranella Saryanna Selyse Semella Shaella Sherion Shireen Shirei Syrianne
			Tanda Tarianne Tysane Tyta
			Wella
			Ysabel
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 25
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 5
		mat_grm_name_chance = 20
		mother_name_chance = 0

		modifier = default_culture_modifier
	}

	dornish_andal = { # Dornishman
		#Andal Dornishmen before Rhoynar invasion
		graphical_cultures = { crownlandergfx normangfx westerngfx } # units
		color = { 0.95 0.6 0 }

		used_for_random = no
		allow_in_ruler_designer = yes

		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }

		male_names = {
			#canon
			Albin  
			Benedict 
			Davos
			Ferris
			Garrison
			Joffrey
			Lucifer
			Morgan
			Olyvar
			Samwell
			Vorian
			Yoren Yorick

			Alaric Anders Andrey Archibald Arthur Alester Andar Albar Aladore Arys Armond
			Bedwyck Beldecar Bors Ben Byren Bayard
			Cletus Colin Clayton
			Dagos Deziel Dickon Desmond
			Edric Elyas Edwyn
			Franklyn Florian Fredrec
			Gerold Gerris Gulian Gwayne
			Harmen Humfrey Harrold
			Igon
			Jon Jeremy
			Kyle
			Lyonel Lucas Lyman
			Mallor Manfrey Mors Myles Mark Martyn Meros
			Normund
			Ormond Orton Olymer
			Pate Petyr
			Robert Ronnel Raymund Rickard
			Symon Steffon
			Theodan Timeon Timoth Trebor Tremond Titus Theodore
			Ulrick Ulwyck
			Victor
			Willam Wyman
			Yandry
		}
		female_names = {
			Alyce Alys Alysanne Alyssa Alyx Amabel Amerei Arwyn Allise Aleria Alerie Amena Arwen Aglantine
			Arianne:1000 Ammara Aelinor Andrya Amyra Amera Alise Alessa Amane Alayne Annara
			Alianne Arysa Alle Alannis Andara Aemma Alicent Ariela Alla Amalia Annet
			Alison Ashara Agnes Amarei Anya Alynne
			Barba Barbara Becca Bella_Belle Bellena_Belenna Bess Bessa Bethany Belarra Bea Betha Brella
			Beony Barra Brienne Belandra Briony
			Carellen Cass Catelyn Cynthea Carene_Cerena Carene_Carene Cyrene Carolei Cariah
			Cassana Corenna Cyrenna Cassandra Cymoril Ceryse Cersei Cindra Cerelle Cerissa Cerenna
			Danelle Della Darianne Dolessa Damina Darlessa Dorcas Dorna Deana Donella Desmerei Dalla
			Delena Denyse Desmera Donyse Dyna Darla
			Eleanor Ellery Elyana Emberlei Emphyria Ermesande Elonne  Eleyne Elyria Elenya Elinor
			Eglantine Elene Elyn Ellyn Emma Eionne Elspeth Elenora  Emmara Elanna Elyanna
			Elenei Estalia Esme Ella
			Fiona Falena Falyse Flora Falia Freya Fruella Frynne
			Genna Gallisa Gretchel Grisel Ganna Gwyn Gwyneth Gella
			Harra Hostella Helya Halanna Helicent Helena Helyna
			Ilyse_Ilysa Ilyse_Ilyse Idera Idana Ismyra Irya
			Jayne Jeyne:1000 Jonquil Joyeuse Jyanna  Jannia Janei Janna Jeyne Joanne Jocelyn Joy Joyeuse
			Jeane Jenna Johanna Janyce Jonella Jonnela Jyana Janye Jerene Jalona Jena
			Jeona Jancia Jennelyn:1500 Joanna
			Kyra Keleria Kellyn Kelyce Kella
			Lacey Leana Leslyn Liane Lysa Lythene Leomella Lorell Leonylla Lanna Layna Leana Leonella
			Leonette Lira Lucia Lenora Lysessa Lynessa Lia Lelia Lorea Leona Lyessa Lyra Lythene Lyssa
			Lia Leyla Lynesse Lysana Lemore Loreza:1000 Lynora Leila Lollys
			Maerie Marianne Marissa Mariya Masha Mellara Merianne Minisa Mordane Morya Mylessa
			Meredith_Meredyth Melene Melantha Malessa Margot Melesa Mina Myrielle Myrcella Melessa Margot Meria 
			Manysa Marsella Mela Melicent Mya Myranda Mary Marya Mhaegan Moelle Mylenda Megga
			Maris Matrice Margaery Mychella Malora Melissa Maegelle Maia
			Orissa Osmeria Olanei Olene Olenna Obela:1500
			Nolla Nysterica:500 Nym:500
			Perra Perriane Pia Philena Patrice Pya Pella Peony
			Randa Ravella Rhialta Roslin Ryella Riona Ronessa Rosamund Reyna Rowena Rienne Roelle Rhonda
			Rhea Rohanne Rylene Rylla Rylena
			Sallei Sarra Sarya Serra Sharna Shella Shirei Sylwa Sylvia Syrella Sabrina Sabitha Shiera Saranella
			Sybell Sharra Sirona Sharena Serina Scolera Selyse Sabrine Senelle Shireen Shyra
			Tysane Tyta Tyressa Tyiah Tilly Tiona Tya Tysha Tinessa Tyshara Theona Tarissa Teora Tyana
			Talla Tanda Tania Tanselle Tyene:1000
			Unella Ursula
			Violet Victoria_Victaria Valiete
			Yolanda Ysila Yorena
			Walda Wynafrei Wylla
			Zhoe Zia
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 15
		mat_grf_name_chance = 10
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 15
		mat_grm_name_chance = 10
		mother_name_chance = 0

		modifier = default_culture_modifier
	}

	iron_andal = { # Ironman
		#Andal Iron Islanders if they succeed in taking over the Iron Isles
		graphical_cultures = { ironborngfx } # portraits
		color = { 0.25 0.35 0.5 }

		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }

		male_names = {
			Adrack Aeron Aggar Agil Aladale Alester Alton Alvyn Alyn Ambrode Andrik Arthur
			Baelor Balder Balon Bennarion Beron Boremund Burton
			Cadwyl Cotter Craghorn Cragorn Cromm
			Daegon Dagmer Dagon Dale Dalton Dan Denys Donel Donnor Drennan Dunstan Dykk
			Eerl Eldiss Eldred Emmond Endehar Erich Erik Erne Euron
			Falki Faste Fergon Fralegg
			Gelmarr Germund Gevin Godric Goren Gorm Gormond Gorold Gran Greydon Grimm Grimm Gunnar Gunthor Gylbert Gyles Gynir
			Hagen Hagon Hake Hakon Harl Harlan Harlon Harmund Harrag Harrald Harras Harren Harrock Harron Harwyn Hilmar Horgan Hotho Hrolf
			Jon Joron Joseran
			Kemmett Kenned Konrad Kromm
			Lenwood Lodos Loron Lorren Lucas Lucimore
			Manfryd Maron Mats Meldred
			Norjen Norne Nute
			Odd Ormm Othgar Ottar
			Pate
			Qalen Qarl Qhored Qhorin Qhorwyn Quellon Quenton
			Ragnal Ragnor Ralf Rance Ravos Regnar Robin Rodrik Roggon Rognar Rolfe Romny Rook Roryn Rus Rymolf
			Sargon Sawane Sigfry Sigfryd Sigrin Skyte Steffar Steffarion Stygg Sylas Symond
			Tarle Theomore Theon Thormor Todric Toke Tom Torbund Torgon Toron Torwold Torwyn Tristifer Triston Tymor
			Ulf Uller Urek Urragon Urras Urrathon Urrigon Urron Urzen
			Veron Vickon Victarion
			Waldon Werlag Wex Will Wulfe Wulfgar
			Ygon Yohn
		}
		female_names = {
			Abni Agmuna Agnis Alannys Aly Ana Arana Arsa Arwyn Arya Asa Asgrima Asha:1000 Asharra Aslaka Aynika Ayssi
			Barana Barbrey Barrya Barsa Beinerra Beony Bhurta Brella
			Ceyri Cholvi Corah Cromma
			Dags Dorna
			Eddara Efla Ela Erana Ernmunda Errika Errya Ersa Esgred:500
			Falia Falyse Farra Ferny Frenya Frynne
			Gella Gerika Ghaela Ghauda Ghulmi Gremma Gretchel Grisel Grisella Gritha Gwin Gwynesse Gwyneth Gysella
			Hagnaeh Harma Harra Hegga Helgarda Helya Herrika
			Irica
			Jeyne Jhena Johsi Jolfa Jomara Jonella Jonna Jonnela Jorelle
			Kella
			Larana Laransa Larsa Larya Lil Lyssa
			Mabs Malora Margadra Margot Megga Meredyth Mhaegan Mhaegen Millga Moelle Morna Munda Myrielle
			Nanna Narrana Narrsa Narrya Noshi
			Olma Ora Orrana Orrsa Orya Oshara
			Raddera Raly Ransa Ravella Rhonda Riga Roelle Rokah Ryella Ryka Ryloka
			Selyse Shierle Skati Skeyga Skoni Snela Soyni Suna Syansa Syga Syganna Sygara Sygarda Syri
			Talla Tansa Tarya Tekkeda Thalvi Thesa Thoki Toraka Trabi Tralmi Turri Tyosa
			Ulfara Unella Utmara
			Valy Varana Victaria:500
			Wynafrei Wynafryd
			Yara:500 Yasha Ynys Ysilla
			Zansa Zia Zosha
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 25
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 5
		mat_grm_name_chance = 20
		mother_name_chance = 0

		allow_looting = yes
		seafarer = yes

		modifier = default_culture_modifier
	}

	northman_andal = { # Northlander
		#Andal Northmen if they succeed in taking over the North
		graphical_cultures = { normangfx } # portraits
		color = { 0.87 0.87 0.87 }

		used_for_random = no
		allow_in_ruler_designer = yes
		alternate_start = {
			has_alternate_start_parameter = { key = culture value = full_random }
			NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } }
		}

		male_names = {
			Aethan Alaric Alyn Arnolf Arthor Artos
			Bannen Barth Bartimus Bass Benfred Benjen Benjicot Beren Bernarr Beron Bowen Brandon Brenett Byam
			Calon Cayn Cellador Chayle Cley Cregan
			Damon Danny Daryn Desmond Domeric Donnel Donnis Donnor Dorren Duncan Dywen
			Eddard Edderion Edrick Edwyle Edwyn Ethan Eyron
			Farlen Florian
			Galbart Gariss Garlan Garreth Garse Garth Gawen Glendon Gormon
			Hallis Halys Hareth Harlon Harmond Harrion Harwin Harwood Helman Heward Hoarfrost Hobber Horas Hosman Hother Howland Hugo Hullen
			Jeor Jojen Jon Jonnel Jonos Jorah Jory Joseth
			Karlon Kyle
			Larence Leobald Loras Luthor Luton
			Mallador Mark Marlon Martyn Medger Mern Mikken Morgan Moribald Mors
			Ondrew Osric Owen
			Pate Porther
			Qhorin Quent
			Ramsay Rickard Rickon Robb Robett Robin Rodrik Rodwell Roger Ronnel Roose
			Samwell Samwyle
			Theo Theodan Theon Tomard Torghen Torren Torrhen Theobald
			Vayon Vortimer
			Walder Walton Warryn Wayn Wilbert Wendel Willam Willas Wyl Wylis Wyman Wynton
			Yoren
		}
		female_names = {
			Alyce Alys Alysanne Alyssa Alyx Amabel Amerei Arwyn Allise Aleria Alerie Amena Arwen Aglantine
			Arianne Ammara Aelinor Andrya Amyra Amera Alise Alessa Amane Agnes Alayne Annara
			Alianne Arysa Alle Alannis Andara Aemma Alyce Alicent Ariela Arwyn Alla Amalia Annet
			Alison Ashara Agnes Amarei Anya Alynne
			Barba Barbara Becca Bella_Belle Bellena_Belenna Bess Bessa Bethany Belarra Bea Betha Brella
			Beony Barra Brienne Belandra Briony Barbrey:500 Berena:500 Sansa:350 Arya:200
			Carellen Cass Catelyn Cynthea Carene_Cerena Carene_Carene Cyrene Carolei Cariah
			Cassana Corenna Cyrenna Cassandra Cymoril Ceryse Cersei Cindra Cerelle Cerissa Cerenna
			Danelle Della Darianne Dolessa Damina Darlessa Dorcas Dorna Deana Donella Desmerei Dalla
			Delena Denyse Desmera Donyse Dyna Darla
			Eleanor Ellery Elyana Emberlei Emphyria Ermesande Elonne Eldacey Eleyne Elyria Elenya Elinor
			Eglantine Elene Elyn Ellyn Emma Eionne Elyria Elspeth Elenora  Emmara Elanna Elyanna
			Elenei Estalia Esme Ella
			Fiona Falena Falyse Flora Falia Freya Fruella Frynne
			Genna Gallisa Gretchel Grisel Ganna Gwyn Gwyneth Gella
			Harra Hostella Helya Halanna Helicent Helena Helyna
			Ilyse_Ilysa Ilyse_Ilyse Idera Idana Ismyra Irya
			Jayne Jeyne Jonquil Joyeuse Jyanna  Jannia Janei Janna Jeyne Joanne Jocelyn Joy Joyeuse
			Jeane Jenna Johanna Janyce Jonella Jonnela Jyana Janye Jerene Joyeuse Jalona Jena
			Jeona Jancia Jennelyn Joanna
			Kyra Keleria Kellyn Kelyce Kella
			Lacey Leana Leslyn Liane Lysa Lythene Leomella Lorell Leonylla Lanna Layna Leana Leonella
			Leonette Lira Lucia Lenora Lysessa Lynessa Lia Lelia Lorea Leona Lyessa Lyra Lythene Lyssa
			Lia Leyla Lynesse Lysana Lemore Lorra Lynora Leila Lollys
			Maegelle Maerie Marianne Marissa Mariya Masha Mellara Merianne Minisa Mordane Morya Mylessa Meria 
			Meredith_Meredyth Melene Melantha Malessa Margot Melesa Mina Myrielle Myrcella Melessa Margot Meria 
			Manysa Marsella Mela Melicent Minisa Mya Myranda Mary Marya Mhaegan Moelle Mylenda Megga
			Maris Matrice Margaery Mychella Mina Malora Melissa Maegelle Maia
			Orissa Osmeria Olanei Olene Olenna Obela
			Nolla
			Perra Perriane Pia Philena Patrice Pya Pella Peony Peony Palla:200
			Randa Ravella Rhialta Roslin Ryella Riona Ronessa Rosamund Reyna Rowena Rienne Roelle Rhonda
			Rhea Rohanne Rylene Rylla Rylena
			Sallei Sarra Sarya Serra Sharna Shella Shirei Sylwa Sylvia Syrella Sabrina Sabitha Shiera Saranella
			Sybell Sharra Sirona Sharena Serina Scolera Selyse Sabrine Senelle Shireen Shyra:700
			Tysane Tyta Tyressa Tinessa Tyiah Tilly Tiona Tya Tysha Tinessa Tyshara Theona Tarissa Teora Tyana
			Talla Tanda Tania Tanselle Tyene
			Unella Ursula
			Violet Victoria_Victaria Valiete
			Yolanda Ysila Yorena
			Walda Wynafrei:700 Wynafryd:700 Wylla:700
			Zhoe Zia

			Dacey:500 Eddara:500
		}
		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 25
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 5
		mat_grm_name_chance = 20
		mother_name_chance = 0

		modifier = default_culture_modifier
	}

	reachman = { # Reachman
		graphical_cultures = { reachmangfx normangfx } # units
		color = { 0.1 0.5 0.2 }

		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }

		male_names = {
			Abelar Addam Aegon Aemon Agramore Aladore Alan Alekyne Alester Alyn Amaury Androw Armen Armond Arthor Arthur Arys Aubrey Axell 
			Baelor Barquen Bayard Bernard Bertram Bertrand Branston Braxton Bryan Bryndon Buford Byren
			Clement Cleyton Clifford Colin
			Damon Denys Derrick Desmond Dickon Donald Donnel
			Eddison Eden Edgar Edgerran Edmund Edwyd Edwyn Ellard Elwood Elyas Emerick Emmon Erren Erton Estyn Eustace
			Franklyn
			Gareth Garlan Garmon Garmund Garrett Garse Garth Gawen Gedmund George Gerold Glendon Gormon Greydon Gwayne Gunthor Guthor Gyles
			Harlan Harrold Harys Hobber Hobert Horas Hosman Hugh Humfrey Hyle
			Igon Imry
			Jafer Jaime Jason Jeffory Jeremy Joffrey John Jon Jorgen Josua
			Laswell Leo Leyton Loras Lorence Lorent Lorimar Lucantine Lucas Luthor Lyman Lymond Lyonel
			Mace Manfred Manfryd Marq Martyn Mathis Mattheus Matthos Medwick Mern Merrell Mervyn Meryn Morgan Morgil Moribald Moryn Myles
			Nicol Norman Normund Nyles
			Olymer Olyvar Omer Orbert Ormond Ormund Orton Osbert Osmund Otho Otto Ottyn Owen
			Parmen Paxter Perwyn Portifer Pykewood
			Randyll Raymun Raymund Renly Reynard Richard Rickard Robert Robyn Rolland Runceford Runcel Russell Ryam Rycherd
			Samwell Samwyle Simon Steffon
			Quentin Quenton Quentyn
			Talbert Tanton Thaddeus Theo Theodore Theomore Titus Tommen Torgen Torman Triston Tyler
			Unwin Urrathon Urrigon Uther Uthor
			Victor Vortimer
			Walys Warryn Wendell Wilbert Willam Willas Wyman
		}
		female_names = {
			Alena Alera Alerie Aleyne Alla Allene Alicent Alinor Alyce Alys Alysanne Ameria Amyria Annet Annys Arana Arrene Arwyn Asta
			Beliona Beony Bera Bethany Bressa
			Calla Carellen Celessa Cella Cerenna Ceryse Clarice
			Dalla Delena Delina Denyse Desmera
			Elara Eldara Eldene Eldrene Elia Elinor Elissa Ella Elorra Elsabeth Elvyra Elwene Elyn Elyse Enna Erayna
			Falia Florence
			Galissa Gelenna
			Helicent
			Jancia Janna Janyra Jayne Jeona Jeyne Jomylla Jorella Josella
			Kalianne Keleria Kenella
			Lanette Lerra Leyla Lia Liera Lena Leona Leonette Lerra Lorianne Lyessa Lynesse Lynett Lyrissa Lysa
			Malyssa Marella Margaery Maris Megga Melara Melessa Melina Mellei Meredyth Merissa Mesella Mina Myleria Mylessa Myria Myrielle
			Nella Nemony Nyrise
			Olanne Olene Olenna Oraney Ottyria
			Patrice Patricia
			Rhea Rhonda Rohanne Rosalei Rosamund Roslin Rylene Rylla Rymella
			Salene Samantha Sansara Selyse Sharis Sheralla Syldia Sylvina
			Taena Talla Taena Tanera Tenda Tylarra
			Vanera Victaria
			Wynella
			Yrma
		}
		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 10
		mat_grf_name_chance = 5
		father_name_chance = 5

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 5
		mat_grm_name_chance = 10
		mother_name_chance = 5

		modifier = default_culture_modifier
	}

	riverlander = { # Riverman
		graphical_cultures = { riverlandergfx normangfx } # units
		color = { 0 0 0.8 }

		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }

		male_names = {
			Addam Aegon Aemon Aenys Alesander Alton Alvyn Alyn Ambrose Amos Andrey Androw Anson Arwood Axel
			Benedict Benfrey Benjicot Bennifer Bradamar Brus Bryan Bryar Brynden Bywin
			Clarence Clement Cleos Colmar
			Dafyn Damon Danwell Darnold Davos Denys Deremond Derrick Desmond Dickon Donnel Duncan
			Edmund Edmure Edmyn Edwyn Ellery Elmar Elmo Elston Elwood Emmon
			Florian Forrest Franklyn
			Garibald Garrett Garse Garth Gaston Gerald Geremy Glendon Grover Guy Gyles
			Halder Halmon Harbert Harwin Harstyn Harys Hendry Horas Hosteen Hoster Hugo Humfrey
			Igon
			Jaime Jammos Jared Jaremy Jason Jeffory Jon Jonah Jonos Jonothor Jorah Jordan Joscelyn Joseth Josmon Jostyn Josua
			Karyl Kennoth Kenric Kermit Kirth Kyle
			Larys Leslyn Lewys Lorence Lothar Lucamore Lucas Luceon Lucias Lucos Luthor Lyle Lyman Lymond Lyonel
			Maegor Malwyn Manfred Manfryd Marq Martyn Mathis Medgar Melwys Merrett Morgan Mychel Myles
			Norbert
			Olyvar Olyver Orland Ormond Orstos Oscar Osmund Oswald Othan Otho Owain Owen
			Patrek Perwyn Petyr Prentys 
			Quentyn Quincy
			Raylon Raymond Raymun Raymund Rhaegar Richard Robb Robert Robin Roderick Roger Roland Ronald Ronel Ronnel Royce Ryger Ryman
			Samwell Sandor Simon Stanton Steffon Stevron Symond
			Theo Theomar Thoren Tion Tommard Tommen Tristan Tristifer Tyler Tymond Tytos Tywin
			Utherydes
			Walder Waldron Walter Walton Waltyr Walys Wendel Whalen Wilbert Willamen Willam Willard Willamen Willem William Willis Wyllis
			Zachery
		}
		female_names = {
			Agnes Alarra Aleona Alessa Alynne Alys Alysanne Alyssa Alyx Amerei Ammara Amyra Andacey Andorea Andrya Anwyn Arona Arwaya Arwyn
			Barba Barbara Beatrice Belarra Belle Bellena Bess Betha Bethany
			Calera Carellen Catelyn Celia Cersei Ceryne Crynna Cynthea
			Dalla Damina Danelle Darla Delanei Delanna Della Derwa Dianessa Doryssa
			Edmaria Eldacey Eleanor Eleyne Elerra Ellenei Elona Elonne Elyana Emberlei Emphyria Esmene
			Freya 
			Halanna Hanna Helya Hostella
			Idera Ilyse
			Janei Janeya Jannia Jayne Jenna Jeyne Jirelle Jonquil Joanna Jonara Josella Joyeuse Jyanna
			Kelarra Kyra
			Lansia Leana Liane Lysa Lythene
			Maegelle Marianne Marissa Mariya Melantha Melene Mellara Melissa Melony Meredyth Merianne Minisa Molianne Morya
			Neria Neyela
			Orissa
			Perra Perriane
			Rhialta Riona Ronessa Rosamund Roslin Ryella
			Sabitha Sallei Saranna Sarra Sarya Serra Sharra Shella Shiera Shirei Sylwa Syrella
			Tinessa Tyressa Tysane Tyta
			Walda Waldra Wenella Wynafrei
			Zia Zhoe
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 25
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 5
		mat_grm_name_chance = 20
		mother_name_chance = 0

		modifier = default_culture_modifier
	}

	sisterman = { # Sisterman
		graphical_cultures = { sistermangfx } # units
		color = { 0.15 0.75 0.95 }

		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }

		male_names = {
			Alaric Albar Alesandor Allyn Andar Artys Alaric Allard Androw Arnos Athan
			Berion Bryen Byron Bors
			Colemon Creighton
			Damon Denys Donnel Dan
			Eddison Edmund Elbert Ellery Elys Eon Eustace
			Florian
			Gelion Gerold Gilwood Godric Gwayne Gyles
			Harlan Harrold Horton Hugh Humfrey
			Jasper Jon Jormar
			Kyle Konrad
			Lark Lothor Lucas Lyn Lyonel Luke
			Maron Marwyn Morgarth Morton Mychel Mors
			Nestor
			Osfryd Osgood Osmund Osney Oswell Othor Ondrew Owen Omer Osbert Olymer Olyvar
			Pate Petyr
			Randyll Raymar Robar Robert Robin Rolland Rolley Ronnel Roger Redwyn
			Samwell Serwyn Symond Steffon
			Terrance Triston Tylos Tanton
			Umfred Uthor Unwin
			Vardis
			Wallace Wallace Waymar Wyl
			Yohn
		}
		female_names = {
			Alyce Alys Alysanne Alyssa Alyx Amabel Amerei Arwyn Allise Aleria Alerie Amena Arwen Aglantine
			Alison Ashara Agnes Amarei Anya Alynne
			Danelle Della Darianne Dolessa Damina Darlessa Dorcas Dorna Deana Donella Desmerei Dalla
			Delena Denyse Desmera Donyse Dyna Darla
			Fiona Falena Falyse Flora Falia Freya Fruella Frynne
			Gella
			Harra Hostella Helya Halanna Helicent Helena Helyna
			Jathany Jeyna
			Kelanna
			Lacey Leana Leslyn Liane Lysa Lythene Leomella Lorell Leonylla Lanna Layna Leana Leonella
			Leonette Lira Lucia Lenora Lysessa Lynessa Lia Lelia Lorea Leona Lyessa Lyra Lythene Lyssa
			Lia Leyla Lynesse Lysana Lemore Lorra Lynora Leila Lollys Lily
			Marei Marla Maronne Melyssa Morya
			Orissa Osmeria Olanei Olene Olenna Obela
			Nolla
			Perra Perriane Pia Philena Patrice Pya Pella Peony
			Randa Ravella Rhialta Roslin Ryella Riona Ronessa Rosamund Reyna Rowena Rienne Roelle Rhonda
			Rhea Rohanne Rylene Rylla Rylena Rosey
			Sallei Sarra Sarya Serra Sharna Shella Shirei Sylwa Sylvia Syrella Sabrina Sabitha Shiera Saranella
			Sybell Sharra Sirona Sharena Serina Scolera Selyse Sabrine Senelle Shireen Shyra
			Tysane Tyta Tyressa Tyiah Tilly Tiona Tya Tysha Tinessa Tyshara Theona Tarissa Teora Tyana
			Talla Tanda Tania Tanselle Tyene
			Unella Ursula
			Yenna
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 25
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 5
		mat_grm_name_chance = 20
		mother_name_chance = 0

		seafarer = yes

		modifier = default_culture_modifier
	}

	stormlander = { # Stormlander
		graphical_cultures = { stormlandergfx normangfx } # units
		color = { 0.8 0.6 0.0 }

		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }

		male_names = {
			Addam Aegon Aemon Alesander Allard Allos Alyn Anders Andren Andrew Andrion Argilac Arion Arlan Armond Arrec Arstan Arthur Axel
			Baldric Balon Barristan Barron Benedict Beren Beric Bonifer Boremund Borros Borys Brus Bryce Bryen Bryndemere Byron
			Cameron Casper Clement Cleoden Clifford Cortnay Corwen Corwin Criston
			Dale Damon Davos Denys Devan Dickon Donnel Doran Durran Durwald
			Ector Edric Edwyn Eldon Elys Endrew Erich
			Galladon Gallard Garon Garse Gawen Gerald Gladden Glendon Gowen Grance Gulian Gunthor Guyard Gyles
			Harbert Harlan Harmon Harrold Harwood Harys Hoster Hugh Humfrey
			Jared Jasper Jerion Joffrey Jon Jonald
			Kyle
			Leslyn Lester Lomas Lorent Luceon Lyonel
			Maldon Manfred Maric Marq Matthos Meryn Michael Monfryd Morden Morgan Morton Mychel Myles
			Narbert
			Olyver Ormund Orryn Orson Orys Osmund
			Pearse
			Qarlton Quentyn
			Ralph Raymont Raymund Renly Richard Rickard Robert Robin Rogar Rolland Ronald Ronard Ronnal Ronnel Ronnet Roy Royce Ryman
			Sarmion Sebastion Selwyn Serwyn Simon Stannis Steffon
			Tancred Tarwen Terrence Theo Thurgood Tristan
			Walter Willem Willis
		}
		female_names = {
			Adena Aelinor Alanei Aliona Alora Alynne Alys Alysanne Amanda Ammina Argalia Argella Arianne Arlana Arrana Artaria Arya Audrey
			Belissa Benara Benera Benyssa Beonelle Bonessa Brella Brienne
			Carella Carmella Carolei Caronne Cassana Cassandra Corenna Coryanne Cyrenna
			Dalia Damella Demerei
			Eddane Elane Elaena Elanna Elayne Eldera Eleanor Elenda Elenei Ella Ellyn Elmeria Elyana Elyanna Elyssa Emmara
			Floris
			Genna
			Hanna
			Idena Idorea Ismane
			Jaenyssa Jasonnei Jena Jeyne Jocelyn Johanna Joy Jynna
			Kelyce Kerena
			Laena Lanna Laressa Leyne Lollys Lucinda Lynera Lynett Lyria Lysanne Lysena
			Maelane Maella Maenylla Malarra Malia Malyce Marena Marenda Maris Mary Marya Meona Merei Milerra Mylenda Mylla Myrena Myrren
			Narra
			Olyssa Onora Osmera
			Patrice Pelyne
			Raena Ravella Rella Rolanna
			Sarella Sarena Sarissa Sarya Sebara Shalenna Sharon Shireen Shyra Simona Symalla Syrene
			Talenna Tasene Tyana Tynalla Tysane
			Walessa
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 25
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 5
		mat_grm_name_chance = 20
		mother_name_chance = 0

		modifier = default_culture_modifier
	}

	valeman = { # Valeman
		graphical_cultures = { valemangfx normangfx } # units
		color = { 0.1 0.75 1 }

		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }

		male_names = {
			Adrian Allard Albar Alec Alester Almyn Alyn Andar Andrew Arlan Arlon Armond Arnel Arnold Arran Artys
			Banor Benedar Benedict Benfrey Beomar Bryan Bryen Bryon
			Colin Conrad Cortnay Corwyn Creighton
			Damon Darnold Dennard Dermett Desmond Denys Donnel Dunstan
			Eddison Edmore Edmund Edryn Elbert Elbyn Eldred Eldric Elron Elwood Elys Emond Endrew Eon Eustace
			Gawen Gerold Gilwood Guncer Gunthor Gwayne Gyles
			Harlan Harrold Horton Hubert Hugh Hugo Humfrey
			Isembard
			Kennard Kyle
			Luceon
			Jaime Jasper Jaymar Jaymond Joffrey Jon Jonos Jorence Josten
			Leowyn Lucas Lucos Lymond Lyn Lyonel
			Maladon Mandon Marq Marwyn Mathos Merton Morton Mychel
			Nestor
			Ober Olryn Olwyn Orson Orton Osric Osgood Osmund Ossifer Oswell Oswin Otho Owen
			Pearse Peron Petyr
			Qarl Quenton Qyle
			Randyll Raymar Renly Robar Robert Robin Rodrik Roland Ronnel Ryckard Rymond
			Samwell Symond
			Terrance Triston
			Uther Uthor
			Vardis Victor
			Wallace Waltyr Waymar Willam Wyllem
			Yohn Yorbert Yorwyck
		}
		female_names = {
			Aemma Alanna Alayne Alianne Alina Allana Allyne Alora Alys Alyssa Amanda Anya Arelle Arwen Aryse
			Bellara Beriona Berra Brianne
			Cariah Carina Carissa Carolei Cersei Cynthea
			Damana Dandia Deana Della Demerei
			Eddyne Edmera Edmyne Edrise Elanei Eldrane Elene Elesa Elina Ellyn Elyra
			Falora Fiona
			Grenna Gwenda Gwenna
			Idona Isabel
			Jacene Janyce Janyra Jennis Jessamyn Jeyne Jyana
			Kalyra Karella Karene Kelarra Kerelle
			Lanalla Leonella Liona Lorra Lyorena Lyseria
			Mara Marelle Marissa Marsella Mena Moryssa Mya Mynalla Myranda
			Nynia
			Obyra Olana Oryne Osmerei
			Parella Parra Perianne Perra Polianne
			Qyrene
			Rhea Rolessa Ronella Rowena Ryella
			Salanne Sanora Sharei Sharra Sherlyse Sylana Syrona
			Tarissa Teora Theona
			Ursula
			Wylera
			Yolanda Yorena Ysilla
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 25
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 5
		mat_grm_name_chance = 20
		mother_name_chance = 0

		modifier = default_culture_modifier
	}

	westerman = { # Westerman
		graphical_cultures = { westermangfx normangfx } # units
		color = { 0.6 0 0 }

		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }

		male_names = {
			Addam Addison Adrian Alan Alastor Alester Alfred Alfryn Alton Alyn Amory Andren Andros Androw Antario Arron Aubrey
			Benedict Bertram Bertrand Burton
			Caspor Cedric Cerion Clarent Cleos
			Damion Damon Daven Dennis Desmond Denys Derrick Donnel
			Edwell Eldon Eldrick Elmer Elys Emory Emrick Erren Erwin Ethan
			Flement Forley Franklyn 
			Gareth Garrison Garth Gawen Geremy Gerion Gerold Gregor
			Harrold Harwyn Harys Herrock Horton Horys Hugh Humfrey
			Ilyn
			Jaime Jason Joffrey Jon Jonos Josmyn
			Kennet Kevan
			Lambert Lancel Leo Leslyn Lester Lewis Lewys Loren Lorent Loreon Lucion Lyle Lyman Lymond Lyndon Lyonel
			Manfryd Marq Martyn Mathin Maynard Melwyn Merlon Mern Myles
			Norwin
			Orbert Ormond Osbert Ossifer Othell
			Peter Petyr Philip Podrick Preston
			Quenten
			Raynald Raynard Regenard Reginald Reynard Richard Robb Robert Robin Roger Roland Rollam Rolph Rupert
			Samwell Sandor Selmond Stafford Steffon Sumner Symond
			Terrence Theomore Tion Tommard Tommen Tybolt Tygett Tyland Tyler Tymond Tyrek Tyrion Tytos Tywald Tywell Tywin
			Uthor
			Victor Viserys
			Walder Walderan Wendel Willam Willem Wyllis
		}
		female_names = {
			Aleyne Alys Alysandra Alysanne Amarei Amarya Amella Arnessa
			Benera Bryssa
			Calynne Celessa Celyria Cenlia Cerelle Cerenna Cerissa Cersei Cindria Corianne Cynda Cyralei
			Damia Damylla Darlessa Delena Dolessa Domella Dorene Dorna
			Edella Edrina Elanna Elene Elenna Elessa Eleyna Elinor Elissa Ella Elle Ellyn Elona Elyne Erena
			Falyse Farena Fiona
			Genna Gisella
			Hamina
			Jamylla Janei Jeyne Joanna Jocasta Jocelyn Johanna Jorelle Joy
			Kellyn
			Lanna Lelia Leomella Leona Leonella Lucinda Lyene Lynora Lysa
			Malanna Malanne Manysa Marla Margot Melara Melesa Melissa Mendre Milerra Molianne Moranne Mylana Myranda Myrcella Myrielle
			Nesella
			Olenei Orena
			Parena Philena
			Renna Rohanne Rosalind Rosamund Ryalla Rylene Rylenna Ryone
			Samalla Serylla Sharra Sherei Shiera Shierle Sybell
			Tamena Tarina Teora Teressa Tya Tyrianne Tyshara
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 25
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 5
		mat_grm_name_chance = 20
		mother_name_chance = 0

		modifier = default_culture_modifier
	}


	hedge = {
		graphical_cultures = { crownlandergfx normangfx } # units

		used_for_random = no
		allow_in_ruler_designer = no
		alternate_start = { always = no }

		color = { 0.7 0.7 0.1 }

		male_names = {
			Addam Addison Alton Alyn Amory Andros Antario
		}
		female_names = {
			Alys Alysanne Amarei
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 25
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 5
		mat_grm_name_chance = 20
		mother_name_chance = 0

		modifier = default_culture_modifier
	}
}