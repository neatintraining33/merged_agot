dothraki_group = {
	graphical_cultures = { mongolgfx } # portraits, units

	dothraki = {
		graphical_cultures = { dothrakigfx } # buildings, portraits
		color = { 0.775 0.5 0.15 }

		#horde = yes
		nomadic_in_alt_start = yes
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }

		male_names = {
			# Canon
			Aggo
			Bharbo
			Caggo Cohollo
			Drogo Dhako
			Fogo
			Horro Haro Haggo
			Iggo
			Jhaqo Jhogo Jommo
			Kovarro
			Loso
			Mago Moro Motho Mengo
			Ogo Onqo
			Pono
			Qotho Quaro Qano
			Rakharo Rhogoro Rommo Rogo
			Scoro
			Temmo
			Vaggoro
			Zekko Zollo Zhako Zeggo

			# Damoc
			Acchakko Addro Ahaqo Arrisso
			Bhaqo Bhoqo Boggo Bollo Bommo Brabo
			Carro Chaqo Collo Covo Cozammo Crozozzo
			Darro Dazho Dhaqo Dhoqo Diggo Doggo Dollo Dono Dozzo
			Emmatto
			Gorro
			Kaffo Khaqo Khazzo Khobbo Kogo Kollo Kozatho Krommo
			Lajatto Lajo Lhaqo Lollo
			Maggo Matto Mhaqo Mhoqo Mogo Mollo Mono
			Nhaqo Nollo
			Qhaqo Qozzo Qrakko
			Raggo Rakko Rhoqo Rhozzo Rono
			Shaqo Shiggo Sollo
			Villo
			Zhaqo Zhoqo Zogo Zommo Zono

			# Non-Canon
			Azho Arakho 
			Fono Fesho
			Hezzo
			Jarbo Jasso
			Kattaro
			Lanno
			Malakho Marro Mosko
			Najaho
			Rovo Remekko
			Tihho Tokko
			Virsallo
			Zerqollo Zirqo
		}
		female_names = {
			# Canon
			Doshi
			Irri
			Jhiqui

			# Non-Canon
			Achri Aggi Aheni Allayi Ashini Ayi
			Chakki Coholli
			Dalfi Dalli Davri Dhaki Drigi
			Eshinni Esinni Essi
			Feldi Filki Filli Fitti
			Hari Havvi Hirri
			Iggi Immissi Isili Izzatti Izzi
			Jeddi Jeshi Jhigi Jhogi Jolinni
			Kendri Kessi Kovarri
			Lavakhi Layi Leqsi Lihezhi Losi
			Malakhi Mavi Mengi Miri Mothi
			Naqqi Nishavi
			Onqi Oqetti Ovahi
			Qissabi Qizhanni Qossi Qothi Qovatti Quiri
			Rakhiri Rakki Rhogiri Rimmi Risemi Rogi
			Scori Shasi Sihi
			Tehhi Temmi Tezhimi Thiqui Thirli Thirri
			Valeqi Verri Vizi Vorsakhi
			Zhaki Zhavvi Zichi Zichomi Ziganni Zikki Zirqi Zirri Zolli
		}

		character_modifier = {
			#demesne_size = 6
		}

		male_patronym = " Son of "
		female_patronym = " Daughter of "
		prefix = yes

		#dynasty_title_names = yes
		founder_named_dynasties = yes
		dukes_called_kings = yes

		#from_dynasty_prefix = "of "

		pat_grf_name_chance = 25
		mat_grf_name_chance = 25
		father_name_chance = 25

		pat_grm_name_chance = 25
		mat_grm_name_chance = 25
		mother_name_chance = 25

		modifier = default_culture_modifier
	}

	lhazareen = {
		graphical_cultures = { lhazareengfx } # buildings, portraits
		color = { 0.675 0.675 0 }

		#horde = yes
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }

		male_names = {
			# Canon

			# Non-Canon
			Aba Alp Araslan Arslan
			Bagha Baghatur Baitzas Balgici Balgor Barjik Barsbek Bashtwa Bastu Batan Batas Bator Bayca Belet Bickili Bihor Bizel Boga Bori Boru Bota Bugha Bula Bulan Bulcan Busir Buzer
			Catan Celgil Chalis Corpan
			Ezra
			Giazis
			Hanukkah Hezekiah
			Ildey Ilik Iltabbr Ipa Ipaos Ipoch Itakh
			Kabuksin Kajdan Kamaj Karaca Karadakh Kaydum Kayghalagh Kazar Kegen Khatir Khuterkin Kibar Kisa Konel Korkutan Kortan Kostas Kourkoutai Kuchuk Kuerci Kugel Kundac Kundaciq Kure
			Manar Manas Manasseh Menumarót Metigay
			Nisi
			Osul Otemis Papacyz Qorqutay Sagay Samsam Sartac Sethlav Simsam Sol Suru Tabar Tabghac Tarkhan Tarmac Tatus Teber Temir Tiraq Tuzniq Tzegul
			Vakrim
			Yabghu Yavantey Yavdi Yazi Yectirek Yeke Yencepi Yerneslu Yilig
			Zebulun
		}
		female_names = {
			# Canon
			Eroeh

			# Non-Canon
			Akgul Aneseh Asli Ayasun Ayten
			Bozcin
			Cece Cecki Cectilet Cica Cice Cilen Cilena Cilta
			Dilek Dileki
			Esin Esini
			Gulaya Gulayi Gulca Gulcicek Gundeh Gundoeh Gunesa
			Ikalaya Ilkaya Ipeka Ipekeh Irgehi
			Karacik Karoeh Katyarikka Keenissa Kelyamal Khatun
			Larkka Lyukha
			Mala Maturkka Mutlu
			Nacoeh
			Ozgula Ozlem Ozzi
			Parsbiti Patiyera Paykelti Peksen Pekseni Puyantay
			Samureh Samuri Sarantay Sareh Sarica Sarikeli Sati Savacka Savilyi Savintiki Savki Savtilek Sevilayi Sevindiki Shurkka Sibela Sibeli Sirini Sirmeh Sirmi Songul Songula
			Tahtani Tekce Tekeh Tura
			Umay Umaya Usunbike Usunbiki
			Xatti
			Yartilek Yasar Yasari Yeldem Yildizeh Yildizi
			Ziggi
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 30
		mat_grf_name_chance = 10
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0

		modifier = default_culture_modifier
	}
}

sarnor_group = {
	graphical_cultures = { indiangfx } # buildings

	sarnorian = { 
		graphical_cultures = { sarnorigfx } # portraits
		color = { 0.72 0.8 0.2 }

		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }

		male_names = { # Gujarati influence
			# Canon
			Huzhor:400 Mazor:400

			# Non-Canon
			Amritlal Avilash Atharvan
			Balavant Burhan Bhagesh
			Chitresh Chanak
			Dinesh Darshan Dahak
			Etash Ekavir
			Fanish
			Ganesh Gayan Girivar
			Harshil Harith Harihar
			Ishan Ishwar
			Jash Jaafar
			Kushal Kalpesh Kedaar
			Lokesh Lakshan
			Manish Manaar Mukesh
			Nitish Nayan Nishad
			Onkar
			Palash Paarth Parvesh
			Ritesh Rathin Ranak
			Sagar Sahat Shyam Sahasrad
			Tarakesh Taresh
			Uttamesh Utkarsh
			Vyomesh Vishruth
			Yogesh Yathavan
		}
		female_names = {
			Ashna Anhithi Ameena Aryana
			Bidisha Barsha
			Chandira Charita
			Dhanashri Darshita Diya
			Eesha
			Faina
			Gomathi Girisha Gayathri
			Hansha Havya Heera
			Ishanya Iksha
			Jalbala Jithya Jisha
			Kirthana Kanisha Kaira
			Lalasa Lekisha
			Malashree Monisha Misa Mahiya
			Nithya Nishi Nalika
			Oliyarasi
			Plaksha Priya
			Rushada Ruthvika Rashmi
			Suvitha Saira Sitara Shashi
			Tannistha Treya Tanisha
			Urishilla Usha
			Vishva Vihana
			Yashika Yaswitha
			Zaara
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0

		modifier = default_culture_modifier
	}

	omberi = { 
		graphical_cultures = { sarnorigfx } # portraits
		color = { 0.5 0.9 0.35 }

		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }

		male_names = { #Gujarati influence		
			Amritlal Avilash Atharvan
			Balavant Burhan Bhagesh
			Chitresh Chanak
			Dinesh Darshan Dahak
			Etash Ekavir
			Fanish
			Ganesh Gayan Girivar
			Harshil Harith Harihar
			Ishan Ishwar
			Jash Jaafar
			Kushal Kalpesh Kedaar
			Lokesh Lakshan
			Manish Manaar Mukesh
			Nitish Nayan Nishad
			Onkar
			Palash Paarth Parvesh
			Ritesh Rathin Ranak
			Sagar Sahat Shyam Sahasrad
			Tarakesh Taresh
			Uttamesh Utkarsh
			Vyomesh Vishruth
			Yogesh Yathavan
		}
		female_names = {
			Ashna Anhithi Ameena Aryana
			Bidisha Barsha
			Chandira Charita
			Dhanashri Darshita Diya
			Eesha
			Faina
			Gomathi Girisha Gayathri
			Hansha Havya Heera
			Ishanya Iksha
			Jalbala Jithya Jisha
			Kirthana Kanisha Kaira
			Lalasa Lekisha
			Malashree Monisha Misa Mahiya
			Nithya Nishi Nalika
			Oliyarasi
			Plaksha Priya
			Rushada Ruthvika Rashmi
			Suvitha Saira Sitara Shashi
			Tannistha Treya Tanisha
			Urishilla Usha
			Vishva Vihana
			Yashika Yaswitha
			Zaara
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0

		modifier = default_culture_modifier
	}
}