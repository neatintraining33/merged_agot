defined_text = {
	name = GetCrownlanderWaterman

	text = {
		trigger = {
			NOT = {
				year = 7998
				month = 2
				day = 1
				NOR = {
					month =3
					day=2
				}
			}
		}
		localisation_key = waterman
	}
	text = {
		trigger = {
			year = 7998
			month = 2
			day = 1
			NOR = {
				month =3
				day=2
			}
		}
		localisation_key = kingslander
	}
}
