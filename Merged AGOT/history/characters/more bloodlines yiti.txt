5543001 = {
	name="Line of Chai Duq"	# a lord
	dynasty=65962

	father=10065962

	occluded=yes

	religion="yiti_gods"
	culture="yi_ti"
	
	7284.1.1 = {birth="7284.1.1"}
	7878.1.1 = {death="7878.1.1"}
}
5643002 = {
	name="Lu"	# a lord
	dynasty=65962

	father=5543001

	religion="yiti_gods"
	culture="yi_ti"
	
	7878.1.1 = {birth="7878.1.1"}
	7879.1.1 = {
		add_claim=e_yi_ti
	}
	7899.1.1 = {
		death = {
			death_reason = death_missing 
		}
	}
}
5643003 = {
	name="Han"	# a lord
	dynasty=65962

	father=5643002

	religion="yiti_gods"
	culture="yi_ti"
	
	7897.1.1 = {birth="7897.1.1"}
	7899.1.1 = {
		add_claim=e_yi_ti
	}
	7983.1.1 = { death="7983.1.1" }
}
5643004 = {
	name="Har-Lo"	# a lord
	dynasty=65962

	father=5643003

	religion="yiti_gods"
	culture="yi_ti"
	
	7928.1.1 = {birth="7928.1.1"}
	7983.1.1 = {
		add_claim=e_yi_ti
	}
	7990.1.1 = { death="7990.1.1" }
}
5543002 = {
	name="Ao"	# a lord
	dynasty=65962

	father=5643004

	religion="yiti_gods"
	culture="yi_ti"
	
	7952.1.1 = {birth="7952.1.1"}
	7972.1.1 = {
		add_spouse = 5543003
	}
	7990.1.1 = {
		add_claim=e_yi_ti
	}
	7994.1.1 = {death="7994.1.1"}
}
5543003 = {
	name="Jia"
	female=yes

	religion="yiti_gods"
	culture="yi_ti"
	
	7957.1.1 = {birth="7957.1.1"}
	7995.1.1 = {death="7995.1.1"}
}
5543004 = {
	name="Lo"	# a lord
	dynasty=65962

	father=5543002
	mother=5543003

	religion="yiti_gods"
	culture="yi_ti"
	
	7973.1.1 = {birth="7973.1.1"}
	7989.1.1 = {
		add_spouse = 5543005
	}
	7994.1.1 = {add_claim=e_yi_ti}
	8050.1.1 = {death="8050.1.1"}
}
5543005 = {
	name="Xian"
	female=yes

	religion="yiti_gods"
	culture="yi_ti"
	
	7973.1.1 = {birth="7973.1.1"}
	8016.1.1 = {death="8016.1.1"}
}
5543006 = {
	name="Quen"	# a lord
	dynasty=65962

	father=5543004
	mother=5543005

	religion="yiti_gods"
	culture="yi_ti"
	
	7990.1.1 = {birth="7990.1.1"}
	8008.1.1 = {
		add_spouse = 5543007
	}
	8050.1.1 = {add_claim=e_yi_ti}
	8060.1.1 = {death="8060.1.1"}
}
5543007 = {
	name="Li"
	female=yes

	religion="yiti_gods"
	culture="yi_ti"
	
	7991.1.1 = {birth="7991.1.1"}
	8034.1.1 = {death="8034.1.1"}
}
5543008 = {
	name="Quen"	# a lord
	dynasty=65962

	father=5543006
	mother=5543007

	religion="yiti_gods"
	culture="yi_ti"
	
	8021.1.1 = {birth="8021.1.1"}
	8037.1.1 = {
		add_spouse = 5543009
	}
	8060.1.1 = {add_claim=e_yi_ti}
	8080.1.1 = {death="8080.1.1"}
}
5543009 = {
	name="Ji Si"
	female=yes

	religion="yiti_gods"
	culture="yi_ti"
	
	8023.1.1 = {birth="8023.1.1"}
	8085.1.1 = {death="8085.1.1"}
}
5543010 = {
	name="Qo"	# a lord
	dynasty=65962

	father=5543008
	mother=5543009

	religion="yiti_gods"
	culture="yi_ti"
	
	8042.1.1 = {birth="8042.1.1"}
	8060.1.1 = {
		add_spouse = 5543011
	}
	8080.1.1 = {add_claim=e_yi_ti}
	8111.1.1 = {death="8111.1.1"}
}
5543011 = {
	name="Shu"
	female=yes

	religion="yiti_gods"
	culture="yi_ti"
	
	8044.1.1 = {birth="8044.1.1"}
	8111.1.1 = {death="8111.1.1"}
}
5543012 = {
	name="Xhe"	# a lord
	dynasty=65962

	father=5543010
	mother=5543011

	religion="yiti_gods"
	culture="yi_ti"
	
	8060.9.9 = {birth="8060.9.9"}
	8078.1.1 = {
		add_spouse = 5543013
	}
	8111.1.1 = {add_claim=e_yi_ti}
	8113.1.1 = {death="8113.1.1"}
}
5543013 = {
	name="Nha"
	female=yes

	religion="yiti_gods"
	culture="yi_ti"
	
	8060.1.1 = {birth="8060.1.1"}
	8115.1.1 = {death="8111.1.1"}
}
5543014 = {
	name="Ji"	# a lord
	dynasty=65962

	father=5543012
	mother=5543013

	religion="yiti_gods"
	culture="yi_ti"
	
	8080.1.1 = {birth="8080.1.1"}
	8096.1.1 = {
		add_spouse = 5543015
	}
	8113.1.1 = {add_claim=e_yi_ti}
	8135.1.1 = {death="8135.1.1"}
}
5543015 = {
	name="Ming"
	female=yes

	religion="yiti_gods"
	culture="yi_ti"
	
	8082.1.1 = {birth="8082.1.1"}
	8140.1.1 = {death="8140.1.1"}
}
5543016 = {
	name="Ye Sun"	# a lord
	dynasty=65962

	father=5543014
	mother=5543015

	religion="yiti_gods"
	culture="yi_ti"
	
	8100.1.1 = {birth="8100.1.1"}
	8116.1.1 = {
		add_spouse = 5543017
	}
	8135.1.1 = {add_claim=e_yi_ti}
	8159.1.1 = {death="8159.1.1"}
}
5543017 = {
	name="Xian"
	female=yes

	religion="yiti_gods"
	culture="yi_ti"
	
	8100.1.1 = {birth="8100.1.1"}
	8161.1.1 = {death="8161.1.1"}
}
5543018 = {
	name="Chai"	# a lord
	dynasty=65962

	father=5543016
	mother=5543017

	religion="yiti_gods"
	culture="yi_ti"
	
	8120.1.1 = {birth="8120.1.1"}
	8136.1.1 = {
		add_spouse = 5543019
	}
	8159.1.1 = {add_claim=e_yi_ti}
	8172.1.1 = {death="8172.1.1"}
}
5543019 = {
	name="Xian"
	female=yes

	religion="yiti_gods"
	culture="yi_ti"
	
	8120.1.1 = {birth="8120.1.1"}
	8172.1.1 = {death="8172.1.1"}
}
5543020 = {
	name="Xao"	# a lord
	dynasty=65962

	father=5543018
	mother=5543019

	religion="yiti_gods"
	culture="yi_ti"
	
	8137.1.1 = {birth="8137.1.1"}
	8155.1.1 = {
		add_spouse = 5543020
	}
	8172.1.1 = {add_claim=e_yi_ti}
	8192.1.1 = {death="8192.1.1"}
}
5543021 = {
	name="Bia"
	female=yes

	religion="yiti_gods"
	culture="yi_ti"
	
	8141.1.1 = {birth="8141.1.1"}
	8195.1.1 = {death="8195.1.1"}
}
5543022 = {
	name="Voq"	# a lord
	dynasty=65962

	father=5543020
	mother=5543021

	religion="yiti_gods"
	culture="yi_ti"
	
	8156.1.1 = {birth="8156.1.1"}
	8170.1.1 = {
		add_spouse = 5543023
	}
	8192.1.1 = {add_claim=e_yi_ti}
	8202.1.1 = {death="8202.1.1"}
}
5543023 = {
	name="Ae"
	female=yes

	religion="yiti_gods"
	culture="yi_ti"
	
	8156.1.1 = {birth="8156.1.1"}
	8198.1.1 = {death="8198.1.1"}
}
5543024 = {
	name="Nai"	# a lord
	dynasty=65962

	father=5543022
	mother=5543023

	religion="yiti_gods"
	culture="yi_ti"
	
	8179.1.1 = {birth="8179.1.1"}
	8197.1.1 = {
		add_spouse = 5543025
	}
	8202.1.1 = {add_claim=e_yi_ti}
	8227.1.1 = {death="8227.1.1"}
}
5543025 = {
	name="Ling"
	female=yes

	religion="yiti_gods"
	culture="yi_ti"
	
	8182.1.1 = {birth="8182.1.1"}
	8239.1.1 = {death="8239.1.1"}
}
5543026 = {
	name="Han"	# a lord
	dynasty=65962

	father=5543024
	mother=5543025

	religion="yiti_gods"
	culture="yi_ti"
	
	8200.1.1 = {birth="8200.1.1"}
	8220.1.1 = {
		add_spouse = 5543027
	}
	8227.1.1 = {add_claim=e_yi_ti}
	8267.1.1 = {death="8267.1.1"}
}
5543027 = {
	name="Lia"
	female=yes

	religion="yiti_gods"
	culture="yi_ti"
	
	8202.1.1 = {birth="8202.1.1"}
	8273.1.1 = {death="8273.1.1"}
}
5543028 = {
	name="Pha"	# a lord
	dynasty=65962

	father=5543026
	mother=5543027

	religion="yiti_gods"
	culture="yi_ti"
	
	8225.1.1 = {birth="8225.1.1"}
	8239.1.1 = {
		add_spouse = 5543029
	}
	8267.1.1 = {add_claim=e_yi_ti}
	8283.1.1 = {death="8283.1.1"}
}
5543029 = {
	name="Kai"
	female=yes

	religion="yiti_gods"
	culture="yi_ti"
	
	8202.1.1 = {birth="8202.1.1"}
	8273.1.1 = {death="8273.1.1"}
}
5543030 = {
	name="Zo"	# a lord
	dynasty=65962

	father=5543028
	mother=5543029

	religion="yiti_gods"
	culture="yi_ti"
	
	8240.1.1 = {birth="8240.1.1"}
	8268.1.1 = {
		add_spouse = 5543031
	}
	8283.1.1 = {add_claim=e_yi_ti}
	8290.1.1 = {death="8290.1.1"}
}
5543031 = {
	name="Sha"
	female=yes

	religion="yiti_gods"
	culture="yi_ti"
	
	8252.1.1 = {birth="8252.1.1"}
	8293.1.1 = {death="8293.1.1"}
}
5543032 = {
	name="Zong San"	# a lord
	dynasty=65962

	father=5543030
	mother=5543031

	add_trait="ambitious"
	add_trait="mystic"

	religion="yiti_gods"
	culture="yi_ti"
	
	8270.1.1 = {birth="8270.1.1"}
	8290.1.1 = {
		add_claim=e_yi_ti
		add_spouse = 5543033
		create_bloodline = {
			type = yellow_sorceror
			has_dlc = "Holy Fury"
		}
	}
}
5543033 = {
	name="Tha Chi"	# a lord
	female=yes

	add_trait="ambitious"

	religion="yiti_gods"
	culture="yi_ti"
	
	8270.1.1 = {birth="8270.1.1"}
}
5543034 = {
	name="Quan"	# a lord
	dynasty=65962

	father=5543032
	mother=5543033

	add_trait="mystic"

	religion="yiti_gods"
	culture="yi_ti"
	
	8291.1.1 = {birth="8291.1.1"}
}
5543035 = {
	name="Ru"	# a lord
	dynasty=65962

	father=5543032
	mother=5543033

	add_trait="twin"
	add_trait="mystic"

	religion="yiti_gods"
	culture="yi_ti"
	
	8292.1.1 = {birth="8292.1.1"}
}
5543036 = {
	name="San"	# a lord
	dynasty=65962

	father=5543032
	mother=5543033

	add_trait="twin"
	add_trait="mystic"

	religion="yiti_gods"
	culture="yi_ti"
	
	8292.1.1 = {birth="8292.1.1"}
}
5543037 = {
	name="Mha Jai"	# a lord
	female=yes
	dynasty=65962

	father=5543032
	mother=5543033

	add_trait="twin"
	add_trait="mystic"

	religion="yiti_gods"
	culture="yi_ti"
	
	8293.1.1 = {birth="8293.1.1"}
}