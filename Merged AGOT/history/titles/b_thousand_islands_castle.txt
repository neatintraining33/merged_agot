20.1.2 = {
	law=succ_gavelkind
	law=cognatic_succession
	holder = 0
}

900.1.1 = {
	law=cognatic_succession
	holder = 0
}

7970.1.1 = {
	holder = 770654 # Verdeos
}

8068.6.27 = {
	holder = 770662 # Skugg
}

8089.3.15 = {
	holder = 770665 # Green
}

8092.3.4 = {
	holder = 770667 # Ethel
}

8133.3.15 = {
	holder = 770669 # Ethelene
}

8141.3.15 = {
	holder = 770675 # Howard
}

8172.1.6 = {
	holder = 770676 # Phillips
}

8180.3.15 = {
	holder = 770677 # Verdeos
}

8180.3.19 = {
	holder = 770681 # Verdeos
}

8212.3.15 = {
	holder = 770687 # Oog
}

8212.4.1 = {
	holder = 770691 # Green (THE MAN, THE MYTH, THE LEGEND)
}

8260.12.30 = {
	holder = 770695 # Verdeos
}

8284.6.27 = {
	holder = 770700 # Oba (adopted)
}

8309.12.31 = {
	holder = 770702 # Depp
}

8320.9.1 = {
	holder = 770707 # Verdeos
}

8342.11.11 = {
	holder = 0 # I've honored my request up to the correct date
}