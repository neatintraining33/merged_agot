800.1.1={
	liege="e_stormlands"
	name = D_BLACKWATER_MOUTH
	effect = { location = { set_name = D_BLACKWATER_MOUTH } }
}

##Durrandon holders, so it doesnt erroneously go to aegon 
7760.1.1 = {
     holder = 3041544 #Durran
}	
7803.1.1 = {
     holder = 3031544 #Alrec
}	
7840.1.1 = {
     holder = 3021544 #Durran
}	
7887.1.1 = {
     holder = 3011544 #Angron
}
7921.1.1 = { 
     holder = 3001544 #Durran
}
7925.1.1={
	holder = 3002544 #Angron
}
7932.1.1={
	holder = 31544 #Arrec (C)
}
7945.1.1 = {
	holder = 21544 #Arlan V(C)
}
7955.1.1={
	holder = 1544 # Argilac the Arrogant (C)
}
##

7998.2.1={
	holder = 77000		#Aegon I 'the Conqueror'
	liege="d_kings_landing"
	reset_name = yes
	effect = { 
		location = { 
			set_name = c_kl_city
			set_province_flag = kings_landing_established 
		} 
	}
}

8000.1.1 = {
	law = succ_appointment
	reset_name = yes
	effect = {
		set_title_flag = military_command
		holder_scope = {
			if = {
				limit = { primary_title = { title = PREVPREV } }
				set_government_type = military_command_government 
				PREV = { succession = appointment }
				recalc_succession = yes
				give_minor_title = title_commander_gold_cloaks
			}
		}
	}
}
8018.1.1 = { 
	effect = {
		b_kl_cobblers_square = { 
			 set_title_flag = targshipyard_complete
		} 
		b_kl_dragon_pit = { 
			 set_title_flag = targshipyard_complete
		} 
		b_kl_dockyards = { 
			 set_title_flag = targshipyard_complete
		} 
		b_kl_flea_bottom = { 
			 set_title_flag = targshipyard_complete
		} 
		b_kl_dragon_gate = { 
			 set_title_flag = targshipyard_complete
		} 
		b_kl_kings_gate = { 
			 set_title_flag = targshipyard_complete
		} 
	}	
}
#Since Qarl Corbray was lord, his office is ommited (8048.3.7 - 8059.2.1)
8059.2.1 = {
	holder = 16288 #Robert Redwyne
}
8089.1.1 = {
	holder = 0
}
8104.1.1 = {
	holder=779496 #Daemon Targaryen
}
8105.12.25={
	holder = 77009
}
8120.1.1={
	holder = 30509006 #Luthor Largent
}
8130.5.22={ 
	holder = 0	#Balon Byrch ommited, as he was Captain for Rhaenyra and she is never holder of e_iron_throne in our mod
}
8133.8.1={
	holder=6012299 #Lucas Leygood
}
8136.1.1={
	holder=6001430 #Adrian Thorne
}
8160.1.1={
	holder=6001033 #Richard Harte
}
8165.1.1={
	holder=220156 # Luthor Rivers
}
8196.6.1={
	holder=0
}
8275.1.1={
	holder = 77103 #Manly Stokeworth
}
8283.6.1={
	holder = 200353 #Janos Slynt
}
8298.11.2={
	holder = 2033045 #Jacelyn Bywater
}
8299.7.2 = {
	holder = 1212 #Addam Marbrand
}
8300.2.9 = {
	holder = 202352 #Osfryd Kettleback
}
8300.5.17 = {
	holder = 696969106 #Humfrey Waters
}



