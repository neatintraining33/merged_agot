# 226 - Kings Landing

# County Title
title = c_kings_landing

# Settlements
max_settlements = 7

b_red_keep = castle

# Misc
culture = old_first_man
religion = old_gods

# History
6700.1.1 = { 
	culture = stormlander
	religion = the_seven
}
7998.2.1 = { #Aegon Lands
	b_red_keep = ca_asoiaf_crown_basevalue_1
	b_red_keep = ca_asoiaf_crown_basevalue_2
}
8000.1.1 = { #Aegon Establishes Capital
	b_rk_tower_hand = castle
	b_white_sword_tower = castle
	b_rk_kitchen_keep = castle
	b_rk_maidenvault = castle
	b_rk_black_cells = castle	
	b_rk_sept = temple	
	b_red_keep = ca_iron_throne
	
}
8009.1.1 = { 
	b_red_keep = ca_asoiaf_crown_basevalue_3
	b_red_keep = ca_dragon_pit
	b_red_keep = ca_cellar_1
	b_rk_tower_hand = ca_asoiaf_crown_basevalue_1
	b_white_sword_tower = ca_asoiaf_crown_basevalue_1
	b_rk_maidenvault = ca_asoiaf_crown_basevalue_1
	b_rk_kitchen_keep = ca_asoiaf_crown_basevalue_1
	b_rk_black_cells = ca_asoiaf_crown_basevalue_1
}
8018.1.1 = { 
	b_red_keep = ca_asoiaf_crown_basevalue_4
	b_red_keep = ca_asoiaf_targshipyard
	b_rk_tower_hand = ca_asoiaf_targshipyard
	b_rk_tower_hand = ca_asoiaf_crown_basevalue_2
	b_white_sword_tower = ca_asoiaf_crown_basevalue_2
	b_rk_maidenvault = ca_asoiaf_crown_basevalue_2
	b_rk_kitchen_keep = ca_asoiaf_crown_basevalue_2
	b_rk_black_cells = ca_asoiaf_crown_basevalue_2
	culture = crownlander
}
8027.1.1 = { 
	b_red_keep = ca_asoiaf_crown_basevalue_5
	b_rk_tower_hand = ca_asoiaf_crown_basevalue_3
	b_rk_maidenvault = ca_asoiaf_crown_basevalue_3
	b_rk_kitchen_keep = ca_asoiaf_crown_basevalue_3
	b_white_sword_tower = ca_asoiaf_crown_basevalue_3
	b_rk_black_cells = ca_asoiaf_crown_basevalue_3
}

8045.1.1 = { #Completed during reign of Maegor the Cruel
	b_red_keep = ca_asoiaf_crown_basevalue_6
}
8070.1.1 = { #Jaehaerys constructs road network
	#b_red_keep = ca_kingsroad
	#b_red_keep = ca_goldroad
	#b_red_keep = ca_roseroad
}
8165.1.1 = { #Baelor establishes Great Sept
	b_rk_sept = tp_monastery_1
	b_rk_sept = tp_monastery_2
}