# 1167  - Changhan

# County Title
title = c_changhan

# Settlements
max_settlements = 4

b_changhan_castle = castle
b_changhan_city1 = city
b_changhan_temple = temple
b_changhan_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_changhan_castle = ca_asoiaf_yiti_basevalue_1
	b_changhan_castle = ca_asoiaf_yiti_basevalue_2

	b_changhan_city1 = ct_asoiaf_yiti_basevalue_1

}
	