# 1184  - Jingding

# County Title
title = c_jingding

# Settlements
max_settlements = 4

b_jingding_castle = castle
b_jingding_city1 = city
b_jingding_temple = temple
b_jingding_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_jingding_castle = ca_asoiaf_yiti_basevalue_1
	b_jingding_castle = ca_asoiaf_yiti_basevalue_2

	b_jingding_city1 = ct_asoiaf_yiti_basevalue_1
	b_jingding_city1 = ct_asoiaf_yiti_basevalue_2

}
	