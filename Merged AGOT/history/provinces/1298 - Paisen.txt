# 1201  - Paisen

# County Title
title = c_paisen

# Settlements
max_settlements = 4

b_paisen_castle = castle
b_paisen_city1 = city
b_paisen_temple = temple
b_paisen_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_paisen_castle = ca_asoiaf_yiti_basevalue_1
	b_paisen_castle = ca_asoiaf_yiti_basevalue_2

	b_paisen_city1 = ct_asoiaf_yiti_basevalue_1
	b_paisen_city1 = ct_asoiaf_yiti_basevalue_2

}
	