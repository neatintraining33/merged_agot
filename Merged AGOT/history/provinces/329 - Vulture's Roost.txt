# 329 - Vultures Roost

# County Title
title = c_vulturesroost

# Settlements
max_settlements = 4
b_vulturesroost = castle
b_stonebarrow = city
b_dedworth = castle
#b_hogfort = castle
#b_dufftown = city

# Misc
culture = old_first_man
religion = old_gods
terrain = mountain


1.1.1 = {
	b_vulturesroost = ca_asoiaf_dorne_basevalue_1
	b_vulturesroost = ca_asoiaf_dorne_basevalue_2
}

1.1.1 = {
	b_stonebarrow = ct_asoiaf_dorne_basevalue_1
}

1.1.1 = {
	b_dedworth = ca_asoiaf_dorne_basevalue_1
}
6700.1.1 = {
	culture = dornish_andal
	religion = the_seven
}
7350.1.1 = {
	culture = stone_dornish
}