# 1218  - Menghua

# County Title
title = c_menghua

# Settlements
max_settlements = 4

b_menghua_castle = castle
b_menghua_city1 = city
b_menghua_temple = temple
b_menghua_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_menghua_castle = ca_asoiaf_yiti_basevalue_1
	b_menghua_castle = ca_asoiaf_yiti_basevalue_2

	b_menghua_city1 = ct_asoiaf_yiti_basevalue_1
	b_menghua_city1 = ct_asoiaf_yiti_basevalue_2

}
	