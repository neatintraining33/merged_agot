# 1179  - Shandeng

# County Title
title = c_shandeng

# Settlements
max_settlements = 4

b_shandeng_castle = castle
b_shandeng_city1 = city
b_shandeng_temple = temple
b_shandeng_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_shandeng_castle = ca_asoiaf_yiti_basevalue_1
	b_shandeng_castle = ca_asoiaf_yiti_basevalue_2

	b_shandeng_city1 = ct_asoiaf_yiti_basevalue_1
	b_shandeng_city1 = ct_asoiaf_yiti_basevalue_2

}
	