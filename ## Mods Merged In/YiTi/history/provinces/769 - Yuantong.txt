# 769  - Yuantong

# County Title
title = c_yuantong

# Settlements
max_settlements = 6

b_yuantong_castle1 = castle
b_yuantong_city1 = city
b_yuantong_temple = temple
b_yuantong_city2 = city
b_yuantong_castle2 = city
b_yuantong_castle3 = castle

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_yuantong_castle1 = ca_asoiaf_yiti_basevalue_1
	b_yuantong_castle1 = ca_asoiaf_yiti_basevalue_2
	b_yuantong_castle1 = ca_asoiaf_yiti_basevalue_3

	b_yuantong_city1 = ct_asoiaf_yiti_basevalue_1
	b_yuantong_city1 = ct_asoiaf_yiti_basevalue_2
	b_yuantong_city1 = ct_asoiaf_yiti_basevalue_3

	b_yuantong_city2 = ct_asoiaf_yiti_basevalue_1
	b_yuantong_city2 = ct_asoiaf_yiti_basevalue_2
	b_yuantong_city2 = ct_asoiaf_yiti_basevalue_3

}
	