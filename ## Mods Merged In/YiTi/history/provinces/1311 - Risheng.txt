# 1214  - Risheng

# County Title
title = c_risheng

# Settlements
max_settlements = 4

b_risheng_castle = castle
b_risheng_city1 = city
b_risheng_temple = temple
b_risheng_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_risheng_castle = ca_asoiaf_yiti_basevalue_1
	b_risheng_castle = ca_asoiaf_yiti_basevalue_2

	b_risheng_city1 = ct_asoiaf_yiti_basevalue_1
	b_risheng_city1 = ct_asoiaf_yiti_basevalue_2

}
	