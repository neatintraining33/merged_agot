# 868  - Tanqu

# County Title
title = c_tanqu

# Settlements
max_settlements = 6

b_tanqu_castle1 = castle
b_tanqu_city1 = city
b_tanqu_temple = temple
b_tanqu_city2 = city
b_tanqu_castle2 = city
b_tanqu_castle3 = castle

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_tanqu_castle1 = ca_asoiaf_yiti_basevalue_1
	b_tanqu_castle1 = ca_asoiaf_yiti_basevalue_2
	b_tanqu_castle1 = ca_asoiaf_yiti_basevalue_3

	b_tanqu_city1 = ct_asoiaf_yiti_basevalue_1
	b_tanqu_city1 = ct_asoiaf_yiti_basevalue_2

	b_tanqu_city2 = ct_asoiaf_yiti_basevalue_1
	b_tanqu_city2 = ct_asoiaf_yiti_basevalue_2

}
	