# 1195  - Nuhong

# County Title
title = c_nuhong

# Settlements
max_settlements = 4

b_nuhong_castle = castle
b_nuhong_city1 = city
b_nuhong_temple = temple
b_nuhong_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_nuhong_castle = ca_asoiaf_yiti_basevalue_1
	b_nuhong_castle = ca_asoiaf_yiti_basevalue_2

	b_nuhong_city1 = ct_asoiaf_yiti_basevalue_1
	b_nuhong_city1 = ct_asoiaf_yiti_basevalue_2

}
	