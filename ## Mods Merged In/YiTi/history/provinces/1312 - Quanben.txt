# 1215  - Quanben

# County Title
title = c_quanben

# Settlements
max_settlements = 4

b_quanben_castle = castle
b_quanben_city1 = city
b_quanben_temple = temple
b_quanben_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_quanben_castle = ca_asoiaf_yiti_basevalue_1
	b_quanben_castle = ca_asoiaf_yiti_basevalue_2

	b_quanben_city1 = ct_asoiaf_yiti_basevalue_1
	b_quanben_city1 = ct_asoiaf_yiti_basevalue_2

}
	