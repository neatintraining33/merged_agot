# 770  - Jinqi

# County Title
title = c_jinqi

# Settlements
max_settlements = 7

b_mingqiu_castle = castle
b_jinqi_city1 = city
b_jinqi_temple = temple
b_jinqi_city2 = city
b_jinqi_castle1 = city
b_jinqi_castle2 = city
b_jinqi_castle3 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_mingqiu_castle = ca_asoiaf_yiti_basevalue_1
	b_mingqiu_castle = ca_asoiaf_yiti_basevalue_2
	b_mingqiu_castle = ca_asoiaf_yiti_basevalue_3
	b_mingqiu_castle = ca_asoiaf_yiti_basevalue_4
	b_mingqiu_castle = ca_asoiaf_yiti_basevalue_5
	b_mingqiu_castle = ca_mingqiu_castle

	b_jinqi_city1 = ct_asoiaf_yiti_basevalue_1
	b_jinqi_city1 = ct_asoiaf_yiti_basevalue_2
	b_jinqi_city1 = ct_asoiaf_yiti_basevalue_3
	b_jinqi_city1 = ct_asoiaf_yiti_basevalue_4
	b_jinqi_city1 = ct_asoiaf_yiti_basevalue_5

	b_jinqi_city2 = ct_asoiaf_yiti_basevalue_1
	b_jinqi_city2 = ct_asoiaf_yiti_basevalue_2
	b_jinqi_city2 = ct_asoiaf_yiti_basevalue_3
	b_jinqi_city2 = ct_asoiaf_yiti_basevalue_4
	b_jinqi_city2 = ct_asoiaf_yiti_basevalue_5

	b_jinqi_castle1 = ct_asoiaf_yiti_basevalue_1
	b_jinqi_castle1 = ct_asoiaf_yiti_basevalue_2
	b_jinqi_castle1 = ct_asoiaf_yiti_basevalue_3
	b_jinqi_castle1 = ct_asoiaf_yiti_basevalue_4

	b_jinqi_castle2 = ct_asoiaf_yiti_basevalue_1
	b_jinqi_castle2 = ct_asoiaf_yiti_basevalue_2
	b_jinqi_castle2 = ct_asoiaf_yiti_basevalue_3
	b_jinqi_castle2 = ct_asoiaf_yiti_basevalue_4

	b_jinqi_castle3 = ct_asoiaf_yiti_basevalue_1
	b_jinqi_castle3 = ct_asoiaf_yiti_basevalue_2
	b_jinqi_castle3 = ct_asoiaf_yiti_basevalue_3
	b_jinqi_castle3 = ct_asoiaf_yiti_basevalue_4

}
	