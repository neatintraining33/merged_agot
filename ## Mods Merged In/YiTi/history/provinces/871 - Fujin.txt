# 871  - Fujin

# County Title
title = c_fujin

# Settlements
max_settlements = 6

b_fujin_castle1 = castle
b_fujin_city1 = city
b_fujin_temple = temple
b_fujin_city2 = city
b_fujin_castle2 = city
b_fujin_castle3 = castle

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_fujin_castle1 = ca_asoiaf_yiti_basevalue_1
	b_fujin_castle1 = ca_asoiaf_yiti_basevalue_2
	b_fujin_castle1 = ca_asoiaf_yiti_basevalue_3

	b_fujin_city1 = ct_asoiaf_yiti_basevalue_1
	b_fujin_city1 = ct_asoiaf_yiti_basevalue_2

	b_fujin_city2 = ct_asoiaf_yiti_basevalue_1
	b_fujin_city2 = ct_asoiaf_yiti_basevalue_2

}
	