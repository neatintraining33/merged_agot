# 1161  - Fengtun

# County Title
title = c_fengtun

# Settlements
max_settlements = 4

b_fengtun_castle = castle
b_fengtun_city1 = city
b_fengtun_temple = temple
b_fengtun_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_fengtun_castle = ca_asoiaf_yiti_basevalue_1
	b_fengtun_castle = ca_asoiaf_yiti_basevalue_2

	b_fengtun_city1 = ct_asoiaf_yiti_basevalue_1

}
	