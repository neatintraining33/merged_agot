# 1204  - Guishui

# County Title
title = c_guishui

# Settlements
max_settlements = 4

b_guishui_castle = castle
b_guishui_city1 = city
b_guishui_temple = temple
b_guishui_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_guishui_castle = ca_asoiaf_yiti_basevalue_1
	b_guishui_castle = ca_asoiaf_yiti_basevalue_2

	b_guishui_city1 = ct_asoiaf_yiti_basevalue_1
	b_guishui_city1 = ct_asoiaf_yiti_basevalue_2

}
	