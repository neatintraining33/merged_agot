# County Title
title = c_bleeding_marches

# Settlements
max_settlements = 4

b_bleeding_marches_castle = castle
b_bleeding_marches_city1 = city
b_bleeding_marches_temple = temple
b_bleeding_marches_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {
	b_bleeding_marches_castle = ca_asoiaf_yiti_basevalue_1
	b_bleeding_marches_castle = ca_asoiaf_yiti_basevalue_2

	b_bleeding_marches_city1 = ca_asoiaf_yiti_basevalue_2

}
	