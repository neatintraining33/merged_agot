# 1219  - Jiaying

# County Title
title = c_jiaying

# Settlements
max_settlements = 4

b_jiaying_castle = castle
b_jiaying_city1 = city
b_jiaying_temple = temple
b_jiaying_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_jiaying_castle = ca_asoiaf_yiti_basevalue_1
	b_jiaying_castle = ca_asoiaf_yiti_basevalue_2

	b_jiaying_city1 = ct_asoiaf_yiti_basevalue_1
	b_jiaying_city1 = ct_asoiaf_yiti_basevalue_2

}
	