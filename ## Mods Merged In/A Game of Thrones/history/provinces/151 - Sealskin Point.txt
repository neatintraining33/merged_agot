# 151 - Sealskin Point

# County Title
title = c_sealskin_point

# Settlements
max_settlements = 3
b_sealskin_point = castle
b_faroar = castle

#b_wynd_point


# Misc
culture = old_ironborn
religion = drowned_god

#History

1.1.1 = {b_sealskin_point = ca_asoiaf_ironislands_basevalue_1}
1.1.1 = {b_sealskin_point = ca_asoiaf_ironislands_basevalue_2}
1.1.1 = {b_sealskin_point = ca_asoiaf_ironislands_basevalue_3}
1.1.1 = {b_sealskin_point = ca_asoiaf_ironislands_basevalue_4}

1.1.1 = {b_faroar = ca_asoiaf_ironislands_basevalue_1}
1.1.1 = {b_faroar = ca_asoiaf_ironislands_basevalue_2}

6700.1.1 = {
	culture = ironborn
}
