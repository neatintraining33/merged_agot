# 46 - Barrowton

# County Title
title = c_barrowton

# Settlements
max_settlements = 5
b_highbarrow = castle
b_barrowton = city
b_godshill = temple
b_brownbarrow = castle
#b_harwoodshold = castle

# Misc
culture = old_first_man
religion = old_gods

# History

1.1.1 = {
	b_highbarrow = ca_asoiaf_north_basevalue_1
	b_highbarrow = ca_asoiaf_north_basevalue_2
	b_highbarrow = ca_asoiaf_north_basevalue_3
	
	b_barrowton = ct_asoiaf_north_basevalue_1
	b_barrowton = ct_asoiaf_north_basevalue_2
	b_barrowton = ct_asoiaf_north_basevalue_3
	b_barrowton = ct_asoiaf_north_basevalue_4

	b_brownbarrow = ca_asoiaf_north_basevalue_1
}
6700.1.1 = {
	culture = northman
}