# 98 - Saltpans

# County Title
title = c_saltpans

# Settlements
max_settlements = 5
b_saltpansholdfast = castle
b_saltpans = city
b_quietisle = temple
b_hawick = castle

# Misc
culture = old_first_man
religion = old_gods

# History

1.1.1 = {

b_saltpansholdfast = ca_asoiaf_river_basevalue_1
b_saltpansholdfast = ca_asoiaf_river_basevalue_2

b_saltpans = ct_asoiaf_river_basevalue_1
b_saltpans = ct_asoiaf_river_basevalue_2
b_saltpans = ct_asoiaf_river_basevalue_3


b_hawick = ca_asoiaf_river_basevalue_1

}

6700.1.1 = {
	culture = riverlander
	religion = the_seven
}