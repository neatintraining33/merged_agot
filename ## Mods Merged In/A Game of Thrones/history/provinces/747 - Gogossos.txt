# 747 - Gogossos

# County Title
title = c_gogossos

# Settlements
max_settlements = 7

b_gogossos_castle = castle
b_gogossos = city
b_gogossos_temple = temple
b_gogossos_4 = city
b_gogossos_5 = castle
b_gogossos_6 = city

# Misc
culture = ghiscari
religion = harpy

3010.1.1 = {
	culture = eastern_valyrian
	religion = valyrian_rel
}
5890.1.1 = {
	b_gogossos = ct_asoiaf_summerisland_basevalue_1
	b_gogossos = ct_asoiaf_summerisland_basevalue_2
	b_gogossos = ct_asoiaf_summerisland_basevalue_3
	b_gogossos = ct_asoiaf_summerisland_basevalue_4
	b_gogossos = ct_asoiaf_summerisland_basevalue_5
	b_gogossos = ct_asoiaf_summerisland_basevalue_6

	b_gogossos_castle = ca_asoiaf_summerisland_basevalue_1
	b_gogossos_castle = ca_asoiaf_summerisland_basevalue_2
	b_gogossos_castle = ca_asoiaf_summerisland_basevalue_3
	b_gogossos_castle = ca_asoiaf_summerisland_basevalue_4
	b_gogossos_castle = ca_asoiaf_summerisland_basevalue_5
	b_gogossos_castle = ca_asoiaf_summerisland_basevalue_6
	
	b_gogossos_4 = ct_asoiaf_summerisland_basevalue_1
	b_gogossos_4 = ct_asoiaf_summerisland_basevalue_2
	b_gogossos_4 = ct_asoiaf_summerisland_basevalue_3
	b_gogossos_4 = ct_asoiaf_summerisland_basevalue_4
	
	b_gogossos_5 = ca_asoiaf_summerisland_basevalue_1
	b_gogossos_5 = ca_asoiaf_summerisland_basevalue_2
	b_gogossos_5 = ca_asoiaf_summerisland_basevalue_3
	b_gogossos_5 = ca_asoiaf_summerisland_basevalue_4
	
	b_gogossos_6 = ct_asoiaf_summerisland_basevalue_1
	b_gogossos_6 = ct_asoiaf_summerisland_basevalue_2
	b_gogossos_6 = ct_asoiaf_summerisland_basevalue_3
}
7886.1.1 = {
	culture = gogossosi
}
