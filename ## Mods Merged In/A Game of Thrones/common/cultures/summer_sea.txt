summer_islands = {
	graphical_cultures = { africangfx } # buildings
	
	summer_islander = {
		graphical_cultures = { westafricangfx africangfx muslimgfx }
		
		color = { 0.0 0.6 0.0 } #Green
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		male_names = {
			#Canon
			Jalabhar:1000 Quhuru:500 Tal:300 Xaro:300 Xhondo:1000 Xhoan:300 Malthar:500 Bokokko:500 Balaq:200
			
			#Non Canon
			Abrafo Adeyemu Anyim Arkamun
			Bapato
			Chakoro Chibundo
			Delmabhar Dubaku Doshuru
			Eskender
			Gwalahuru Gatubhar
			Isingodo Isodo
			Jaramogo Jiradhar Jalahuru Jarado
			Konde Koroxhar Kwasdo
			Lusaxharo
			Makhan Manahuru Mermesbhar
			Nyadundo 
			Orengo Odingaxho Olujondo Oluhuru
			Punuxharo Paro 
			Qaro Qubhar
			Ratemo Rudo
			Sango Simbhar Siboxhoan
			Tapuhuru Thabo
			Udo 
			Xhelimo Xhakan Xhonodo Xhal
				
			Xala Xaladaar Xaros Xhallar Marral Sallal Xalloba Sololhor Zabaho Xolhabba Zabhar Zhalla Zhabar Baqual Bharral Zolhallar
			Bolhabhos Saros Xobha Zalor Jallono Jaros Santarro Mabhor Solor Xalla Dorral Dallalhor Danadhar Zhobaba Tolhar Zontar Jonaba
			Maro Mhondo Malabhar Thondo Thoan Talthar Talabhar Qal Qaro Qhondo Qalthar Qalabhar Xuhuru Johuru Tuhuru Buhuru Kuhuru Nuhuru
			Luhuru Laro Lhondo Lhoan Lalthar Labhar Maballa Lonaba Lontar Sadharo Radharo Solhonar Xolhonar Molhonor Talhonar Zonar Tanal Tanaba Tolor
			Toralhal Tollo Banara Maras Bobhas Tadhontor Dalhobos Xhoquolhor Zoltharror Xobas Dadhor Xabo Salor Xadha Bollolhol Zhadhollar Bharrala Tallaquol
			Tarras Bolos Tolar Mabar Joros Solthoquo Zhala Jallo Dabas Allos Altharo Marallar Ballarros Xhoquar Sorronar Morror Zantas Zhoraral Jadharror
			Zhanar Jallol Zalar Xalar Bhalontar Madhaquar Salthbor Santonar Zarralha Bhoralthar Xhononar Manos Talla Salhontal Danoquas Dhobol Danaba Dhalla
			Thondo Dalthar Donaba Dalaxaar Dhallar Dolzhallar Dalabhar Dolhonara Jobhal Sobhar Tolhor Torra Xaladhor Jhabar Janaba Dalabhar Salabhar Kalabhar Nalabhar
			Yalabhar Balabhar Talabhar Ralabhar Alabhar Calabhar Mokokko Jokokko Dokokko Sokokko Tokokko Kokokko
			
			Adedamola Afolabi Adebimpe Anulowaposi Olanrewaju Omobolanle Abiodun Enitan Oluwatoyin Babatunde Oladele Babajide Aderounke Oluwarantimi Adeyemi Ekundayo Teniayo
		}
		female_names = {
			#Canon
			Alayaya:700 Chataya:700 Kojja:700 Xanda:500 Chatana:500
			
			#Non Canon
			Arjana Abraya
			Bujiba Bapaya
			Chakoya Chijja
			Dumisa Dubaya Doshura
			Emejja
			Fulataya
			Gebhuya Gwahura
			Isoba
			Jalataya Jalahura Jarada
			Kobijja Koraxha Kwasya
			Lusala Lujja
			Manyara Makya Manahura
			Onyaya Oluhura
			Pojja Paya
			Quanda Quhura Qubya
			Shanika Simbaya
			Tatenda Talyaya
			Ujja Uda
			Xara Xhonda Xha Xajja Xataya
			
			Tanta Chasa Xhola Nola Xolla Xhonorra Xhotana Raraza Zadhata Tolorra Sala Tolla Xara Chadha Xharra Marrara Totara Rarrasa Raradha Rollaza Rana
			Nolta Xola Joza Xatalha Larana Lodhara Zalonta Sojja Xojja Tojja Rojja Fojja Qojja Mhatana Rhatana Shatana Phatana Lhatana Dhatana Bhatana Qhatana
			Gwanda Manda Banda Sanda Landa Bhataya Ihataya Lhataya Dhataya Jhataya Shataya Khataya Xhataya Ghataya Zhataya Tataya Tayaya Lhayaya Dhalaya Khalaya
			Chalayaya Palaya Malayaya Phataya Natana Xatalla Zhadhata Zhonda
		}
		
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		seafarer = yes
		
		modifier = default_culture_modifier
	}
}

sothoryos_islands = {
	graphical_cultures = { africangfx } # buildings
	
	basilisk_islander = {
		graphical_cultures = { westafricangfx africangfx muslimgfx }
		
		color = { 0.0 0.4 0.0 } #Green
		
		used_for_random = no #Removed from the mod, retained for save compatibility
		allow_in_ruler_designer = no
		alternate_start = { 
			has_alternate_start_parameter = { key = culture value = full_random } 
			NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } }
		}
		
		male_names = {
			#Canon
			Tumco:500
			
			#Non Canon
			Abrafo Adeyemu Anyim Arkamun
			Bapato
			Chakoro Chibundo
			Delmabhar Dubaku Doshuru
			Eskender
			Gwalahuru Gatubhar
			Isingodo Isodo
			Jaramogo Jiradhar Jalahuru Jarado
			Konde Koroxhar Kwasdo
			Lusaxharo
			Makhan Manahuru Mermesbhar
			Nyadundo 
			Orengo Odingaxho Olujondo Oluhuru
			Punuxharo Paro 
			Qaro Qubhar
			Ratemo Rudo
			Sango Simbhar Siboxhoan
			Tapuhuru Thabo
			Udo 
			Xhelimo Xhakan Xhonodo Xhal
			
			Adedo Molo Afolo Abo Adebimo Anulo Wapos Olanro waju Omobo Lano Abiodun Enitan Oluwat Oyin Babo Tundo Oladelo Babado Aderoko Oluwar Antimo Adeyo Ekund Unayo Teniayo
			Ayotunde Olatunji Tokunbo Omowale Temilo Oluwak Ayomido Eniolo Tejumola Adeymo Morounkola Ifedapo Adesanya Adisa Adeboye Omolayo Durosin Jibola Jelani Jabari
			Zuberi Kayin Temidare Folami Tiwalade Dayo Kolawole Akande Sunkanmi Ifedayo Gboyega Iodowu Kayode Zumco Sunbol
		}
		female_names = {
			#Canon
			
			#Non Canon
			Arjana Abraya
			Bujiba Bapaya
			Chakoya Chijja
			Dumisa Dubaya Doshura
			Emejja
			Fulataya
			Gebhuya Gwahura
			Isoba
			Jalataya Jalahura Jarada
			Kobijja Koraxha Kwasya
			Lusala Lujja
			Manyara Makya Manahura
			Onyaya Oluhura
			Pojja Paya
			Quanda Quhura Qubya
			Shanika Simbaya
			Tatenda Talyaya
			Ujja Uda
			Xara Xhonda Xha Xajja Xataya
			
			Yewande Moniade Fiayosemi Akintoye Gbolahun Bosedi Mojisole Boluwatife Ololade Oluwafemi Tenia Oluwa Olola Ifeda Adebi Bola Oluwakana Moruna Adetola Oba
			Ireti Jiboli Moni Sunbola Omo Omota Omotanwa Oluwaya Aderi Oni Ani Kolapi Anituke Temi Itana Morenike Pami Numi Aya Unumi Kunumi Miula Unmise Boyi IYabi
			Ayatani Anuwani Feyisa Taya Oya Abi Koniya Kejja Sejja Pojja Impe Titilaya Esanya Enika Kunle Ibola Boli
		}
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0

		seafarer = yes
		
		modifier = default_culture_modifier
	
	}	
	brindlemen = {
		graphical_cultures = { brindlegfx westafricangfx africangfx muslimgfx }
		
		color = { 0.5 0.7 0.2 } #Green
		
		used_for_random = no
		allow_in_ruler_designer = no
		
		alternate_start = { 
			has_alternate_start_parameter = { key = culture value = full_random } 
			NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } }
		}
		
		male_names = {
			Borug Buggug Burghed Bagdud
			Durzub
			Faghig Fidgug
			Garekk Gomatug Ghamonk Grung Gnabadug Gaakt
			Hibub
			Ignatz Ignorg
			Karrghed Kilug
			Mug Murkub
			Oggugat Otugbu Oglub Oakgu
			Pehrakgu Parfu
			Sorgulg Shamob Sulgha
			Urgran Uzul Urim Ug Urbul
			Vigdug Vulug Vetorkag Viguka Vamugbu
			Xothkug Xugarf	Xutjja	 		
			Zurgha Zarod Zurpigig Zorgulg Zulmthu
			
			Xeqeth Nuchoh Sagoc Yeke Zunza Zezakua Ajousux Neshuhgu Juesesh Xujut Sikhas Rijush Rugas Thaqese Xujut
			Chagoth Sheszeth Heszah Uxus Thigik Zaaz Nagakh Neguh Yegeh Uhnuq Saka Zehga Jhakesu Xuga Sauggu Najahn
			Naukhaq Thougras Chux Choqha Ruxag Roukru Yoguk Hejaug Yaukraq Jikzo Xaqhut Shugru Ekako Nunshu 
			Zinsbog Derthag Viggu Baugh Worthag Zonagh Mazhug Burub Shagdub Kharzug Mogak Ghorz Durgat Durz Shagar
			
		}
		female_names = {
			Bumph Borgakh Bulfim Bula Bulak Bolar Bor Badbog
			Dulug
			Gashnakh Gluronk Gharol Ghob Gulfim Ghak Ghorza
			Homraz
			Lambug Lazgar Lagakh
			Mor Morn Murzush Mazoga Mogak
			Oghash
			Rogmesh Rulfim Ragash
			Sharn Shel Shazgob Shagar Sharamph Sharog
			Ulumpha Urog Ushat Ushug Urzoth Ugak Ugor
			Yazgash Yotul

			Durgati Burzobi Ghorza Shadbaka Mogaka Buruba Ragasha Moga Snaki Mazoga Rogbati Urogi Agrobi Nargoli Shagdubi Kharzuga
			Grati Bulfimi Razobi Globi Gharoli Gula Lagakhi Borgakha Oghasa Yotuli Durza Shagara Bula Glasha Rogmesha Shelura
			Araobi Ghaki Murzushi Orbula
		}

		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		modifier = backward_culture_modifier
	}
}

naath_group = {
	graphical_cultures = { africangfx } # buildings

	naathi = {
		graphical_cultures = { africangfx naathigfx westafricangfx } # portraits

		color = { 0.1373 0.8235 0.4118 } #blue
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		male_names = {
			#Canon
			Marselen:500 Mossador:500
			
			#DAMOC - NON-CANON
			
			Marselon Massador Mossudan Messudan Messador Mersalan Massodar Mardelan Marssala
			Arselen Ossador Karselen Kossador Lossador Nossador Narselen Harselen Hossador Jossador Jarselen
			Barselen Bossador Cossador Carselen Gossador Garselen Martellar Mertallar Kartellar Hotellar Jartellar
			Artellar Nartellara Kosstellar Mosstellar Milsaran Kilsaran Hilsaran Bilsaran Ibran Jilan Edellan Rissend 
			Athanis Reyan Carran Issand Issandor Gallendor Esselador Janselen Atylen Vilsaran Vissador Rissador
			Wissador Wistelar Kissador Retellar Retelen Edelen Martelen Jartelen Mostelen Artelen 
			
			
		}
		female_names = {
		
			#Canon
			Missandei:500
			
			#DAMOC - NON-CANON
			
			Nyssa Nissandei Lissandei Issandei Aleriara Unalaeria Gisellara Orrisei Issacharis Bersendei Neritine Alyrelle Macharissei Charissai Atylrra Merelendra
			Urilei Yssendai Hyssanda Janalaena Manyllei Jismedei Evelendai Athessei Orissanda Ryssa Rissanda Rissendai Jissendai Missendai Mossadorai Marselenei
			Meliryn Isoronei Mahtarnei Amradei Ibranei Marivyna Massadei Mysalana Esselana Essadorei Bassadorei Khissandei Maidelena Darrisana Adelya Jilana
			Carrai Fissana Reyana Laisendai Assadai Maisinia Millansia Nannalyn Melicia Edellana Gallenai Miasellenai Mariheria Eysania Hilania Athanissai
		}
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
}