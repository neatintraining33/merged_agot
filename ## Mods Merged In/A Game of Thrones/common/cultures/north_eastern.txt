ibbenese_group = {
	graphical_cultures = { norsegfx } # buildings
	
	ibbenese = {
		graphical_cultures = { celticgfx ibbenesegfx } # portraits, units
		
		color = { 0.45 0.45 0.55 }
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		male_names = {
			Togg Bodger Brogg
			Caloth Cull Clargg Colosh
			Doth Drogg Durr Draloth	
			Erroth Eleogg
			Galt Groo Gurn
			Haggon Holger Hogg
			Jogg Joth Jarr
			Kull			
			Morr Moggoth			
			Quort
			Slugg Sogg Sogoloth
			Tumosh Tugg Torr Teleoth
			Ulf Umar Ygon
			
			Unul Rotsang Ullan Jumir Luzag Ahum Enruk Zunal Gikel Chek Uzam Karor Zutsu Osran Gyar Arun
			Zogg Nulrogg Buzogg Uzugg Azogg Azol Kyogg Noth Koth Toth Azoth Guzar Urash Sarron Usog
			Rufo Rufog Poleg Phik Ziweg Lameg Irog Uhnog Shikog Fanog Kalleg
			
		}
		female_names = {
			Thirsoth Togga Kotha Culla Clargga Colosha Galta Groona Gurna Jarra Krulla Morra Moggotha Slugga Sogga Sogolotha Tumosha Tugga Torra Telotha Ulfa Umara Ygona Uma
			Unula Rogsanga Ullana Jumira Luzaqa Ahuma Enruka Zunala Gikela Cheka Uzama Karora Zutsua Osrana Gyara Aruna Zogga Nulrogga Buzogga Azogga Azola Kyogga Uda
			Notha Totha Azotha Guzara Sarra Sarrona Usoga Rufoga Polega Phika Alussa Kalle Ehol Fanna Leke Shike Uhnath Irah Lame Miluse Ziwega Una Ona Oda Ogga Yona Zuna Rona 		
		}
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
}
jogos_nhai_group = {
	graphical_cultures = { mongolgfx } # buildings

	jogos_nhai = {
		graphical_cultures = { jogosgfx muslimgfx }
		#horde = yes
		
		color = { 0.28 0 0.28 }
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		nomadic_in_alt_start = yes
		
		male_names = { 
			#Canon
			Gharak:500
			
			#Non-canon
			Aamir Aarak 
			Babrak Bakhshi Baktash Bakhtiyar Barialy Basir	
			Changaze Churak Chaman
			Dastgir
			Enullak Elhash Emal	
			Ghulak
			Hafak 
			Isak Ikhlas Inzir
			Jawak Khalek Jakan
			Laiqshah Lajrak Latik 
			Mahwak
			Naherak
			Partak Parviz 
			Tabish Tahib Temur Taj Tamim Tarakai
			Uzrak
			Vidak
			
			#DAMOC #mostly taken from Khitan names as placeholder
			
			Abo Aerlu'on Aerlu'or An Anba An-in Agdzi Agdzin Ago Agu Asar Auluon 
			Baisbun Baisbur Baisha-an Baishar Belbur Belbun Bian Bi'en Bi'er Bug 
			Daudzil Dauluor Dein Dileqa Dilug Dior Diorin Danir Dile Dilen Diolin Diluguin Dolbur Drola-an Dorhan Dza Dzi'en Dzirug Dzirgu Dzirugun Dzobolin Dzoborin Dzuburug Dzirgung 
			Ebege Ebegin Esbur Esen Esenin 
			Gemin Gileg Gu Guju Guin Guinin 
			Hail Hailin Haru Harun Harubu Harubub Harubun Hede Hedeng Heugu Hua Huldzi 
			Inig Inigin Iri'en Iri'er 
			Kail Kianshinge Kung-in Kung-ur Kudzi 
			Laugu Laugung 
			Ma'an Mosa Moshan Moshar Muru'on Muruor 
			Namsiha-an Narsin Niar Niargu Niargun Niarin Niauraq Niauraqan Niomog Niomgu Niomgun Noilhan Noilhar Nomoshan Noyer Ngai Nghi Ngozi Ngu 
			Odula-an Odu-on Ordelhan Ordu'on Orduor 
			Paudin Paulir Peyen Pod Poden Poder Pul Pulin Purbu 
			Qadzu Qadzung Qang Qan-in Qomol Qutug Qutugin 
			Sabo Sabon Saqadzi Sara Saradin Saulir Sha-ar Shilor Shishial Shishialin Siaugu Sinelger Singen Singer Susaq 
			Tabuyen Tabuye Tabuyer Tang-ur Telid Tshala Tshalan Tshauer Tshaung Tshugu Telin Telgen Teme Temen Temer Tsha Tshaqal Tsheu'er Tshuburur Tshugung Tud Tudin Tudedin Tugusir Turburur 
			Udu'on Udu'or Ugul Ugulin Uldzin Uldzi Ulsi Ulsin Urel Urelbun Urelbur Urilhar Uryen Uryer Uroer Uyen Uyer 
			Yange Yanin Yulgu-r Yuldugu Yuldugun Yundeshi				
		}
		
		female_names = {
			#Canon
			Zhea:500
			
			#Non-canon
			Aatifa Abagull 
			Bahara Bakhtawar
			Chehra
			Daywa Darya
			Ghezal Geeti 
			Ilaaha Inaara 		
			Leepa
			Makai Malalai
			Naazi Nabi Nabila Nadera Nagia
			Obiada Omira Orzala	 	
			Pari Parwana Patra	
			Qais 
			Raihana
			Sabroo
			Xebaida		
			Yalda Yama

			#DAMOC #mostly taken from Khitan names as placeholder
			
			Abuq Aerlu'on An Agdzin Ago Agu Agulin Aidan Alidzu Amqa An'e Anu Arel Au'a Auye Auluon-agu 
			Baisbun Baish Baisha-an Baishan Balhan Be'el Be'en Belbun Bian Bi'en Bugin Burelhan 
			Dali Daqal Dang Daudzi Dei Diamtshan Dile Dilen Dilug Dior Diori Dianir Dorla-an Durin Dorhan Dza Dzaur Dzaugui Dzugu Dzi'en Dzirugun Dzobol Dzobor Dariai 
			El Gem-shi Gileg Gonu Gu Guju Gugu Guinin 
			Hail Hailin Harun Harur Hediro Hio Hua Huri Inigin Irbu Irier 
			Kudzi Kun Kung-ur Kuren 
			Labu Laugung Liaugu Libgu Limir 
			Ma'an Manir Masgu Moro'en Moshan Maen Mirig Moya Mo-sa Muru'on 
			Naqai Namsiha-an Naihad Narsin Niarhari Niaur None Nugur Niamgonen Niauraq Niargun Noilhan Nomoshan Noyen Niomgun Niar Niargung Niomgung Nosilhan Ngai Nge Nga Ngha
			Odula-an Ordelhan Ordu'on 
			Paril Paudin Paudun Paylin Payen Poden Polian Poselgen Poso'en Purbu Purbun 
			Qadzu Qag Qalaqa Qan Qanan Quguden Quru'on Qutug 
			Sabo Sabon Saq Saqadzi Sara Sarad Siaugu Sinelgen Singen Shishial Shugur Shuru'ui Singer Songulhan Suirgen
			Tabuye Tabuyen Tadi Tang Talis Tang-ur Taug Telbe Temeyen Telgen Telid Teme Temen To'o Tsha Tshadeilhan Tsharde Tshong Tsheumuyang Tsheu'en Tshubug Tshudgdzi Tshugu Tshugurel Tianba Tud Tuded Tug Tugusir Tulu'en 
			Un Ugul Urilhan Uryen Uroen Uldzi Urel Uyen 
			Yash Yudgu Yugu Yuldugu Yarel Yeugun Yogun			
		}
		
		character_modifier = {
			#demesne_size = 6
		}
		
		male_patronym = " Son of "
		female_patronym = " Daughter of "
		prefix = yes
		
		#dynasty_title_names = yes
		founder_named_dynasties = yes
		dukes_called_kings = yes
		allow_looting = yes
		
		#from_dynasty_prefix = "of "
		
		pat_grf_name_chance = 25
		mat_grf_name_chance = 25
		father_name_chance = 25
		
		pat_grm_name_chance = 25
		mat_grm_name_chance = 25
		mother_name_chance = 25
		
		modifier = default_culture_modifier
	}
}
nghai_group = {
	graphical_cultures = { bodpagfx } # buildings
	
	nghai = {
		graphical_cultures = { mongolgfx bodpagfx muslimgfx }
		
		color = { 0.775 0.5 0.15 }
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		male_names = {
			#CANON
			#NON-CANON
			
			#DAMOC #mostly taken from Khitan names as placeholder
			
			Abo Aerlu'on Aerlu'or An Anba An-in Agdzi Agdzin Ago Agu Asar Auluon 
			Baisbun Baisbur Baisha-an Baishar Belbur Belbun Bian Bi'en Bi'er Bug 
			Daudzil Dauluor Dein Dileqa Dilug Dior Diorin Danir Dile Dilen Diolin Diluguin Dolbur Drola-an Dorhan Dza Dzi'en Dzirug Dzirgu Dzirugun Dzobolin Dzoborin Dzuburug Dzirgung 
			Ebege Ebegin Esbur Esen Esenin 
			Gemin Gileg Gu Guju Guin Guinin 
			Hail Hailin Haru Harun Harubu Harubub Harubun Hede Hedeng Heugu Hua Huldzi 
			Inig Inigin Iri'en Iri'er 
			Kail Kianshinge Kung-in Kung-ur Kudzi 
			Laugu Laugung 
			Ma'an Mosa Moshan Moshar Muru'on Muruor 
			Namsiha-an Narsin Niar Niargu Niargun Niarin Niauraq Niauraqan Niomog Niomgu Niomgun Noilhan Noilhar Nomoshan Noyer Ngai Nghi Ngozi Ngu 
			Odula-an Odu-on Ordelhan Ordu'on Orduor 
			Paudin Paulir Peyen Pod Poden Poder Pul Pulin Purbu 
			Qadzu Qadzung Qang Qan-in Qomol Qutug Qutugin 
			Sabo Sabon Saqadzi Sara Saradin Saulir Sha-ar Shilor Shishial Shishialin Siaugu Sinelger Singen Singer Susaq 
			Tabuyen Tabuye Tabuyer Tang-ur Telid Tshala Tshalan Tshauer Tshaung Tshugu Telin Telgen Teme Temen Temer Tsha Tshaqal Tsheu'er Tshuburur Tshugung Tud Tudin Tudedin Tugusir Turburur 
			Udu'on Udu'or Ugul Ugulin Uldzin Uldzi Ulsi Ulsin Urel Urelbun Urelbur Urilhar Uryen Uryer Uroer Uyen Uyer 
			Yange Yanin Yulgu-r Yuldugu Yuldugun Yundeshi			
		}
		
		female_names = {
			#CANON
			#NON-CANON

			#DAMOC #mostly taken from Khitan names as placeholder
			
			Abuq Aerlu'on An Agdzin Ago Agu Agulin Aidan Alidzu Amqa An'e Anu Arel Au'a Auye Auluon-agu 
			Baisbun Baish Baisha-an Baishan Balhan Be'el Be'en Belbun Bian Bi'en Bugin Burelhan 
			Dali Daqal Dang Daudzi Dei Diamtshan Dile Dilen Dilug Dior Diori Dianir Dorla-an Durin Dorhan Dza Dzaur Dzaugui Dzugu Dzi'en Dzirugun Dzobol Dzobor Dariai 
			El Gem-shi Gileg Gonu Gu Guju Gugu Guinin 
			Hail Hailin Harun Harur Hediro Hio Hua Huri Inigin Irbu Irier 
			Kudzi Kun Kung-ur Kuren 
			Labu Laugung Liaugu Libgu Limir 
			Ma'an Manir Masgu Moro'en Moshan Maen Mirig Moya Mo-sa Muru'on 
			Naqai Namsiha-an Naihad Narsin Niarhari Niaur None Nugur Niamgonen Niauraq Niargun Noilhan Nomoshan Noyen Niomgun Niar Niargung Niomgung Nosilhan Ngai Nge Nga Ngha
			Odula-an Ordelhan Ordu'on 
			Paril Paudin Paudun Paulin Peyen Poden Polian Poselgen Poso'en Purbu Purbun 
			Qadzu Qag Qalaqa Qan Qanan Quguden Quru'on Qutug 
			Sabo Sabon Saq Saqadzi Sara Sarad Siaugu Sinelgen Singen Shishial Shugur Shuru'ui Singer Songulhan Suirgen
			Tabuye Tabuyen Tadi Tang Talis Tang-ur Taug Telbe Temeyen Telgen Telid Teme Temen To'o Tsha Tshadeilhan Tsharde Tshong Tsheumuyang Tsheu'en Tshubug Tshudgdzi Tshugu Tshugurel Tianba Tud Tuded Tug Tugusir Tulu'en 
			Un Ugul Urilhan Uryen Uroen Uldzi Urel Uyen 
			Yash Yudgu Yugu Yuldugu Yarel Yeugun Yogun					
		}
		
		#from_dynasty_prefix = "of "
		
		pat_grf_name_chance = 25
		mat_grf_name_chance = 25
		father_name_chance = 25
		
		pat_grm_name_chance = 25
		mat_grm_name_chance = 25
		mother_name_chance = 25
		
		modifier = default_culture_modifier
	}
}	
islander_group = {
	graphical_cultures = { norsegfx } # buildings
	
	islander = {
		graphical_cultures = { islandergfx }

		used_for_random = no
		alternate_start = { 
			OR = {
				has_alternate_start_parameter = { key = special_culture value = mythological } 
				has_alternate_start_parameter = { key = special_culture value = all } 
			}	
		}
		
		color = { 0.45 0.45 0.55 }
		
		male_names = {
			Togg Bodger Brogg
			Caloth Cull Clargg Colosh
			Doth Drogg Durr Draloth			Erroth Eleogg			Galt Groo Gurn
			Haggon Holger Howd
			Jogg Joth Jarr
			Krull			Morr Moggoth			Quort
			Slugg Sogg Sogoloth
			Tumosh Tugg Torr Teleoth
			Ulf Umar Ygon
			
		}
		female_names = {
			Assadora Brella Brea Bellegere Betharios
			Cossoma Denia
			Ferny Frynne
			Gylenios Grisel Gylora
			Izembara Harra Jaqea
			Mellario Lanna Meralyia Mera Mordea			Rogga Rowan
			Yorka Yna Umma
			Ynys Tagganaria Torma
			Zia
		}
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		modifier = backward_culture_modifier
	}
}