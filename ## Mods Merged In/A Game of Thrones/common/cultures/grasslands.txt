dothraki_group = {
	graphical_cultures = { mongolgfx  }# portraits, units

	dothraki = {
		graphical_cultures = { dothrakigfx } # buildings, portraits
		#horde = yes
		
		color = { 0.775 0.5 0.15 }
		
		nomadic_in_alt_start = yes
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		male_names = {
			# Canon Names		 
			Aggo Bharbo Caggo Cohollo Drogo Dhako Fogo 
			Horro Haro Haggo Iggo
			Jhaqo Jhogo Jommo Kovarro Loso
			Mago Moro Motho Mengo
			Ogo Onqo Pono Qotho Quaro Qano
			Rakharo Rhogoro Rommo Rogo Scoro
			Temmo Vaggoro Zekko Zollo Zhako Zeggo
			
			# DAMOC - MORE MALE NAMES
			
			Kaffo Villo Crozozzo Qozzo Carro Shiggo Lajatto Emmatto Arrisso Acchakko Krommo Rhozzo Covo Qrakko Rakko
			Kozatho Dazho Khazzo Gorro Brabo Lajo Addro Matto Boggo Cozammo Collo Kogo Maggo Khobbo Darro Raggo Zommo
			Bommo Qhaqo Dhaqo Khaqo Bhaqo Zhaqo Lhaqo Mhaqo Chaqo Ahaqo Nhaqo Shaqo Mono Rono Mogo Zono Zogo Mollo
			Bollo Dollo Lollo Nollo Kollo Sollo Dono Diggo Dozzo Doggo Dhoqo Bhoqo Dhoqo Zhoqo Mhoqo Rhoqo
			
			# Non-canon
			Azho Arakho 
			Fono Fesho
			Hezzo
			Jarbo Jasso
			Kattaro
			Lanno
			Malakho Marro Mosko
			Najaho
			Rovo Remekko
			Tihho Tokko
			Virsallo
			Zerqollo Zirqo
		}
		
		female_names = {
			# Canon Names
			Doshi Irri Jhiqui
			
			# There aren't exactly a lot, so the following are feminine versions of male Dothraki names
			Ayi Aggi Coholli Chakki 
			Dalli Drigi Dhaki
			Esinni Fitti
			Hirri Hari Havvi Iggi Izzi
			Jhigi Jhogi Jolinni Kovarri Kendri
			Losi Leqsi Lavakhi
			Miri Mothi Malakhi Mengi Naqqi
			Oqetti Onqi
			Qothi Quiri Qovatti
			Rakhiri Rhogiri Rimmi Rogi Scori
			Temmi Thirri
			Vorsakhi
			Zikki Zolli Zhaki Zichi Ziganni
			
			Isili Tezhimi Thiqui Sihi Lihezhi Zirri Allayi Zichomi Leqsi Filki Layi Verri Mavi Eshinni Thirli Feldi Davri Rakki Valeqi Ashini
			Qizhanni Qissabi Qossi Ovahi Shasi Vizi Kessi Jeshi Jeddi Achri Nishavi Dalfi Filli Ziganni Zirqi Izzatti Risemi Essi Immissi Aheni Tehhi Zhavvi
		}
		
		character_modifier = {
			#demesne_size = 6
		}
		
		male_patronym = " Son of "
		female_patronym = " Daughter of "
		prefix = yes
		
		#dynasty_title_names = yes
		founder_named_dynasties = yes
		dukes_called_kings = yes
		
		#from_dynasty_prefix = "of "
		
		pat_grf_name_chance = 25
		mat_grf_name_chance = 25
		father_name_chance = 25
		
		pat_grm_name_chance = 25
		mat_grm_name_chance = 25
		mother_name_chance = 25
		
		modifier = default_culture_modifier
	}

	lhazareen = {
		graphical_cultures = { lhazareengfx } # buildings, portraits
		#horde = yes
		
		color = { 0.675 0.675 0 }
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		male_names = {
			# Canon Names
			
			#NON-CANON NAMES
			Celgil Aba Baitzas Balgor Batan Batas Bator Bayca Bickili Bizel Boga Boru Bota Bula Chalis Giazis Ildey Ipa Ipaos Ipoch Kabuksin Kajdan Karaca Kaydum Kazar Kegen Konel  
			Korkutan Kortan Kostas Kourkoutai Kuchuk Kuerci Kugel Kure Metigay Osul Qorqutay Sagay Sethlav Sol Suru Tatus Teber Temir Tiraq Tzegul Yavdi Yazi Yeke
			Alp Araslan Arslan Bagha Baghatur Balgici Barjik Barsbek Bastu Bashtwa Bihor Bori Bugha Bulan Bulcan Busir Buzer Catan Corpan Ilik Iltabbr Itakh Kamaj Karadakh Kayghalagh Khatir Khuterkin
			Kibar Kisa Kundaciq Kundac Manar Manas Menumarót Otemis Papacyz Samsam Sartac Simsam Tabar Tabghac Tarkhan Tarmac Tuzniq Vakrim Yabghu Yavantey Yencepi Yerneslu Yectirek Yilig
			Belet Ezra Hanukkah Hezekiah Manasseh Nisi Yavdi Zebulun	
		}			
		
		female_names = {
			#Canon
			Eroeh
			
			#NON-CANON NAMES
			Cica Cece Cecki Cectilet Cilena Aneseh Ziggi Ozzi Akgul Asli Ayasun Ayten Bozcin Dileki Esini Gulca Gulaya Gundoeh Gundeh Gunesa Ikalaya Ilkaya Ipeka Ipekeh Irgehi 
			Karacik Katyarikka Keenissa Kelyamal Khatun Larkka Lyukha Mala Maturkka Mutlu Nacoeh Parsbiti Patiyera Paykelti Peksen Puyantay Samureh Sarantay Sareh Sarica Sarikeli
			Sati Savacka Savki Savtilek Savilyi Savintiki Shurkka Sibela Sirini Sirmeh Songul Tahtani Tekce Tura Umay Usunbike Yasar Yeldem Yildizeh Yartilek Xatti 
			Cice Cilen Cilta Ozgula Ozlem Akgul Asli Ayasun Ayten Bozcin Dilek Esin Gulcicek Gulayi Karoeh Mala Mutlu Pekseni Samuri Sarica Sati Sevilayi Sevindiki Sibeli 
			Sirmi Songula Tekeh Tura Umaya Usunbiki Yasari Yeldem Yildizi			
		}
		
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 30
		mat_grf_name_chance = 10
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
}
sarnor_group = {
	graphical_cultures = { indiangfx } # buildings
	
	sarnorian = { 
		graphical_cultures = { sarnorigfx } # portraits
		
		color = { 0.72 0.8 0.2 }
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		male_names = { #Gujarati influence
			#canon
			Huzhor:400 Mazor:400
			
			
			Amritlal Avilash Atharvan
			Balavant Burhan Bhagesh
			Chitresh Chanak
			Dinesh Darshan Dahak
			Etash Ekavir
			Fanish
			Ganesh Gayan Girivar
			Harshil Harith Harihar
			Ishan Ishwar
			Jash Jaafar
			Kushal Kalpesh Kedaar
			Lokesh Lakshan
			Manish Manaar Mukesh
			Nitish Nayan Nishad
			Onkar
			Palash Paarth Parvesh
			Ritesh Rathin Ranak
			Sagar Sahat Shyam Sahasrad
			Tarakesh Taresh
			Uttamesh Utkarsh
			Vyomesh Vishruth
			Yogesh Yathavan
		}
		female_names = {
			Ashna Anhithi Ameena Aryana
			Bidisha Barsha
			Chandira Charita
			Dhanashri Darshita Diya
			Eesha
			Faina
			Gomathi Girisha Gayathri
			Hansha Havya Heera
			Ishanya Iksha
			Jalbala Jithya Jisha
			Kirthana Kanisha Kaira
			Lalasa Lekisha
			Malashree Monisha Misa Mahiya
			Nithya Nishi Nalika
			Oliyarasi
			Plaksha Priya
			Rushada Ruthvika Rashmi
			Suvitha Saira Sitara Shashi
			Tannistha Treya Tanisha
			Urishilla Usha
			Vishva Vihana
			Yashika Yaswitha
			Zaara
		}
		
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
	
	omberi = { 
		graphical_cultures = { sarnorigfx } # portraits
		
		color = { 0.5 0.9 0.35 }
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		male_names = { #Gujarati influence		
			Amritlal Avilash Atharvan
			Balavant Burhan Bhagesh
			Chitresh Chanak
			Dinesh Darshan Dahak
			Etash Ekavir
			Fanish
			Ganesh Gayan Girivar
			Harshil Harith Harihar
			Ishan Ishwar
			Jash Jaafar
			Kushal Kalpesh Kedaar
			Lokesh Lakshan
			Manish Manaar Mukesh
			Nitish Nayan Nishad
			Onkar
			Palash Paarth Parvesh
			Ritesh Rathin Ranak
			Sagar Sahat Shyam Sahasrad
			Tarakesh Taresh
			Uttamesh Utkarsh
			Vyomesh Vishruth
			Yogesh Yathavan
		}
		female_names = {
			Ashna Anhithi Ameena Aryana
			Bidisha Barsha
			Chandira Charita
			Dhanashri Darshita Diya
			Eesha
			Faina
			Gomathi Girisha Gayathri
			Hansha Havya Heera
			Ishanya Iksha
			Jalbala Jithya Jisha
			Kirthana Kanisha Kaira
			Lalasa Lekisha
			Malashree Monisha Misa Mahiya
			Nithya Nishi Nalika
			Oliyarasi
			Plaksha Priya
			Rushada Ruthvika Rashmi
			Suvitha Saira Sitara Shashi
			Tannistha Treya Tanisha
			Urishilla Usha
			Vishva Vihana
			Yashika Yaswitha
			Zaara
		}
		
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
}