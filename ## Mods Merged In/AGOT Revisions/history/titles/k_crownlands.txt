# unclaimed ingame
900.1.1={
	law = succ_primogeniture
	law = cognatic_succession
	law = investiture_law_2
	law = first_night_1
	law = centralization_1
	law = slavery_0
	effect = { set_variable = { which = "lord_paramount_status" value = 1 }}
	de_jure_liege = e_crownlands
	name = BLACKWATER
}
7998.1.2={
	de_jure_liege = e_iron_throne
	reset_name = yes
}
8048.3.7={
	law = investiture_law_1 
}
8060.1.1={ law = first_night_0 } #First Night abolished