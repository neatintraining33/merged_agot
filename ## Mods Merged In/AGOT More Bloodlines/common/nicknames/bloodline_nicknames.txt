#############################################
# CHARACTER NICKNAMES
# Independent dukes, kings and emperors only
#
# 'factor' is the percentage per year of 
# gaining the nickname
#
#############################################

# TRIGGERED ONLY
################

nick_wolfsbane = {}
nick_greatwarg = {}
nick_beasttamer = {}
nick_manyskinned = {}
nick_king_of_the_woods = {}
nick_husband_to_bears = {}
nick_the_raventree = {}
nick_the_oakenseat_bl = {}
nick_the_palanquin = {}
nick_the_bloodstone_bl = {}
nick_the_eunuch_emperor_1 = {}
nick_the_eunuch_emperor_2 = {}
nick_the_eunuch_emperor_3 = {}
nick_the_eunuch_emperor_4 = {}
nick_the_eunuch_emperor_5 = {}
nick_the_eunuch_emperor_6 = {}
nick_the_eunuch_emperor_7 = {}
nick_the_eunuch_emperor_8 = {}
nick_the_scourge_emperor_1 = {}
nick_the_scourge_emperor_2 = {}
nick_the_scourge_emperor_3 = {}
nick_the_scourge_emperor_4 = {}
nick_the_shield_emperor_1 = {}
nick_the_shield_emperor_2 = {}
nick_the_shield_emperor_3 = {}
nick_the_shield_emperor_4 = {}
nick_the_scion_emperor_1 = {}
nick_the_scion_emperor_2 = {}
nick_the_scion_emperor_3 = {}
nick_the_scion_emperor_4 = {}
nick_the_sorceror_emperor_1 = {}
nick_the_sorceror_emperor_2 = {}
nick_the_sorceror_emperor_3 = {}
nick_the_sorceror_emperor_4 = {}
nick_the_peasant_emperor_1 = {}
nick_the_peasant_emperor_2 = {}
nick_the_peasant_emperor_3 = {}
nick_the_peasant_emperor_4 = {}
nick_the_bureaucrat_emperor_1 = {}
nick_the_bureaucrat_emperor_2 = {}
nick_the_bureaucrat_emperor_3 = {}
nick_the_bureaucrat_emperor_4 = {}
nick_the_diplomat_emperor_1 = {}
nick_the_diplomat_emperor_2 = {}
nick_the_diplomat_emperor_3 = {}
nick_the_diplomat_emperor_4 = {}
nick_the_living_god_emperor_1 = {}
nick_the_living_god_emperor_2 = {}
nick_the_living_god_emperor_3 = {}
nick_the_living_god_emperor_4 = {}
nick_lion_tamer = {}
nick_hrakkar_tamer = {}
nick_whale_tamer = {}
nick_mammoth_tamer = {}
nick_unicorn_tamer = {}
nick_barrow_wolf = {}
nick_barstark = {}
nick_harbour_wolf = {}
nick_harstark = {}
nick_warg_king = {}
nick_warg_prince = {}
nick_the_green_king = {}
nick_the_serpent_in_scarlet = {}
nick_bastard_of_location = {}
nick_of_the_bear_of_location = {}
nick_the_young_bear = {}
nick_the_bastard_bear = {}
nick_of_the_raven_of_location = {}
nick_the_young_raven = {}
nick_the_bastard_raven = {}
nick_the_cunning_raven = {}
nick_of_the_direwolf_of_location = {}
nick_the_bastard_wolf = {}
nick_the_cunning_wolf = {}
nick_of_the_lizardlion_of_location = {}
nick_the_young_lizardlion = {}
nick_the_bastard_lizardlion = {}
nick_the_cunning_lizardlion = {}
nick_the_young_lion = {}
nick_the_bastard_lion = {}
nick_the_cunning_lion = {}





nick_mbs_the_harper = {}
nick_mbs_the_lutist = {}
nick_mbs_the_singer = {}
nick_mbs_the_silvertongue = {}
nick_mbs_the_goldentongue = {}
nick_mbs_the_sweettongue = {}
nick_mbs_the_sweetvoice = {}
nick_mbs_the_purevoice = {}
nick_mbs_the_goldenvoice = {}
nick_mbs_the_silvervoice = {}
nick_mbs_the_wanderingbard = {}
nick_mbs_the_bluebard = {}
nick_mbs_the_redbard = {}
nick_mbs_the_goldbard = {}
nick_mbs_the_greenbard = {}
nick_mbs_the_silverbard = {}
nick_mbs_the_blackbard = {}
nick_mbs_the_orangebard = {}
nick_mbs_the_rhymer = {}
nick_mbs_the_strummer = {}
nick_mbs_the_melodious = {}
nick_mbs_the_nightingale = {}
nick_mbs_the_lark = {}
nick_mbs_the_fairfinger = {}
nick_mbs_the_swiftfinger = {}
nick_mbs_bones = {}