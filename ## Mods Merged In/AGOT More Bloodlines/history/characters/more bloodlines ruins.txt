#c_skane
1893001 = {
    name = " "
   
    religion="ruin_rel"
    culture="ruin" 
   
    6000.1.1 = {
        birth="6000.1.1"
        effect = {
            if = {
                limit = { is_ruler = no }
                death = yes
            }  
            if = {
                limit = { is_ruler = yes }
                add_trait = ruin
                set_defacto_liege = THIS
                c_skane = {
                    location = {
                        set_province_flag = ct_colony_2
                        set_province_flag = ruined_province
                    }
                }
                character_event = { id = unoccupied.101 }  
                create_character = {
                    name = ""
                    religion="ruin_rel"
                    culture="ruin" 
                    dynasty=none
                    attributes = {
                        martial = 0
                        diplomacy = 0
                        intrigue = 0
                        stewardship = 0
                        learning = 0
                    }
                    trait = ruin
                }
                new_character = {
                    b_skane = { gain_title = PREV }
                }
            }
        }
    }
}
#c_hoare_castle
1893005 = {
    name = " "
   
    religion="ruin_rel"
    culture="ruin" 
   
    7623.1.1 = {
        birth="7623.1.1"
        effect = {
            if = {
                limit = { is_ruler = no }
                death = yes
            }  
            if = {
                limit = { is_ruler = yes }
                add_trait = ruin
                set_defacto_liege = THIS
                c_hoare_castle = {
                    location = {
                        set_province_flag = ct_colony_2
                        set_province_flag = ruined_province
                    }
                }
                character_event = { id = unoccupied.101 }  
                create_character = {
                    name = ""
                    religion="ruin_rel"
                    culture="ruin" 
                    dynasty=none
                    attributes = {
                        martial = 0
                        diplomacy = 0
                        intrigue = 0
                        stewardship = 0
                        learning = 0
                    }
                    trait = ruin
                }
                new_character = {
                    b_hoareton_mbs = { gain_title = PREV }
                }
                create_character = {
                    name = ""
                    religion="ruin_rel"
                    culture="ruin" 
                    dynasty=none
                    attributes = {
                        martial = 0
                        diplomacy = 0
                        intrigue = 0
                        stewardship = 0
                        learning = 0
                    }
                    trait = ruin
                }
                new_character = {
                    b_hoaregrave_mbs = { gain_title = PREV }
                }
            }
        }
    }
}
#c_morne
1893009 = {
    name = " "
   
    religion="ruin_rel"
    culture="ruin" 
   
    6000.1.1 = {
        birth="6000.1.1"
        effect = {
            if = {
                limit = { is_ruler = no }
                death = yes
            }  
            if = {
                limit = { is_ruler = yes }
                add_trait = ruin
                set_defacto_liege = THIS
                c_morne = {
                    location = {
                        set_province_flag = ct_colony_2
                        set_province_flag = ruined_province
                    }
                }
                character_event = { id = unoccupied.101 }  
                create_character = {
                    name = ""
                    religion="ruin_rel"
                    culture="ruin" 
                    dynasty=none
                    attributes = {
                        martial = 0
                        diplomacy = 0
                        intrigue = 0
                        stewardship = 0
                        learning = 0
                    }
                    trait = ruin
                }
                new_character = {
                    b_morne_sept_mbs = { gain_title = PREV }
                }
                create_character = {
                    name = ""
                    religion="ruin_rel"
                    culture="ruin" 
                    dynasty=none
                    attributes = {
                        martial = 0
                        diplomacy = 0
                        intrigue = 0
                        stewardship = 0
                        learning = 0
                    }
                    trait = ruin
                }
                new_character = {
                    b_morne_town_mbs = { gain_title = PREV }
                }
            }
        }
    }
}
#c_westwatch_mbs
1893010 = {
    name = " "
   
    religion="ruin_rel"
    culture="ruin" 
   
    8050.1.1 = {
        birth="8050.1.1"
        effect = {
            if = {
                limit = { is_ruler = no }
                death = yes
            }  
            if = {
                limit = { is_ruler = yes }
                add_trait = ruin
                set_defacto_liege = THIS
                c_westwatch_mbs = {
                    location = {
                        set_province_flag = ct_colony_2
                        set_province_flag = ruined_province
                    }
                }
                character_event = { id = unoccupied.101 }  
            }
        }
    }
}
#c_river_of_morn
1893011 = {
    name = " "
   
    religion="ruin_rel"
    culture="ruin" 
   
    6000.1.1 = {
        birth="6000.1.1"
        effect = {
            if = {
                limit = { is_ruler = no }
                death = yes
            }  
            if = {
                limit = { is_ruler = yes }
                add_trait = ruin
                set_defacto_liege = THIS
                c_river_of_morn = {
                    location = {
                        set_province_flag = ct_colony_2
                        set_province_flag = ruined_province
                    }
                }
                character_event = { id = unoccupied.101 }  
                create_character = {
                    name = ""
                    religion="ruin_rel"
                    culture="ruin" 
                    dynasty=none
                    attributes = {
                        martial = 0
                        diplomacy = 0
                        intrigue = 0
                        stewardship = 0
                        learning = 0
                    }
                    trait = ruin
                }
                new_character = {
                    b_river_of_morn_temple = { gain_title = PREV }
                }
            }
        }
    }
}
#c_hidden_pass
1893012 = {
    name = " "
   
    religion="ruin_rel"
    culture="ruin" 
   
    6000.1.1 = {
        birth="6000.1.1"
        effect = {
            if = {
                limit = { is_ruler = no }
                death = yes
            }  
            if = {
                limit = { is_ruler = yes }
                add_trait = ruin
                set_defacto_liege = THIS
                c_hidden_pass = {
                    location = {
                        set_province_flag = ct_colony_2
                        set_province_flag = ruined_province
                    }
                }
                character_event = { id = unoccupied.101 }  
                create_character = {
                    name = ""
                    religion="ruin_rel"
                    culture="ruin" 
                    dynasty=none
                    attributes = {
                        martial = 0
                        diplomacy = 0
                        intrigue = 0
                        stewardship = 0
                        learning = 0
                    }
                    trait = ruin
                }
                new_character = {
                    b_hidden_pass_temple = { gain_title = PREV }
                }
            }
        }
    }
}