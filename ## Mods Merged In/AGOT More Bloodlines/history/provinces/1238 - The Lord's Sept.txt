

# County Title
title = c_mbs_lords_sept

# Settlements
max_settlements = 1
b_lords_sept = temple

# Misc
culture = old_first_man
religion = old_gods

# History
1.1.1 = {
	
	b_lords_sept = tp_monastery_1

}
6700.1.1 = {
	culture = reachman
	religion = the_seven
}