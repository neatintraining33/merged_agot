
# County Title
title = c_mbs_holy_house

# Settlements
max_settlements = 1
b_mbs_holy_house = temple

# Misc
culture = old_first_man
religion = old_gods


# History

1.1.1 = {

	b_mbs_holy_house = tp_monastery_1
}
6700.1.1 = {
	culture = riverlander
	religion = the_seven
}