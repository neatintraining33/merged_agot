# 88 - Blackrush

# County Title
title = c_blackrush

# Settlements
max_settlements = 4
b_halterfalls = castle
b_shawneytown = city
b_riverhalt = city

# Misc
culture = old_first_man
religion = old_gods

# History


1.1.1 = {

b_halterfalls = ca_asoiaf_river_basevalue_1
b_halterfalls = ca_asoiaf_river_basevalue_2


b_shawneytown = ct_asoiaf_river_basevalue_1
b_shawneytown = ct_asoiaf_river_basevalue_2
b_shawneytown = ct_asoiaf_river_basevalue_3

b_riverhalt = ct_asoiaf_river_basevalue_1
b_riverhalt = ct_asoiaf_river_basevalue_2


}

6700.1.1 = {
	culture = riverlander
}