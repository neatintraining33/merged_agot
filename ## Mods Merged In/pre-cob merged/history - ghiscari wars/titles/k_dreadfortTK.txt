1.1.1={
	law = succ_primogeniture
	law = cognatic_succession
	effect = {
		holder_scope = {
			set_special_character_title = dreadking_male
		}	
	}
}


2901.1.1 = { holder=60087 } # Ralton (nc)
2926.1.1 = { holder=60187 } # Rodrick (nc)
2938.1.1 = { holder=60287 } # Roose (nc)
2960.1.1 = { holder=60387 } # Beren (nc)
2962.1.1 = { holder=60487 } # Donnis (nc)
2969.1.1 = { holder=60587 } # Durin (nc)

2978.1.1={
	holder = 13087
}

3008.1.1={
	holder = 12087
}
3046.1.1={
	holder = 11087
}
3069.1.1={
	holder = 10087
}
3095.1.1={
	holder = 9087
}
3139.1.1={
	holder = 8087
}
3154.1.1={
	holder = 7087
}
3170.1.1={
	holder = 6087
}
3201.1.1={
	holder = 5087
}
3245.1.1={
	holder = 4087
}
