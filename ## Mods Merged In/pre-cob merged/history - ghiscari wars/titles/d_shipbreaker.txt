20.1.1 = {
	holder = 41544 #Durran Godsgrief
	liege="e_stormlands"
	law = succ_primogeniture
}
90.1.1 = { holder = 1501544 } #Durran II Durrandon
122.1.1 = { holder = 1511544 } #Durran III Durrandon (NC)
149.1.1 = { holder = 1521544 } #Durran IV Durrandon
162.1.1 = { holder = 1531544 } #Durran V Durrandon
189.1.1 = { holder = 1541544 } #Durran VI Durrandon
219.1.1 = { holder = 1551544 } #Erich I Durrandon (NC)
230.1.1 = { holder = 1561544 } #Durran VII Durrandon
282.1.1 = { holder = 0 }

2874.1.1 = { holder=10001544 } # Durhahn (nc)
2895.1.1 = { holder=10011544 } # Harlan (nc)
2933.1.1 = { holder=10041544 } # Donnel (nc)
2966.1.1 = { holder=10061544 } # Durhahn (nc)
2973.1.1 = { holder=10081544 } # Richard (nc)
3012.1.1 = { holder=10091544 } # Stannis (nc)

3050.1.1 = { holder = 1591544 } #Durran VIII Durrandon
3078.1.1 = { holder = 1601544 } #Erich II Durrandon (NC)
3090.1.1 = { holder = 1611544 } #Erich III Durrandon
3126.1.1 = { holder = 1621544 } #Maldon I Durrandon (NC)
3145.1.1 = { holder = 1631544 } #Maldon II Durrandon (NC)
3156.1.1 = { holder = 1641544 } #Ormund I Durrandon (NC)
3198.1.1 = { holder = 1651544 } #Durran IX Durrandon (NC)
3205.1.1 = { holder = 1661544 } #Morden I Durrandon (NC)
3238.1.1 = { holder = 1671544 } #Erich IV Durrandon (NC)
3249.1.1 = { holder = 1681544 } #Durran X Durrandon
3275.1.1 = { holder = 1691544 } #Monfryd I Durrandon
3299.1.1 = { holder = 1701544 } #Durran XI Durrandon
3331.1.1 = { holder = 1711544 } #Barron  Durrandon
3365.1.1 = { holder = 1721544 } #Monfryd II Durrandon (NC)
3380.1.1 = { holder = 1731544 } #Durran XII Durrandon (NC)
