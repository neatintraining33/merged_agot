#NOTE: The holder of this title has the House Dondarrion dynasty tracker set in roberts_rebellion_events
1.1.1 = { liege="e_stormlands" }

2994.1.1 = { holder = 300303 } # Alaric Dondarrion (nc)

3006.1.1 = { holder = 301303 } # Donnel Dondarrion (nc)
3040.1.1 = { holder = 302303 } # Harbert Dondarrion (nc)
3069.1.1 = { holder = 303303 } # Galladon Dondarrion (nc)
3086.1.1 = { holder = 304303 } # Maric Dondarrion (nc)
3107.1.1 = { holder = 306303 } # Rickard Dondarrion (nc)
3160.1.1 = { holder = 307303 } # Devan Dondarrion (nc)
3176.1.1 = { holder = 200303 } # Lyonel Dondarrion (C)
3212.1.1 = { holder = 201303 } # Manfred Dondarrion (C)
3241.1.1 = { holder = 310303 } # Gulian Dondarrion (nc)
3271.1.1 = { holder = 303 } # Edric Dondarrion (nc)
