1.1.1={ 
	liege="c_meereen"
	effect = {
		set_title_flag = military_command
		holder_scope = {
			if = {
				limit = { primary_title = { title = PREVPREV } }
				set_government_type = military_command_government_city 
				PREV = { succession = appointment }
				recalc_succession = yes
			}
		}
	}
}
3245.1.1 = { holder=101360014 }
3300.1.1 = { holder=102360014 }
