1.1.1={
	liege="d_mandervale"
	name = c_dunstonbury
	effect = {
		if = {
			limit = { NOT = { year = 8196 } }
			location = { set_name = c_dunstonbury } # Manderly Castle before exile
		}	
	}
}

2840.1.1 = { holder=660080 } # Wyman (nc)
2860.1.1 = { holder=670080 } # Borian (nc)
2897.1.1 = { holder=680080 } # Torrhen (nc)
2928.1.1 = { holder=60080 } # Emmett (nc)
2937.1.1 = { holder=60180 } # Wylan (nc)
2961.1.1 = { holder=60280 } # Elman (nc)

2965.1.1={
	holder = 20080
}
3004.1.1={
	holder = 19080
}
3045.1.1={
	holder = 17080
}
3058.1.1={
	holder = 16080
}
3108.1.1={
	holder = 15080 # Heward
}
3116.1.1={
	holder = 13080 # Desmond
}
3132.2.2={
	holder = 1015080 # Torrhen
}
3139.2.8={
	holder = 12080 # Borion
}
3172.1.1={
	holder = 11080 # Waymar
}
3222.1.1={
	holder = 10080 # Wayn
}
3232.1.1={
	holder = 1034080
}
3251.1.1={
	holder = 1004080
}






