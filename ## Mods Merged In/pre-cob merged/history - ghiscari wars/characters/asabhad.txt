#####################  Asabhad  #################################

###  Kataan
10055942 = {
	name="Wahab"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	2962.1.1 = {birth="2962.1.1"}
	2978.1.1 = {add_trait=poor_warrior}
	2980.1.1 = {add_spouse=40055942}
	3026.1.1 = {death="3026.1.1"}
}
40055942 = {
	name="Reshawna"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	2962.1.1 = {birth="2962.1.1"}
	3026.1.1 = {death="3026.1.1"}
}
10155942 = {
	name="Paymaneh"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10055942
	mother=40055942

	2990.1.1 = {birth="2990.1.1"}
	3050.1.1 = {death="3050.1.1"}
}
10255942 = {
	name="Sahba"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10055942
	mother=40055942

	2991.1.1 = {birth="2991.1.1"}
	2996.1.1 = {death = {death_reason = death_accident}}
}
10355942 = {
	name="Jabir"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10055942
	mother=40055942

	2993.1.1 = {birth="2993.1.1"}
	3009.1.1 = {add_trait=poor_warrior}
	3011.1.1 = {add_spouse=40355942}
	3052.1.1 = {death="3052.1.1"}
}
40355942 = {
	name="Hanifa"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	2993.1.1 = {birth="2993.1.1"}
	3052.1.1 = {death="3052.1.1"}
}
10455942 = {
	name="Ghalib"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10355942
	mother=40355942

	3014.1.1 = {birth="3014.1.1"}
	3030.1.1 = {add_trait=skilled_warrior}
	3032.1.1 = {add_spouse=40455942}
	3067.1.1 = {death="3067.1.1"}
}
40455942 = {
	name="Paymaneh"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3014.1.1 = {birth="3014.1.1"}
	3067.1.1 = {death="3067.1.1"}
}
10555942 = {
	name="Sheeftah"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10355942
	mother=40355942

	3015.1.1 = {birth="3015.1.1"}
	3067.1.1 = {death="3067.1.1"}
}
10655942 = {
	name="Yagana"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10355942
	mother=40355942

	3016.1.1 = {birth="3016.1.1"}
	3018.1.1 = {death="3018.1.1"}
}
10755942 = {
	name="Simin"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10455942
	mother=40455942

	3040.1.1 = {birth="3040.1.1"}
	3094.1.1 = {death="3094.1.1"}
}
10855942 = {
	name="Ramadan"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10455942
	mother=40455942

	3041.1.1 = {birth="3041.1.1"}
	3057.1.1 = {add_trait=poor_warrior}
	3059.1.1 = {add_spouse=40855942}
	3094.1.1 = {death="3094.1.1"}
}
40855942 = {
	name="Sholah"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3041.1.1 = {birth="3041.1.1"}
	3094.1.1 = {death="3094.1.1"}
}
10955942 = {
	name="Samira"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10455942
	mother=40455942

	3043.1.1 = {birth="3043.1.1"}
	3120.1.1 = {death="3120.1.1"}
}
11055942 = {
	name="Gafur"	# not a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10455942
	mother=40455942

	3046.1.1 = {birth="3046.1.1"}
	3062.1.1 = {add_trait=skilled_warrior}
	3078.1.1 = {death="3078.1.1"}
}
11155942 = {
	name="Wahab"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10855942
	mother=40855942

	3060.1.1 = {birth="3060.1.1"}
	3076.1.1 = {add_trait=poor_warrior}
	3078.1.1 = {add_spouse=41155942}
	3101.1.1 = {death="3101.1.1"}
}
41155942 = {
	name="Jahaira"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3060.1.1 = {birth="3060.1.1"}
	3101.1.1 = {death="3101.1.1"}
}
11255942 = {
	name="Fadl"	# not a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10855942
	mother=40855942

	3061.1.1 = {birth="3061.1.1"}
	3077.1.1 = {add_trait=skilled_warrior}
	3120.1.1 = {death="3120.1.1"}
}
11355942 = {
	name="Burhanaddin"	# not a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10855942
	mother=40855942

	3064.1.1 = {birth="3064.1.1"}
	3080.1.1 = {add_trait=poor_warrior}
	3100.1.1 = {death="3100.1.1"}
}
11455942 = {
	name="Qawurd"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11155942
	mother=41155942

	3083.1.1 = {birth="3083.1.1"}
	3099.1.1 = {add_trait=trained_warrior}
	3101.1.1 = {add_spouse=41455942}
	3128.1.1 = {death="3128.1.1"}
}
41455942 = {
	name="Shararah"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3083.1.1 = {birth="3083.1.1"}
	3128.1.1 = {death="3128.1.1"}
}
11555942 = {
	name="Fadl"	# not a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11155942
	mother=41155942

	3084.1.1 = {birth="3084.1.1"}
	3100.1.1 = {add_trait=poor_warrior}
	3135.1.1 = {death="3135.1.1"}
}
11655942 = {
	name="Hafiz"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11455942
	mother=41455942

	3110.1.1 = {birth="3110.1.1"}
	3126.1.1 = {add_trait=trained_warrior}
	3128.1.1 = {add_spouse=41655942}
	3159.1.1 = {death="3159.1.1"}
}
41655942 = {
	name="Nyawela"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3110.1.1 = {birth="3110.1.1"}
	3159.1.1 = {death="3159.1.1"}
}
11755942 = {
	name="Simin"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11455942
	mother=41455942

	3112.1.1 = {birth="3112.1.1"}
	3166.1.1 = {death="3166.1.1"}
}
11855942 = {
	name="Shahzadah"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11455942
	mother=41455942

	3113.1.1 = {birth="3113.1.1"}
	3170.1.1 = {death="3170.1.1"}
}
11955942 = {
	name="Hanifa"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11455942
	mother=41455942

	3114.1.1 = {birth="3114.1.1"}
	3159.1.1 = {death="3159.1.1"}
}
12055942 = {
	name="Nizam"	# not a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11455942
	mother=41455942

	3116.1.1 = {birth="3116.1.1"}
	3132.1.1 = {add_trait=poor_warrior}
	3172.1.1 = {death="3172.1.1"}
}
12155942 = {
	name="Asiya"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11655942
	mother=41655942

	3135.1.1 = {birth="3135.1.1"}
	3189.1.1 = {death = {death_reason = death_accident}}
}
12255942 = {
	name="Abdullah"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11655942
	mother=41655942

	3137.1.1 = {birth="3137.1.1"}
	3153.1.1 = {add_trait=trained_warrior}
	3155.1.1 = {add_spouse=42255942}
	3190.1.1 = {death="3190.1.1"}
}
42255942 = {
	name="Zaynab"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3137.1.1 = {birth="3137.1.1"}
	3190.1.1 = {death="3190.1.1"}
}
12355942 = {
	name="Aghlab"	# not a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11655942
	mother=41655942

	3139.1.1 = {birth="3139.1.1"}
	3155.1.1 = {add_trait=trained_warrior}
	3201.1.1 = {death="3201.1.1"}
}
12455942 = {
	name="Layla"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11655942
	mother=41655942

	3140.1.1 = {birth="3140.1.1"}
	3192.1.1 = {death="3192.1.1"}
}
12555942 = {
	name="Sheeftah"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=12255942
	mother=42255942

	3158.1.1 = {birth="3158.1.1"}
	3201.1.1 = {death="3201.1.1"}
}
12655942 = {
	name="Bahir"	# not a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=12255942
	mother=42255942

	3161.1.1 = {birth="3161.1.1"}
	3177.1.1 = {add_trait=trained_warrior}
	3185.1.1 = {death="3185.1.1"}
}
12755942 = {
	name="Ali"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=12255942
	mother=42255942

	3164.1.1 = {birth="3164.1.1"}
	3180.1.1 = {add_trait=trained_warrior}
	3182.1.1 = {add_spouse=42755942}
	3215.1.1 = {death="3215.1.1"}
}
42755942 = {
	name="Setara"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3164.1.1 = {birth="3164.1.1"}
	3215.1.1 = {death="3215.1.1"}
}
12855942 = {
	name="Taneen"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=12755942
	mother=42755942

	3190.1.1 = {birth="3190.1.1"}
	3250.1.1 = {death="3250.1.1"}
}
12955942 = {
	name="Alim"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=12755942
	mother=42755942

	3193.1.1 = {birth="3193.1.1"}
	3209.1.1 = {add_trait=skilled_warrior}
	3211.1.1 = {add_spouse=42955942}
	3256.1.1 = {death="3256.1.1"}
}
42955942 = {
	name="Simin"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3193.1.1 = {birth="3193.1.1"}
	3256.1.1 = {death="3256.1.1"}
}
13055942 = {
	name="Kamala"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=12755942
	mother=42755942

	3195.1.1 = {birth="3195.1.1"}
	3227.1.1 = {death="3227.1.1"}
}
13155942 = {
	name="Shamir"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=12955942
	mother=42955942

	3221.1.1 = {birth="3221.1.1"}
	3237.1.1 = {add_trait=poor_warrior}
	3239.1.1 = {add_spouse=43155942}
	3273.1.1 = {death="3273.1.1"}
}
43155942 = {
	name="Rafiqa"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3221.1.1 = {birth="3221.1.1"}
	3273.1.1 = {death="3273.1.1"}
}
13255942 = {
	name="Souzan"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=12955942
	mother=42955942

	3224.1.1 = {birth="3224.1.1"}
	3286.1.1 = {death="3286.1.1"}
}
13355942 = {
	name="Husam"	# not a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=12955942
	mother=42955942

	3226.1.1 = {birth="3226.1.1"}
	3242.1.1 = {add_trait=poor_warrior}
	3278.1.1 = {death="3278.1.1"}
}
13455942 = {
	name="Reshawna"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=12955942
	mother=42955942

	3227.1.1 = {birth="3227.1.1"}
	3266.1.1 = {death="3266.1.1"}
}
13555942 = {
	name="Shamir"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=13155942
	mother=43155942

	3240.1.1 = {birth="3240.1.1"}
	3256.1.1 = {add_trait=poor_warrior}
	3258.1.1 = {add_spouse=43555942}
}
43555942 = {
	name="Rasa"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3240.1.1 = {birth="3240.1.1"}
}
13655942 = {
	name="Youkhanna"	# not a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=13555942
	mother=43555942

	3264.1.1 = {birth="3264.1.1"}
	3280.1.1 = {add_trait=poor_warrior}
}
13755942 = {
	name="Shokouh"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=13555942
	mother=43555942

	3266.1.1 = {birth="3266.1.1"}
}

###  House Issa
100055943 = {
	name="Uways"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	2969.1.1 = {birth="2969.1.1"}
	2985.1.1 = {add_trait=poor_warrior}
	2987.1.1 = {add_spouse=400055943}
	3036.1.1 = {death="3036.1.1"}
}
400055943 = {
	name="Faghira"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	2969.1.1 = {birth="2969.1.1"}
	3036.1.1 = {death="3036.1.1"}
}
100155943 = {
	name="Muzaffaraddin"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100055943
	mother=400055943

	2995.1.1 = {birth="2995.1.1"}
	3011.1.1 = {add_trait=poor_warrior}
	3013.1.1 = {add_spouse=400155943}
	3048.1.1 = {death="3048.1.1"}
}
400155943 = {
	name="Parween"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	2995.1.1 = {birth="2995.1.1"}
	3048.1.1 = {death="3048.1.1"}
}
100255943 = {
	name="Faghira"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100155943
	mother=400155943

	3013.1.1 = {birth="3013.1.1"}
	3070.1.1 = {death="3070.1.1"}
}
100355943 = {
	name="Youkhanna"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100155943
	mother=400155943

	3014.1.1 = {birth="3014.1.1"}
	3030.1.1 = {add_trait=trained_warrior}
	3032.1.1 = {add_spouse=400355943}
	3082.1.1 = {death="3082.1.1"}
}
400355943 = {
	name="Shararah"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3014.1.1 = {birth="3014.1.1"}
	3082.1.1 = {death="3082.1.1"}
}
100455943 = {
	name="Adila"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100155943
	mother=400155943

	3017.1.1 = {birth="3017.1.1"}
	3065.1.1 = {death="3065.1.1"}
}
100555943 = {
	name="Shola"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100155943
	mother=400155943

	3019.1.1 = {birth="3019.1.1"}
	3050.1.1 = {death="3050.1.1"}
}
100655943 = {
	name="Sheeva"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100155943
	mother=400155943

	3020.1.1 = {birth="3020.1.1"}
	3093.1.1 = {death="3093.1.1"}
}
100755943 = {
	name="Halil"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100355943
	mother=400355943

	3033.1.1 = {birth="3033.1.1"}
	3049.1.1 = {add_trait=trained_warrior}
	3051.1.1 = {add_spouse=400755943}
	3089.1.1 = {death="3089.1.1"}
}
400755943 = {
	name="Hanifa"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3033.1.1 = {birth="3033.1.1"}
	3089.1.1 = {death="3089.1.1"}
}
100855943 = {
	name="Yusuf"	# not a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100355943
	mother=400355943

	3034.1.1 = {birth="3034.1.1"}
	3050.1.1 = {add_trait=poor_warrior}
	3065.1.1 = {death="3065.1.1"}
}
100955943 = {
	name="Jahaira"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100355943
	mother=400355943

	3036.1.1 = {birth="3036.1.1"}
	3077.1.1 = {death = {death_reason = death_murder}}
}
101055943 = {
	name="Aghlab"	# not a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100355943
	mother=400355943

	3037.1.1 = {birth="3037.1.1"}
	3053.1.1 = {add_trait=trained_warrior}
	3093.1.1 = {death="3093.1.1"}
}
101155943 = {
	name="Hanifa"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100355943
	mother=400355943

	3038.1.1 = {birth="3038.1.1"}
	3081.1.1 = {death="3081.1.1"}
}
101255943 = {
	name="Muzaffaraddin"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100755943
	mother=400755943

	3051.1.1 = {birth="3051.1.1"}
	3067.1.1 = {add_trait=trained_warrior}
	3069.1.1 = {add_spouse=401255943}
	3092.1.1 = {death="3092.1.1"}
}
401255943 = {
	name="Kamala"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3051.1.1 = {birth="3051.1.1"}
	3092.1.1 = {death="3092.1.1"}
}
101355943 = {
	name="Nizam"	# not a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100755943
	mother=400755943

	3052.1.1 = {birth="3052.1.1"}
	3068.1.1 = {add_trait=poor_warrior}
	3090.1.1 = {death="3090.1.1"}
}
101455943 = {
	name="Qawurd"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101255943
	mother=401255943

	3076.1.1 = {birth="3076.1.1"}
	3092.1.1 = {add_trait=trained_warrior}
	3094.1.1 = {add_spouse=401455943}
	3130.1.1 = {death="3130.1.1"}
}
401455943 = {
	name="Souzan"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3076.1.1 = {birth="3076.1.1"}
	3130.1.1 = {death="3130.1.1"}
}
101555943 = {
	name="Yusuf"	# not a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101255943
	mother=401255943

	3079.1.1 = {birth="3079.1.1"}
	3095.1.1 = {add_trait=trained_warrior}
	3128.1.1 = {death="3128.1.1"}
}
101655943 = {
	name="Mukhtar"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101455943
	mother=401455943

	3108.1.1 = {birth="3108.1.1"}
	3124.1.1 = {add_trait=poor_warrior}
	3126.1.1 = {add_spouse=401655943}
	3169.1.1 = {death="3169.1.1"}
}
401655943 = {
	name="Nyawela"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3108.1.1 = {birth="3108.1.1"}
	3169.1.1 = {death="3169.1.1"}
}
101755943 = {
	name="Sami"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101655943
	mother=401655943

	3136.1.1 = {birth="3136.1.1"}
	3152.1.1 = {add_trait=poor_warrior}
	3154.1.1 = {add_spouse=401755943}
	3170.1.1 = {death="3170.1.1"}
}
401755943 = {
	name="Habiba"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3136.1.1 = {birth="3136.1.1"}
	3170.1.1 = {death="3170.1.1"}
}
101855943 = {
	name="Yahya"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101755943
	mother=401755943

	3159.1.1 = {birth="3159.1.1"}
	3175.1.1 = {add_trait=trained_warrior}
	3177.1.1 = {add_spouse=401855943}
	3219.1.1 = {death="3219.1.1"}
}
401855943 = {
	name="Nyawela"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3159.1.1 = {birth="3159.1.1"}
	3219.1.1 = {death="3219.1.1"}
}
101955943 = {
	name="Nizam"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101855943
	mother=401855943

	3191.1.1 = {birth="3191.1.1"}
	3207.1.1 = {add_trait=poor_warrior}
	3209.1.1 = {add_spouse=401955943}
	3246.1.1 = {death = {death_reason = death_accident}}
}
401955943 = {
	name="Taliba"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3191.1.1 = {birth="3191.1.1"}
	3246.1.1 = {death="3246.1.1"}
}
102055943 = {
	name="Samira"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101855943
	mother=401855943

	3194.1.1 = {birth="3194.1.1"}
	3228.1.1 = {death="3228.1.1"}
}
102155943 = {
	name="Bakr"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101955943
	mother=401955943

	3214.1.1 = {birth="3214.1.1"}
	3230.1.1 = {add_trait=poor_warrior}
	3232.1.1 = {add_spouse=402155943}
	3293.1.1 = {death="3293.1.1"}
}
402155943 = {
	name="Maryam"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3214.1.1 = {birth="3214.1.1"}
	3293.1.1 = {death="3293.1.1"}
}
102255943 = {
	name="Habiba"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101955943
	mother=401955943

	3216.1.1 = {birth="3216.1.1"}
	3250.1.1 = {death="3250.1.1"}
}
102355943 = {
	name="Shokouh"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101955943
	mother=401955943

	3219.1.1 = {birth="3219.1.1"}
	3267.1.1 = {death="3267.1.1"}
}
102455943 = {
	name="Aarif"	# not a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101955943
	mother=401955943

	3222.1.1 = {birth="3222.1.1"}
	3238.1.1 = {add_trait=trained_warrior}
	3288.1.1 = {death="3288.1.1"}
}
102555943 = {
	name="Samir"	# not a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=102155943
	mother=402155943

	3240.1.1 = {birth="3240.1.1"}
	3256.1.1 = {add_trait=skilled_warrior}
	3287.1.1 = {death="3287.1.1"}
}
102655943 = {
	name="Aghlab"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=102155943
	mother=402155943

	3243.1.1 = {birth="3243.1.1"}
	3259.1.1 = {add_trait=trained_warrior}
	3261.1.1 = {add_spouse=402655943}
}
402655943 = {
	name="Saaman"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3243.1.1 = {birth="3243.1.1"}
}
102755943 = {
	name="Sholah"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=102155943
	mother=402155943

	3246.1.1 = {birth="3246.1.1"}
	3299.1.1 = {death="3299.1.1"}
}
102855943 = {
	name="Jahaira"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=102155943
	mother=402155943

	3248.1.1 = {birth="3248.1.1"}
}
102955943 = {
	name="Ramadan"	# not a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=102655943
	mother=402655943

	3261.1.1 = {birth="3261.1.1"}
	3277.1.1 = {add_trait=skilled_warrior}
	3279.1.1 = {add_spouse=402955943}
}
402955943 = {
	name="Faghira"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3261.1.1 = {birth="3261.1.1"}
}
103055943 = {
	name="Alim"	# not a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=102955943
	mother=402955943

	3294.1.1 = {birth="3294.1.1"}
	3310.1.1 = {add_trait=poor_warrior}
}
103155943 = {
	name="Bakr"	# not a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=102955943
	mother=402955943

	3295.1.1 = {birth="3295.1.1"}
	3311.1.1 = {add_trait=poor_warrior}
}
103255943 = {
	name="Paymaneh"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=102955943
	mother=402955943

	3297.1.1 = {birth="3297.1.1"}
}
103355943 = {
	name="Nyawela"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=102955943
	mother=402955943

	3298.1.1 = {birth="3298.1.1"}
}

###  House Kassis
100055944 = {
	name="Husam"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	2965.1.1 = {birth="2965.1.1"}
	2981.1.1 = {add_trait=poor_warrior}
	2983.1.1 = {add_spouse=400055944}
	3028.1.1 = {death="3028.1.1"}
}
400055944 = {
	name="Rashida"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	2965.1.1 = {birth="2965.1.1"}
	3028.1.1 = {death="3028.1.1"}
}
100155944 = {
	name="Nizam"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100055944
	mother=400055944

	2993.1.1 = {birth="2993.1.1"}
	3009.1.1 = {add_trait=poor_warrior}
	3011.1.1 = {add_spouse=400155944}
	3034.1.1 = {death="3034.1.1"}
}
400155944 = {
	name="Tanaz"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	2993.1.1 = {birth="2993.1.1"}
	3034.1.1 = {death="3034.1.1"}
}
100255944 = {
	name="Sabba"	# not a lord
	female=yes
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100055944
	mother=400055944

	2995.1.1 = {birth="2995.1.1"}
	3064.1.1 = {death="3064.1.1"}
}
100355944 = {
	name="Alim"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100155944
	mother=400155944

	3013.1.1 = {birth="3013.1.1"}
	3029.1.1 = {add_trait=poor_warrior}
	3031.1.1 = {add_spouse=400355944}
	3093.1.1 = {death="3093.1.1"}
}
400355944 = {
	name="Sahba"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3013.1.1 = {birth="3013.1.1"}
	3093.1.1 = {death="3093.1.1"}
}
100455944 = {
	name="Shameem"	# not a lord
	female=yes
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100155944
	mother=400155944

	3016.1.1 = {birth="3016.1.1"}
	3083.1.1 = {death="3083.1.1"}
}
100555944 = {
	name="Ramadan"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100355944
	mother=400355944

	3036.1.1 = {birth="3036.1.1"}
	3052.1.1 = {add_trait=skilled_warrior}
	3054.1.1 = {add_spouse=400555944}
	3094.1.1 = {death="3094.1.1"}
}
400555944 = {
	name="Saghar"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3036.1.1 = {birth="3036.1.1"}
	3094.1.1 = {death="3094.1.1"}
}
100655944 = {
	name="Jaleel"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100355944
	mother=400355944

	3039.1.1 = {birth="3039.1.1"}
	3055.1.1 = {add_trait=poor_warrior}
	3086.1.1 = {death="3086.1.1"}
}
100755944 = {
	name="Samir"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100355944
	mother=400355944

	3042.1.1 = {birth="3042.1.1"}
	3058.1.1 = {add_trait=poor_warrior}
	3090.1.1 = {death="3090.1.1"}
}
100855944 = {
	name="Mansur"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100555944
	mother=400555944

	3065.1.1 = {birth="3065.1.1"}
	3081.1.1 = {add_trait=trained_warrior}
	3083.1.1 = {add_spouse=400855944}
	3123.1.1 = {death="3123.1.1"}
}
400855944 = {
	name="Sheeftah"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3065.1.1 = {birth="3065.1.1"}
	3123.1.1 = {death="3123.1.1"}
}
100955944 = {
	name="Bahir"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100855944
	mother=400855944

	3087.1.1 = {birth="3087.1.1"}
	3103.1.1 = {add_trait=trained_warrior}
	3105.1.1 = {add_spouse=400955944}
	3139.1.1 = {death="3139.1.1"}
}
400955944 = {
	name="Amsha"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3087.1.1 = {birth="3087.1.1"}
	3139.1.1 = {death="3139.1.1"}
}
101055944 = {
	name="Ramadan"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100955944
	mother=400955944

	3112.1.1 = {birth="3112.1.1"}
	3128.1.1 = {add_trait=poor_warrior}
	3130.1.1 = {add_spouse=401055944}
	3171.1.1 = {death="3171.1.1"}
}
401055944 = {
	name="Shokouh"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3112.1.1 = {birth="3112.1.1"}
	3171.1.1 = {death="3171.1.1"}
}
101155944 = {
	name="Hafiz"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100955944
	mother=400955944

	3113.1.1 = {birth="3113.1.1"}
	3129.1.1 = {add_trait=poor_warrior}
	3151.1.1 = {death="3151.1.1"}
}
101255944 = {
	name="Mansur"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100955944
	mother=400955944

	3116.1.1 = {birth="3116.1.1"}
	3132.1.1 = {add_trait=trained_warrior}
	3161.1.1 = {death="3161.1.1"}
}
101355944 = {
	name="Faruk"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=101055944
	mother=401055944

	3135.1.1 = {birth="3135.1.1"}
	3151.1.1 = {add_trait=master_warrior}
	3171.1.1 = {death="3171.1.1"}
}
101455944 = {
	name="Talib"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=101055944
	mother=401055944

	3137.1.1 = {birth="3137.1.1"}
	3153.1.1 = {add_trait=poor_warrior}
	3155.1.1 = {add_spouse=401455944}
	3205.1.1 = {death="3205.1.1"}
}
401455944 = {
	name="Nyawela"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3137.1.1 = {birth="3137.1.1"}
	3205.1.1 = {death="3205.1.1"}
}
101555944 = {
	name="Sadiq"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=101455944
	mother=401455944

	3166.1.1 = {birth="3166.1.1"}
	3182.1.1 = {add_trait=poor_warrior}
	3184.1.1 = {add_spouse=401555944}
	3219.1.1 = {death="3219.1.1"}
}
401555944 = {
	name="Yakta"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3166.1.1 = {birth="3166.1.1"}
	3219.1.1 = {death="3219.1.1"}
}
101655944 = {
	name="Mansur"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=101455944
	mother=401455944

	3167.1.1 = {birth="3167.1.1"}
	3183.1.1 = {add_trait=trained_warrior}
	3244.1.1 = {death="3244.1.1"}
}
101755944 = {
	name="Shokouh"	# not a lord
	female=yes
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=101455944
	mother=401455944

	3169.1.1 = {birth="3169.1.1"}
	3226.1.1 = {death="3226.1.1"}
}
101855944 = {
	name="Qadir"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=101455944
	mother=401455944

	3170.1.1 = {birth="3170.1.1"}
	3186.1.1 = {add_trait=skilled_warrior}
	3208.1.1 = {death = {death_reason = death_battle}}
}
101955944 = {
	name="Akin"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=101455944
	mother=401455944

	3172.1.1 = {birth="3172.1.1"}
	3188.1.1 = {add_trait=poor_warrior}
	3225.1.1 = {death="3225.1.1"}
}
102055944 = {
	name="Mirza"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=101555944
	mother=401555944

	3201.1.1 = {birth="3201.1.1"}
	3217.1.1 = {add_trait=trained_warrior}
	3219.1.1 = {add_spouse=402055944}
	3250.1.1 = {death="3250.1.1"}
}
402055944 = {
	name="Amsha"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3201.1.1 = {birth="3201.1.1"}
	3250.1.1 = {death="3250.1.1"}
}
102155944 = {
	name="Talib"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=102055944
	mother=402055944

	3220.1.1 = {birth="3220.1.1"}
	3236.1.1 = {add_trait=poor_warrior}
	3238.1.1 = {add_spouse=402155944}
	3266.1.1 = {death="3266.1.1"}
}
402155944 = {
	name="Paymaneh"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3220.1.1 = {birth="3220.1.1"}
	3266.1.1 = {death="3266.1.1"}
}
102255944 = {
	name="Zaynab"	# not a lord
	female=yes
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=102055944
	mother=402055944

	3223.1.1 = {birth="3223.1.1"}
	3260.1.1 = {death="3260.1.1"}
}
102355944 = {
	name="Husam"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=102055944
	mother=402055944

	3225.1.1 = {birth="3225.1.1"}
	3241.1.1 = {add_trait=poor_warrior}
	3294.1.1 = {death="3294.1.1"}
}
102455944 = {
	name="Azam"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=102155944
	mother=402155944

	3249.1.1 = {birth="3249.1.1"}
	3265.1.1 = {add_trait=poor_warrior}
	3267.1.1 = {add_spouse=402455944}
	3286.1.1 = {death = {death_reason = death_murder}}
}
402455944 = {
	name="Hanifa"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3249.1.1 = {birth="3249.1.1"}
	3286.1.1 = {death="3286.1.1"}
}
102555944 = {
	name="Sadiq"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=102455944
	mother=402455944

	3273.1.1 = {birth="3273.1.1"}
	3289.1.1 = {add_trait=trained_warrior}
	3291.1.1 = {add_spouse=402555944}
}
402555944 = {
	name="Yakta"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3273.1.1 = {birth="3273.1.1"}
}
102655944 = {
	name="Halil"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=102455944
	mother=402455944

	3276.1.1 = {birth="3276.1.1"}
	3292.1.1 = {add_trait=poor_warrior}
}
102755944 = {
	name="Bakr"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=102455944
	mother=402455944

	3278.1.1 = {birth="3278.1.1"}
	3294.1.1 = {add_trait=poor_warrior}
}
102855944 = {
	name="Sheeva"	# not a lord
	female=yes
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=102555944
	mother=402555944

	3298.1.1 = {birth="3298.1.1"}
}

###  House Kalzarria
100069165 = {
	name="Abdullah"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	2963.1.1 = {birth="2963.1.1"}
	2979.1.1 = {add_trait=poor_warrior}
	2981.1.1 = {add_spouse=400069165}
	3032.1.1 = {death="3032.1.1"}
}
400069165 = {
	name="Hanifa"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	2963.1.1 = {birth="2963.1.1"}
	3032.1.1 = {death="3032.1.1"}
}
100169165 = {
	name="Ghalib"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100069165
	mother=400069165

	2988.1.1 = {birth="2988.1.1"}
	3004.1.1 = {add_trait=skilled_warrior}
	3006.1.1 = {add_spouse=400169165}
	3041.1.1 = {death="3041.1.1"}
}
400169165 = {
	name="Nyawela"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	2988.1.1 = {birth="2988.1.1"}
	3041.1.1 = {death="3041.1.1"}
}
100269165 = {
	name="Fadl"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100169165
	mother=400169165

	3009.1.1 = {birth="3009.1.1"}
	3025.1.1 = {add_trait=poor_warrior}
	3027.1.1 = {add_spouse=400269165}
	3053.1.1 = {death="3053.1.1"}
}
400269165 = {
	name="Shahrbano"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3009.1.1 = {birth="3009.1.1"}
	3053.1.1 = {death="3053.1.1"}
}
100369165 = {
	name="Aarif"	# not a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100169165
	mother=400169165

	3011.1.1 = {birth="3011.1.1"}
	3027.1.1 = {add_trait=poor_warrior}
	3069.1.1 = {death="3069.1.1"}
}
100469165 = {
	name="Idris"	# not a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100169165
	mother=400169165

	3013.1.1 = {birth="3013.1.1"}
	3029.1.1 = {add_trait=poor_warrior}
	3047.1.1 = {death = {death_reason = death_murder}}
}
100569165 = {
	name="Amsha"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100169165
	mother=400169165

	3016.1.1 = {birth="3016.1.1"}
	3064.1.1 = {death="3064.1.1"}
}
100669165 = {
	name="Maryam"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100269165
	mother=400269165

	3034.1.1 = {birth="3034.1.1"}
	3065.1.1 = {death="3065.1.1"}
}
100769165 = {
	name="Shola"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100269165
	mother=400269165

	3036.1.1 = {birth="3036.1.1"}
	3089.1.1 = {death="3089.1.1"}
}
100869165 = {
	name="Is'mail"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100269165
	mother=400269165

	3038.1.1 = {birth="3038.1.1"}
	3054.1.1 = {add_trait=master_warrior}
	3056.1.1 = {add_spouse=400869165}
	3112.1.1 = {death="3112.1.1"}
}
400869165 = {
	name="Zaynab"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3038.1.1 = {birth="3038.1.1"}
	3112.1.1 = {death="3112.1.1"}
}
100969165 = {
	name="Youkhanna"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100869165
	mother=400869165

	3071.1.1 = {birth="3071.1.1"}
	3087.1.1 = {add_trait=poor_warrior}
	3089.1.1 = {add_spouse=400969165}
	3130.1.1 = {death="3130.1.1"}
}
400969165 = {
	name="Shola"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3071.1.1 = {birth="3071.1.1"}
	3130.1.1 = {death="3130.1.1"}
}
101069165 = {
	name="Paywand"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100869165
	mother=400869165

	3072.1.1 = {birth="3072.1.1"}
	3118.1.1 = {death="3118.1.1"}
}
101169165 = {
	name="Taliba"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100969165
	mother=400969165

	3105.1.1 = {birth="3105.1.1"}
	3137.1.1 = {death="3137.1.1"}
}
101269165 = {
	name="Khaireddin"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100969165
	mother=400969165

	3106.1.1 = {birth="3106.1.1"}
	3122.1.1 = {add_trait=trained_warrior}
	3124.1.1 = {add_spouse=401269165}
	3145.1.1 = {death="3145.1.1"}
}
401269165 = {
	name="Setara"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3106.1.1 = {birth="3106.1.1"}
	3145.1.1 = {death="3145.1.1"}
}
101369165 = {
	name="Muslihiddin"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101269165
	mother=401269165

	3125.1.1 = {birth="3125.1.1"}
	3141.1.1 = {add_trait=poor_warrior}
	3143.1.1 = {add_spouse=401369165}
	3175.1.1 = {death="3175.1.1"}
}
401369165 = {
	name="Saaman"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3125.1.1 = {birth="3125.1.1"}
	3175.1.1 = {death="3175.1.1"}
}
101469165 = {
	name="Shogofa"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101269165
	mother=401269165

	3126.1.1 = {birth="3126.1.1"}
	3147.1.1 = {death = {death_reason = death_accident}}
}
101569165 = {
	name="Saghar"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101369165
	mother=401369165

	3152.1.1 = {birth="3152.1.1"}
	3208.1.1 = {death="3208.1.1"}
}
101669165 = {
	name="Jibril"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101369165
	mother=401369165

	3153.1.1 = {birth="3153.1.1"}
	3169.1.1 = {add_trait=trained_warrior}
	3171.1.1 = {add_spouse=401669165}
	3193.1.1 = {death="3193.1.1"}
}
401669165 = {
	name="Semeah"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3153.1.1 = {birth="3153.1.1"}
	3193.1.1 = {death="3193.1.1"}
}
101769165 = {
	name="Gafur"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101669165
	mother=401669165

	3176.1.1 = {birth="3176.1.1"}
	3192.1.1 = {add_trait=poor_warrior}
	3194.1.1 = {add_spouse=401769165}
	3251.1.1 = {death="3251.1.1"}
}
401769165 = {
	name="Habiba"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3176.1.1 = {birth="3176.1.1"}
	3251.1.1 = {death="3251.1.1"}
}
101869165 = {
	name="Yahya"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101769165
	mother=401769165

	3202.1.1 = {birth="3202.1.1"}
	3218.1.1 = {add_trait=poor_warrior}
	3220.1.1 = {add_spouse=401869165}
	3272.1.1 = {death="3272.1.1"}
}
401869165 = {
	name="Qamara"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3202.1.1 = {birth="3202.1.1"}
	3272.1.1 = {death="3272.1.1"}
}
101969165 = {
	name="Rashida"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101769165
	mother=401769165

	3203.1.1 = {birth="3203.1.1"}
	3236.1.1 = {death="3236.1.1"}
}
102069165 = {
	name="Muzaffaraddin"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101869165
	mother=401869165

	3229.1.1 = {birth="3229.1.1"}
	3245.1.1 = {add_trait=poor_warrior}
	3247.1.1 = {add_spouse=402069165}
	3291.1.1 = {death="3291.1.1"}
}
402069165 = {
	name="Kamala"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3229.1.1 = {birth="3229.1.1"}
	3291.1.1 = {death="3291.1.1"}
}
102169165 = {
	name="Qamara"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101869165
	mother=401869165

	3232.1.1 = {birth="3232.1.1"}
}
102269165 = {
	name="Semeah"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101869165
	mother=401869165

	3233.1.1 = {birth="3233.1.1"}
	3272.1.1 = {death="3272.1.1"}
}
102369165 = {
	name="Jaleel"	# not a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101869165
	mother=401869165

	3236.1.1 = {birth="3236.1.1"}
	3252.1.1 = {add_trait=poor_warrior}
	3280.1.1 = {death="3280.1.1"}
}
102469165 = {
	name="Yusuf"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=102069165
	mother=402069165

	3247.1.1 = {birth="3247.1.1"}
	3263.1.1 = {add_trait=poor_warrior}
	3265.1.1 = {add_spouse=402469165}
}
402469165 = {
	name="Sheeva"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3247.1.1 = {birth="3247.1.1"}
}
102569165 = {
	name="Sabba"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=102069165
	mother=402069165

	3249.1.1 = {birth="3249.1.1"}
}
102669165 = {
	name="Hafiz"	# not a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=102069165
	mother=402069165

	3250.1.1 = {birth="3250.1.1"}
	3266.1.1 = {add_trait=poor_warrior}
}
102769165 = {
	name="Fadl"	# not a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=102069165
	mother=402069165

	3252.1.1 = {birth="3252.1.1"}
	3268.1.1 = {add_trait=poor_warrior}
	3294.1.1 = {death="3294.1.1"}
}
102869165 = {
	name="Souzan"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=102069165
	mother=402069165

	3253.1.1 = {birth="3253.1.1"}
}
102969165 = {
	name="Nasr"	# not a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=102469165
	mother=402469165

	3275.1.1 = {birth="3275.1.1"}
	3291.1.1 = {add_trait=poor_warrior}
	3293.1.1 = {add_spouse=402969165}
}
402969165 = {
	name="Shameem"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	3275.1.1 = {birth="3275.1.1"}
}
