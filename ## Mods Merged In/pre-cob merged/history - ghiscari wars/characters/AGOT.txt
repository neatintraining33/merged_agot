##AGOT
##ids 450000-450999

450000 = {
	name="George" #
	dynasty=450000 #Martin
	dna="cbbabiceegn"
	properties="0a00b"
	learning = 12
	religion="the_seven"
	culture="old_andal"
	add_trait = scholarly_theologian
	add_trait = poet
	add_trait = genius
	add_trait = gregarious
	add_trait = patient
	add_trait = gluttonous
	add_trait = slothful
	add_trait = cruel
	2933.9.20 = {
		birth="2933.9.20"
		effect = { health = 4 }
	}
	2992.1.1 = {
		employer=901174999
	}
	3001.1.1 = {
		death="3001.1.1"
	}
}
450999 = { #Dummy character, hold e_rebels at game start to ensure initialisation event doesnt trigger when loading save games
	name="Jon" #
	religion="the_seven"
	culture="crownlander"
	1.1.1 = {
		birth="1.1.1"
		effect = { set_character_flag = trigger_startup_event }
	}
}
450998 = { #Dummy character, hosts link to reforgable valyrian steel images
	name="Valyrian Steel" 
	
	religion="valyrian_rel"
	culture="high_valyrian"
	occluded = yes
	
	1.1.1 = {
		birth = yes
	}
	1.1.1 = {
		death = yes
	}
}
