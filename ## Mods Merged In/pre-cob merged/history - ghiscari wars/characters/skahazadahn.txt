#############   House zo Pazak

1000208 = {
	name="Grazdhar"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	2974.1.1 = {birth="2974.1.1"}
	2990.1.1 = {add_trait=poor_warrior}
	2992.1.1 = {add_spouse=4000208}
	3030.1.1 = {death="3030.1.1"}
}
4000208 = {
	name="Yezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	2974.1.1 = {birth="2974.1.1"}
	3030.1.1 = {death="3030.1.1"}
}
1001208 = {
	name="Yarkhaz"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1000208
	mother=4000208

	3007.1.1 = {birth="3007.1.1"}
	3023.1.1 = {add_trait=trained_warrior}
	3025.1.1 = {add_spouse=4001208}
	3082.1.1 = {death="3082.1.1"}
}
4001208 = {
	name="Yezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3007.1.1 = {birth="3007.1.1"}
	3082.1.1 = {death="3082.1.1"}
}
1002208 = {
	name="Morghaz"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1001208
	mother=4001208

	3032.1.1 = {birth="3032.1.1"}
	3048.1.1 = {add_trait=skilled_warrior}
	3050.1.1 = {add_spouse=4002208}
	3109.1.1 = {death = {death_reason = death_accident}}
}
4002208 = {
	name="Gezzella"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3032.1.1 = {birth="3032.1.1"}
	3109.1.1 = {death="3109.1.1"}
}
1003208 = {
	name="Maezon"	# not a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1001208
	mother=4001208

	3033.1.1 = {birth="3033.1.1"}
	3049.1.1 = {add_trait=trained_warrior}
	3107.1.1 = {death="3107.1.1"}
}
1004208 = {
	name="Zellazara"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1002208
	mother=4002208

	3063.1.1 = {birth="3063.1.1"}
	3107.1.1 = {death="3107.1.1"}
}
1005208 = {
	name="Reznak"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1002208
	mother=4002208

	3066.1.1 = {birth="3066.1.1"}
	3082.1.1 = {add_trait=poor_warrior}
	3084.1.1 = {add_spouse=4005208}
	3129.1.1 = {death="3129.1.1"}
}
4005208 = {
	name="Zellazara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3066.1.1 = {birth="3066.1.1"}
	3129.1.1 = {death="3129.1.1"}
}
1006208 = {
	name="Zaraq"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1005208
	mother=4005208

	3092.1.1 = {birth="3092.1.1"}
	3108.1.1 = {add_trait=skilled_warrior}
	3110.1.1 = {add_spouse=4006208}
	3155.1.1 = {death="3155.1.1"}
}
4006208 = {
	name="Qezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3092.1.1 = {birth="3092.1.1"}
	3155.1.1 = {death="3155.1.1"}
}
1007208 = {
	name="Hazzea"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1005208
	mother=4005208

	3095.1.1 = {birth="3095.1.1"}
	3137.1.1 = {death="3137.1.1"}
}
1008208 = {
	name="Ghazdor"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1006208
	mother=4006208

	3124.1.1 = {birth="3124.1.1"}
	3140.1.1 = {add_trait=skilled_warrior}
	3142.1.1 = {add_spouse=4008208}
	3193.1.1 = {death="3193.1.1"}
}
4008208 = {
	name="Selezzqa"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3124.1.1 = {birth="3124.1.1"}
	3193.1.1 = {death="3193.1.1"}
}
1009208 = {
	name="Grazdhar"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1008208
	mother=4008208

	3149.1.1 = {birth="3149.1.1"}
	3165.1.1 = {add_trait=poor_warrior}
	3167.1.1 = {add_spouse=4009208}
	3224.1.1 = {death="3224.1.1"}
}
4009208 = {
	name="Mezzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3149.1.1 = {birth="3149.1.1"}
	3224.1.1 = {death="3224.1.1"}
}
1010208 = {
	name="Mazdhan"	# not a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1008208
	mother=4008208

	3152.1.1 = {birth="3152.1.1"}
	3168.1.1 = {add_trait=skilled_warrior}
	3190.1.1 = {death="3190.1.1"}
}
1011208 = {
	name="Ellezzara"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1009208
	mother=4009208

	3182.1.1 = {birth="3182.1.1"}
	3219.1.1 = {death="3219.1.1"}
}
1012208 = {
	name="Fendahl"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1009208
	mother=4009208

	3183.1.1 = {birth="3183.1.1"}
	3199.1.1 = {add_trait=poor_warrior}
	3201.1.1 = {add_spouse=4012208}
	3238.1.1 = {death="3238.1.1"}
}
4012208 = {
	name="Qezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3183.1.1 = {birth="3183.1.1"}
	3238.1.1 = {death="3238.1.1"}
}
1013208 = {
	name="Yarkhaz"	# not a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1009208
	mother=4009208

	3184.1.1 = {birth="3184.1.1"}
	3200.1.1 = {add_trait=trained_warrior}
	3226.1.1 = {death="3226.1.1"}
}
1014208 = {
	name="Marghaz"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1012208
	mother=4012208

	3207.1.1 = {birth="3207.1.1"}
	3223.1.1 = {add_trait=poor_warrior}
	3225.1.1 = {add_spouse=4014208}
	3253.1.1 = {death="3253.1.1"}
}
4014208 = {
	name="Hazzea"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3207.1.1 = {birth="3207.1.1"}
	3253.1.1 = {death="3253.1.1"}
}
1015208 = {
	name="Reznak"	# not a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1012208
	mother=4012208

	3208.1.1 = {birth="3208.1.1"}
	3224.1.1 = {add_trait=poor_warrior}
	3279.1.1 = {death="3279.1.1"}
}
1016208 = {
	name="Dhazzar"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1012208
	mother=4012208

	3209.1.1 = {birth="3209.1.1"}
	3274.1.1 = {death="3274.1.1"}
}
1017208 = {
	name="Haznak"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1014208
	mother=4014208

	3231.1.1 = {birth="3231.1.1"}
	3247.1.1 = {add_trait=poor_warrior}
	3249.1.1 = {add_spouse=4017208}
	3283.1.1 = {death="3283.1.1"}
}
4017208 = {
	name="Jezhane"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3231.1.1 = {birth="3231.1.1"}
	3283.1.1 = {death="3283.1.1"}
}
1018208 = {
	name="Tezza"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1014208
	mother=4014208

	3233.1.1 = {birth="3233.1.1"}
	3292.1.1 = {death="3292.1.1"}
}
1019208 = {
	name="Selezzqa"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1014208
	mother=4014208

	3236.1.1 = {birth="3236.1.1"}
	3277.1.1 = {death="3277.1.1"}
}
1020208 = {
	name="Azzara"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1014208
	mother=4014208

	3238.1.1 = {birth="3238.1.1"}
	3290.1.1 = {death="3290.1.1"}
}
1021208 = {
	name="Skahaz"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1017208
	mother=4017208

	3259.1.1 = {birth="3259.1.1"}
	3275.1.1 = {add_trait=trained_warrior}
	3277.1.1 = {add_spouse=4021208}
	3316.1.1 = {death="3316.1.1"}
}
4021208 = {
	name="Ellezzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3259.1.1 = {birth="3259.1.1"}
	3316.1.1 = {death="3316.1.1"}
}
1022208 = {
	name="Yezza"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1017208
	mother=4017208

	3260.1.1 = {birth="3260.1.1"}
	3336.1.1 = {death="3336.1.1"}
}
1023208 = {
	name="Fendahl"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1021208
	mother=4021208

	3283.1.1 = {birth="3283.1.1"}
	3299.1.1 = {add_trait=poor_warrior}
	3331.1.1 = {death="3331.1.1"}
}


#################   House zo Zasnzn

1000213 = {
	name="Miklaz"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	2960.1.1 = {birth="2960.1.1"}
	2976.1.1 = {add_trait=poor_warrior}
	2978.1.1 = {add_spouse=4000213}
	3023.1.1 = {death="3023.1.1"}
}
4000213 = {
	name="Galazza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	2960.1.1 = {birth="2960.1.1"}
	3023.1.1 = {death="3023.1.1"}
}
1001213 = {
	name="Skahaz"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1000213
	mother=4000213

	2987.1.1 = {birth="2987.1.1"}
	3003.1.1 = {add_trait=poor_warrior}
	3005.1.1 = {add_spouse=4001213}
	3043.1.1 = {death="3043.1.1"}
}
4001213 = {
	name="Galazza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	2987.1.1 = {birth="2987.1.1"}
	3043.1.1 = {death="3043.1.1"}
}
1002213 = {
	name="Skahaz"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1000213
	mother=4000213

	2990.1.1 = {birth="2990.1.1"}
	3006.1.1 = {add_trait=poor_warrior}
	3057.1.1 = {death="3057.1.1"}
}
1003213 = {
	name="Azzak"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1000213
	mother=4000213

	2993.1.1 = {birth="2993.1.1"}
	3009.1.1 = {add_trait=trained_warrior}
	3042.1.1 = {death="3042.1.1"}
}
1004213 = {
	name="Yezza"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1000213
	mother=4000213

	2996.1.1 = {birth="2996.1.1"}
	3047.1.1 = {death="3047.1.1"}
}
1005213 = {
	name="Tellezzara"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1001213
	mother=4001213

	3011.1.1 = {birth="3011.1.1"}
	3090.1.1 = {death="3090.1.1"}
}
1006213 = {
	name="Sezza"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1001213
	mother=4001213

	3013.1.1 = {birth="3013.1.1"}
	3045.1.1 = {death="3045.1.1"}
}
1007213 = {
	name="Maezon"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1001213
	mother=4001213

	3016.1.1 = {birth="3016.1.1"}
	3032.1.1 = {add_trait=poor_warrior}
	3034.1.1 = {add_spouse=4007213}
	3057.1.1 = {death="3057.1.1"}
}
4007213 = {
	name="Hazzaara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3016.1.1 = {birth="3016.1.1"}
	3057.1.1 = {death="3057.1.1"}
}
1008213 = {
	name="Maezon"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1001213
	mother=4001213

	3018.1.1 = {birth="3018.1.1"}
	3034.1.1 = {add_trait=poor_warrior}
	3079.1.1 = {death = {death_reason = death_accident}}
}
1009213 = {
	name="Azzara"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1007213
	mother=4007213

	3034.1.1 = {birth="3034.1.1"}
	3085.1.1 = {death="3085.1.1"}
}
1010213 = {
	name="Hizdhar"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1007213
	mother=4007213

	3036.1.1 = {birth="3036.1.1"}
	3052.1.1 = {add_trait=trained_warrior}
	3054.1.1 = {add_spouse=4010213}
	3109.1.1 = {death="3109.1.1"}
}
4010213 = {
	name="Zellazara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3036.1.1 = {birth="3036.1.1"}
	3109.1.1 = {death="3109.1.1"}
}
1011213 = {
	name="Fendahl"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1010213
	mother=4010213

	3054.1.1 = {birth="3054.1.1"}
	3070.1.1 = {add_trait=master_warrior}
	3072.1.1 = {add_spouse=4011213}
	3112.1.1 = {death="3112.1.1"}
}
4011213 = {
	name="Selezzqa"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3054.1.1 = {birth="3054.1.1"}
	3112.1.1 = {death="3112.1.1"}
}
1012213 = {
	name="Azzea"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1010213
	mother=4010213

	3055.1.1 = {birth="3055.1.1"}
	3112.1.1 = {death = {death_reason = death_accident}}
}
1013213 = {
	name="Galazza"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1010213
	mother=4010213

	3058.1.1 = {birth="3058.1.1"}
	3108.1.1 = {death="3108.1.1"}
}
1014213 = {
	name="Dhazzar"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1010213
	mother=4010213

	3061.1.1 = {birth="3061.1.1"}
	3123.1.1 = {death="3123.1.1"}
}
1015213 = {
	name="Morghaz"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1011213
	mother=4011213

	3082.1.1 = {birth="3082.1.1"}
	3098.1.1 = {add_trait=poor_warrior}
	3100.1.1 = {add_spouse=4015213}
	3137.1.1 = {death="3137.1.1"}
}
4015213 = {
	name="Ellezzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3082.1.1 = {birth="3082.1.1"}
	3137.1.1 = {death="3137.1.1"}
}
1016213 = {
	name="Azzea"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1015213
	mother=4015213

	3114.1.1 = {birth="3114.1.1"}
	3176.1.1 = {death="3176.1.1"}
}
1017213 = {
	name="Paezhar"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1015213
	mother=4015213

	3116.1.1 = {birth="3116.1.1"}
	3132.1.1 = {add_trait=poor_warrior}
	3134.1.1 = {add_spouse=4017213}
	3184.1.1 = {death="3184.1.1"}
}
4017213 = {
	name="Azzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3116.1.1 = {birth="3116.1.1"}
	3184.1.1 = {death="3184.1.1"}
}
1018213 = {
	name="Qazella"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1015213
	mother=4015213

	3118.1.1 = {birth="3118.1.1"}
	3195.1.1 = {death="3195.1.1"}
}
1019213 = {
	name="Maezon"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1015213
	mother=4015213

	3119.1.1 = {birth="3119.1.1"}
	3135.1.1 = {add_trait=poor_warrior}
	3189.1.1 = {death = {death_reason = death_accident}}
}
1020213 = {
	name="Ghazdor"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1017213
	mother=4017213

	3143.1.1 = {birth="3143.1.1"}
	3159.1.1 = {add_trait=skilled_warrior}
	3173.1.1 = {death = {death_reason = death_murder}}
}
1021213 = {
	name="Mazdhan"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1017213
	mother=4017213

	3145.1.1 = {birth="3145.1.1"}
	3161.1.1 = {add_trait=poor_warrior}
	3163.1.1 = {add_spouse=4021213}
	3198.1.1 = {death="3198.1.1"}
}
4021213 = {
	name="Hazzea"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3145.1.1 = {birth="3145.1.1"}
	3198.1.1 = {death="3198.1.1"}
}
1022213 = {
	name="Faezdhar"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1021213
	mother=4021213

	3163.1.1 = {birth="3163.1.1"}
	3179.1.1 = {add_trait=poor_warrior}
	3181.1.1 = {add_spouse=4022213}
	3231.1.1 = {death="3231.1.1"}
}
4022213 = {
	name="Ezzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3163.1.1 = {birth="3163.1.1"}
	3231.1.1 = {death="3231.1.1"}
}
1023213 = {
	name="Kraznys"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1022213
	mother=4022213

	3192.1.1 = {birth="3192.1.1"}
	3208.1.1 = {add_trait=trained_warrior}
	3210.1.1 = {add_spouse=4023213}
	3267.1.1 = {death="3267.1.1"}
}
4023213 = {
	name="Qazella"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3192.1.1 = {birth="3192.1.1"}
	3267.1.1 = {death="3267.1.1"}
}
1024213 = {
	name="Haznak"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1022213
	mother=4022213

	3195.1.1 = {birth="3195.1.1"}
	3211.1.1 = {add_trait=poor_warrior}
	3240.1.1 = {death="3240.1.1"}
}
1025213 = {
	name="Ghazdor"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1022213
	mother=4022213

	3196.1.1 = {birth="3196.1.1"}
	3212.1.1 = {add_trait=skilled_warrior}
	3256.1.1 = {death="3256.1.1"}
}
1026213 = {
	name="Harghaz"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1022213
	mother=4022213

	3199.1.1 = {birth="3199.1.1"}
	3215.1.1 = {add_trait=poor_warrior}
	3256.1.1 = {death="3256.1.1"}
}
1027213 = {
	name="Gezzella"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1023213
	mother=4023213

	3219.1.1 = {birth="3219.1.1"}
	3252.1.1 = {death="3252.1.1"}
}
1028213 = {
	name="Jezhane"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1023213
	mother=4023213

	3222.1.1 = {birth="3222.1.1"}
	3295.1.1 = {death="3295.1.1"}
}
1029213 = {
	name="Ghazdor"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1023213
	mother=4023213

	3224.1.1 = {birth="3224.1.1"}
	3240.1.1 = {add_trait=poor_warrior}
	3242.1.1 = {add_spouse=4029213}
	3272.1.1 = {death="3272.1.1"}
}
4029213 = {
	name="Ezzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3224.1.1 = {birth="3224.1.1"}
	3272.1.1 = {death="3272.1.1"}
}
1030213 = {
	name="Sezqa"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1023213
	mother=4023213

	3226.1.1 = {birth="3226.1.1"}
	3300.1.1 = {death="3300.1.1"}
}
1031213 = {
	name="Paezhar"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1029213
	mother=4029213

	3246.1.1 = {birth="3246.1.1"}
	3262.1.1 = {add_trait=poor_warrior}
	3264.1.1 = {add_spouse=4031213}
	3289.1.1 = {death="3289.1.1"}
}
4031213 = {
	name="Qezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3246.1.1 = {birth="3246.1.1"}
	3289.1.1 = {death="3289.1.1"}
}
1032213 = {
	name="Selezzqa"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1031213
	mother=4031213

	3264.1.1 = {birth="3264.1.1"}
	3340.1.1 = {death="3340.1.1"}
}
1033213 = {
	name="Skahaz"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1031213
	mother=4031213

	3265.1.1 = {birth="3265.1.1"}
	3281.1.1 = {add_trait=skilled_warrior}
	3283.1.1 = {add_spouse=4033213}
	3324.1.1 = {death="3324.1.1"}
}
4033213 = {
	name="Mezzala"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3265.1.1 = {birth="3265.1.1"}
	3324.1.1 = {death="3324.1.1"}
}
1034213 = {
	name="Ellezzara"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1031213
	mother=4031213

	3268.1.1 = {birth="3268.1.1"}
	3337.1.1 = {death="3337.1.1"}
}
1035213 = {
	name="Barghaz"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1031213
	mother=4031213

	3271.1.1 = {birth="3271.1.1"}
	3287.1.1 = {add_trait=trained_warrior}
	3295.1.1 = {death="3295.1.1"}
}
1036213 = {
	name="Reznak"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1033213
	mother=4033213

	3285.1.1 = {birth="3285.1.1"}
	3301.1.1 = {add_trait=poor_warrior}
	3341.1.1 = {death="3341.1.1"}
}
1037213 = {
	name="Galazza"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1033213
	mother=4033213

	3287.1.1 = {birth="3287.1.1"}
	3290.1.1 = {death="3290.1.1"}
}


######################  House na Yhezher

1000220 = {
	name="Paezhar"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	2980.1.1 = {birth="2980.1.1"}
	2996.1.1 = {add_trait=poor_warrior}
	2998.1.1 = {add_spouse=4000220}
	3035.1.1 = {death="3035.1.1"}
}
4000220 = {
	name="Yezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	2980.1.1 = {birth="2980.1.1"}
	3035.1.1 = {death="3035.1.1"}
}
1001220 = {
	name="Haznak"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1000220
	mother=4000220

	3006.1.1 = {birth="3006.1.1"}
	3022.1.1 = {add_trait=poor_warrior}
	3024.1.1 = {add_spouse=4001220}
	3074.1.1 = {death="3074.1.1"}
}
4001220 = {
	name="Zellazara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3006.1.1 = {birth="3006.1.1"}
	3074.1.1 = {death="3074.1.1"}
}
1002220 = {
	name="Fendahl"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1001220
	mother=4001220

	3030.1.1 = {birth="3030.1.1"}
	3046.1.1 = {add_trait=trained_warrior}
	3055.1.1 = {death="3055.1.1"}
}
1003220 = {
	name="Gezzella"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1001220
	mother=4001220

	3032.1.1 = {birth="3032.1.1"}
	3085.1.1 = {death="3085.1.1"}
}
1004220 = {
	name="Skahaz"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1001220
	mother=4001220

	3033.1.1 = {birth="3033.1.1"}
	3049.1.1 = {add_trait=poor_warrior}
	3051.1.1 = {add_spouse=4004220}
	3091.1.1 = {death="3091.1.1"}
}
4004220 = {
	name="Hazzea"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3033.1.1 = {birth="3033.1.1"}
	3091.1.1 = {death="3091.1.1"}
}
1005220 = {
	name="Skahaz"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1001220
	mother=4001220

	3035.1.1 = {birth="3035.1.1"}
	3051.1.1 = {add_trait=trained_warrior}
	3090.1.1 = {death="3090.1.1"}
}
1006220 = {
	name="Zaraq"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1004220
	mother=4004220

	3060.1.1 = {birth="3060.1.1"}
	3076.1.1 = {add_trait=poor_warrior}
	3078.1.1 = {add_spouse=4006220}
	3126.1.1 = {death="3126.1.1"}
}
4006220 = {
	name="Qazella"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3060.1.1 = {birth="3060.1.1"}
	3126.1.1 = {death="3126.1.1"}
}
1007220 = {
	name="Yezza"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1006220
	mother=4006220

	3091.1.1 = {birth="3091.1.1"}
	3155.1.1 = {death="3155.1.1"}
}
1008220 = {
	name="Gorzhak"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1006220
	mother=4006220

	3092.1.1 = {birth="3092.1.1"}
	3108.1.1 = {add_trait=trained_warrior}
	3110.1.1 = {add_spouse=4008220}
	3158.1.1 = {death="3158.1.1"}
}
4008220 = {
	name="Tezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3092.1.1 = {birth="3092.1.1"}
	3158.1.1 = {death="3158.1.1"}
}
1009220 = {
	name="Reznak"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1006220
	mother=4006220

	3093.1.1 = {birth="3093.1.1"}
	3109.1.1 = {add_trait=poor_warrior}
	3122.1.1 = {death="3122.1.1"}
}
1010220 = {
	name="Morghaz"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1006220
	mother=4006220

	3095.1.1 = {birth="3095.1.1"}
	3111.1.1 = {add_trait=trained_warrior}
	3167.1.1 = {death="3167.1.1"}
}
1011220 = {
	name="Mezzala"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1008220
	mother=4008220

	3126.1.1 = {birth="3126.1.1"}
	3175.1.1 = {death="3175.1.1"}
}
1012220 = {
	name="Ghazdor"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1008220
	mother=4008220

	3129.1.1 = {birth="3129.1.1"}
	3145.1.1 = {add_trait=poor_warrior}
	3147.1.1 = {add_spouse=4012220}
	3190.1.1 = {death="3190.1.1"}
}
4012220 = {
	name="Azzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3129.1.1 = {birth="3129.1.1"}
	3190.1.1 = {death="3190.1.1"}
}
1013220 = {
	name="Azzara"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1008220
	mother=4008220

	3132.1.1 = {birth="3132.1.1"}
	3164.1.1 = {death="3164.1.1"}
}
1014220 = {
	name="Barkhaz"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1008220
	mother=4008220

	3133.1.1 = {birth="3133.1.1"}
	3149.1.1 = {add_trait=trained_warrior}
	3210.1.1 = {death="3210.1.1"}
}
1015220 = {
	name="Yurkhaz"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1012220
	mother=4012220

	3155.1.1 = {birth="3155.1.1"}
	3171.1.1 = {add_trait=skilled_warrior}
	3173.1.1 = {add_spouse=4015220}
	3203.1.1 = {death="3203.1.1"}
}
4015220 = {
	name="Dhazzar"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3155.1.1 = {birth="3155.1.1"}
	3203.1.1 = {death="3203.1.1"}
}
1016220 = {
	name="Hazzea"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1012220
	mother=4012220

	3158.1.1 = {birth="3158.1.1"}
	3211.1.1 = {death="3211.1.1"}
}
1017220 = {
	name="Grazdhan"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1015220
	mother=4015220

	3174.1.1 = {birth="3174.1.1"}
	3190.1.1 = {add_trait=trained_warrior}
	3192.1.1 = {add_spouse=4017220}
	3224.1.1 = {death="3224.1.1"}
}
4017220 = {
	name="Dhazzar"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3174.1.1 = {birth="3174.1.1"}
	3224.1.1 = {death="3224.1.1"}
}
1018220 = {
	name="Barkhaz"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1015220
	mother=4015220

	3175.1.1 = {birth="3175.1.1"}
	3191.1.1 = {add_trait=poor_warrior}
	3244.1.1 = {death="3244.1.1"}
}
1019220 = {
	name="Bharkaz"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1017220
	mother=4017220

	3197.1.1 = {birth="3197.1.1"}
	3213.1.1 = {add_trait=skilled_warrior}
	3215.1.1 = {add_spouse=4019220}
	3250.1.1 = {death="3250.1.1"}
}
4019220 = {
	name="Jezhane"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3197.1.1 = {birth="3197.1.1"}
	3250.1.1 = {death="3250.1.1"}
}
1020220 = {
	name="Mellezzara"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1017220
	mother=4017220

	3198.1.1 = {birth="3198.1.1"}
	3228.1.1 = {death="3228.1.1"}
}
1021220 = {
	name="Skahaz"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1017220
	mother=4017220

	3200.1.1 = {birth="3200.1.1"}
	3216.1.1 = {add_trait=skilled_warrior}
	3238.1.1 = {death="3238.1.1"}
}
1022220 = {
	name="Azzara"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1017220
	mother=4017220

	3201.1.1 = {birth="3201.1.1"}
	3272.1.1 = {death="3272.1.1"}
}
1023220 = {
	name="Yarkhaz"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1019220
	mother=4019220

	3216.1.1 = {birth="3216.1.1"}
	3232.1.1 = {add_trait=trained_warrior}
	3234.1.1 = {add_spouse=4023220}
	3282.1.1 = {death="3282.1.1"}
}
4023220 = {
	name="Gezzella"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3216.1.1 = {birth="3216.1.1"}
	3282.1.1 = {death="3282.1.1"}
}
1024220 = {
	name="Tezza"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1019220
	mother=4019220

	3218.1.1 = {birth="3218.1.1"}
	3262.1.1 = {death="3262.1.1"}
}
1025220 = {
	name="Yezza"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1019220
	mother=4019220

	3220.1.1 = {birth="3220.1.1"}
	3273.1.1 = {death="3273.1.1"}
}
1026220 = {
	name="Yezzan"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1019220
	mother=4019220

	3221.1.1 = {birth="3221.1.1"}
	3237.1.1 = {add_trait=trained_warrior}
	3296.1.1 = {death="3296.1.1"}
}
1027220 = {
	name="Barkhaz"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1023220
	mother=4023220

	3251.1.1 = {birth="3251.1.1"}
	3267.1.1 = {add_trait=poor_warrior}
	3269.1.1 = {add_spouse=4027220}
	3305.1.1 = {death="3305.1.1"}
}
4027220 = {
	name="Tezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3251.1.1 = {birth="3251.1.1"}
	3305.1.1 = {death="3305.1.1"}
}
1028220 = {
	name="Mezzala"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1023220
	mother=4023220

	3252.1.1 = {birth="3252.1.1"}
	3302.1.1 = {death="3302.1.1"}
}
1029220 = {
	name="Tezza"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1023220
	mother=4023220

	3255.1.1 = {birth="3255.1.1"}
	3313.1.1 = {death="3313.1.1"}
}
1030220 = {
	name="Gezzella"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1023220
	mother=4023220

	3258.1.1 = {birth="3258.1.1"}
	3316.1.1 = {death="3316.1.1"}
}
1031220 = {
	name="Hazzaara"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1023220
	mother=4023220

	3261.1.1 = {birth="3261.1.1"}
	3333.1.1 = {death="3333.1.1"}
}
1032220 = {
	name="Azzara"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1027220
	mother=4027220

	3273.1.1 = {birth="3273.1.1"}
	3340.1.1 = {death="3340.1.1"}
}
1033220 = {
	name="Hizdhar"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1027220
	mother=4027220

	3274.1.1 = {birth="3274.1.1"}
	3290.1.1 = {add_trait=trained_warrior}
	3292.1.1 = {add_spouse=4033220}
	3318.1.1 = {death="3318.1.1"}
}
4033220 = {
	name="Hazzaara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	3274.1.1 = {birth="3274.1.1"}
	3318.1.1 = {death="3318.1.1"}
}
1034220 = {
	name="Kraznys"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1027220
	mother=4027220

	3277.1.1 = {birth="3277.1.1"}
	3293.1.1 = {add_trait=poor_warrior}
	3336.1.1 = {death="3336.1.1"}
}
1035220 = {
	name="Faezdhar"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1033220
	mother=4033220

	3297.1.1 = {birth="3297.1.1"}
	3313.1.1 = {add_trait=poor_warrior}
	3357.1.1 = {death="3357.1.1"}
}
1036220 = {
	name="Jezhane"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1033220
	mother=4033220

	3298.1.1 = {birth="3298.1.1"}
	3355.1.1 = {death="3355.1.1"}
}


