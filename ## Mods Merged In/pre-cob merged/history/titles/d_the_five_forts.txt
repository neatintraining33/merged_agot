1.1.1={
	liege="e_yi_ti"
	law = succ_appointment
	law = agnatic_succession
	effect = {
		set_title_flag = military_command
		holder_scope = { 
			if = {
				limit = { primary_title = { title = PREVPREV } }
				set_government_type = military_command_government 
				PREV = { succession = appointment }
				recalc_succession = yes
			}	
		}	
	}	
}
6474.1.1 = { 
	holder=10055954 # Gafur (nc)
} 
6505.1.1 = { holder=10155954 } # Hasan_Hasan (nc)
6534.1.1 = { holder=10655954 } # Mukhtar (nc)
6557.1.1 = { holder=10755954 } # Azam (nc)
6559.1.1 = { holder=11155954 } # Fadil (nc)
6601.1.1 = { holder=11455954 } # Mukhtar (nc)
6634.1.1 = { holder=11755954 } # Ramadan (nc)
6643.1.1 = { holder=11955954 } # Jibril (nc)
6675.1.1 = { holder=12155954 } # Uways (nc)
6699.1.1 = { holder=12355954 } # Burhanaddin (nc)
6730.1.1 = { holder=13055954 } # Bakr (nc)



