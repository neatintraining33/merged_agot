5778.1.1 = {
	law = slavery_2
	law = centralization_2
	law = first_night_0
	effect = {
		holder_scope = { 
			e_new_valyria = {
				holder_scope = {
					make_tributary = { who = PREVPREV tributary_type = valyrian_tributary }
				}
			}
		}	
	}
}
 
6474.12.16 = { holder=100174373 } # Trianna (C) 
6479.1.1 = { holder=100174372 } 
6488.1.1 = { holder=101174373 } # Haerys (nc)
6492.1.1 = { holder=100174372 } 
6503.1.1 = { holder=102174372 } 
6527.1.1 = { holder=105174372 } 
6554.1.1 = { holder=111174371 } # Belicho (nc)
6564.1.1 = { holder=115174371 } # Benerro (nc)
6587.1.1 = { holder=111174373 } # Colloquo (nc)
6592.1.1 = { holder=115174373 } # Vaeron (nc)
6616.1.1 = { holder=113174396 } # Methyso (nc)
6626.1.1 = { holder=115174396 } # Gorys (nc)
6645.1.1 = { holder=119174397 } # Methyso (nc)
6648.1.1 = { holder=125174397 } # Parquello (nc)
6681.1.1 = { holder=127174396 } # Matarys (nc)
6728.1.1 = { holder=131174396 } # Benerro (nc)
6734.1.1 = { holder=133174397 } # Qavo (nc)
6738.1.1 = { holder=139174397 } # Vogarro (C)
6744.1.1 = { holder=141174397 } # Qavo (nc)
6750.1.1 = { holder=137174372 } 
6754.1.1 = { holder=147174371 } # Malaquo
