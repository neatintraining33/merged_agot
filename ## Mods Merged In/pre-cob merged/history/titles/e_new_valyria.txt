1101.1.1 = {
	law = slavery_2
	law = centralization_2
	law = first_night_0
	law = republic_law_oligarchy
	law = noble_oligarchy_elective
	law = cognatic_succession
	effect = { add_law = republic_law_oligarchy }
}
6473.1.1 = { holder=10006013 } # Daeron (nc)
6515.1.1 = { holder=10016013 } # Rhaegel (nc)
6534.1.1 = { holder=10066013 } # Daeron (nc)
6571.1.1 = { holder=10106013 } # Vaermon (nc)
6582.1.1 = { holder=10136013 } # Orys (nc)
6616.1.1 = { holder=10196013 } # Daegar (nc)
6638.1.1 = { holder=10216013 } # Maegor (nc)
6648.1.1 = { holder=10246013 } # Aemon (nc)
6656.1.1 = { holder=10266013 } # Vaekar (nc)
6701.1.1 = { holder=10336013 } # Rhaekar (nc)
6763.1.1 = { holder=10356013 } # Daeron (nc)