#NOTE: The holder of this title has the House Harlaw dynasty tracker set in roberts_rebellion_events
1.1.1={ 
	law=succ_primogeniture 
	law=agnatic_succession
	liege="d_harlaw"
}
3929.1.1 = {
	holder = 500122
}
3976.1.1 = {
	holder = 501122
}
3993.1.1 = {
	holder = 0
}

6453.1.1 = { holder=300122 } # Ambrode (nc)
6476.2.1 = { holder=302122 } # Sigrin (nc)
6510.1.1 = { holder=308122 } # Cotter (nc)
6538.1.1 = { holder=310122 } # Tom (nc)
6568.1.1 = { holder=312122 } # Gormond (nc)
6593.1.1 = { holder=313122 } # Maron (nc)
6603.1.1 = { holder=316122 } # Kromm (nc)
6641.1.1 = {	
	holder=16122  #Theomore
}	
6679.1.1 = {	
	holder=14122  #Tristifer #(nc)
}
6712.1.1 = {	
	holder=6122  #Sigrin (nc)
}
6737.1.1 = {	
	holder=3122  #Sigfryd (nc)
}
6768.1.1 = {	
	holder=122 #Rodrik
}
