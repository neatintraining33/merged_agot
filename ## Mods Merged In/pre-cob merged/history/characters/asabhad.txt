#####################  Asabhad  #################################

###  Kataan
10055942 = {
	name="Wahab"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	6440.1.1 = {birth="6440.1.1"}
	6456.1.1 = {add_trait=poor_warrior}
	6458.1.1 = {add_spouse=40055942}
	6504.1.1 = {death="6504.1.1"}
}
40055942 = {
	name="Reshawna"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6440.1.1 = {birth="6440.1.1"}
	6504.1.1 = {death="6504.1.1"}
}
10155942 = {
	name="Paymaneh"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10055942
	mother=40055942

	6468.1.1 = {birth="6468.1.1"}
	6528.1.1 = {death="6528.1.1"}
}
10255942 = {
	name="Sahba"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10055942
	mother=40055942

	6469.1.1 = {birth="6469.1.1"}
	6474.1.1 = {death = {death_reason = death_accident}}
}
10355942 = {
	name="Jabir"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10055942
	mother=40055942

	6471.1.1 = {birth="6471.1.1"}
	6487.1.1 = {add_trait=poor_warrior}
	6489.1.1 = {add_spouse=40355942}
	6530.1.1 = {death="6530.1.1"}
}
40355942 = {
	name="Hanifa"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6471.1.1 = {birth="6471.1.1"}
	6530.1.1 = {death="6530.1.1"}
}
10455942 = {
	name="Ghalib"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10355942
	mother=40355942

	6492.1.1 = {birth="6492.1.1"}
	6508.1.1 = {add_trait=skilled_warrior}
	6510.1.1 = {add_spouse=40455942}
	6545.1.1 = {death="6545.1.1"}
}
40455942 = {
	name="Paymaneh"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6492.1.1 = {birth="6492.1.1"}
	6545.1.1 = {death="6545.1.1"}
}
10555942 = {
	name="Sheeftah"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10355942
	mother=40355942

	6493.1.1 = {birth="6493.1.1"}
	6545.1.1 = {death="6545.1.1"}
}
10655942 = {
	name="Yagana"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10355942
	mother=40355942

	6494.1.1 = {birth="6494.1.1"}
	6496.1.1 = {death="6496.1.1"}
}
10755942 = {
	name="Simin"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10455942
	mother=40455942

	6518.1.1 = {birth="6518.1.1"}
	6572.1.1 = {death="6572.1.1"}
}
10855942 = {
	name="Ramadan"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10455942
	mother=40455942

	6519.1.1 = {birth="6519.1.1"}
	6535.1.1 = {add_trait=poor_warrior}
	6537.1.1 = {add_spouse=40855942}
	6572.1.1 = {death="6572.1.1"}
}
40855942 = {
	name="Sholah"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6519.1.1 = {birth="6519.1.1"}
	6572.1.1 = {death="6572.1.1"}
}
10955942 = {
	name="Samira"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10455942
	mother=40455942

	6521.1.1 = {birth="6521.1.1"}
	6598.1.1 = {death="6598.1.1"}
}
11055942 = {
	name="Gafur"	# not a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10455942
	mother=40455942

	6524.1.1 = {birth="6524.1.1"}
	6540.1.1 = {add_trait=skilled_warrior}
	6556.1.1 = {death="6556.1.1"}
}
11155942 = {
	name="Wahab"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10855942
	mother=40855942

	6538.1.1 = {birth="6538.1.1"}
	6554.1.1 = {add_trait=poor_warrior}
	6556.1.1 = {add_spouse=41155942}
	6579.1.1 = {death="6579.1.1"}
}
41155942 = {
	name="Jahaira"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6538.1.1 = {birth="6538.1.1"}
	6579.1.1 = {death="6579.1.1"}
}
11255942 = {
	name="Fadl"	# not a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10855942
	mother=40855942

	6539.1.1 = {birth="6539.1.1"}
	6555.1.1 = {add_trait=skilled_warrior}
	6598.1.1 = {death="6598.1.1"}
}
11355942 = {
	name="Burhanaddin"	# not a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=10855942
	mother=40855942

	6542.1.1 = {birth="6542.1.1"}
	6558.1.1 = {add_trait=poor_warrior}
	6578.1.1 = {death="6578.1.1"}
}
11455942 = {
	name="Qawurd"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11155942
	mother=41155942

	6561.1.1 = {birth="6561.1.1"}
	6577.1.1 = {add_trait=trained_warrior}
	6579.1.1 = {add_spouse=41455942}
	6606.1.1 = {death="6606.1.1"}
}
41455942 = {
	name="Shararah"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6561.1.1 = {birth="6561.1.1"}
	6606.1.1 = {death="6606.1.1"}
}
11555942 = {
	name="Fadl"	# not a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11155942
	mother=41155942

	6562.1.1 = {birth="6562.1.1"}
	6578.1.1 = {add_trait=poor_warrior}
	6613.1.1 = {death="6613.1.1"}
}
11655942 = {
	name="Hafiz"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11455942
	mother=41455942

	6588.1.1 = {birth="6588.1.1"}
	6604.1.1 = {add_trait=trained_warrior}
	6606.1.1 = {add_spouse=41655942}
	6637.1.1 = {death="6637.1.1"}
}
41655942 = {
	name="Nyawela"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6588.1.1 = {birth="6588.1.1"}
	6637.1.1 = {death="6637.1.1"}
}
11755942 = {
	name="Simin"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11455942
	mother=41455942

	6590.1.1 = {birth="6590.1.1"}
	6644.1.1 = {death="6644.1.1"}
}
11855942 = {
	name="Shahzadah"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11455942
	mother=41455942

	6591.1.1 = {birth="6591.1.1"}
	6648.1.1 = {death="6648.1.1"}
}
11955942 = {
	name="Hanifa"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11455942
	mother=41455942

	6592.1.1 = {birth="6592.1.1"}
	6637.1.1 = {death="6637.1.1"}
}
12055942 = {
	name="Nizam"	# not a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11455942
	mother=41455942

	6594.1.1 = {birth="6594.1.1"}
	6610.1.1 = {add_trait=poor_warrior}
	6650.1.1 = {death="6650.1.1"}
}
12155942 = {
	name="Asiya"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11655942
	mother=41655942

	6613.1.1 = {birth="6613.1.1"}
	6667.1.1 = {death = {death_reason = death_accident}}
}
12255942 = {
	name="Abdullah"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11655942
	mother=41655942

	6615.1.1 = {birth="6615.1.1"}
	6631.1.1 = {add_trait=trained_warrior}
	6633.1.1 = {add_spouse=42255942}
	6668.1.1 = {death="6668.1.1"}
}
42255942 = {
	name="Zaynab"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6615.1.1 = {birth="6615.1.1"}
	6668.1.1 = {death="6668.1.1"}
}
12355942 = {
	name="Aghlab"	# not a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11655942
	mother=41655942

	6617.1.1 = {birth="6617.1.1"}
	6633.1.1 = {add_trait=trained_warrior}
	6679.1.1 = {death="6679.1.1"}
}
12455942 = {
	name="Layla"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=11655942
	mother=41655942

	6618.1.1 = {birth="6618.1.1"}
	6670.1.1 = {death="6670.1.1"}
}
12555942 = {
	name="Sheeftah"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=12255942
	mother=42255942

	6636.1.1 = {birth="6636.1.1"}
	6679.1.1 = {death="6679.1.1"}
}
12655942 = {
	name="Bahir"	# not a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=12255942
	mother=42255942

	6639.1.1 = {birth="6639.1.1"}
	6655.1.1 = {add_trait=trained_warrior}
	6663.1.1 = {death="6663.1.1"}
}
12755942 = {
	name="Ali"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=12255942
	mother=42255942

	6642.1.1 = {birth="6642.1.1"}
	6658.1.1 = {add_trait=trained_warrior}
	6660.1.1 = {add_spouse=42755942}
	6693.1.1 = {death="6693.1.1"}
}
42755942 = {
	name="Setara"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6642.1.1 = {birth="6642.1.1"}
	6693.1.1 = {death="6693.1.1"}
}
12855942 = {
	name="Taneen"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=12755942
	mother=42755942

	6668.1.1 = {birth="6668.1.1"}
	6728.1.1 = {death="6728.1.1"}
}
12955942 = {
	name="Alim"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=12755942
	mother=42755942

	6671.1.1 = {birth="6671.1.1"}
	6687.1.1 = {add_trait=skilled_warrior}
	6689.1.1 = {add_spouse=42955942}
	6734.1.1 = {death="6734.1.1"}
}
42955942 = {
	name="Simin"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6671.1.1 = {birth="6671.1.1"}
	6734.1.1 = {death="6734.1.1"}
}
13055942 = {
	name="Kamala"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=12755942
	mother=42755942

	6673.1.1 = {birth="6673.1.1"}
	6705.1.1 = {death="6705.1.1"}
}
13155942 = {
	name="Shamir"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=12955942
	mother=42955942

	6699.1.1 = {birth="6699.1.1"}
	6715.1.1 = {add_trait=poor_warrior}
	6717.1.1 = {add_spouse=43155942}
	6751.1.1 = {death="6751.1.1"}
}
43155942 = {
	name="Rafiqa"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6699.1.1 = {birth="6699.1.1"}
	6751.1.1 = {death="6751.1.1"}
}
13255942 = {
	name="Souzan"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=12955942
	mother=42955942

	6702.1.1 = {birth="6702.1.1"}
	6764.1.1 = {death="6764.1.1"}
}
13355942 = {
	name="Husam"	# not a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=12955942
	mother=42955942

	6704.1.1 = {birth="6704.1.1"}
	6720.1.1 = {add_trait=poor_warrior}
	6756.1.1 = {death="6756.1.1"}
}
13455942 = {
	name="Reshawna"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=12955942
	mother=42955942

	6705.1.1 = {birth="6705.1.1"}
	6744.1.1 = {death="6744.1.1"}
}
13555942 = {
	name="Shamir"	# a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=13155942
	mother=43155942

	6718.1.1 = {birth="6718.1.1"}
	6734.1.1 = {add_trait=poor_warrior}
	6736.1.1 = {add_spouse=43555942}
}
43555942 = {
	name="Rasa"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6718.1.1 = {birth="6718.1.1"}
}
13655942 = {
	name="Youkhanna"	# not a lord
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=13555942
	mother=43555942

	6742.1.1 = {birth="6742.1.1"}
	6758.1.1 = {add_trait=poor_warrior}
}
13755942 = {
	name="Shokouh"	# not a lord
	female=yes
	dynasty=55942

	religion="stone_cow"
	culture="asabhadi"

	father=13555942
	mother=43555942

	6744.1.1 = {birth="6744.1.1"}
}

###  House Issa
100055943 = {
	name="Uways"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	6447.1.1 = {birth="6447.1.1"}
	6463.1.1 = {add_trait=poor_warrior}
	6465.1.1 = {add_spouse=400055943}
	6514.1.1 = {death="6514.1.1"}
}
400055943 = {
	name="Faghira"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6447.1.1 = {birth="6447.1.1"}
	6514.1.1 = {death="6514.1.1"}
}
100155943 = {
	name="Muzaffaraddin"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100055943
	mother=400055943

	6473.1.1 = {birth="6473.1.1"}
	6489.1.1 = {add_trait=poor_warrior}
	6491.1.1 = {add_spouse=400155943}
	6526.1.1 = {death="6526.1.1"}
}
400155943 = {
	name="Parween"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6473.1.1 = {birth="6473.1.1"}
	6526.1.1 = {death="6526.1.1"}
}
100255943 = {
	name="Faghira"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100155943
	mother=400155943

	6491.1.1 = {birth="6491.1.1"}
	6548.1.1 = {death="6548.1.1"}
}
100355943 = {
	name="Youkhanna"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100155943
	mother=400155943

	6492.1.1 = {birth="6492.1.1"}
	6508.1.1 = {add_trait=trained_warrior}
	6510.1.1 = {add_spouse=400355943}
	6560.1.1 = {death="6560.1.1"}
}
400355943 = {
	name="Shararah"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6492.1.1 = {birth="6492.1.1"}
	6560.1.1 = {death="6560.1.1"}
}
100455943 = {
	name="Adila"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100155943
	mother=400155943

	6495.1.1 = {birth="6495.1.1"}
	6543.1.1 = {death="6543.1.1"}
}
100555943 = {
	name="Shola"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100155943
	mother=400155943

	6497.1.1 = {birth="6497.1.1"}
	6528.1.1 = {death="6528.1.1"}
}
100655943 = {
	name="Sheeva"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100155943
	mother=400155943

	6498.1.1 = {birth="6498.1.1"}
	6571.1.1 = {death="6571.1.1"}
}
100755943 = {
	name="Halil"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100355943
	mother=400355943

	6511.1.1 = {birth="6511.1.1"}
	6527.1.1 = {add_trait=trained_warrior}
	6529.1.1 = {add_spouse=400755943}
	6567.1.1 = {death="6567.1.1"}
}
400755943 = {
	name="Hanifa"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6511.1.1 = {birth="6511.1.1"}
	6567.1.1 = {death="6567.1.1"}
}
100855943 = {
	name="Yusuf"	# not a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100355943
	mother=400355943

	6512.1.1 = {birth="6512.1.1"}
	6528.1.1 = {add_trait=poor_warrior}
	6543.1.1 = {death="6543.1.1"}
}
100955943 = {
	name="Jahaira"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100355943
	mother=400355943

	6514.1.1 = {birth="6514.1.1"}
	6555.1.1 = {death = {death_reason = death_murder}}
}
101055943 = {
	name="Aghlab"	# not a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100355943
	mother=400355943

	6515.1.1 = {birth="6515.1.1"}
	6531.1.1 = {add_trait=trained_warrior}
	6571.1.1 = {death="6571.1.1"}
}
101155943 = {
	name="Hanifa"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100355943
	mother=400355943

	6516.1.1 = {birth="6516.1.1"}
	6559.1.1 = {death="6559.1.1"}
}
101255943 = {
	name="Muzaffaraddin"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100755943
	mother=400755943

	6529.1.1 = {birth="6529.1.1"}
	6545.1.1 = {add_trait=trained_warrior}
	6547.1.1 = {add_spouse=401255943}
	6570.1.1 = {death="6570.1.1"}
}
401255943 = {
	name="Kamala"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6529.1.1 = {birth="6529.1.1"}
	6570.1.1 = {death="6570.1.1"}
}
101355943 = {
	name="Nizam"	# not a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=100755943
	mother=400755943

	6530.1.1 = {birth="6530.1.1"}
	6546.1.1 = {add_trait=poor_warrior}
	6568.1.1 = {death="6568.1.1"}
}
101455943 = {
	name="Qawurd"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101255943
	mother=401255943

	6554.1.1 = {birth="6554.1.1"}
	6570.1.1 = {add_trait=trained_warrior}
	6572.1.1 = {add_spouse=401455943}
	6608.1.1 = {death="6608.1.1"}
}
401455943 = {
	name="Souzan"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6554.1.1 = {birth="6554.1.1"}
	6608.1.1 = {death="6608.1.1"}
}
101555943 = {
	name="Yusuf"	# not a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101255943
	mother=401255943

	6557.1.1 = {birth="6557.1.1"}
	6573.1.1 = {add_trait=trained_warrior}
	6606.1.1 = {death="6606.1.1"}
}
101655943 = {
	name="Mukhtar"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101455943
	mother=401455943

	6586.1.1 = {birth="6586.1.1"}
	6602.1.1 = {add_trait=poor_warrior}
	6604.1.1 = {add_spouse=401655943}
	6647.1.1 = {death="6647.1.1"}
}
401655943 = {
	name="Nyawela"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6586.1.1 = {birth="6586.1.1"}
	6647.1.1 = {death="6647.1.1"}
}
101755943 = {
	name="Sami"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101655943
	mother=401655943

	6614.1.1 = {birth="6614.1.1"}
	6630.1.1 = {add_trait=poor_warrior}
	6632.1.1 = {add_spouse=401755943}
	6648.1.1 = {death="6648.1.1"}
}
401755943 = {
	name="Habiba"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6614.1.1 = {birth="6614.1.1"}
	6648.1.1 = {death="6648.1.1"}
}
101855943 = {
	name="Yahya"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101755943
	mother=401755943

	6637.1.1 = {birth="6637.1.1"}
	6653.1.1 = {add_trait=trained_warrior}
	6655.1.1 = {add_spouse=401855943}
	6697.1.1 = {death="6697.1.1"}
}
401855943 = {
	name="Nyawela"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6637.1.1 = {birth="6637.1.1"}
	6697.1.1 = {death="6697.1.1"}
}
101955943 = {
	name="Nizam"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101855943
	mother=401855943

	6669.1.1 = {birth="6669.1.1"}
	6685.1.1 = {add_trait=poor_warrior}
	6687.1.1 = {add_spouse=401955943}
	6724.1.1 = {death = {death_reason = death_accident}}
}
401955943 = {
	name="Taliba"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6669.1.1 = {birth="6669.1.1"}
	6724.1.1 = {death="6724.1.1"}
}
102055943 = {
	name="Samira"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101855943
	mother=401855943

	6672.1.1 = {birth="6672.1.1"}
	6706.1.1 = {death="6706.1.1"}
}
102155943 = {
	name="Bakr"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101955943
	mother=401955943

	6692.1.1 = {birth="6692.1.1"}
	6708.1.1 = {add_trait=poor_warrior}
	6710.1.1 = {add_spouse=402155943}
	6771.1.1 = {death="6771.1.1"}
}
402155943 = {
	name="Maryam"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6692.1.1 = {birth="6692.1.1"}
	6771.1.1 = {death="6771.1.1"}
}
102255943 = {
	name="Habiba"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101955943
	mother=401955943

	6694.1.1 = {birth="6694.1.1"}
	6728.1.1 = {death="6728.1.1"}
}
102355943 = {
	name="Shokouh"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101955943
	mother=401955943

	6697.1.1 = {birth="6697.1.1"}
	6745.1.1 = {death="6745.1.1"}
}
102455943 = {
	name="Aarif"	# not a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=101955943
	mother=401955943

	6700.1.1 = {birth="6700.1.1"}
	6716.1.1 = {add_trait=trained_warrior}
	6766.1.1 = {death="6766.1.1"}
}
102555943 = {
	name="Samir"	# not a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=102155943
	mother=402155943

	6718.1.1 = {birth="6718.1.1"}
	6734.1.1 = {add_trait=skilled_warrior}
	6765.1.1 = {death="6765.1.1"}
}
102655943 = {
	name="Aghlab"	# a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=102155943
	mother=402155943

	6721.1.1 = {birth="6721.1.1"}
	6737.1.1 = {add_trait=trained_warrior}
	6739.1.1 = {add_spouse=402655943}
}
402655943 = {
	name="Saaman"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6721.1.1 = {birth="6721.1.1"}
}
102755943 = {
	name="Sholah"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=102155943
	mother=402155943

	6724.1.1 = {birth="6724.1.1"}
	6777.1.1 = {death="6777.1.1"}
}
102855943 = {
	name="Jahaira"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=102155943
	mother=402155943

	6726.1.1 = {birth="6726.1.1"}
}
102955943 = {
	name="Ramadan"	# not a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=102655943
	mother=402655943

	6739.1.1 = {birth="6739.1.1"}
	6755.1.1 = {add_trait=skilled_warrior}
	6757.1.1 = {add_spouse=402955943}
}
402955943 = {
	name="Faghira"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6739.1.1 = {birth="6739.1.1"}
}
103055943 = {
	name="Alim"	# not a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=102955943
	mother=402955943

	6772.1.1 = {birth="6772.1.1"}
	6788.1.1 = {add_trait=poor_warrior}
}
103155943 = {
	name="Bakr"	# not a lord
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=102955943
	mother=402955943

	6773.1.1 = {birth="6773.1.1"}
	6789.1.1 = {add_trait=poor_warrior}
}
103255943 = {
	name="Paymaneh"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=102955943
	mother=402955943

	6775.1.1 = {birth="6775.1.1"}
}
103355943 = {
	name="Nyawela"	# not a lord
	female=yes
	dynasty=55943

	religion="stone_cow"
	culture="asabhadi"

	father=102955943
	mother=402955943

	6776.1.1 = {birth="6776.1.1"}
}

###  House Kassis
100055944 = {
	name="Husam"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	6443.1.1 = {birth="6443.1.1"}
	6459.1.1 = {add_trait=poor_warrior}
	6461.1.1 = {add_spouse=400055944}
	6506.1.1 = {death="6506.1.1"}
}
400055944 = {
	name="Rashida"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6443.1.1 = {birth="6443.1.1"}
	6506.1.1 = {death="6506.1.1"}
}
100155944 = {
	name="Nizam"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100055944
	mother=400055944

	6471.1.1 = {birth="6471.1.1"}
	6487.1.1 = {add_trait=poor_warrior}
	6489.1.1 = {add_spouse=400155944}
	6512.1.1 = {death="6512.1.1"}
}
400155944 = {
	name="Tanaz"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6471.1.1 = {birth="6471.1.1"}
	6512.1.1 = {death="6512.1.1"}
}
100255944 = {
	name="Sabba"	# not a lord
	female=yes
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100055944
	mother=400055944

	6473.1.1 = {birth="6473.1.1"}
	6542.1.1 = {death="6542.1.1"}
}
100355944 = {
	name="Alim"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100155944
	mother=400155944

	6491.1.1 = {birth="6491.1.1"}
	6507.1.1 = {add_trait=poor_warrior}
	6509.1.1 = {add_spouse=400355944}
	6571.1.1 = {death="6571.1.1"}
}
400355944 = {
	name="Sahba"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6491.1.1 = {birth="6491.1.1"}
	6571.1.1 = {death="6571.1.1"}
}
100455944 = {
	name="Shameem"	# not a lord
	female=yes
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100155944
	mother=400155944

	6494.1.1 = {birth="6494.1.1"}
	6561.1.1 = {death="6561.1.1"}
}
100555944 = {
	name="Ramadan"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100355944
	mother=400355944

	6514.1.1 = {birth="6514.1.1"}
	6530.1.1 = {add_trait=skilled_warrior}
	6532.1.1 = {add_spouse=400555944}
	6572.1.1 = {death="6572.1.1"}
}
400555944 = {
	name="Saghar"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6514.1.1 = {birth="6514.1.1"}
	6572.1.1 = {death="6572.1.1"}
}
100655944 = {
	name="Jaleel"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100355944
	mother=400355944

	6517.1.1 = {birth="6517.1.1"}
	6533.1.1 = {add_trait=poor_warrior}
	6564.1.1 = {death="6564.1.1"}
}
100755944 = {
	name="Samir"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100355944
	mother=400355944

	6520.1.1 = {birth="6520.1.1"}
	6536.1.1 = {add_trait=poor_warrior}
	6568.1.1 = {death="6568.1.1"}
}
100855944 = {
	name="Mansur"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100555944
	mother=400555944

	6543.1.1 = {birth="6543.1.1"}
	6559.1.1 = {add_trait=trained_warrior}
	6561.1.1 = {add_spouse=400855944}
	6601.1.1 = {death="6601.1.1"}
}
400855944 = {
	name="Sheeftah"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6543.1.1 = {birth="6543.1.1"}
	6601.1.1 = {death="6601.1.1"}
}
100955944 = {
	name="Bahir"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100855944
	mother=400855944

	6565.1.1 = {birth="6565.1.1"}
	6581.1.1 = {add_trait=trained_warrior}
	6583.1.1 = {add_spouse=400955944}
	6617.1.1 = {death="6617.1.1"}
}
400955944 = {
	name="Amsha"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6565.1.1 = {birth="6565.1.1"}
	6617.1.1 = {death="6617.1.1"}
}
101055944 = {
	name="Ramadan"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100955944
	mother=400955944

	6590.1.1 = {birth="6590.1.1"}
	6606.1.1 = {add_trait=poor_warrior}
	6608.1.1 = {add_spouse=401055944}
	6649.1.1 = {death="6649.1.1"}
}
401055944 = {
	name="Shokouh"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6590.1.1 = {birth="6590.1.1"}
	6649.1.1 = {death="6649.1.1"}
}
101155944 = {
	name="Hafiz"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100955944
	mother=400955944

	6591.1.1 = {birth="6591.1.1"}
	6607.1.1 = {add_trait=poor_warrior}
	6629.1.1 = {death="6629.1.1"}
}
101255944 = {
	name="Mansur"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=100955944
	mother=400955944

	6594.1.1 = {birth="6594.1.1"}
	6610.1.1 = {add_trait=trained_warrior}
	6639.1.1 = {death="6639.1.1"}
}
101355944 = {
	name="Faruk"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=101055944
	mother=401055944

	6613.1.1 = {birth="6613.1.1"}
	6629.1.1 = {add_trait=master_warrior}
	6649.1.1 = {death="6649.1.1"}
}
101455944 = {
	name="Talib"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=101055944
	mother=401055944

	6615.1.1 = {birth="6615.1.1"}
	6631.1.1 = {add_trait=poor_warrior}
	6633.1.1 = {add_spouse=401455944}
	6683.1.1 = {death="6683.1.1"}
}
401455944 = {
	name="Nyawela"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6615.1.1 = {birth="6615.1.1"}
	6683.1.1 = {death="6683.1.1"}
}
101555944 = {
	name="Sadiq"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=101455944
	mother=401455944

	6644.1.1 = {birth="6644.1.1"}
	6660.1.1 = {add_trait=poor_warrior}
	6662.1.1 = {add_spouse=401555944}
	6697.1.1 = {death="6697.1.1"}
}
401555944 = {
	name="Yakta"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6644.1.1 = {birth="6644.1.1"}
	6697.1.1 = {death="6697.1.1"}
}
101655944 = {
	name="Mansur"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=101455944
	mother=401455944

	6645.1.1 = {birth="6645.1.1"}
	6661.1.1 = {add_trait=trained_warrior}
	6722.1.1 = {death="6722.1.1"}
}
101755944 = {
	name="Shokouh"	# not a lord
	female=yes
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=101455944
	mother=401455944

	6647.1.1 = {birth="6647.1.1"}
	6704.1.1 = {death="6704.1.1"}
}
101855944 = {
	name="Qadir"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=101455944
	mother=401455944

	6648.1.1 = {birth="6648.1.1"}
	6664.1.1 = {add_trait=skilled_warrior}
	6686.1.1 = {death = {death_reason = death_battle}}
}
101955944 = {
	name="Akin"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=101455944
	mother=401455944

	6650.1.1 = {birth="6650.1.1"}
	6666.1.1 = {add_trait=poor_warrior}
	6703.1.1 = {death="6703.1.1"}
}
102055944 = {
	name="Mirza"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=101555944
	mother=401555944

	6679.1.1 = {birth="6679.1.1"}
	6695.1.1 = {add_trait=trained_warrior}
	6697.1.1 = {add_spouse=402055944}
	6728.1.1 = {death="6728.1.1"}
}
402055944 = {
	name="Amsha"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6679.1.1 = {birth="6679.1.1"}
	6728.1.1 = {death="6728.1.1"}
}
102155944 = {
	name="Talib"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=102055944
	mother=402055944

	6698.1.1 = {birth="6698.1.1"}
	6714.1.1 = {add_trait=poor_warrior}
	6716.1.1 = {add_spouse=402155944}
	6744.1.1 = {death="6744.1.1"}
}
402155944 = {
	name="Paymaneh"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6698.1.1 = {birth="6698.1.1"}
	6744.1.1 = {death="6744.1.1"}
}
102255944 = {
	name="Zaynab"	# not a lord
	female=yes
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=102055944
	mother=402055944

	6701.1.1 = {birth="6701.1.1"}
	6738.1.1 = {death="6738.1.1"}
}
102355944 = {
	name="Husam"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=102055944
	mother=402055944

	6703.1.1 = {birth="6703.1.1"}
	6719.1.1 = {add_trait=poor_warrior}
	6772.1.1 = {death="6772.1.1"}
}
102455944 = {
	name="Azam"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=102155944
	mother=402155944

	6727.1.1 = {birth="6727.1.1"}
	6743.1.1 = {add_trait=poor_warrior}
	6745.1.1 = {add_spouse=402455944}
	6764.1.1 = {death = {death_reason = death_murder}}
}
402455944 = {
	name="Hanifa"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6727.1.1 = {birth="6727.1.1"}
	6764.1.1 = {death="6764.1.1"}
}
102555944 = {
	name="Sadiq"	# a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=102455944
	mother=402455944

	6751.1.1 = {birth="6751.1.1"}
	6767.1.1 = {add_trait=trained_warrior}
	6769.1.1 = {add_spouse=402555944}
}
402555944 = {
	name="Yakta"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6751.1.1 = {birth="6751.1.1"}
}
102655944 = {
	name="Halil"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=102455944
	mother=402455944

	6754.1.1 = {birth="6754.1.1"}
	6770.1.1 = {add_trait=poor_warrior}
}
102755944 = {
	name="Bakr"	# not a lord
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=102455944
	mother=402455944

	6756.1.1 = {birth="6756.1.1"}
	6772.1.1 = {add_trait=poor_warrior}
}
102855944 = {
	name="Sheeva"	# not a lord
	female=yes
	dynasty=55944

	religion="stone_cow"
	culture="asabhadi"

	father=102555944
	mother=402555944

	6776.1.1 = {birth="6776.1.1"}
}

###  House Kalzarria
100069165 = {
	name="Abdullah"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	6441.1.1 = {birth="6441.1.1"}
	6457.1.1 = {add_trait=poor_warrior}
	6459.1.1 = {add_spouse=400069165}
	6510.1.1 = {death="6510.1.1"}
}
400069165 = {
	name="Hanifa"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6441.1.1 = {birth="6441.1.1"}
	6510.1.1 = {death="6510.1.1"}
}
100169165 = {
	name="Ghalib"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100069165
	mother=400069165

	6466.1.1 = {birth="6466.1.1"}
	6482.1.1 = {add_trait=skilled_warrior}
	6484.1.1 = {add_spouse=400169165}
	6519.1.1 = {death="6519.1.1"}
}
400169165 = {
	name="Nyawela"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6466.1.1 = {birth="6466.1.1"}
	6519.1.1 = {death="6519.1.1"}
}
100269165 = {
	name="Fadl"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100169165
	mother=400169165

	6487.1.1 = {birth="6487.1.1"}
	6503.1.1 = {add_trait=poor_warrior}
	6505.1.1 = {add_spouse=400269165}
	6531.1.1 = {death="6531.1.1"}
}
400269165 = {
	name="Shahrbano"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6487.1.1 = {birth="6487.1.1"}
	6531.1.1 = {death="6531.1.1"}
}
100369165 = {
	name="Aarif"	# not a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100169165
	mother=400169165

	6489.1.1 = {birth="6489.1.1"}
	6505.1.1 = {add_trait=poor_warrior}
	6547.1.1 = {death="6547.1.1"}
}
100469165 = {
	name="Idris"	# not a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100169165
	mother=400169165

	6491.1.1 = {birth="6491.1.1"}
	6507.1.1 = {add_trait=poor_warrior}
	6525.1.1 = {death = {death_reason = death_murder}}
}
100569165 = {
	name="Amsha"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100169165
	mother=400169165

	6494.1.1 = {birth="6494.1.1"}
	6542.1.1 = {death="6542.1.1"}
}
100669165 = {
	name="Maryam"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100269165
	mother=400269165

	6512.1.1 = {birth="6512.1.1"}
	6543.1.1 = {death="6543.1.1"}
}
100769165 = {
	name="Shola"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100269165
	mother=400269165

	6514.1.1 = {birth="6514.1.1"}
	6567.1.1 = {death="6567.1.1"}
}
100869165 = {
	name="Is'mail"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100269165
	mother=400269165

	6516.1.1 = {birth="6516.1.1"}
	6532.1.1 = {add_trait=master_warrior}
	6534.1.1 = {add_spouse=400869165}
	6590.1.1 = {death="6590.1.1"}
}
400869165 = {
	name="Zaynab"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6516.1.1 = {birth="6516.1.1"}
	6590.1.1 = {death="6590.1.1"}
}
100969165 = {
	name="Youkhanna"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100869165
	mother=400869165

	6549.1.1 = {birth="6549.1.1"}
	6565.1.1 = {add_trait=poor_warrior}
	6567.1.1 = {add_spouse=400969165}
	6608.1.1 = {death="6608.1.1"}
}
400969165 = {
	name="Shola"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6549.1.1 = {birth="6549.1.1"}
	6608.1.1 = {death="6608.1.1"}
}
101069165 = {
	name="Paywand"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100869165
	mother=400869165

	6550.1.1 = {birth="6550.1.1"}
	6596.1.1 = {death="6596.1.1"}
}
101169165 = {
	name="Taliba"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100969165
	mother=400969165

	6583.1.1 = {birth="6583.1.1"}
	6615.1.1 = {death="6615.1.1"}
}
101269165 = {
	name="Khaireddin"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=100969165
	mother=400969165

	6584.1.1 = {birth="6584.1.1"}
	6600.1.1 = {add_trait=trained_warrior}
	6602.1.1 = {add_spouse=401269165}
	6623.1.1 = {death="6623.1.1"}
}
401269165 = {
	name="Setara"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6584.1.1 = {birth="6584.1.1"}
	6623.1.1 = {death="6623.1.1"}
}
101369165 = {
	name="Muslihiddin"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101269165
	mother=401269165

	6603.1.1 = {birth="6603.1.1"}
	6619.1.1 = {add_trait=poor_warrior}
	6621.1.1 = {add_spouse=401369165}
	6653.1.1 = {death="6653.1.1"}
}
401369165 = {
	name="Saaman"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6603.1.1 = {birth="6603.1.1"}
	6653.1.1 = {death="6653.1.1"}
}
101469165 = {
	name="Shogofa"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101269165
	mother=401269165

	6604.1.1 = {birth="6604.1.1"}
	6625.1.1 = {death = {death_reason = death_accident}}
}
101569165 = {
	name="Saghar"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101369165
	mother=401369165

	6630.1.1 = {birth="6630.1.1"}
	6686.1.1 = {death="6686.1.1"}
}
101669165 = {
	name="Jibril"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101369165
	mother=401369165

	6631.1.1 = {birth="6631.1.1"}
	6647.1.1 = {add_trait=trained_warrior}
	6649.1.1 = {add_spouse=401669165}
	6671.1.1 = {death="6671.1.1"}
}
401669165 = {
	name="Semeah"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6631.1.1 = {birth="6631.1.1"}
	6671.1.1 = {death="6671.1.1"}
}
101769165 = {
	name="Gafur"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101669165
	mother=401669165

	6654.1.1 = {birth="6654.1.1"}
	6670.1.1 = {add_trait=poor_warrior}
	6672.1.1 = {add_spouse=401769165}
	6729.1.1 = {death="6729.1.1"}
}
401769165 = {
	name="Habiba"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6654.1.1 = {birth="6654.1.1"}
	6729.1.1 = {death="6729.1.1"}
}
101869165 = {
	name="Yahya"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101769165
	mother=401769165

	6680.1.1 = {birth="6680.1.1"}
	6696.1.1 = {add_trait=poor_warrior}
	6698.1.1 = {add_spouse=401869165}
	6750.1.1 = {death="6750.1.1"}
}
401869165 = {
	name="Qamara"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6680.1.1 = {birth="6680.1.1"}
	6750.1.1 = {death="6750.1.1"}
}
101969165 = {
	name="Rashida"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101769165
	mother=401769165

	6681.1.1 = {birth="6681.1.1"}
	6714.1.1 = {death="6714.1.1"}
}
102069165 = {
	name="Muzaffaraddin"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101869165
	mother=401869165

	6707.1.1 = {birth="6707.1.1"}
	6723.1.1 = {add_trait=poor_warrior}
	6725.1.1 = {add_spouse=402069165}
	6769.1.1 = {death="6769.1.1"}
}
402069165 = {
	name="Kamala"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6707.1.1 = {birth="6707.1.1"}
	6769.1.1 = {death="6769.1.1"}
}
102169165 = {
	name="Qamara"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101869165
	mother=401869165

	6710.1.1 = {birth="6710.1.1"}
}
102269165 = {
	name="Semeah"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101869165
	mother=401869165

	6711.1.1 = {birth="6711.1.1"}
	6750.1.1 = {death="6750.1.1"}
}
102369165 = {
	name="Jaleel"	# not a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=101869165
	mother=401869165

	6714.1.1 = {birth="6714.1.1"}
	6730.1.1 = {add_trait=poor_warrior}
	6758.1.1 = {death="6758.1.1"}
}
102469165 = {
	name="Yusuf"	# a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=102069165
	mother=402069165

	6725.1.1 = {birth="6725.1.1"}
	6741.1.1 = {add_trait=poor_warrior}
	6743.1.1 = {add_spouse=402469165}
}
402469165 = {
	name="Sheeva"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6725.1.1 = {birth="6725.1.1"}
}
102569165 = {
	name="Sabba"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=102069165
	mother=402069165

	6727.1.1 = {birth="6727.1.1"}
}
102669165 = {
	name="Hafiz"	# not a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=102069165
	mother=402069165

	6728.1.1 = {birth="6728.1.1"}
	6744.1.1 = {add_trait=poor_warrior}
}
102769165 = {
	name="Fadl"	# not a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=102069165
	mother=402069165

	6730.1.1 = {birth="6730.1.1"}
	6746.1.1 = {add_trait=poor_warrior}
	6772.1.1 = {death="6772.1.1"}
}
102869165 = {
	name="Souzan"	# not a lord
	female=yes
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=102069165
	mother=402069165

	6731.1.1 = {birth="6731.1.1"}
}
102969165 = {
	name="Nasr"	# not a lord
	dynasty=69165

	religion="stone_cow"
	culture="asabhadi"

	father=102469165
	mother=402469165

	6753.1.1 = {birth="6753.1.1"}
	6769.1.1 = {add_trait=poor_warrior}
	6771.1.1 = {add_spouse=402969165}
}
402969165 = {
	name="Shameem"
	female=yes

	religion="stone_cow"
	culture="asabhadi"

	6753.1.1 = {birth="6753.1.1"}
}
