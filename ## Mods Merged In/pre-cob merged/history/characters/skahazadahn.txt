#############   House zo Pazak

1000208 = {
	name="Grazdhar"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	6452.1.1 = {birth="6452.1.1"}
	6468.1.1 = {add_trait=poor_warrior}
	6470.1.1 = {add_spouse=4000208}
	6508.1.1 = {death="6508.1.1"}
}
4000208 = {
	name="Yezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6452.1.1 = {birth="6452.1.1"}
	6508.1.1 = {death="6508.1.1"}
}
1001208 = {
	name="Yarkhaz"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1000208
	mother=4000208

	6485.1.1 = {birth="6485.1.1"}
	6501.1.1 = {add_trait=trained_warrior}
	6503.1.1 = {add_spouse=4001208}
	6560.1.1 = {death="6560.1.1"}
}
4001208 = {
	name="Yezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6485.1.1 = {birth="6485.1.1"}
	6560.1.1 = {death="6560.1.1"}
}
1002208 = {
	name="Morghaz"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1001208
	mother=4001208

	6510.1.1 = {birth="6510.1.1"}
	6526.1.1 = {add_trait=skilled_warrior}
	6528.1.1 = {add_spouse=4002208}
	6587.1.1 = {death = {death_reason = death_accident}}
}
4002208 = {
	name="Gezzella"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6510.1.1 = {birth="6510.1.1"}
	6587.1.1 = {death="6587.1.1"}
}
1003208 = {
	name="Maezon"	# not a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1001208
	mother=4001208

	6511.1.1 = {birth="6511.1.1"}
	6527.1.1 = {add_trait=trained_warrior}
	6585.1.1 = {death="6585.1.1"}
}
1004208 = {
	name="Zellazara"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1002208
	mother=4002208

	6541.1.1 = {birth="6541.1.1"}
	6585.1.1 = {death="6585.1.1"}
}
1005208 = {
	name="Reznak"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1002208
	mother=4002208

	6544.1.1 = {birth="6544.1.1"}
	6560.1.1 = {add_trait=poor_warrior}
	6562.1.1 = {add_spouse=4005208}
	6607.1.1 = {death="6607.1.1"}
}
4005208 = {
	name="Zellazara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6544.1.1 = {birth="6544.1.1"}
	6607.1.1 = {death="6607.1.1"}
}
1006208 = {
	name="Zaraq"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1005208
	mother=4005208

	6570.1.1 = {birth="6570.1.1"}
	6586.1.1 = {add_trait=skilled_warrior}
	6588.1.1 = {add_spouse=4006208}
	6633.1.1 = {death="6633.1.1"}
}
4006208 = {
	name="Qezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6570.1.1 = {birth="6570.1.1"}
	6633.1.1 = {death="6633.1.1"}
}
1007208 = {
	name="Hazzea"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1005208
	mother=4005208

	6573.1.1 = {birth="6573.1.1"}
	6615.1.1 = {death="6615.1.1"}
}
1008208 = {
	name="Ghazdor"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1006208
	mother=4006208

	6602.1.1 = {birth="6602.1.1"}
	6618.1.1 = {add_trait=skilled_warrior}
	6620.1.1 = {add_spouse=4008208}
	6671.1.1 = {death="6671.1.1"}
}
4008208 = {
	name="Selezzqa"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6602.1.1 = {birth="6602.1.1"}
	6671.1.1 = {death="6671.1.1"}
}
1009208 = {
	name="Grazdhar"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1008208
	mother=4008208

	6627.1.1 = {birth="6627.1.1"}
	6643.1.1 = {add_trait=poor_warrior}
	6645.1.1 = {add_spouse=4009208}
	6702.1.1 = {death="6702.1.1"}
}
4009208 = {
	name="Mezzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6627.1.1 = {birth="6627.1.1"}
	6702.1.1 = {death="6702.1.1"}
}
1010208 = {
	name="Mazdhan"	# not a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1008208
	mother=4008208

	6630.1.1 = {birth="6630.1.1"}
	6646.1.1 = {add_trait=skilled_warrior}
	6668.1.1 = {death="6668.1.1"}
}
1011208 = {
	name="Ellezzara"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1009208
	mother=4009208

	6660.1.1 = {birth="6660.1.1"}
	6697.1.1 = {death="6697.1.1"}
}
1012208 = {
	name="Fendahl"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1009208
	mother=4009208

	6661.1.1 = {birth="6661.1.1"}
	6677.1.1 = {add_trait=poor_warrior}
	6679.1.1 = {add_spouse=4012208}
	6716.1.1 = {death="6716.1.1"}
}
4012208 = {
	name="Qezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6661.1.1 = {birth="6661.1.1"}
	6716.1.1 = {death="6716.1.1"}
}
1013208 = {
	name="Yarkhaz"	# not a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1009208
	mother=4009208

	6662.1.1 = {birth="6662.1.1"}
	6678.1.1 = {add_trait=trained_warrior}
	6704.1.1 = {death="6704.1.1"}
}
1014208 = {
	name="Marghaz"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1012208
	mother=4012208

	6685.1.1 = {birth="6685.1.1"}
	6701.1.1 = {add_trait=poor_warrior}
	6703.1.1 = {add_spouse=4014208}
	6731.1.1 = {death="6731.1.1"}
}
4014208 = {
	name="Hazzea"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6685.1.1 = {birth="6685.1.1"}
	6731.1.1 = {death="6731.1.1"}
}
1015208 = {
	name="Reznak"	# not a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1012208
	mother=4012208

	6686.1.1 = {birth="6686.1.1"}
	6702.1.1 = {add_trait=poor_warrior}
	6757.1.1 = {death="6757.1.1"}
}
1016208 = {
	name="Dhazzar"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1012208
	mother=4012208

	6687.1.1 = {birth="6687.1.1"}
	6752.1.1 = {death="6752.1.1"}
}
1017208 = {
	name="Haznak"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1014208
	mother=4014208

	6709.1.1 = {birth="6709.1.1"}
	6725.1.1 = {add_trait=poor_warrior}
	6727.1.1 = {add_spouse=4017208}
	6761.1.1 = {death="6761.1.1"}
}
4017208 = {
	name="Jezhane"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6709.1.1 = {birth="6709.1.1"}
	6761.1.1 = {death="6761.1.1"}
}
1018208 = {
	name="Tezza"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1014208
	mother=4014208

	6711.1.1 = {birth="6711.1.1"}
	6770.1.1 = {death="6770.1.1"}
}
1019208 = {
	name="Selezzqa"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1014208
	mother=4014208

	6714.1.1 = {birth="6714.1.1"}
	6755.1.1 = {death="6755.1.1"}
}
1020208 = {
	name="Azzara"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1014208
	mother=4014208

	6716.1.1 = {birth="6716.1.1"}
	6768.1.1 = {death="6768.1.1"}
}
1021208 = {
	name="Skahaz"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1017208
	mother=4017208

	6737.1.1 = {birth="6737.1.1"}
	6753.1.1 = {add_trait=trained_warrior}
	6755.1.1 = {add_spouse=4021208}
	6794.1.1 = {death="6794.1.1"}
}
4021208 = {
	name="Ellezzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6737.1.1 = {birth="6737.1.1"}
	6794.1.1 = {death="6794.1.1"}
}
1022208 = {
	name="Yezza"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1017208
	mother=4017208

	6738.1.1 = {birth="6738.1.1"}
	6814.1.1 = {death="6814.1.1"}
}
1023208 = {
	name="Fendahl"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1021208
	mother=4021208

	6761.1.1 = {birth="6761.1.1"}
	6777.1.1 = {add_trait=poor_warrior}
	6809.1.1 = {death="6809.1.1"}
}


#################   House zo Zasnzn

1000213 = {
	name="Miklaz"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	6438.1.1 = {birth="6438.1.1"}
	6454.1.1 = {add_trait=poor_warrior}
	6456.1.1 = {add_spouse=4000213}
	6501.1.1 = {death="6501.1.1"}
}
4000213 = {
	name="Galazza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6438.1.1 = {birth="6438.1.1"}
	6501.1.1 = {death="6501.1.1"}
}
1001213 = {
	name="Skahaz"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1000213
	mother=4000213

	6465.1.1 = {birth="6465.1.1"}
	6481.1.1 = {add_trait=poor_warrior}
	6483.1.1 = {add_spouse=4001213}
	6521.1.1 = {death="6521.1.1"}
}
4001213 = {
	name="Galazza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6465.1.1 = {birth="6465.1.1"}
	6521.1.1 = {death="6521.1.1"}
}
1002213 = {
	name="Skahaz"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1000213
	mother=4000213

	6468.1.1 = {birth="6468.1.1"}
	6484.1.1 = {add_trait=poor_warrior}
	6535.1.1 = {death="6535.1.1"}
}
1003213 = {
	name="Azzak"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1000213
	mother=4000213

	6471.1.1 = {birth="6471.1.1"}
	6487.1.1 = {add_trait=trained_warrior}
	6520.1.1 = {death="6520.1.1"}
}
1004213 = {
	name="Yezza"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1000213
	mother=4000213

	6474.1.1 = {birth="6474.1.1"}
	6525.1.1 = {death="6525.1.1"}
}
1005213 = {
	name="Tellezzara"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1001213
	mother=4001213

	6489.1.1 = {birth="6489.1.1"}
	6568.1.1 = {death="6568.1.1"}
}
1006213 = {
	name="Sezza"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1001213
	mother=4001213

	6491.1.1 = {birth="6491.1.1"}
	6523.1.1 = {death="6523.1.1"}
}
1007213 = {
	name="Maezon"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1001213
	mother=4001213

	6494.1.1 = {birth="6494.1.1"}
	6510.1.1 = {add_trait=poor_warrior}
	6512.1.1 = {add_spouse=4007213}
	6535.1.1 = {death="6535.1.1"}
}
4007213 = {
	name="Hazzaara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6494.1.1 = {birth="6494.1.1"}
	6535.1.1 = {death="6535.1.1"}
}
1008213 = {
	name="Maezon"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1001213
	mother=4001213

	6496.1.1 = {birth="6496.1.1"}
	6512.1.1 = {add_trait=poor_warrior}
	6557.1.1 = {death = {death_reason = death_accident}}
}
1009213 = {
	name="Azzara"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1007213
	mother=4007213

	6512.1.1 = {birth="6512.1.1"}
	6563.1.1 = {death="6563.1.1"}
}
1010213 = {
	name="Hizdhar"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1007213
	mother=4007213

	6514.1.1 = {birth="6514.1.1"}
	6530.1.1 = {add_trait=trained_warrior}
	6532.1.1 = {add_spouse=4010213}
	6587.1.1 = {death="6587.1.1"}
}
4010213 = {
	name="Zellazara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6514.1.1 = {birth="6514.1.1"}
	6587.1.1 = {death="6587.1.1"}
}
1011213 = {
	name="Fendahl"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1010213
	mother=4010213

	6532.1.1 = {birth="6532.1.1"}
	6548.1.1 = {add_trait=master_warrior}
	6550.1.1 = {add_spouse=4011213}
	6590.1.1 = {death="6590.1.1"}
}
4011213 = {
	name="Selezzqa"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6532.1.1 = {birth="6532.1.1"}
	6590.1.1 = {death="6590.1.1"}
}
1012213 = {
	name="Azzea"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1010213
	mother=4010213

	6533.1.1 = {birth="6533.1.1"}
	6590.1.1 = {death = {death_reason = death_accident}}
}
1013213 = {
	name="Galazza"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1010213
	mother=4010213

	6536.1.1 = {birth="6536.1.1"}
	6586.1.1 = {death="6586.1.1"}
}
1014213 = {
	name="Dhazzar"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1010213
	mother=4010213

	6539.1.1 = {birth="6539.1.1"}
	6601.1.1 = {death="6601.1.1"}
}
1015213 = {
	name="Morghaz"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1011213
	mother=4011213

	6560.1.1 = {birth="6560.1.1"}
	6576.1.1 = {add_trait=poor_warrior}
	6578.1.1 = {add_spouse=4015213}
	6615.1.1 = {death="6615.1.1"}
}
4015213 = {
	name="Ellezzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6560.1.1 = {birth="6560.1.1"}
	6615.1.1 = {death="6615.1.1"}
}
1016213 = {
	name="Azzea"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1015213
	mother=4015213

	6592.1.1 = {birth="6592.1.1"}
	6654.1.1 = {death="6654.1.1"}
}
1017213 = {
	name="Paezhar"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1015213
	mother=4015213

	6594.1.1 = {birth="6594.1.1"}
	6610.1.1 = {add_trait=poor_warrior}
	6612.1.1 = {add_spouse=4017213}
	6662.1.1 = {death="6662.1.1"}
}
4017213 = {
	name="Azzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6594.1.1 = {birth="6594.1.1"}
	6662.1.1 = {death="6662.1.1"}
}
1018213 = {
	name="Qazella"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1015213
	mother=4015213

	6596.1.1 = {birth="6596.1.1"}
	6673.1.1 = {death="6673.1.1"}
}
1019213 = {
	name="Maezon"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1015213
	mother=4015213

	6597.1.1 = {birth="6597.1.1"}
	6613.1.1 = {add_trait=poor_warrior}
	6667.1.1 = {death = {death_reason = death_accident}}
}
1020213 = {
	name="Ghazdor"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1017213
	mother=4017213

	6621.1.1 = {birth="6621.1.1"}
	6637.1.1 = {add_trait=skilled_warrior}
	6651.1.1 = {death = {death_reason = death_murder}}
}
1021213 = {
	name="Mazdhan"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1017213
	mother=4017213

	6623.1.1 = {birth="6623.1.1"}
	6639.1.1 = {add_trait=poor_warrior}
	6641.1.1 = {add_spouse=4021213}
	6676.1.1 = {death="6676.1.1"}
}
4021213 = {
	name="Hazzea"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6623.1.1 = {birth="6623.1.1"}
	6676.1.1 = {death="6676.1.1"}
}
1022213 = {
	name="Faezdhar"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1021213
	mother=4021213

	6641.1.1 = {birth="6641.1.1"}
	6657.1.1 = {add_trait=poor_warrior}
	6659.1.1 = {add_spouse=4022213}
	6709.1.1 = {death="6709.1.1"}
}
4022213 = {
	name="Ezzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6641.1.1 = {birth="6641.1.1"}
	6709.1.1 = {death="6709.1.1"}
}
1023213 = {
	name="Kraznys"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1022213
	mother=4022213

	6670.1.1 = {birth="6670.1.1"}
	6686.1.1 = {add_trait=trained_warrior}
	6688.1.1 = {add_spouse=4023213}
	6745.1.1 = {death="6745.1.1"}
}
4023213 = {
	name="Qazella"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6670.1.1 = {birth="6670.1.1"}
	6745.1.1 = {death="6745.1.1"}
}
1024213 = {
	name="Haznak"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1022213
	mother=4022213

	6673.1.1 = {birth="6673.1.1"}
	6689.1.1 = {add_trait=poor_warrior}
	6718.1.1 = {death="6718.1.1"}
}
1025213 = {
	name="Ghazdor"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1022213
	mother=4022213

	6674.1.1 = {birth="6674.1.1"}
	6690.1.1 = {add_trait=skilled_warrior}
	6734.1.1 = {death="6734.1.1"}
}
1026213 = {
	name="Harghaz"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1022213
	mother=4022213

	6677.1.1 = {birth="6677.1.1"}
	6693.1.1 = {add_trait=poor_warrior}
	6734.1.1 = {death="6734.1.1"}
}
1027213 = {
	name="Gezzella"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1023213
	mother=4023213

	6697.1.1 = {birth="6697.1.1"}
	6730.1.1 = {death="6730.1.1"}
}
1028213 = {
	name="Jezhane"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1023213
	mother=4023213

	6700.1.1 = {birth="6700.1.1"}
	6773.1.1 = {death="6773.1.1"}
}
1029213 = {
	name="Ghazdor"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1023213
	mother=4023213

	6702.1.1 = {birth="6702.1.1"}
	6718.1.1 = {add_trait=poor_warrior}
	6720.1.1 = {add_spouse=4029213}
	6750.1.1 = {death="6750.1.1"}
}
4029213 = {
	name="Ezzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6702.1.1 = {birth="6702.1.1"}
	6750.1.1 = {death="6750.1.1"}
}
1030213 = {
	name="Sezqa"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1023213
	mother=4023213

	6704.1.1 = {birth="6704.1.1"}
	6778.1.1 = {death="6778.1.1"}
}
1031213 = {
	name="Paezhar"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1029213
	mother=4029213

	6724.1.1 = {birth="6724.1.1"}
	6740.1.1 = {add_trait=poor_warrior}
	6742.1.1 = {add_spouse=4031213}
	6767.1.1 = {death="6767.1.1"}
}
4031213 = {
	name="Qezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6724.1.1 = {birth="6724.1.1"}
	6767.1.1 = {death="6767.1.1"}
}
1032213 = {
	name="Selezzqa"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1031213
	mother=4031213

	6742.1.1 = {birth="6742.1.1"}
	6818.1.1 = {death="6818.1.1"}
}
1033213 = {
	name="Skahaz"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1031213
	mother=4031213

	6743.1.1 = {birth="6743.1.1"}
	6759.1.1 = {add_trait=skilled_warrior}
	6761.1.1 = {add_spouse=4033213}
	6802.1.1 = {death="6802.1.1"}
}
4033213 = {
	name="Mezzala"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6743.1.1 = {birth="6743.1.1"}
	6802.1.1 = {death="6802.1.1"}
}
1034213 = {
	name="Ellezzara"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1031213
	mother=4031213

	6746.1.1 = {birth="6746.1.1"}
	6815.1.1 = {death="6815.1.1"}
}
1035213 = {
	name="Barghaz"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1031213
	mother=4031213

	6749.1.1 = {birth="6749.1.1"}
	6765.1.1 = {add_trait=trained_warrior}
	6773.1.1 = {death="6773.1.1"}
}
1036213 = {
	name="Reznak"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1033213
	mother=4033213

	6763.1.1 = {birth="6763.1.1"}
	6779.1.1 = {add_trait=poor_warrior}
	6819.1.1 = {death="6819.1.1"}
}
1037213 = {
	name="Galazza"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1033213
	mother=4033213

	6765.1.1 = {birth="6765.1.1"}
	6768.1.1 = {death="6768.1.1"}
}


######################  House na Yhezher

1000220 = {
	name="Paezhar"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	6458.1.1 = {birth="6458.1.1"}
	6474.1.1 = {add_trait=poor_warrior}
	6476.1.1 = {add_spouse=4000220}
	6513.1.1 = {death="6513.1.1"}
}
4000220 = {
	name="Yezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6458.1.1 = {birth="6458.1.1"}
	6513.1.1 = {death="6513.1.1"}
}
1001220 = {
	name="Haznak"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1000220
	mother=4000220

	6484.1.1 = {birth="6484.1.1"}
	6500.1.1 = {add_trait=poor_warrior}
	6502.1.1 = {add_spouse=4001220}
	6552.1.1 = {death="6552.1.1"}
}
4001220 = {
	name="Zellazara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6484.1.1 = {birth="6484.1.1"}
	6552.1.1 = {death="6552.1.1"}
}
1002220 = {
	name="Fendahl"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1001220
	mother=4001220

	6508.1.1 = {birth="6508.1.1"}
	6524.1.1 = {add_trait=trained_warrior}
	6533.1.1 = {death="6533.1.1"}
}
1003220 = {
	name="Gezzella"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1001220
	mother=4001220

	6510.1.1 = {birth="6510.1.1"}
	6563.1.1 = {death="6563.1.1"}
}
1004220 = {
	name="Skahaz"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1001220
	mother=4001220

	6511.1.1 = {birth="6511.1.1"}
	6527.1.1 = {add_trait=poor_warrior}
	6529.1.1 = {add_spouse=4004220}
	6569.1.1 = {death="6569.1.1"}
}
4004220 = {
	name="Hazzea"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6511.1.1 = {birth="6511.1.1"}
	6569.1.1 = {death="6569.1.1"}
}
1005220 = {
	name="Skahaz"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1001220
	mother=4001220

	6513.1.1 = {birth="6513.1.1"}
	6529.1.1 = {add_trait=trained_warrior}
	6568.1.1 = {death="6568.1.1"}
}
1006220 = {
	name="Zaraq"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1004220
	mother=4004220

	6538.1.1 = {birth="6538.1.1"}
	6554.1.1 = {add_trait=poor_warrior}
	6556.1.1 = {add_spouse=4006220}
	6604.1.1 = {death="6604.1.1"}
}
4006220 = {
	name="Qazella"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6538.1.1 = {birth="6538.1.1"}
	6604.1.1 = {death="6604.1.1"}
}
1007220 = {
	name="Yezza"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1006220
	mother=4006220

	6569.1.1 = {birth="6569.1.1"}
	6633.1.1 = {death="6633.1.1"}
}
1008220 = {
	name="Gorzhak"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1006220
	mother=4006220

	6570.1.1 = {birth="6570.1.1"}
	6586.1.1 = {add_trait=trained_warrior}
	6588.1.1 = {add_spouse=4008220}
	6636.1.1 = {death="6636.1.1"}
}
4008220 = {
	name="Tezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6570.1.1 = {birth="6570.1.1"}
	6636.1.1 = {death="6636.1.1"}
}
1009220 = {
	name="Reznak"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1006220
	mother=4006220

	6571.1.1 = {birth="6571.1.1"}
	6587.1.1 = {add_trait=poor_warrior}
	6600.1.1 = {death="6600.1.1"}
}
1010220 = {
	name="Morghaz"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1006220
	mother=4006220

	6573.1.1 = {birth="6573.1.1"}
	6589.1.1 = {add_trait=trained_warrior}
	6645.1.1 = {death="6645.1.1"}
}
1011220 = {
	name="Mezzala"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1008220
	mother=4008220

	6604.1.1 = {birth="6604.1.1"}
	6653.1.1 = {death="6653.1.1"}
}
1012220 = {
	name="Ghazdor"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1008220
	mother=4008220

	6607.1.1 = {birth="6607.1.1"}
	6623.1.1 = {add_trait=poor_warrior}
	6625.1.1 = {add_spouse=4012220}
	6668.1.1 = {death="6668.1.1"}
}
4012220 = {
	name="Azzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6607.1.1 = {birth="6607.1.1"}
	6668.1.1 = {death="6668.1.1"}
}
1013220 = {
	name="Azzara"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1008220
	mother=4008220

	6610.1.1 = {birth="6610.1.1"}
	6642.1.1 = {death="6642.1.1"}
}
1014220 = {
	name="Barkhaz"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1008220
	mother=4008220

	6611.1.1 = {birth="6611.1.1"}
	6627.1.1 = {add_trait=trained_warrior}
	6688.1.1 = {death="6688.1.1"}
}
1015220 = {
	name="Yurkhaz"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1012220
	mother=4012220

	6633.1.1 = {birth="6633.1.1"}
	6649.1.1 = {add_trait=skilled_warrior}
	6651.1.1 = {add_spouse=4015220}
	6681.1.1 = {death="6681.1.1"}
}
4015220 = {
	name="Dhazzar"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6633.1.1 = {birth="6633.1.1"}
	6681.1.1 = {death="6681.1.1"}
}
1016220 = {
	name="Hazzea"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1012220
	mother=4012220

	6636.1.1 = {birth="6636.1.1"}
	6689.1.1 = {death="6689.1.1"}
}
1017220 = {
	name="Grazdhan"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1015220
	mother=4015220

	6652.1.1 = {birth="6652.1.1"}
	6668.1.1 = {add_trait=trained_warrior}
	6670.1.1 = {add_spouse=4017220}
	6702.1.1 = {death="6702.1.1"}
}
4017220 = {
	name="Dhazzar"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6652.1.1 = {birth="6652.1.1"}
	6702.1.1 = {death="6702.1.1"}
}
1018220 = {
	name="Barkhaz"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1015220
	mother=4015220

	6653.1.1 = {birth="6653.1.1"}
	6669.1.1 = {add_trait=poor_warrior}
	6722.1.1 = {death="6722.1.1"}
}
1019220 = {
	name="Bharkaz"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1017220
	mother=4017220

	6675.1.1 = {birth="6675.1.1"}
	6691.1.1 = {add_trait=skilled_warrior}
	6693.1.1 = {add_spouse=4019220}
	6728.1.1 = {death="6728.1.1"}
}
4019220 = {
	name="Jezhane"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6675.1.1 = {birth="6675.1.1"}
	6728.1.1 = {death="6728.1.1"}
}
1020220 = {
	name="Mellezzara"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1017220
	mother=4017220

	6676.1.1 = {birth="6676.1.1"}
	6706.1.1 = {death="6706.1.1"}
}
1021220 = {
	name="Skahaz"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1017220
	mother=4017220

	6678.1.1 = {birth="6678.1.1"}
	6694.1.1 = {add_trait=skilled_warrior}
	6716.1.1 = {death="6716.1.1"}
}
1022220 = {
	name="Azzara"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1017220
	mother=4017220

	6679.1.1 = {birth="6679.1.1"}
	6750.1.1 = {death="6750.1.1"}
}
1023220 = {
	name="Yarkhaz"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1019220
	mother=4019220

	6694.1.1 = {birth="6694.1.1"}
	6710.1.1 = {add_trait=trained_warrior}
	6712.1.1 = {add_spouse=4023220}
	6760.1.1 = {death="6760.1.1"}
}
4023220 = {
	name="Gezzella"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6694.1.1 = {birth="6694.1.1"}
	6760.1.1 = {death="6760.1.1"}
}
1024220 = {
	name="Tezza"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1019220
	mother=4019220

	6696.1.1 = {birth="6696.1.1"}
	6740.1.1 = {death="6740.1.1"}
}
1025220 = {
	name="Yezza"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1019220
	mother=4019220

	6698.1.1 = {birth="6698.1.1"}
	6751.1.1 = {death="6751.1.1"}
}
1026220 = {
	name="Yezzan"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1019220
	mother=4019220

	6699.1.1 = {birth="6699.1.1"}
	6715.1.1 = {add_trait=trained_warrior}
	6774.1.1 = {death="6774.1.1"}
}
1027220 = {
	name="Barkhaz"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1023220
	mother=4023220

	6729.1.1 = {birth="6729.1.1"}
	6745.1.1 = {add_trait=poor_warrior}
	6747.1.1 = {add_spouse=4027220}
	6783.1.1 = {death="6783.1.1"}
}
4027220 = {
	name="Tezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6729.1.1 = {birth="6729.1.1"}
	6783.1.1 = {death="6783.1.1"}
}
1028220 = {
	name="Mezzala"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1023220
	mother=4023220

	6730.1.1 = {birth="6730.1.1"}
	6780.1.1 = {death="6780.1.1"}
}
1029220 = {
	name="Tezza"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1023220
	mother=4023220

	6733.1.1 = {birth="6733.1.1"}
	6791.1.1 = {death="6791.1.1"}
}
1030220 = {
	name="Gezzella"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1023220
	mother=4023220

	6736.1.1 = {birth="6736.1.1"}
	6794.1.1 = {death="6794.1.1"}
}
1031220 = {
	name="Hazzaara"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1023220
	mother=4023220

	6739.1.1 = {birth="6739.1.1"}
	6811.1.1 = {death="6811.1.1"}
}
1032220 = {
	name="Azzara"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1027220
	mother=4027220

	6751.1.1 = {birth="6751.1.1"}
	6818.1.1 = {death="6818.1.1"}
}
1033220 = {
	name="Hizdhar"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1027220
	mother=4027220

	6752.1.1 = {birth="6752.1.1"}
	6768.1.1 = {add_trait=trained_warrior}
	6770.1.1 = {add_spouse=4033220}
	6796.1.1 = {death="6796.1.1"}
}
4033220 = {
	name="Hazzaara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	6752.1.1 = {birth="6752.1.1"}
	6796.1.1 = {death="6796.1.1"}
}
1034220 = {
	name="Kraznys"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1027220
	mother=4027220

	6755.1.1 = {birth="6755.1.1"}
	6771.1.1 = {add_trait=poor_warrior}
	6814.1.1 = {death="6814.1.1"}
}
1035220 = {
	name="Faezdhar"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1033220
	mother=4033220

	6775.1.1 = {birth="6775.1.1"}
	6791.1.1 = {add_trait=poor_warrior}
	6835.1.1 = {death="6835.1.1"}
}
1036220 = {
	name="Jezhane"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1033220
	mother=4033220

	6776.1.1 = {birth="6776.1.1"}
	6833.1.1 = {death="6833.1.1"}
}


