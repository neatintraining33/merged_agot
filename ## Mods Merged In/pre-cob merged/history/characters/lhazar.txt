###Izven of Lhazosh###
10055916 = {
	name="Jommo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	6443.1.1 = {birth="6443.1.1"}
	6459.1.1 = {add_trait=poor_warrior}
	6461.1.1 = {add_spouse=40055916}
	6496.1.1 = {death="6496.1.1"}
}
40055916 = {
	name="Jhiqui"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6443.1.1 = {birth="6443.1.1"}
	6496.1.1 = {death="6496.1.1"}
}
10155916 = {
	name="Jhigi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10055916
	mother=40055916

	6462.1.1 = {birth="6462.1.1"}
	6526.1.1 = {death="6526.1.1"}
}
10255916 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10055916
	mother=40055916

	6464.1.1 = {birth="6464.1.1"}
	6507.1.1 = {death="6507.1.1"}
}
10355916 = {
	name="Fogo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10055916
	mother=40055916

	6465.1.1 = {birth="6465.1.1"}
	6481.1.1 = {add_trait=poor_warrior}
	6483.1.1 = {add_spouse=40355916}
	6513.1.1 = {death="6513.1.1"}
}
40355916 = {
	name="Zikki"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6465.1.1 = {birth="6465.1.1"}
	6513.1.1 = {death="6513.1.1"}
}
10455916 = {
	name="Rommo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10055916
	mother=40055916

	6466.1.1 = {birth="6466.1.1"}
	6482.1.1 = {add_trait=poor_warrior}
	6506.1.1 = {death="6506.1.1"}
}
10555916 = {
	name="Rommo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10055916
	mother=40055916

	6468.1.1 = {birth="6468.1.1"}
	6484.1.1 = {add_trait=poor_warrior}
	6544.1.1 = {death="6544.1.1"}
}
10655916 = {
	name="Zollo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10355916
	mother=40355916

	6494.1.1 = {birth="6494.1.1"}
	6510.1.1 = {add_trait=trained_warrior}
	6512.1.1 = {add_spouse=40655916}
	6565.1.1 = {death="6565.1.1"}
}
40655916 = {
	name="Zikki"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6494.1.1 = {birth="6494.1.1"}
	6565.1.1 = {death="6565.1.1"}
}
10755916 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10355916
	mother=40355916

	6495.1.1 = {birth="6495.1.1"}
	6532.1.1 = {death="6532.1.1"}
}
10855916 = {
	name="Aggo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10655916
	mother=40655916

	6515.1.1 = {birth="6515.1.1"}
	6531.1.1 = {add_trait=poor_warrior}
	6533.1.1 = {add_spouse=40855916}
	6572.1.1 = {death="6572.1.1"}
}
40855916 = {
	name="Jhigi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6515.1.1 = {birth="6515.1.1"}
	6572.1.1 = {death="6572.1.1"}
}
10955916 = {
	name="Zolli"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10655916
	mother=40655916

	6518.1.1 = {birth="6518.1.1"}
	6587.1.1 = {death="6587.1.1"}
}
11055916 = {
	name="Rhogoro"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10655916
	mother=40655916

	6519.1.1 = {birth="6519.1.1"}
	6535.1.1 = {add_trait=trained_warrior}
	6576.1.1 = {death="6576.1.1"}
}
11155916 = {
	name="Iggo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10855916
	mother=40855916

	6540.1.1 = {birth="6540.1.1"}
	6556.1.1 = {add_trait=trained_warrior}
	6558.1.1 = {add_spouse=41155916}
	6616.1.1 = {death="6616.1.1"}
}
41155916 = {
	name="Rimmi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6540.1.1 = {birth="6540.1.1"}
	6616.1.1 = {death="6616.1.1"}
}
11255916 = {
	name="Arakh"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10855916
	mother=40855916

	6542.1.1 = {birth="6542.1.1"}
	6558.1.1 = {add_trait=poor_warrior}
	6560.1.1 = {add_spouse=41255916}
	6603.1.1 = {death="6603.1.1"}
}
41255916 = {
	name="Coholli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6542.1.1 = {birth="6542.1.1"}
	6603.1.1 = {death="6603.1.1"}
}
11355916 = {
	name="Quiri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11155916
	mother=41155916

	6562.1.1 = {birth="6562.1.1"}
	6641.1.1 = {death="6641.1.1"}
}
11455916 = {
	name="Mago"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11255916
	mother=41255916

	6567.1.1 = {birth="6567.1.1"}
	6583.1.1 = {add_trait=trained_warrior}
	6585.1.1 = {add_spouse=41455916}
	6609.1.1 = {death="6609.1.1"}
}
41455916 = {
	name="Coholli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6567.1.1 = {birth="6567.1.1"}
	6609.1.1 = {death="6609.1.1"}
}
11555916 = {
	name="Fogo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11255916
	mother=41255916

	6569.1.1 = {birth="6569.1.1"}
	6585.1.1 = {add_trait=poor_warrior}
	6622.1.1 = {death="6622.1.1"}
}
11655916 = {
	name="Zolli"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11255916
	mother=41255916

	6570.1.1 = {birth="6570.1.1"}
	6635.1.1 = {death="6635.1.1"}
}
11755916 = {
	name="Ogo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11455916
	mother=41455916

	6586.1.1 = {birth="6586.1.1"}
	6602.1.1 = {add_trait=poor_warrior}
	6604.1.1 = {add_spouse=41755916}
	6654.1.1 = {death="6654.1.1"}
}
41755916 = {
	name="Mirri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6586.1.1 = {birth="6586.1.1"}
	6654.1.1 = {death="6654.1.1"}
}
11855916 = {
	name="Jhigi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11455916
	mother=41455916

	6587.1.1 = {birth="6587.1.1"}
	6645.1.1 = {death="6645.1.1"}
}
11955916 = {
	name="Mago"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11455916
	mother=41455916

	6589.1.1 = {birth="6589.1.1"}
	6605.1.1 = {add_trait=poor_warrior}
	6636.1.1 = {death="6636.1.1"}
}
12055916 = {
	name="Qothi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11455916
	mother=41455916

	6591.1.1 = {birth="6591.1.1"}
	6620.1.1 = {death = {death_reason = death_murder}}
}
12155916 = {
	name="Zollo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11755916
	mother=41755916

	6605.1.1 = {birth="6605.1.1"}
	6621.1.1 = {add_trait=trained_warrior}
	6623.1.1 = {add_spouse=42155916}
	6663.1.1 = {death="6663.1.1"}
}
42155916 = {
	name="Zikki"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6605.1.1 = {birth="6605.1.1"}
	6663.1.1 = {death="6663.1.1"}
}
12255916 = {
	name="Rakharo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11755916
	mother=41755916

	6607.1.1 = {birth="6607.1.1"}
	6623.1.1 = {add_trait=trained_warrior}
	6638.1.1 = {death="6638.1.1"}
}
12355916 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11755916
	mother=41755916

	6608.1.1 = {birth="6608.1.1"}
	6656.1.1 = {death="6656.1.1"}
}
12455916 = {
	name="Mago"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11755916
	mother=41755916

	6610.1.1 = {birth="6610.1.1"}
	6626.1.1 = {add_trait=poor_warrior}
	6682.1.1 = {death="6682.1.1"}
}
12555916 = {
	name="Rimmi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11755916
	mother=41755916

	6611.1.1 = {birth="6611.1.1"}
	6655.1.1 = {death="6655.1.1"}
}
12655916 = {
	name="Bharbo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12155916
	mother=42155916

	6631.1.1 = {birth="6631.1.1"}
	6647.1.1 = {add_trait=poor_warrior}
	6649.1.1 = {add_spouse=42655916}
	6676.1.1 = {death="6676.1.1"}
}
42655916 = {
	name="Zikki"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6631.1.1 = {birth="6631.1.1"}
	6676.1.1 = {death="6676.1.1"}
}
12755916 = {
	name="Rimmi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12155916
	mother=42155916

	6632.1.1 = {birth="6632.1.1"}
	6681.1.1 = {death="6681.1.1"}
}
12855916 = {
	name="Zolli"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12155916
	mother=42155916

	6633.1.1 = {birth="6633.1.1"}
	6681.1.1 = {death = {death_reason = death_accident}}
}
12955916 = {
	name="Haggo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12655916
	mother=42655916

	6652.1.1 = {birth="6652.1.1"}
	6668.1.1 = {add_trait=trained_warrior}
	6670.1.1 = {add_spouse=42955916}
	6717.1.1 = {death="6717.1.1"}
}
42955916 = {
	name="Jhigi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6652.1.1 = {birth="6652.1.1"}
	6717.1.1 = {death="6717.1.1"}
}
13055916 = {
	name="Rakharo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12655916
	mother=42655916

	6653.1.1 = {birth="6653.1.1"}
	6669.1.1 = {add_trait=trained_warrior}
	6671.1.1 = {add_spouse=43055916}
	6703.1.1 = {death="6703.1.1"}
}
43055916 = {
	name="Irri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6653.1.1 = {birth="6653.1.1"}
	6703.1.1 = {death="6703.1.1"}
}
13155916 = {
	name="Mothi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12655916
	mother=42655916

	6655.1.1 = {birth="6655.1.1"}
	6705.1.1 = {death="6705.1.1"}
}
13255916 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12955916
	mother=42955916

	6679.1.1 = {birth="6679.1.1"}
	6723.1.1 = {death="6723.1.1"}
}
13355916 = {
	name="Rakhiri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13055916
	mother=43055916

	6678.1.1 = {birth="6678.1.1"}
	6754.1.1 = {death="6754.1.1"}
}
13455916 = {
	name="Cohollo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13055916
	mother=43055916

	6679.1.1 = {birth="6679.1.1"}
	6695.1.1 = {add_trait=poor_warrior}
	6697.1.1 = {add_spouse=43455916}
	6744.1.1 = {death="6744.1.1"}
}
43455916 = {
	name="Coholli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6679.1.1 = {birth="6679.1.1"}
	6744.1.1 = {death="6744.1.1"}
}
13555916 = {
	name="Rakharo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13055916
	mother=43055916

	6680.1.1 = {birth="6680.1.1"}
	6696.1.1 = {add_trait=poor_warrior}
	6733.1.1 = {death="6733.1.1"}
}
13655916 = {
	name="Pono"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13455916
	mother=43455916

	6712.1.1 = {birth="6712.1.1"}
	6728.1.1 = {add_trait=trained_warrior}
	6730.1.1 = {add_spouse=43655916}
	6765.1.1 = {death="6765.1.1"}
}
43655916 = {
	name="Jhiqui"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6712.1.1 = {birth="6712.1.1"}
	6765.1.1 = {death="6765.1.1"}
}
13755916 = {
	name="Rhogoro"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13455916
	mother=43455916

	6715.1.1 = {birth="6715.1.1"}
	6731.1.1 = {add_trait=trained_warrior}
	6733.1.1 = {add_spouse=43755916}
	6747.1.1 = {death = {death_reason = death_murder}}
}
43755916 = {
	name="Kovarri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6715.1.1 = {birth="6715.1.1"}
	6747.1.1 = {death="6747.1.1"}
}
13855916 = {
	name="Qotho"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13455916
	mother=43455916

	6718.1.1 = {birth="6718.1.1"}
	6734.1.1 = {add_trait=poor_warrior}
	6768.1.1 = {death="6768.1.1"}
}
13955916 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13655916
	mother=43655916

	6736.1.1 = {birth="6736.1.1"}
}
14055916 = {
	name="Rakharo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13755916
	mother=43755916

	6735.1.1 = {birth="6735.1.1"}
	6751.1.1 = {add_trait=poor_warrior}
	6764.1.1 = {add_spouse=44055916}
}
44055916 = {
	name="Jhigi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6746.1.1 = {birth="6746.1.1"}
}
14155916 = {
	name="Coholli"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13755916
	mother=43755916

	6738.1.1 = {birth="6738.1.1"}
}
14255916 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=14055916
	mother=44055916

	6772.1.1 = {birth="6772.1.1"}
}
14355916 = {
	name="Motho"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=14055916
	mother=44055916

	6775.1.1 = {birth="6775.1.1"}
	6791.1.1 = {add_trait=poor_warrior}
}
14455916 = {
	name="Quiri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=14055916
	mother=44055916

	6776.1.1 = {birth="6776.1.1"}
}
14555916 = {
	name="Ogo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=14055916
	mother=44055916

	6777.1.1 = {birth="6777.1.1"}
	6793.1.1 = {add_trait=poor_warrior}
}
###Sheqethi of Hesh###
10055923 = {
	name="Pono"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	6431.1.1 = {birth="6431.1.1"}
	6447.1.1 = {add_trait=poor_warrior}
	6449.1.1 = {add_spouse=40055923}
	6488.1.1 = {death="6488.1.1"}
}
40055923 = {
	name="Rimmi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6431.1.1 = {birth="6431.1.1"}
	6488.1.1 = {death="6488.1.1"}
}
10155923 = {
	name="Rhogoro"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10055923
	mother=40055923

	6460.1.1 = {birth="6460.1.1"}
	6476.1.1 = {add_trait=trained_warrior}
	6531.1.1 = {death="6531.1.1"}
}
10255923 = {
	name="Rhogiri"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10055923
	mother=40055923

	6462.1.1 = {birth="6462.1.1"}
	6516.1.1 = {death="6516.1.1"}
}
10355923 = {
	name="Motho"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10055923
	mother=40055923

	6463.1.1 = {birth="6463.1.1"}
	6479.1.1 = {add_trait=trained_warrior}
	6481.1.1 = {add_spouse=40355923}
	6535.1.1 = {death="6535.1.1"}
}
40355923 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6463.1.1 = {birth="6463.1.1"}
	6535.1.1 = {death="6535.1.1"}
}
10455923 = {
	name="Rakharo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10355923
	mother=40355923

	6481.1.1 = {birth="6481.1.1"}
	6497.1.1 = {add_trait=poor_warrior}
	6558.1.1 = {death="6558.1.1"}
}
10555923 = {
	name="Jhogo"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10355923
	mother=40355923

	6482.1.1 = {birth="6482.1.1"}
	6498.1.1 = {add_trait=trained_warrior}
	6500.1.1 = {add_spouse=40555923}
	6542.1.1 = {death = {death_reason = death_accident}}
}
40555923 = {
	name="Aggi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6482.1.1 = {birth="6482.1.1"}
	6542.1.1 = {death="6542.1.1"}
}
10655923 = {
	name="Rhogiri"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10555923
	mother=40555923

	6501.1.1 = {birth="6501.1.1"}
	6544.1.1 = {death="6544.1.1"}
}
10755923 = {
	name="Pono"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10555923
	mother=40555923

	6504.1.1 = {birth="6504.1.1"}
	6520.1.1 = {add_trait=poor_warrior}
	6522.1.1 = {add_spouse=40755923}
	6560.1.1 = {death="6560.1.1"}
}
40755923 = {
	name="Rhogiri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6504.1.1 = {birth="6504.1.1"}
	6560.1.1 = {death="6560.1.1"}
}
10855923 = {
	name="Coholli"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10555923
	mother=40555923

	6505.1.1 = {birth="6505.1.1"}
	6551.1.1 = {death="6551.1.1"}
}
10955923 = {
	name="Malakho"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10555923
	mother=40555923

	6507.1.1 = {birth="6507.1.1"}
	6523.1.1 = {add_trait=poor_warrior}
	6525.1.1 = {add_spouse=40955923}
	6554.1.1 = {death="6554.1.1"}
}
40955923 = {
	name="Aggi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6507.1.1 = {birth="6507.1.1"}
	6554.1.1 = {death="6554.1.1"}
}
11055923 = {
	name="Jhigi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10555923
	mother=40555923

	6510.1.1 = {birth="6510.1.1"}
	6548.1.1 = {death="6548.1.1"}
}
11155923 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10955923
	mother=40955923

	6527.1.1 = {birth="6527.1.1"}
	6593.1.1 = {death="6593.1.1"}
}
11255923 = {
	name="Aggi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10955923
	mother=40955923

	6529.1.1 = {birth="6529.1.1"}
	6574.1.1 = {death="6574.1.1"}
}
11355923 = {
	name="Zollo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10755923
	mother=40755923

	6525.1.1 = {birth="6525.1.1"}
	6541.1.1 = {add_trait=trained_warrior}
	6583.1.1 = {death="6583.1.1"}
}
11455923 = {
	name="Fogo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10755923
	mother=40755923

	6526.1.1 = {birth="6526.1.1"}
	6542.1.1 = {add_trait=trained_warrior}
	6593.1.1 = {death="6593.1.1"}
}
11555923 = {
	name="Jommo"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10755923
	mother=40755923

	6527.1.1 = {birth="6527.1.1"}
	6543.1.1 = {add_trait=poor_warrior}
	6545.1.1 = {add_spouse=41555923}
	6602.1.1 = {death = {death_reason = death_accident}}
}
41555923 = {
	name="Aggi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6527.1.1 = {birth="6527.1.1"}
	6602.1.1 = {death="6602.1.1"}
}
11655923 = {
	name="Kovarri"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10755923
	mother=40755923

	6530.1.1 = {birth="6530.1.1"}
	6579.1.1 = {death="6579.1.1"}
}
11755923 = {
	name="Rimmi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11555923
	mother=41555923

	6552.1.1 = {birth="6552.1.1"}
	6621.1.1 = {death="6621.1.1"}
}
11855923 = {
	name="Rakharo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11555923
	mother=41555923

	6553.1.1 = {birth="6553.1.1"}
	6569.1.1 = {add_trait=poor_warrior}
	6571.1.1 = {add_spouse=41855923}
	6602.1.1 = {death="6602.1.1"}
}
41855923 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6553.1.1 = {birth="6553.1.1"}
	6602.1.1 = {death="6602.1.1"}
}
11955923 = {
	name="Pono"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11855923
	mother=41855923

	6574.1.1 = {birth="6574.1.1"}
	6584.1.1 = {death = {death_reason = death_accident}}
}
12055923 = {
	name="Motho"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11855923
	mother=41855923

	6576.1.1 = {birth="6576.1.1"}
	6592.1.1 = {add_trait=trained_warrior}
	6650.1.1 = {death="6650.1.1"}
}
12155923 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11855923
	mother=41855923

	6577.1.1 = {birth="6577.1.1"}
	6622.1.1 = {death="6622.1.1"}
}
12255923 = {
	name="Jhigi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11855923
	mother=41855923

	6579.1.1 = {birth="6579.1.1"}
	6588.1.1 = {death = {death_reason = death_accident}}
}
12355923 = {
	name="Drogo"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11855923
	mother=41855923

	6582.1.1 = {birth="6582.1.1"}
	6598.1.1 = {add_trait=trained_warrior}
	6600.1.1 = {add_spouse=42355923}
	6638.1.1 = {death="6638.1.1"}
}
42355923 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6582.1.1 = {birth="6582.1.1"}
	6638.1.1 = {death="6638.1.1"}
}
12455923 = {
	name="Jhiqui"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12355923
	mother=42355923

	6615.1.1 = {birth="6615.1.1"}
	6668.1.1 = {death="6668.1.1"}
}
12555923 = {
	name="Moro"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12355923
	mother=42355923

	6618.1.1 = {birth="6618.1.1"}
	6634.1.1 = {add_trait=trained_warrior}
	6636.1.1 = {add_spouse=42555923}
	6651.1.1 = {death="6651.1.1"}
}
42555923 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6618.1.1 = {birth="6618.1.1"}
	6651.1.1 = {death="6651.1.1"}
}
12655923 = {
	name="Rommo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12555923
	mother=42555923

	6642.1.1 = {birth="6642.1.1"}
	6658.1.1 = {add_trait=trained_warrior}
	6676.1.1 = {death="6676.1.1"}
}
12755923 = {
	name="Rakharo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12555923
	mother=42555923

	6643.1.1 = {birth="6643.1.1"}
	6659.1.1 = {add_trait=trained_warrior}
	6686.1.1 = {death="6686.1.1"}
}
12855923 = {
	name="Zekko"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12555923
	mother=42555923

	6644.1.1 = {birth="6644.1.1"}
	6660.1.1 = {add_trait=poor_warrior}
	6662.1.1 = {add_spouse=42855923}
	6698.1.1 = {death="6698.1.1"}
}
42855923 = {
	name="Jhiqui"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6644.1.1 = {birth="6644.1.1"}
	6698.1.1 = {death="6698.1.1"}
}
12955923 = {
	name="Jommo"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12855923
	mother=42855923

	6669.1.1 = {birth="6669.1.1"}
	6685.1.1 = {add_trait=trained_warrior}
	6687.1.1 = {add_spouse=42955923}
	6710.1.1 = {death="6710.1.1"}
}
42955923 = {
	name="Qothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6669.1.1 = {birth="6669.1.1"}
	6710.1.1 = {death="6710.1.1"}
}
13055923 = {
	name="Rommo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12955923
	mother=42955923

	6693.1.1 = {birth="6693.1.1"}
	6709.1.1 = {add_trait=trained_warrior}
	6740.1.1 = {death = {death_reason = death_accident}}
}
13155923 = {
	name="Iggi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12955923
	mother=42955923

	6694.1.1 = {birth="6694.1.1"}
	6747.1.1 = {death="6747.1.1"}
}
13255923 = {
	name="Qotho"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12955923
	mother=42955923

	6696.1.1 = {birth="6696.1.1"}
	6712.1.1 = {add_trait=trained_warrior}
	6714.1.1 = {add_spouse=43255923}
	6748.1.1 = {death="6748.1.1"}
}
43255923 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6696.1.1 = {birth="6696.1.1"}
	6748.1.1 = {death="6748.1.1"}
}
13355923 = {
	name="Rakharo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13255923
	mother=43255923

	6720.1.1 = {birth="6720.1.1"}
	6736.1.1 = {add_trait=poor_warrior}
}
13455923 = {
	name="Iggo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13255923
	mother=43255923

	6723.1.1 = {birth="6723.1.1"}
	6739.1.1 = {add_trait=poor_warrior}
}
13555923 = {
	name="Mago"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13255923
	mother=43255923

	6724.1.1 = {birth="6724.1.1"}
	6740.1.1 = {add_trait=trained_warrior}
	6742.1.1 = {add_spouse=43555923}
}
43555923 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6724.1.1 = {birth="6724.1.1"}
}
13655923 = {
	name="Malakho"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13555923
	mother=43555923

	6757.1.1 = {birth="6757.1.1"}
	6773.1.1 = {add_trait=poor_warrior}
}
13755923 = {
	name="Jhaquo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13555923
	mother=43555923

	6759.1.1 = {birth="6759.1.1"}
	6775.1.1 = {add_trait=poor_warrior}
}
13855923 = {
	name="Rimmi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13555923
	mother=43555923

	6761.1.1 = {birth="6761.1.1"}
}
###Drogikh of Kosrak###
10055901 = {
	name="Moro"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	6410.1.1 = {birth="6410.1.1"}
	6426.1.1 = {add_trait=trained_warrior}
	6428.1.1 = {add_spouse=40055901}
	6482.1.1 = {death="6482.1.1"}
}
40055901 = {
	name="Irri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6410.1.1 = {birth="6410.1.1"}
	6482.1.1 = {death="6482.1.1"}
}
10155901 = {
	name="Drogo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10055901
	mother=40055901

	6439.1.1 = {birth="6439.1.1"}
	6455.1.1 = {add_trait=poor_warrior}
	6457.1.1 = {add_spouse=40155901}
	6503.1.1 = {death="6503.1.1"}
}
40155901 = {
	name="Mirri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6439.1.1 = {birth="6439.1.1"}
	6503.1.1 = {death="6503.1.1"}
}
10255901 = {
	name="Jhaquo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10055901
	mother=40055901

	6441.1.1 = {birth="6441.1.1"}
	6457.1.1 = {add_trait=poor_warrior}
	6485.1.1 = {death="6485.1.1"}
}
10355901 = {
	name="Rakhiri"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10055901
	mother=40055901

	6442.1.1 = {birth="6442.1.1"}
	6508.1.1 = {death="6508.1.1"}
}
10455901 = {
	name="Mago"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10055901
	mother=40055901

	6445.1.1 = {birth="6445.1.1"}
	6461.1.1 = {add_trait=poor_warrior}
	6463.1.1 = {add_spouse=40455901}
	6524.1.1 = {death="6524.1.1"}
}
40455901 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6445.1.1 = {birth="6445.1.1"}
	6524.1.1 = {death="6524.1.1"}
}
10555901 = {
	name="Pono"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10155901
	mother=40155901

	6467.1.1 = {birth="6467.1.1"}
	6483.1.1 = {add_trait=poor_warrior}
	6528.1.1 = {death="6528.1.1"}
}
10655901 = {
	name="Iggi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10155901
	mother=40155901

	6468.1.1 = {birth="6468.1.1"}
	6501.1.1 = {death="6501.1.1"}
}
10755901 = {
	name="Aggi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10455901
	mother=40455901

	6473.1.1 = {birth="6473.1.1"}
	6529.1.1 = {death="6529.1.1"}
}
10855901 = {
	name="Ogo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10455901
	mother=40455901

	6474.1.1 = {birth="6474.1.1"}
	6490.1.1 = {add_trait=poor_warrior}
	6492.1.1 = {add_spouse=40855901}
	6525.1.1 = {death="6525.1.1"}
}
40855901 = {
	name="Jhigi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6474.1.1 = {birth="6474.1.1"}
	6525.1.1 = {death="6525.1.1"}
}
10955901 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10855901
	mother=40855901

	6506.1.1 = {birth="6506.1.1"}
	6564.1.1 = {death = {death_reason = death_murder}}
}
11055901 = {
	name="Rakharo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10855901
	mother=40855901

	6507.1.1 = {birth="6507.1.1"}
	6523.1.1 = {add_trait=poor_warrior}
	6565.1.1 = {death="6565.1.1"}
}
11155901 = {
	name="Cohollo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10855901
	mother=40855901

	6509.1.1 = {birth="6509.1.1"}
	6525.1.1 = {add_trait=trained_warrior}
	6527.1.1 = {add_spouse=41155901}
	6576.1.1 = {death="6576.1.1"}
}
41155901 = {
	name="Irri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6509.1.1 = {birth="6509.1.1"}
	6576.1.1 = {death="6576.1.1"}
}
11255901 = {
	name="Jhaquo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11155901
	mother=41155901

	6529.1.1 = {birth="6529.1.1"}
	6545.1.1 = {add_trait=poor_warrior}
	6547.1.1 = {add_spouse=41255901}
	6561.1.1 = {death = {death_reason = death_battle}}
}
41255901 = {
	name="Aggi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6529.1.1 = {birth="6529.1.1"}
	6561.1.1 = {death="6561.1.1"}
}
11355901 = {
	name="Haggo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11155901
	mother=41155901

	6532.1.1 = {birth="6532.1.1"}
	6548.1.1 = {add_trait=poor_warrior}
	6576.1.1 = {death="6576.1.1"}
}
11455901 = {
	name="Cohollo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11255901
	mother=41255901

	6552.1.1 = {birth="6552.1.1"}
	6568.1.1 = {add_trait=poor_warrior}
	6570.1.1 = {add_spouse=41455901}
	6606.1.1 = {death="6606.1.1"}
}
41455901 = {
	name="Kovarri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6552.1.1 = {birth="6552.1.1"}
	6606.1.1 = {death="6606.1.1"}
}
11555901 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11255901
	mother=41255901

	6554.1.1 = {birth="6554.1.1"}
	6576.1.1 = {death="6576.1.1"}
}
11655901 = {
	name="Jommo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11255901
	mother=41255901

	6557.1.1 = {birth="6557.1.1"}
	6573.1.1 = {add_trait=poor_warrior}
	6575.1.1 = {add_spouse=41655901}
	6588.1.1 = {death="6588.1.1"}
}
41655901 = {
	name="Coholli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6557.1.1 = {birth="6557.1.1"}
	6588.1.1 = {death="6588.1.1"}
}
11755901 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11255901
	mother=41255901

	6560.1.1 = {birth="6560.1.1"}
	6624.1.1 = {death="6624.1.1"}
}
11855901 = {
	name="Qothi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11655901
	mother=41655901

	6578.1.1 = {birth="6578.1.1"}
	6618.1.1 = {death="6618.1.1"}
}
11955901 = {
	name="Jhigi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11455901
	mother=41455901

	6574.1.1 = {birth="6574.1.1"}
	6631.1.1 = {death="6631.1.1"}
}
12055901 = {
	name="Kovarro"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11455901
	mother=41455901

	6576.1.1 = {birth="6576.1.1"}
	6592.1.1 = {add_trait=poor_warrior}
	6639.1.1 = {death="6639.1.1"}
}
12155901 = {
	name="Qothi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11455901
	mother=41455901

	6578.1.1 = {birth="6578.1.1"}
	6649.1.1 = {death="6649.1.1"}
}
12255901 = {
	name="Rakharo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11455901
	mother=41455901

	6581.1.1 = {birth="6581.1.1"}
	6597.1.1 = {add_trait=poor_warrior}
	6599.1.1 = {add_spouse=42255901}
	6622.1.1 = {death="6622.1.1"}
}
42255901 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6581.1.1 = {birth="6581.1.1"}
	6622.1.1 = {death="6622.1.1"}
}
12355901 = {
	name="Jhiqui"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12255901
	mother=42255901

	6602.1.1 = {birth="6602.1.1"}
	6651.1.1 = {death="6651.1.1"}
}
12455901 = {
	name="Fogo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12255901
	mother=42255901

	6605.1.1 = {birth="6605.1.1"}
	6621.1.1 = {add_trait=trained_warrior}
	6679.1.1 = {death="6679.1.1"}
}
12555901 = {
	name="Jhaquo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12255901
	mother=42255901

	6607.1.1 = {birth="6607.1.1"}
	6623.1.1 = {add_trait=poor_warrior}
	6625.1.1 = {add_spouse=42555901}
	6661.1.1 = {death="6661.1.1"}
}
42555901 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6607.1.1 = {birth="6607.1.1"}
	6661.1.1 = {death="6661.1.1"}
}
12655901 = {
	name="Rommo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12555901
	mother=42555901

	6633.1.1 = {birth="6633.1.1"}
	6649.1.1 = {add_trait=poor_warrior}
	6699.1.1 = {death="6699.1.1"}
}
12755901 = {
	name="Zollo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12555901
	mother=42555901

	6636.1.1 = {birth="6636.1.1"}
	6652.1.1 = {add_trait=poor_warrior}
	6654.1.1 = {add_spouse=42755901}
	6695.1.1 = {death="6695.1.1"}
}
42755901 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6636.1.1 = {birth="6636.1.1"}
	6695.1.1 = {death="6695.1.1"}
}
12855901 = {
	name="Iggi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12555901
	mother=42555901

	6637.1.1 = {birth="6637.1.1"}
	6686.1.1 = {death="6686.1.1"}
}
12955901 = {
	name="Motho"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12755901
	mother=42755901

	6669.1.1 = {birth="6669.1.1"}
	6685.1.1 = {add_trait=poor_warrior}
	6726.1.1 = {death = {death_reason = death_murder}}
}
13055901 = {
	name="Jhiqui"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12755901
	mother=42755901

	6672.1.1 = {birth="6672.1.1"}
	6725.1.1 = {death="6725.1.1"}
}
13155901 = {
	name="Pono"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12755901
	mother=42755901

	6673.1.1 = {birth="6673.1.1"}
	6689.1.1 = {add_trait=trained_warrior}
	6691.1.1 = {add_spouse=43155901}
	6728.1.1 = {death="6728.1.1"}
}
43155901 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6673.1.1 = {birth="6673.1.1"}
	6728.1.1 = {death="6728.1.1"}
}
13255901 = {
	name="Jhiqui"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12755901
	mother=42755901

	6674.1.1 = {birth="6674.1.1"}
	6714.1.1 = {death="6714.1.1"}
}
13355901 = {
	name="Qotho"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13155901
	mother=43155901

	6706.1.1 = {birth="6706.1.1"}
	6722.1.1 = {add_trait=trained_warrior}
	6760.1.1 = {death="6760.1.1"}
}
13455901 = {
	name="Rakharo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13155901
	mother=43155901

	6707.1.1 = {birth="6707.1.1"}
	6723.1.1 = {add_trait=trained_warrior}
	6777.1.1 = {death="6777.1.1"}
}
13555901 = {
	name="Jhaquo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13155901
	mother=43155901

	6709.1.1 = {birth="6709.1.1"}
	6725.1.1 = {add_trait=trained_warrior}
	6727.1.1 = {add_spouse=43555901}
	6757.1.1 = {death="6757.1.1"}
}
43555901 = {
	name="Jhiqui"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6709.1.1 = {birth="6709.1.1"}
	6757.1.1 = {death="6757.1.1"}
}
13655901 = {
	name="Rhogiri"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13155901
	mother=43155901

	6712.1.1 = {birth="6712.1.1"}
	6786.1.1 = {death="6786.1.1"}
}
13755901 = {
	name="Rakhiri"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13555901
	mother=43555901

	6737.1.1 = {birth="6737.1.1"}
}
13855901 = {
	name="Moro"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13555901
	mother=43555901

	6740.1.1 = {birth="6740.1.1"}
	6756.1.1 = {add_trait=poor_warrior}
	6758.1.1 = {add_spouse=43855901}
}
43855901 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6740.1.1 = {birth="6740.1.1"}
}
13955901 = {
	name="Drogo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13555901
	mother=43555901

	6742.1.1 = {birth="6742.1.1"}
	6758.1.1 = {add_trait=poor_warrior}
	6760.1.1 = {add_spouse=43955901}
}
43955901 = {
	name="Jhigi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	6742.1.1 = {birth="6742.1.1"}
}
14055901 = {
	name="Iggi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13855901
	mother=43855901

	6759.1.1 = {birth="6759.1.1"}
}
14155901 = {
	name="Fogo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13955901
	mother=43955901

	6770.1.1 = {birth="6770.1.1"}
	6786.1.1 = {add_trait=poor_warrior}
}
14255901 = {
	name="Haggo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13955901
	mother=43955901

	6773.1.1 = {birth="6773.1.1"}
	6789.1.1 = {add_trait=trained_warrior}
}
14355901 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13955901
	mother=43955901

	6774.1.1 = {birth="6774.1.1"}
}
14455901 = {
	name="Temmo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13955901
	mother=43955901

	6777.1.1 = {birth="6777.1.1"}
	6793.1.1 = {add_trait=poor_warrior}
}
