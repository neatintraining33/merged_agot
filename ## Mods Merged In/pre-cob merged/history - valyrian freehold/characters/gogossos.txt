#### House Paenorhin
10003350 = {
	name="Benerro"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7570.1.1 = {birth="7570.1.1"}
	7586.1.1 = {add_trait=poor_warrior}
	7588.1.1 = {add_spouse=20003350}
	7624.1.1 = {death="7624.1.1"}
}
20003350 = {
	name="Talisa"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7570.1.1 = {birth="7570.1.1"}
	7624.1.1 = {death="7624.1.1"}
}
10013350 = {
	name="Qoherys"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10003350
	mother=20003350

	7605.1.1 = {birth="7605.1.1"}
	7621.1.1 = {add_trait=poor_warrior}
	7623.1.1 = {add_spouse=20013350}
	7649.1.1 = {death="7649.1.1"}
}
20013350 = {
	name="Alearys"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7605.1.1 = {birth="7605.1.1"}
	7649.1.1 = {death="7649.1.1"}
}
10023350 = {
	name="Naerys"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10003350
	mother=20003350

	7607.1.1 = {birth="7607.1.1"}
	7658.1.1 = {death="7658.1.1"}
}
10033350 = {
	name="Vogarra"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10013350
	mother=20013350

	7625.1.1 = {birth="7625.1.1"}
	7694.1.1 = {death="7694.1.1"}
}
10043350 = {
	name="Donipha"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10013350
	mother=20013350

	7627.1.1 = {birth="7627.1.1"}
	7670.1.1 = {death="7670.1.1"}
}
10053350 = {
	name="Aenar"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10013350
	mother=20013350

	7630.1.1 = {birth="7630.1.1"}
	7646.1.1 = {add_trait=trained_warrior}
	7648.1.1 = {add_spouse=20053350}
	7688.1.1 = {death="7688.1.1"}
}
20053350 = {
	name="Horonna"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7630.1.1 = {birth="7630.1.1"}
	7688.1.1 = {death="7688.1.1"}
}
10063350 = {
	name="Cyeana"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10013350
	mother=20013350

	7631.1.1 = {birth="7631.1.1"}
	7686.1.1 = {death="7686.1.1"}
}
10073350 = {
	name="Alios"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10053350
	mother=20053350

	7655.1.1 = {birth="7655.1.1"}
	7671.1.1 = {add_trait=poor_warrior}
	7673.1.1 = {add_spouse=20073350}
	7722.1.1 = {death="7722.1.1"}
}
20073350 = {
	name="Alia"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7655.1.1 = {birth="7655.1.1"}
	7722.1.1 = {death="7722.1.1"}
}
10083350 = {
	name="Allyria"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10053350
	mother=20053350

	7658.1.1 = {birth="7658.1.1"}
	7701.1.1 = {death="7701.1.1"}
}
10093350 = {
	name="Horonno"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10073350
	mother=20073350

	7677.1.1 = {birth="7677.1.1"}
	7693.1.1 = {add_trait=master_warrior}
	7695.1.1 = {add_spouse=20093350}
	7732.1.1 = {death="7732.1.1"}
}
20093350 = {
	name="Parquella"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7677.1.1 = {birth="7677.1.1"}
	7732.1.1 = {death="7732.1.1"}
}
10103350 = {
	name="Vaeron"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10093350
	mother=20093350

	7706.1.1 = {birth="7706.1.1"}
	7722.1.1 = {add_trait=poor_warrior}
	7724.1.1 = {add_spouse=20103350}
	7783.1.1 = {death="7783.1.1"}
}
20103350 = {
	name="Donipha"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7706.1.1 = {birth="7706.1.1"}
	7783.1.1 = {death="7783.1.1"}
}
10113350 = {
	name="Leaysa"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10103350
	mother=20103350

	7728.1.1 = {birth="7728.1.1"}
	7775.1.1 = {death="7775.1.1"}
}
10123350 = {
	name="Nyessos"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10103350
	mother=20103350

	7731.1.1 = {birth="7731.1.1"}
	7747.1.1 = {add_trait=poor_warrior}
	7749.1.1 = {add_spouse=20123350}
	7788.1.1 = {death="7788.1.1"}
}
20123350 = {
	name="Alearys"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7731.1.1 = {birth="7731.1.1"}
	7788.1.1 = {death="7788.1.1"}
}
10133350 = {
	name="Elaena"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10103350
	mother=20103350

	7733.1.1 = {birth="7733.1.1"}
	7791.1.1 = {death="7791.1.1"}
}
10143350 = {
	name="Trianna"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10123350
	mother=20123350

	7766.1.1 = {birth="7766.1.1"}
	7799.1.1 = {death="7799.1.1"}
}
10153350 = {
	name="Rhae"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10123350
	mother=20123350

	7768.1.1 = {birth="7768.1.1"}
	7838.1.1 = {death="7838.1.1"}
}
10163350 = {
	name="Syaella"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10123350
	mother=20123350

	7770.1.1 = {birth="7770.1.1"}
	7844.1.1 = {death="7844.1.1"}
}
10173350 = {
	name="Colloquo"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10123350
	mother=20123350

	7773.1.1 = {birth="7773.1.1"}
	7789.1.1 = {add_trait=poor_warrior}
	7791.1.1 = {add_spouse=20173350}
	7837.1.1 = {death="7837.1.1"}
}
20173350 = {
	name="Syaella"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7773.1.1 = {birth="7773.1.1"}
	7837.1.1 = {death="7837.1.1"}
}
10183350 = {
	name="Orys"	# not a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10123350
	mother=20123350

	7776.1.1 = {birth="7776.1.1"}
	7792.1.1 = {add_trait=poor_warrior}
	7840.1.1 = {death="7840.1.1"}
}
10193350 = {
	name="Gorys"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10173350
	mother=20173350

	7802.1.1 = {birth="7802.1.1"}
	7818.1.1 = {add_trait=skilled_warrior}
	7820.1.1 = {add_spouse=20193350}
	7849.1.1 = {death="7849.1.1"}
}
20193350 = {
	name="Saenrys"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7802.1.1 = {birth="7802.1.1"}
	7849.1.1 = {death="7849.1.1"}
}
10203350 = {
	name="Vaekar"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10193350
	mother=20193350

	7823.1.1 = {birth="7823.1.1"}
	7839.1.1 = {add_trait=poor_warrior}
	7841.1.1 = {add_spouse=20203350}
	7896.1.1 = {death="7896.1.1"}
}
20203350 = {
	name="Moqorra"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7823.1.1 = {birth="7823.1.1"}
	7896.1.1 = {death="7896.1.1"}
}
10213350 = {
	name="Parquello"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10203350
	mother=20203350

	7842.1.1 = {birth="7842.1.1"}
	7858.1.1 = {add_trait=poor_warrior}
	7860.1.1 = {add_spouse=20213350}
	7922.1.1 = {death="7922.1.1"}
}
20213350 = {
	name="Parquella"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7842.1.1 = {birth="7842.1.1"}
	7922.1.1 = {death="7922.1.1"}
}
10223350 = {
	name="Parquella"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10203350
	mother=20203350

	7845.1.1 = {birth="7845.1.1"}
	7894.1.1 = {death="7894.1.1"}
}
10233350 = {
	name="Qoherys"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10213350
	mother=20213350

	7877.1.1 = {birth="7877.1.1"}
	7893.1.1 = {add_trait=poor_warrior}
	7895.1.1 = {add_spouse=20233350}
	7944.1.1 = {death="7944.1.1"}
}
20233350 = {
	name="Rhaeys"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7877.1.1 = {birth="7877.1.1"}
	7944.1.1 = {death="7944.1.1"}
}
10243350 = {
	name="Nyessa"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10233350
	mother=20233350

	7899.1.1 = {birth="7899.1.1"}
	7979.1.1 = {death="7979.1.1"}
}
10253350 = {
	name="Naerys"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10233350
	mother=20233350

	7900.1.1 = {birth="7900.1.1"}
	7944.1.1 = {death="7944.1.1"}
}
10263350 = {
	name="Trianna"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10233350
	mother=20233350

	7903.1.1 = {birth="7903.1.1"}
	7956.1.1 = {death="7956.1.1"}
}
10273350 = {
	name="Aenar"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10233350
	mother=20233350

	7906.1.1 = {birth="7906.1.1"}
	7922.1.1 = {add_trait=poor_warrior}
	7982.1.1 = {death="7982.1.1"}
}
10283350 = {
	name="Haerys"	# not a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10233350
	mother=20233350

	7908.1.1 = {birth="7908.1.1"}
	7924.1.1 = {add_trait=skilled_warrior}
	7960.1.1 = {death="7960.1.1"}
}


