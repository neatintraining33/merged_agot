####  House Moth

1055927 = {
	name="Togg"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	7555.1.1 = {birth="7555.1.1"}
	7571.1.1 = {add_trait=poor_warrior}
	7573.1.1 = {add_spouse=4055927}
	7622.1.1 = {death="7622.1.1"}
}
4055927 = {
	name="Cossoma"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7555.1.1 = {birth="7555.1.1"}
	7622.1.1 = {death="7622.1.1"}
}
1155927 = {
	name="Togg"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1055927
	mother=4055927

	7584.1.1 = {birth="7584.1.1"}
	7600.1.1 = {add_trait=trained_warrior}
	7602.1.1 = {add_spouse=41055927}
	7640.1.1 = {death="7640.1.1"}
}
41055927 = {
	name="Cossoma"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7584.1.1 = {birth="7584.1.1"}
	7640.1.1 = {death="7640.1.1"}
}
1255927 = {
	name="Torma"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1055927
	mother=4055927

	7587.1.1 = {birth="7587.1.1"}
	7618.1.1 = {death="7618.1.1"}
}
1355927 = {
	name="Brea"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1055927
	mother=4055927

	7589.1.1 = {birth="7589.1.1"}
	7645.1.1 = {death="7645.1.1"}
}
1455927 = {
	name="Yorka"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1055927
	mother=4055927

	7591.1.1 = {birth="7591.1.1"}
	7646.1.1 = {death="7646.1.1"}
}
1555927 = {
	name="Tagganaria"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1155927
	mother=41055927

	7612.1.1 = {birth="7612.1.1"}
	7660.1.1 = {death="7660.1.1"}
}
1655927 = {
	name="Groo"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1155927
	mother=41055927

	7614.1.1 = {birth="7614.1.1"}
	7630.1.1 = {add_trait=poor_warrior}
	7632.1.1 = {add_spouse=4655927}
	7642.1.1 = {death="7642.1.1"}
}
4655927 = {
	name="Mera"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7614.1.1 = {birth="7614.1.1"}
	7642.1.1 = {death="7642.1.1"}
}
1755927 = {
	name="Caloth"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1655927
	mother=4655927

	7641.1.1 = {birth="7641.1.1"}
	7657.1.1 = {add_trait=trained_warrior}
	7659.1.1 = {add_spouse=4755927}
	7681.1.1 = {death="7681.1.1"}
}
4755927 = {
	name="Bellegere"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7641.1.1 = {birth="7641.1.1"}
	7681.1.1 = {death="7681.1.1"}
}
1855927 = {
	name="Mordea"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1755927
	mother=4755927

	7670.1.1 = {birth="7670.1.1"}
	7731.1.1 = {death="7731.1.1"}
}
1955927 = {
	name="Tugg"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1755927
	mother=4755927

	7673.1.1 = {birth="7673.1.1"}
	7689.1.1 = {add_trait=trained_warrior}
	7691.1.1 = {add_spouse=4955927}
	7727.1.1 = {death="7727.1.1"}
}
4955927 = {
	name="Rowan"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7673.1.1 = {birth="7673.1.1"}
	7727.1.1 = {death="7727.1.1"}
}
2055927 = {
	name="Jaqea"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1955927
	mother=4955927

	7698.1.1 = {birth="7698.1.1"}
	7753.1.1 = {death="7753.1.1"}
}
2155927 = {
	name="Galt"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1955927
	mother=4955927

	7700.1.1 = {birth="7700.1.1"}
	7716.1.1 = {add_trait=poor_warrior}
	7718.1.1 = {add_spouse=4155927}
	7746.1.1 = {death="7746.1.1"}
}
4155927 = {
	name="Yna"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7700.1.1 = {birth="7700.1.1"}
	7746.1.1 = {death="7746.1.1"}
}
2255927 = {
	name="Galt"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2155927
	mother=4155927

	7727.1.1 = {birth="7727.1.1"}
	7743.1.1 = {add_trait=skilled_warrior}
	7745.1.1 = {add_spouse=42055927}
	7759.1.1 = {death="7759.1.1"}
}
42055927 = {
	name="Betharios"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7727.1.1 = {birth="7727.1.1"}
	7799.1.1 = {death="7799.1.1"}
}
2355927 = {
	name="Joth"	# not a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2155927
	mother=4155927

	7730.1.1 = {birth="7730.1.1"}
	7746.1.1 = {add_trait=skilled_warrior}
	7773.1.1 = {death="7773.1.1"}
}
2455927 = {
	name="Ulf"	# not a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2155927
	mother=4155927

	7732.1.1 = {birth="7732.1.1"}
	7748.1.1 = {add_trait=poor_warrior}
	7768.1.1 = {death="7768.1.1"}
}
2555927 = {
	name="Slugg"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2255927
	mother=42055927

	7757.1.1 = {birth="7757.1.1"}
	7773.1.1 = {add_trait=skilled_warrior}
	7780.1.1 = {add_spouse=4555927}
	7829.1.1 = {death="7829.1.1"}
}
4555927 = {
	name="Jaqea"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7762.1.1 = {birth="7762.1.1"}
	7829.1.1 = {death="7829.1.1"}
}
2655927 = {
	name="Zia"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2255927
	mother=42055927

	7758.1.1 = {birth="7758.1.1"}
	7842.1.1 = {death="7842.1.1"}
}
2755927 = {
	name="Denia"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2555927
	mother=4555927

	7794.1.1 = {birth="7794.1.1"}
	7857.1.1 = {death="7857.1.1"}
}
2855927 = {
	name="Togg"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2555927
	mother=4555927

	7795.1.1 = {birth="7795.1.1"}
	7811.1.1 = {add_trait=trained_warrior}
	7813.1.1 = {add_spouse=4855927}
	7855.1.1 = {death="7855.1.1"}
}
4855927 = {
	name="Bellegere"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7795.1.1 = {birth="7795.1.1"}
	7855.1.1 = {death="7855.1.1"}
}
2955927 = {
	name="Zia"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2555927
	mother=4555927

	7797.1.1 = {birth="7797.1.1"}
	7854.1.1 = {death="7854.1.1"}
}
3055927 = {
	name="Denia"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2855927
	mother=4855927

	7819.1.1 = {birth="7819.1.1"}
	7851.1.1 = {death="7851.1.1"}
}
3155927 = {
	name="Gylenios"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2855927
	mother=4855927

	7820.1.1 = {birth="7820.1.1"}
	7853.1.1 = {death="7853.1.1"}
}
3255927 = {
	name="Krull"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2855927
	mother=4855927

	7823.1.1 = {birth="7823.1.1"}
	7839.1.1 = {add_trait=poor_warrior}
	7841.1.1 = {add_spouse=4255927}
	7863.1.1 = {death="7863.1.1"}
}
4255927 = {
	name="Mellario"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7823.1.1 = {birth="7823.1.1"}
	7863.1.1 = {death="7863.1.1"}
}
3355927 = {
	name="Durr"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=3255927
	mother=4255927

	7841.1.1 = {birth="7841.1.1"}
	7857.1.1 = {add_trait=poor_warrior}
	7859.1.1 = {add_spouse=4355927}
	7896.1.1 = {death="7896.1.1"}
}
4355927 = {
	name="Mordea"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7841.1.1 = {birth="7841.1.1"}
	7896.1.1 = {death="7896.1.1"}
}
3455927 = {
	name="Mordea"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=3255927
	mother=4255927

	7843.1.1 = {birth="7843.1.1"}
	7889.1.1 = {death="7889.1.1"}
}
3555927 = {
	name="Togg"	# not a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=3255927
	mother=4255927

	7846.1.1 = {birth="7846.1.1"}
	7862.1.1 = {add_trait=trained_warrior}
}
3655927 = {
	name="Ferny"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=3355927
	mother=4355927

	7859.1.1 = {birth="7859.1.1"}
	7891.1.1 = {death="7891.1.1"}
}
3755927 = {
	name="Holger"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=3355927
	mother=4355927

	7860.1.1 = {birth="7860.1.1"}
	7876.1.1 = {add_trait=skilled_warrior}
}
3855927 = {
	name="Meralyia"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=3355927
	mother=4355927

	7863.1.1 = {birth="7863.1.1"}
	7890.1.1 = {death="7890.1.1"}
}
3955927 = {
	name="Jogg"	# not a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=3355927
	mother=4355927

	7866.1.1 = {birth="7866.1.1"}
	7882.1.1 = {add_trait=trained_warrior}
}


####  House Bartogg

1055928 = {
	name="Krull"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	7568.1.1 = {birth="7568.1.1"}
	7584.1.1 = {add_trait=poor_warrior}
	7586.1.1 = {add_spouse=40055928}
	7645.1.1 = {death="7645.1.1"}
}
40055928 = {
	name="Denia"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7568.1.1 = {birth="7568.1.1"}
	7645.1.1 = {death="7645.1.1"}
}
1155928 = {
	name="Haggon"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1055928
	mother=40055928

	7595.1.1 = {birth="7595.1.1"}
	7611.1.1 = {add_trait=trained_warrior}
	7613.1.1 = {add_spouse=41055928}
	7653.1.1 = {death="7653.1.1"}
}
41055928 = {
	name="Mera"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7595.1.1 = {birth="7595.1.1"}
	7653.1.1 = {death="7653.1.1"}
}
1255928 = {
	name="Jarr"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1155928
	mother=41055928

	7622.1.1 = {birth="7622.1.1"}
	7638.1.1 = {add_trait=poor_warrior}
	7640.1.1 = {add_spouse=42055928}
	7657.1.1 = {death="7657.1.1"}
}
42055928 = {
	name="Brella"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7622.1.1 = {birth="7622.1.1"}
	7657.1.1 = {death="7657.1.1"}
}
1355928 = {
	name="Torma"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1155928
	mother=41055928

	7624.1.1 = {birth="7624.1.1"}
	7669.1.1 = {death="7669.1.1"}
}
1455928 = {
	name="Umar"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1255928
	mother=42055928

	7653.1.1 = {birth="7653.1.1"}
	7669.1.1 = {add_trait=poor_warrior}
	7671.1.1 = {add_spouse=44055928}
	7692.1.1 = {death="7692.1.1"}
}
44055928 = {
	name="Gylenios"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7653.1.1 = {birth="7653.1.1"}
	7692.1.1 = {death="7692.1.1"}
}
1555928 = {
	name="Grisel"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1255928
	mother=42055928

	7655.1.1 = {birth="7655.1.1"}
	7711.1.1 = {death="7711.1.1"}
}
1655928 = {
	name="Assadora"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1255928
	mother=42055928

	7656.1.1 = {birth="7656.1.1"}
	7711.1.1 = {death="7711.1.1"}
}
1755928 = {
	name="Krull"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1455928
	mother=44055928

	7676.1.1 = {birth="7676.1.1"}
	7692.1.1 = {add_trait=trained_warrior}
	7694.1.1 = {add_spouse=4755928}
	7712.1.1 = {death="7712.1.1"}
}
4755928 = {
	name="Harra"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7676.1.1 = {birth="7676.1.1"}
	7712.1.1 = {death="7712.1.1"}
}
1855928 = {
	name="Clargg"	# not a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1455928
	mother=44055928

	7678.1.1 = {birth="7678.1.1"}
	7694.1.1 = {add_trait=poor_warrior}
	7747.1.1 = {death="7747.1.1"}
}
1955928 = {
	name="Bellegere"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1455928
	mother=44055928

	7680.1.1 = {birth="7680.1.1"}
	7711.1.1 = {death="7711.1.1"}
}
2055928 = {
	name="Ynys"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1455928
	mother=44055928

	7681.1.1 = {birth="7681.1.1"}
	7737.1.1 = {death="7737.1.1"}
}
2155928 = {
	name="Erroth"	# not a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1455928
	mother=44055928

	7682.1.1 = {birth="7682.1.1"}
	7698.1.1 = {add_trait=trained_warrior}
	7739.1.1 = {death="7739.1.1"}
}
2255928 = {
	name="Slugg"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1755928
	mother=4755928

	7697.1.1 = {birth="7697.1.1"}
	7713.1.1 = {add_trait=skilled_warrior}
	7715.1.1 = {add_spouse=4255928}
	7760.1.1 = {death="7760.1.1"}
}
4255928 = {
	name="Yorka"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7697.1.1 = {birth="7697.1.1"}
	7760.1.1 = {death="7760.1.1"}
}
2355928 = {
	name="Torma"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2255928
	mother=4255928

	7722.1.1 = {birth="7722.1.1"}
	7773.1.1 = {death="7773.1.1"}
}
2455928 = {
	name="Izembara"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2255928
	mother=4255928

	7725.1.1 = {birth="7725.1.1"}
	7761.1.1 = {death="7761.1.1"}
}
2555928 = {
	name="Sogg"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2255928
	mother=4255928

	7728.1.1 = {birth="7728.1.1"}
	7744.1.1 = {add_trait=poor_warrior}
	7746.1.1 = {add_spouse=4555928}
	7765.1.1 = {death="7765.1.1"}
}
4555928 = {
	name="Brella"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7728.1.1 = {birth="7728.1.1"}
	7765.1.1 = {death="7765.1.1"}
}
2655928 = {
	name="Meralyia"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2255928
	mother=4255928

	7731.1.1 = {birth="7731.1.1"}
	7800.1.1 = {death = {death_reason = death_murder}}
}
2755928 = {
	name="Brella"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2255928
	mother=4255928

	7732.1.1 = {birth="7732.1.1"}
	7780.1.1 = {death="7780.1.1"}
}
2855928 = {
	name="Joth"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2555928
	mother=4555928

	7741.1.1 = {birth="7741.1.1"}
	7757.1.1 = {add_trait=trained_warrior}
	7758.1.1 = {add_spouse=48055928}
	7797.1.1 = {death = {death_reason = death_battle}}
}
48055928 = {
	name="Izembara"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7741.1.1 = {birth="7741.1.1"}
	7797.1.1 = {death="7797.1.1"}
}
2955928 = {
	name="Ferny"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2555928
	mother=4555928

	7754.1.1 = {birth="7754.1.1"}
	7810.1.1 = {death="7810.1.1"}
}
3055928 = {
	name="Meralyia"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2555928
	mother=4555928

	7755.1.1 = {birth="7755.1.1"}
	7794.1.1 = {death="7794.1.1"}
}
3155928 = {
	name="Galt"	# not a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2555928
	mother=4555928

	7756.1.1 = {birth="7756.1.1"}
	7772.1.1 = {add_trait=poor_warrior}
	7806.1.1 = {death="7806.1.1"}
}
3255928 = {
	name="Rowan"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2855928
	mother=48055928

	7759.1.1 = {birth="7759.1.1"}
	7845.1.1 = {death="7845.1.1"}
}
3355928 = {
	name="Morr"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2855928
	mother=48055928

	7760.1.1 = {birth="7760.1.1"}
	7809.1.1 = {add_trait=poor_warrior}
	7811.1.1 = {add_spouse=4355928}
	7847.1.1 = {death="7847.1.1"}
}
4355928 = {
	name="Yna"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7760.1.1 = {birth="7760.1.1"}
	7847.1.1 = {death="7847.1.1"}
}
3455928 = {
	name="Colosh"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=3355928
	mother=4355928

	7817.1.1 = {birth="7817.1.1"}
	7833.1.1 = {add_trait=poor_warrior}
	7835.1.1 = {add_spouse=4455928}
	7874.1.1 = {death="7874.1.1"}
}
4455928 = {
	name="Harra"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7817.1.1 = {birth="7817.1.1"}
	7874.1.1 = {death="7874.1.1"}
}
3555928 = {
	name="Jarr"	# not a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=3355928
	mother=4355928

	7819.1.1 = {birth="7819.1.1"}
	7835.1.1 = {add_trait=poor_warrior}
	7865.1.1 = {death="7865.1.1"}
}
3655928 = {
	name="Ynys"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=3355928
	mother=4355928

	7822.1.1 = {birth="7822.1.1"}
	7843.1.1 = {death="7843.1.1"}
}
3755928 = {
	name="Izembara"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=3455928
	mother=4455928

	7840.1.1 = {birth="7840.1.1"}
}
3855928 = {
	name="Drogg"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=3455928
	mother=4455928

	7841.1.1 = {birth="7841.1.1"}
	7857.1.1 = {add_trait=skilled_warrior}
	7859.1.1 = {add_spouse=4855928}
	7883.1.1 = {death="7883.1.1"}
}
4855928 = {
	name="Harra"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7841.1.1 = {birth="7841.1.1"}
	7883.1.1 = {death="7883.1.1"}
}
3955928 = {
	name="Betharios"	# a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=3855928
	mother=4855928

	7876.1.1 = {birth="7876.1.1"}
}
4055928 = {
	name="Harra"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	father=3855928
	mother=4855928

	7877.1.1 = {birth="7877.1.1"}
}
4155928 = {
	name="Yorka"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	father=3855928
	mother=4855928

	7880.1.1 = {birth="7880.1.1"}
}


####  House Joth

1055929 = {
	name="Slugg"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	7569.1.1 = {birth="7569.1.1"}
	7585.1.1 = {add_trait=poor_warrior}
	7587.1.1 = {add_spouse=46755929}
	7636.1.1 = {death="7636.1.1"}
}
46755929 = {
	name="Assadora"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7569.1.1 = {birth="7569.1.1"}
	7636.1.1 = {death="7636.1.1"}
}
1155929 = {
	name="Gylenios"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1055929
	mother=46755929

	7591.1.1 = {birth="7591.1.1"}
	7601.1.1 = {death = {death_reason = death_murder}}
}
1255929 = {
	name="Bodger"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1055929
	mother=46755929

	7594.1.1 = {birth="7594.1.1"}
	7610.1.1 = {add_trait=poor_warrior}
	7612.1.1 = {add_spouse=4255929}
	7648.1.1 = {death="7648.1.1"}
}
4255929 = {
	name="Brea"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7594.1.1 = {birth="7594.1.1"}
	7648.1.1 = {death="7648.1.1"}
}
1355929 = {
	name="Sogoloth"	# not a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1055929
	mother=46755929

	7596.1.1 = {birth="7596.1.1"}
	7598.1.1 = {death="7598.1.1"}
}
1455929 = {
	name="Grisel"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1055929
	mother=46755929

	7599.1.1 = {birth="7599.1.1"}
	7637.1.1 = {death="7637.1.1"}
}
1555929 = {
	name="Harra"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1255929
	mother=4255929

	7613.1.1 = {birth="7613.1.1"}
	7684.1.1 = {death = {death_reason = death_murder}}
}
1655929 = {
	name="Tugg"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1255929
	mother=4255929

	7615.1.1 = {birth="7615.1.1"}
	7631.1.1 = {add_trait=skilled_warrior}
	7633.1.1 = {add_spouse=4655929}
	7685.1.1 = {death="7685.1.1"}
}
4655929 = {
	name="Brella"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7615.1.1 = {birth="7615.1.1"}
	7685.1.1 = {death="7685.1.1"}
}
1755929 = {
	name="Grisel"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1655929
	mother=4655929

	7637.1.1 = {birth="7637.1.1"}
	7679.1.1 = {death="7679.1.1"}
}
1855929 = {
	name="Jogg"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1655929
	mother=4655929

	7638.1.1 = {birth="7638.1.1"}
	7654.1.1 = {add_trait=poor_warrior}
	7656.1.1 = {add_spouse=48055929}
	7689.1.1 = {death="7689.1.1"}
}
48055929 = {
	name="Mellario"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7638.1.1 = {birth="7638.1.1"}
	7689.1.1 = {death="7689.1.1"}
}
1955929 = {
	name="Joth"	# not a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1655929
	mother=4655929

	7640.1.1 = {birth="7640.1.1"}
	7656.1.1 = {add_trait=poor_warrior}
	7685.1.1 = {death="7685.1.1"}
}
2055929 = {
	name="Bodger"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1855929
	mother=48055929

	7665.1.1 = {birth="7665.1.1"}
	7681.1.1 = {add_trait=poor_warrior}
	7683.1.1 = {add_spouse=40155929}
	7721.1.1 = {death="7721.1.1"}
}
40155929 = {
	name="Frynne"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7665.1.1 = {birth="7665.1.1"}
	7721.1.1 = {death="7721.1.1"}
}
2155929 = {
	name="Holger"	# not a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1855929
	mother=48055929

	7668.1.1 = {birth="7668.1.1"}
	7684.1.1 = {add_trait=poor_warrior}
	7714.1.1 = {death="7714.1.1"}
}
2255929 = {
	name="Krull"	# not a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1855929
	mother=48055929

	7671.1.1 = {birth="7671.1.1"}
	7687.1.1 = {add_trait=poor_warrior}
	7713.1.1 = {death="7713.1.1"}
}
2355929 = {
	name="Jogg"	# not a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1855929
	mother=48055929

	7674.1.1 = {birth="7674.1.1"}
	7690.1.1 = {add_trait=poor_warrior}
	7746.1.1 = {death="7746.1.1"}
}
2455929 = {
	name="Quort"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=2055929
	mother=40155929

	7688.1.1 = {birth="7688.1.1"}
	7704.1.1 = {add_trait=poor_warrior}
	7706.1.1 = {add_spouse=44055929}
	7749.1.1 = {death="7749.1.1"}
}
44055929 = {
	name="Gylenios"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7688.1.1 = {birth="7688.1.1"}
	7749.1.1 = {death="7749.1.1"}
}
2555929 = {
	name="Togg"	# not a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=2055929
	mother=40155929

	7689.1.1 = {birth="7689.1.1"}
	7705.1.1 = {add_trait=skilled_warrior}
	7721.1.1 = {death="7721.1.1"}
}
2655929 = {
	name="Mellario"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=2055929
	mother=40155929

	7690.1.1 = {birth="7690.1.1"}
	7747.1.1 = {death="7747.1.1"}
}
2755929 = {
	name="Bellegere"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=2055929
	mother=40155929

	7691.1.1 = {birth="7691.1.1"}
	7717.1.1 = {death="7717.1.1"}
}
2855929 = {
	name="Denia"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=2455929
	mother=44055929

	7720.1.1 = {birth="7720.1.1"}
	7790.1.1 = {death="7790.1.1"}
}
2955929 = {
	name="Draloth"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=2455929
	mother=44055929

	7721.1.1 = {birth="7721.1.1"}
	7737.1.1 = {add_trait=poor_warrior}
	7739.1.1 = {add_spouse=49055929}
	7762.1.1 = {death="7762.1.1"}
}
49055929 = {
	name="Brella"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7721.1.1 = {birth="7721.1.1"}
	7762.1.1 = {death="7762.1.1"}
}
3055929 = {
	name="Ulf"	# not a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=2455929
	mother=44055929

	7724.1.1 = {birth="7724.1.1"}
	7740.1.1 = {add_trait=skilled_warrior}
	7754.1.1 = {death="7754.1.1"}
}
3155929 = {
	name="Holger"	# not a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=2455929
	mother=44055929

	7725.1.1 = {birth="7725.1.1"}
	7741.1.1 = {add_trait=skilled_warrior}
	7779.1.1 = {death="7779.1.1"}
}
3255929 = {
	name="Lanna"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=2955929
	mother=49055929

	7749.1.1 = {birth="7749.1.1"}
	7804.1.1 = {death="7804.1.1"}
}
3355929 = {
	name="Ygon"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=2955929
	mother=49055929

	7752.1.1 = {birth="7752.1.1"}
	7768.1.1 = {add_trait=poor_warrior}
	7770.1.1 = {add_spouse=4355929}
	7806.1.1 = {death="7806.1.1"}
}
4355929 = {
	name="Yorka"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7752.1.1 = {birth="7752.1.1"}
	7806.1.1 = {death="7806.1.1"}
}
3455929 = {
	name="Cull"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=3355929
	mother=4355929

	7773.1.1 = {birth="7773.1.1"}
	7789.1.1 = {add_trait=poor_warrior}
	7791.1.1 = {add_spouse=4455929}
	7814.1.1 = {death="7814.1.1"}
}
4455929 = {
	name="Frynne"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7773.1.1 = {birth="7773.1.1"}
	7814.1.1 = {death="7814.1.1"}
}
3555929 = {
	name="Harra"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=3355929
	mother=4355929

	7776.1.1 = {birth="7776.1.1"}
	7822.1.1 = {death = {death_reason = death_accident}}
}
3655929 = {
	name="Ferny"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=3455929
	mother=4455929

	7798.1.1 = {birth="7798.1.1"}
	7858.1.1 = {death="7858.1.1"}
}
3755929 = {
	name="Betharios"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=3455929
	mother=4455929

	7801.1.1 = {birth="7801.1.1"}
	7842.1.1 = {death="7842.1.1"}
}
3855929 = {
	name="Brogg"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=3455929
	mother=4455929

	7803.1.1 = {birth="7803.1.1"}
	7819.1.1 = {add_trait=poor_warrior}
	7821.1.1 = {add_spouse=4855929}
	7858.1.1 = {death="7858.1.1"}
}
4855929 = {
	name="Ferny"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7803.1.1 = {birth="7803.1.1"}
	7858.1.1 = {death="7858.1.1"}
}
3955929 = {
	name="Groo"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=3855929
	mother=4855929

	7838.1.1 = {birth="7838.1.1"}
	7854.1.1 = {add_trait=poor_warrior}
	7856.1.1 = {add_spouse=4955929}
	7897.1.1 = {death="7897.1.1"}
}
4955929 = {
	name="Bellegere"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7838.1.1 = {birth="7838.1.1"}
	7897.1.1 = {death="7897.1.1"}
}
4055929 = {
	name="Rogga"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	father=3855929
	mother=4855929

	7839.1.1 = {birth="7839.1.1"}
	7874.1.1 = {death="7874.1.1"}
}
40055929 = {
	name="Slugg"

	religion="gods_ibben"
	culture="ibbenese"

	father=3955929
	mother=4955929

	7866.1.1 = {birth="7866.1.1"}
	7882.1.1 = {add_trait=poor_warrior}
}
4155929 = {
	name="Assadora"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	father=3955929
	mother=4955929

	7869.1.1 = {birth="7869.1.1"}
}


####  House Sluggolsh

1055931 = {
	name="Cull"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	7562.1.1 = {birth="7562.1.1"}
	7578.1.1 = {add_trait=poor_warrior}
	7580.1.1 = {add_spouse=40155931}
	7629.1.1 = {death="7629.1.1"}
}
40155931 = {
	name="Tagganaria"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7562.1.1 = {birth="7562.1.1"}
	7629.1.1 = {death="7629.1.1"}
}
1155931 = {
	name="Jaqea"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1055931
	mother=40155931

	7586.1.1 = {birth="7586.1.1"}
	7613.1.1 = {death="7613.1.1"}
}
1255931 = {
	name="Gurn"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1055931
	mother=40155931

	7589.1.1 = {birth="7589.1.1"}
	7605.1.1 = {add_trait=poor_warrior}
	7607.1.1 = {add_spouse=42255931}
	7641.1.1 = {death="7641.1.1"}
}
42255931 = {
	name="Gylenios"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7589.1.1 = {birth="7589.1.1"}
	7641.1.1 = {death="7641.1.1"}
}
1355931 = {
	name="Krull"	# not a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1055931
	mother=40155931

	7590.1.1 = {birth="7590.1.1"}
	7606.1.1 = {add_trait=trained_warrior}
	7670.1.1 = {death="7670.1.1"}
}
1455931 = {
	name="Ferny"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1055931
	mother=40155931

	7591.1.1 = {birth="7591.1.1"}
	7635.1.1 = {death="7635.1.1"}
}
1555931 = {
	name="Erroth"	# not a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1055931
	mother=40155931

	7594.1.1 = {birth="7594.1.1"}
	7610.1.1 = {add_trait=trained_warrior}
	7644.1.1 = {death="7644.1.1"}
}
1655931 = {
	name="Frynne"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1255931
	mother=42255931

	7610.1.1 = {birth="7610.1.1"}
	7661.1.1 = {death="7661.1.1"}
}
1755931 = {
	name="Jogg"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1255931
	mother=42255931

	7612.1.1 = {birth="7612.1.1"}
	7628.1.1 = {add_trait=trained_warrior}
	7630.1.1 = {add_spouse=47055931}
	7649.1.1 = {death = {death_reason = death_murder}}
}
47055931 = {
	name="Izembara"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7612.1.1 = {birth="7612.1.1"}
	7649.1.1 = {death="7649.1.1"}
}
1855931 = {
	name="Grisel"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1755931
	mother=47055931

	7632.1.1 = {birth="7632.1.1"}
	7653.1.1 = {death="7653.1.1"}
}
1955931 = {
	name="Brogg"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1755931
	mother=47055931

	7633.1.1 = {birth="7633.1.1"}
	7649.1.1 = {add_trait=trained_warrior}
	7651.1.1 = {add_spouse=49055931}
	7706.1.1 = {death="7706.1.1"}
}
49055931 = {
	name="Bellegere"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7633.1.1 = {birth="7633.1.1"}
	7706.1.1 = {death="7706.1.1"}
}
2055931 = {
	name="Quort"	# not a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1755931
	mother=47055931

	7636.1.1 = {birth="7636.1.1"}
	7652.1.1 = {add_trait=skilled_warrior}
	7697.1.1 = {death="7697.1.1"}
}
2155931 = {
	name="Rowan"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1755931
	mother=47055931

	7638.1.1 = {birth="7638.1.1"}
	7717.1.1 = {death="7717.1.1"}
}
2255931 = {
	name="Jogg"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1955931
	mother=49055931

	7665.1.1 = {birth="7665.1.1"}
	7681.1.1 = {add_trait=trained_warrior}
	7683.1.1 = {add_spouse=42155931}
	7732.1.1 = {death="7732.1.1"}
}
42155931 = {
	name="Mordea"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7665.1.1 = {birth="7665.1.1"}
	7732.1.1 = {death="7732.1.1"}
}
2355931 = {
	name="Cossoma"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1955931
	mother=49055931

	7666.1.1 = {birth="7666.1.1"}
	7700.1.1 = {death="7700.1.1"}
}
2455931 = {
	name="Gylenios"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1955931
	mother=49055931

	7669.1.1 = {birth="7669.1.1"}
	7721.1.1 = {death="7721.1.1"}
}
2555931 = {
	name="Assadora"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1955931
	mother=49055931

	7672.1.1 = {birth="7672.1.1"}
	7726.1.1 = {death="7726.1.1"}
}
2655931 = {
	name="Durr"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=2255931
	mother=42155931

	7688.1.1 = {birth="7688.1.1"}
	7704.1.1 = {add_trait=trained_warrior}
	7706.1.1 = {add_spouse=4655931}
	7758.1.1 = {death="7758.1.1"}
}
4655931 = {
	name="Yna"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7688.1.1 = {birth="7688.1.1"}
	7758.1.1 = {death="7758.1.1"}
}
2755931 = {
	name="Slugg"	# not a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=2255931
	mother=42155931

	7690.1.1 = {birth="7690.1.1"}
	7706.1.1 = {add_trait=trained_warrior}
	7749.1.1 = {death="7749.1.1"}
}
2855931 = {
	name="Jarr"	# not a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=2255931
	mother=42155931

	7693.1.1 = {birth="7693.1.1"}
	7709.1.1 = {add_trait=poor_warrior}
	7769.1.1 = {death="7769.1.1"}
}
2955931 = {
	name="Ynys"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=2255931
	mother=42155931

	7694.1.1 = {birth="7694.1.1"}
	7736.1.1 = {death="7736.1.1"}
}
3055931 = {
	name="Haggon"	# not a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=2255931
	mother=42155931

	7695.1.1 = {birth="7695.1.1"}
	7711.1.1 = {add_trait=poor_warrior}
	7757.1.1 = {death="7757.1.1"}
}
3155931 = {
	name="Umma"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=2655931
	mother=4655931

	7711.1.1 = {birth="7711.1.1"}
	7746.1.1 = {death="7746.1.1"}
}
3255931 = {
	name="Howd"	# not a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=2655931
	mother=4655931

	7712.1.1 = {birth="7712.1.1"}
	7728.1.1 = {add_trait=trained_warrior}
	7755.1.1 = {death="7755.1.1"}
}
3355931 = {
	name="Bodger"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=2655931
	mother=4655931

	7715.1.1 = {birth="7715.1.1"}
	7731.1.1 = {add_trait=poor_warrior}
	7733.1.1 = {add_spouse=4355931}
	7769.1.1 = {death="7769.1.1"}
}
4355931 = {
	name="Tagganaria"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7715.1.1 = {birth="7715.1.1"}
	7769.1.1 = {death="7769.1.1"}
}
3455931 = {
	name="Izembara"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=3355931
	mother=4355931

	7742.1.1 = {birth="7742.1.1"}
	7809.1.1 = {death="7809.1.1"}
}
3555931 = {
	name="Cull"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=3355931
	mother=4355931

	7744.1.1 = {birth="7744.1.1"}
	7760.1.1 = {add_trait=poor_warrior}
	7762.1.1 = {add_spouse=4555931}
	7804.1.1 = {death="7804.1.1"}
}
4555931 = {
	name="Torma"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7744.1.1 = {birth="7744.1.1"}
	7804.1.1 = {death="7804.1.1"}
}
3655931 = {
	name="Ygon"	# not a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=3555931
	mother=4555931

	7762.1.1 = {birth="7762.1.1"}
	7778.1.1 = {add_trait=skilled_warrior}
	7789.1.1 = {death="7789.1.1"}
}
3755931 = {
	name="Quort"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=3555931
	mother=4555931

	7764.1.1 = {birth="7764.1.1"}
	7780.1.1 = {add_trait=trained_warrior}
	7782.1.1 = {add_spouse=4755931}
	7824.1.1 = {death="7824.1.1"}
}
4755931 = {
	name="Zia"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7764.1.1 = {birth="7764.1.1"}
	7824.1.1 = {death="7824.1.1"}
}
3855931 = {
	name="Drogg"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=3755931
	mother=4755931

	7793.1.1 = {birth="7793.1.1"}
	7809.1.1 = {add_trait=master_warrior}
	7811.1.1 = {add_spouse=4855931}
	7832.1.1 = {death="7832.1.1"}
}
4855931 = {
	name="Frynne"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7793.1.1 = {birth="7793.1.1"}
	7832.1.1 = {death="7832.1.1"}
}
3955931 = {
	name="Joth"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=3855931
	mother=4855931

	7812.1.1 = {birth="7812.1.1"}
	7828.1.1 = {add_trait=poor_warrior}
	7830.1.1 = {add_spouse=4955931}
	7864.1.1 = {death="7864.1.1"}
}
4955931 = {
	name="Rowan"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7812.1.1 = {birth="7812.1.1"}
	7864.1.1 = {death="7864.1.1"}
}
40255931 = {
	name="Sogoloth"

	religion="gods_ibben"
	culture="ibbenese"

	father=3855931
	mother=4855931

	7813.1.1 = {birth="7813.1.1"}
	7829.1.1 = {add_trait=trained_warrior}
	7870.1.1 = {death="7870.1.1"}
}
41055931 = {
	name="Meralyia"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	father=3855931
	mother=4855931

	7814.1.1 = {birth="7814.1.1"}
	7854.1.1 = {death="7854.1.1"}
}
4255931 = {
	name="Jogg"

	religion="gods_ibben"
	culture="ibbenese"

	father=3855931
	mother=4855931

	7815.1.1 = {birth="7815.1.1"}
	7831.1.1 = {add_trait=trained_warrior}
	7869.1.1 = {death="7869.1.1"}
}
40355931 = {
	name="Frynne"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	father=3955931
	mother=4955931

	7837.1.1 = {birth="7837.1.1"}
	7869.1.1 = {death="7869.1.1"}
}
4155931 = {
	name="Howd"

	religion="gods_ibben"
	culture="ibbenese"

	father=3955931
	mother=4955931

	7840.1.1 = {birth="7840.1.1"}
	7856.1.1 = {add_trait=poor_warrior}
	7858.1.1 = {add_spouse=41155931}
	7889.1.1 = {death="7889.1.1"}
}
41155931 = {
	name="Mellario"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7840.1.1 = {birth="7840.1.1"}
	7889.1.1 = {death="7889.1.1"}
}
42555931 = {
	name="Assadora"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	father=3955931
	mother=4955931

	7841.1.1 = {birth="7841.1.1"}
}
40555931 = {
	name="Eleogg"

	religion="gods_ibben"
	culture="ibbenese"

	father=4155931
	mother=41155931

	7867.1.1 = {birth="7867.1.1"}
	7883.1.1 = {add_trait=skilled_warrior}
	7885.1.1 = {add_spouse=40455931}
}
40455931 = {
	name="Betharios"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7867.1.1 = {birth="7867.1.1"}
}
4055931 = {
	name="Bellegere"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	father=40555931
	mother=40455931

	7900.1.1 = {birth="7900.1.1"}
}



