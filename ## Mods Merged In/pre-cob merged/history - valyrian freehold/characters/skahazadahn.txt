#############   House zo Pazak

1000208 = {
	name="Grazdhar"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	7474.1.1 = {birth="7474.1.1"}
	7490.1.1 = {add_trait=poor_warrior}
	7492.1.1 = {add_spouse=4000208}
	7530.1.1 = {death="7530.1.1"}
}
4000208 = {
	name="Yezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7474.1.1 = {birth="7474.1.1"}
	7530.1.1 = {death="7530.1.1"}
}
1001208 = {
	name="Yarkhaz"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1000208
	mother=4000208

	7507.1.1 = {birth="7507.1.1"}
	7523.1.1 = {add_trait=trained_warrior}
	7525.1.1 = {add_spouse=4001208}
	7582.1.1 = {death="7582.1.1"}
}
4001208 = {
	name="Yezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7507.1.1 = {birth="7507.1.1"}
	7582.1.1 = {death="7582.1.1"}
}
1002208 = {
	name="Morghaz"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1001208
	mother=4001208

	7532.1.1 = {birth="7532.1.1"}
	7548.1.1 = {add_trait=skilled_warrior}
	7550.1.1 = {add_spouse=4002208}
	7609.1.1 = {death = {death_reason = death_accident}}
}
4002208 = {
	name="Gezzella"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7532.1.1 = {birth="7532.1.1"}
	7609.1.1 = {death="7609.1.1"}
}
1003208 = {
	name="Maezon"	# not a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1001208
	mother=4001208

	7533.1.1 = {birth="7533.1.1"}
	7549.1.1 = {add_trait=trained_warrior}
	7607.1.1 = {death="7607.1.1"}
}
1004208 = {
	name="Zellazara"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1002208
	mother=4002208

	7563.1.1 = {birth="7563.1.1"}
	7607.1.1 = {death="7607.1.1"}
}
1005208 = {
	name="Reznak"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1002208
	mother=4002208

	7566.1.1 = {birth="7566.1.1"}
	7582.1.1 = {add_trait=poor_warrior}
	7584.1.1 = {add_spouse=4005208}
	7629.1.1 = {death="7629.1.1"}
}
4005208 = {
	name="Zellazara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7566.1.1 = {birth="7566.1.1"}
	7629.1.1 = {death="7629.1.1"}
}
1006208 = {
	name="Zaraq"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1005208
	mother=4005208

	7592.1.1 = {birth="7592.1.1"}
	7608.1.1 = {add_trait=skilled_warrior}
	7610.1.1 = {add_spouse=4006208}
	7655.1.1 = {death="7655.1.1"}
}
4006208 = {
	name="Qezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7592.1.1 = {birth="7592.1.1"}
	7655.1.1 = {death="7655.1.1"}
}
1007208 = {
	name="Hazzea"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1005208
	mother=4005208

	7595.1.1 = {birth="7595.1.1"}
	7637.1.1 = {death="7637.1.1"}
}
1008208 = {
	name="Ghazdor"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1006208
	mother=4006208

	7624.1.1 = {birth="7624.1.1"}
	7640.1.1 = {add_trait=skilled_warrior}
	7642.1.1 = {add_spouse=4008208}
	7693.1.1 = {death="7693.1.1"}
}
4008208 = {
	name="Selezzqa"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7624.1.1 = {birth="7624.1.1"}
	7693.1.1 = {death="7693.1.1"}
}
1009208 = {
	name="Grazdhar"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1008208
	mother=4008208

	7649.1.1 = {birth="7649.1.1"}
	7665.1.1 = {add_trait=poor_warrior}
	7667.1.1 = {add_spouse=4009208}
	7724.1.1 = {death="7724.1.1"}
}
4009208 = {
	name="Mezzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7649.1.1 = {birth="7649.1.1"}
	7724.1.1 = {death="7724.1.1"}
}
1010208 = {
	name="Mazdhan"	# not a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1008208
	mother=4008208

	7652.1.1 = {birth="7652.1.1"}
	7668.1.1 = {add_trait=skilled_warrior}
	7690.1.1 = {death="7690.1.1"}
}
1011208 = {
	name="Ellezzara"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1009208
	mother=4009208

	7682.1.1 = {birth="7682.1.1"}
	7719.1.1 = {death="7719.1.1"}
}
1012208 = {
	name="Fendahl"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1009208
	mother=4009208

	7683.1.1 = {birth="7683.1.1"}
	7699.1.1 = {add_trait=poor_warrior}
	7701.1.1 = {add_spouse=4012208}
	7738.1.1 = {death="7738.1.1"}
}
4012208 = {
	name="Qezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7683.1.1 = {birth="7683.1.1"}
	7738.1.1 = {death="7738.1.1"}
}
1013208 = {
	name="Yarkhaz"	# not a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1009208
	mother=4009208

	7684.1.1 = {birth="7684.1.1"}
	7700.1.1 = {add_trait=trained_warrior}
	7726.1.1 = {death="7726.1.1"}
}
1014208 = {
	name="Marghaz"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1012208
	mother=4012208

	7707.1.1 = {birth="7707.1.1"}
	7723.1.1 = {add_trait=poor_warrior}
	7725.1.1 = {add_spouse=4014208}
	7753.1.1 = {death="7753.1.1"}
}
4014208 = {
	name="Hazzea"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7707.1.1 = {birth="7707.1.1"}
	7753.1.1 = {death="7753.1.1"}
}
1015208 = {
	name="Reznak"	# not a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1012208
	mother=4012208

	7708.1.1 = {birth="7708.1.1"}
	7724.1.1 = {add_trait=poor_warrior}
	7779.1.1 = {death="7779.1.1"}
}
1016208 = {
	name="Dhazzar"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1012208
	mother=4012208

	7709.1.1 = {birth="7709.1.1"}
	7774.1.1 = {death="7774.1.1"}
}
1017208 = {
	name="Haznak"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1014208
	mother=4014208

	7731.1.1 = {birth="7731.1.1"}
	7747.1.1 = {add_trait=poor_warrior}
	7749.1.1 = {add_spouse=4017208}
	7783.1.1 = {death="7783.1.1"}
}
4017208 = {
	name="Jezhane"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7731.1.1 = {birth="7731.1.1"}
	7783.1.1 = {death="7783.1.1"}
}
1018208 = {
	name="Tezza"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1014208
	mother=4014208

	7733.1.1 = {birth="7733.1.1"}
	7792.1.1 = {death="7792.1.1"}
}
1019208 = {
	name="Selezzqa"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1014208
	mother=4014208

	7736.1.1 = {birth="7736.1.1"}
	7777.1.1 = {death="7777.1.1"}
}
1020208 = {
	name="Azzara"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1014208
	mother=4014208

	7738.1.1 = {birth="7738.1.1"}
	7790.1.1 = {death="7790.1.1"}
}
1021208 = {
	name="Skahaz"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1017208
	mother=4017208

	7759.1.1 = {birth="7759.1.1"}
	7775.1.1 = {add_trait=trained_warrior}
	7777.1.1 = {add_spouse=4021208}
	7816.1.1 = {death="7816.1.1"}
}
4021208 = {
	name="Ellezzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7759.1.1 = {birth="7759.1.1"}
	7816.1.1 = {death="7816.1.1"}
}
1022208 = {
	name="Yezza"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1017208
	mother=4017208

	7760.1.1 = {birth="7760.1.1"}
	7836.1.1 = {death="7836.1.1"}
}
1023208 = {
	name="Fendahl"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1021208
	mother=4021208

	7783.1.1 = {birth="7783.1.1"}
	7799.1.1 = {add_trait=poor_warrior}
	7831.1.1 = {death="7831.1.1"}
}


#################   House zo Zasnzn

1000213 = {
	name="Miklaz"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	7460.1.1 = {birth="7460.1.1"}
	7476.1.1 = {add_trait=poor_warrior}
	7478.1.1 = {add_spouse=4000213}
	7523.1.1 = {death="7523.1.1"}
}
4000213 = {
	name="Galazza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7460.1.1 = {birth="7460.1.1"}
	7523.1.1 = {death="7523.1.1"}
}
1001213 = {
	name="Skahaz"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1000213
	mother=4000213

	7487.1.1 = {birth="7487.1.1"}
	7503.1.1 = {add_trait=poor_warrior}
	7505.1.1 = {add_spouse=4001213}
	7543.1.1 = {death="7543.1.1"}
}
4001213 = {
	name="Galazza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7487.1.1 = {birth="7487.1.1"}
	7543.1.1 = {death="7543.1.1"}
}
1002213 = {
	name="Skahaz"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1000213
	mother=4000213

	7490.1.1 = {birth="7490.1.1"}
	7506.1.1 = {add_trait=poor_warrior}
	7557.1.1 = {death="7557.1.1"}
}
1003213 = {
	name="Azzak"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1000213
	mother=4000213

	7493.1.1 = {birth="7493.1.1"}
	7509.1.1 = {add_trait=trained_warrior}
	7542.1.1 = {death="7542.1.1"}
}
1004213 = {
	name="Yezza"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1000213
	mother=4000213

	7496.1.1 = {birth="7496.1.1"}
	7547.1.1 = {death="7547.1.1"}
}
1005213 = {
	name="Tellezzara"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1001213
	mother=4001213

	7511.1.1 = {birth="7511.1.1"}
	7590.1.1 = {death="7590.1.1"}
}
1006213 = {
	name="Sezza"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1001213
	mother=4001213

	7513.1.1 = {birth="7513.1.1"}
	7545.1.1 = {death="7545.1.1"}
}
1007213 = {
	name="Maezon"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1001213
	mother=4001213

	7516.1.1 = {birth="7516.1.1"}
	7532.1.1 = {add_trait=poor_warrior}
	7534.1.1 = {add_spouse=4007213}
	7557.1.1 = {death="7557.1.1"}
}
4007213 = {
	name="Hazzaara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7516.1.1 = {birth="7516.1.1"}
	7557.1.1 = {death="7557.1.1"}
}
1008213 = {
	name="Maezon"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1001213
	mother=4001213

	7518.1.1 = {birth="7518.1.1"}
	7534.1.1 = {add_trait=poor_warrior}
	7579.1.1 = {death = {death_reason = death_accident}}
}
1009213 = {
	name="Azzara"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1007213
	mother=4007213

	7534.1.1 = {birth="7534.1.1"}
	7585.1.1 = {death="7585.1.1"}
}
1010213 = {
	name="Hizdhar"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1007213
	mother=4007213

	7536.1.1 = {birth="7536.1.1"}
	7552.1.1 = {add_trait=trained_warrior}
	7554.1.1 = {add_spouse=4010213}
	7609.1.1 = {death="7609.1.1"}
}
4010213 = {
	name="Zellazara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7536.1.1 = {birth="7536.1.1"}
	7609.1.1 = {death="7609.1.1"}
}
1011213 = {
	name="Fendahl"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1010213
	mother=4010213

	7554.1.1 = {birth="7554.1.1"}
	7570.1.1 = {add_trait=master_warrior}
	7572.1.1 = {add_spouse=4011213}
	7612.1.1 = {death="7612.1.1"}
}
4011213 = {
	name="Selezzqa"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7554.1.1 = {birth="7554.1.1"}
	7612.1.1 = {death="7612.1.1"}
}
1012213 = {
	name="Azzea"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1010213
	mother=4010213

	7555.1.1 = {birth="7555.1.1"}
	7612.1.1 = {death = {death_reason = death_accident}}
}
1013213 = {
	name="Galazza"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1010213
	mother=4010213

	7558.1.1 = {birth="7558.1.1"}
	7608.1.1 = {death="7608.1.1"}
}
1014213 = {
	name="Dhazzar"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1010213
	mother=4010213

	7561.1.1 = {birth="7561.1.1"}
	7623.1.1 = {death="7623.1.1"}
}
1015213 = {
	name="Morghaz"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1011213
	mother=4011213

	7582.1.1 = {birth="7582.1.1"}
	7598.1.1 = {add_trait=poor_warrior}
	7600.1.1 = {add_spouse=4015213}
	7637.1.1 = {death="7637.1.1"}
}
4015213 = {
	name="Ellezzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7582.1.1 = {birth="7582.1.1"}
	7637.1.1 = {death="7637.1.1"}
}
1016213 = {
	name="Azzea"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1015213
	mother=4015213

	7614.1.1 = {birth="7614.1.1"}
	7676.1.1 = {death="7676.1.1"}
}
1017213 = {
	name="Paezhar"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1015213
	mother=4015213

	7616.1.1 = {birth="7616.1.1"}
	7632.1.1 = {add_trait=poor_warrior}
	7634.1.1 = {add_spouse=4017213}
	7684.1.1 = {death="7684.1.1"}
}
4017213 = {
	name="Azzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7616.1.1 = {birth="7616.1.1"}
	7684.1.1 = {death="7684.1.1"}
}
1018213 = {
	name="Qazella"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1015213
	mother=4015213

	7618.1.1 = {birth="7618.1.1"}
	7695.1.1 = {death="7695.1.1"}
}
1019213 = {
	name="Maezon"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1015213
	mother=4015213

	7619.1.1 = {birth="7619.1.1"}
	7635.1.1 = {add_trait=poor_warrior}
	7689.1.1 = {death = {death_reason = death_accident}}
}
1020213 = {
	name="Ghazdor"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1017213
	mother=4017213

	7643.1.1 = {birth="7643.1.1"}
	7659.1.1 = {add_trait=skilled_warrior}
	7673.1.1 = {death = {death_reason = death_murder}}
}
1021213 = {
	name="Mazdhan"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1017213
	mother=4017213

	7645.1.1 = {birth="7645.1.1"}
	7661.1.1 = {add_trait=poor_warrior}
	7663.1.1 = {add_spouse=4021213}
	7698.1.1 = {death="7698.1.1"}
}
4021213 = {
	name="Hazzea"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7645.1.1 = {birth="7645.1.1"}
	7698.1.1 = {death="7698.1.1"}
}
1022213 = {
	name="Faezdhar"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1021213
	mother=4021213

	7663.1.1 = {birth="7663.1.1"}
	7679.1.1 = {add_trait=poor_warrior}
	7681.1.1 = {add_spouse=4022213}
	7731.1.1 = {death="7731.1.1"}
}
4022213 = {
	name="Ezzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7663.1.1 = {birth="7663.1.1"}
	7731.1.1 = {death="7731.1.1"}
}
1023213 = {
	name="Kraznys"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1022213
	mother=4022213

	7692.1.1 = {birth="7692.1.1"}
	7708.1.1 = {add_trait=trained_warrior}
	7710.1.1 = {add_spouse=4023213}
	7767.1.1 = {death="7767.1.1"}
}
4023213 = {
	name="Qazella"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7692.1.1 = {birth="7692.1.1"}
	7767.1.1 = {death="7767.1.1"}
}
1024213 = {
	name="Haznak"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1022213
	mother=4022213

	7695.1.1 = {birth="7695.1.1"}
	7711.1.1 = {add_trait=poor_warrior}
	7740.1.1 = {death="7740.1.1"}
}
1025213 = {
	name="Ghazdor"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1022213
	mother=4022213

	7696.1.1 = {birth="7696.1.1"}
	7712.1.1 = {add_trait=skilled_warrior}
	7756.1.1 = {death="7756.1.1"}
}
1026213 = {
	name="Harghaz"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1022213
	mother=4022213

	7699.1.1 = {birth="7699.1.1"}
	7715.1.1 = {add_trait=poor_warrior}
	7756.1.1 = {death="7756.1.1"}
}
1027213 = {
	name="Gezzella"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1023213
	mother=4023213

	7719.1.1 = {birth="7719.1.1"}
	7752.1.1 = {death="7752.1.1"}
}
1028213 = {
	name="Jezhane"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1023213
	mother=4023213

	7722.1.1 = {birth="7722.1.1"}
	7795.1.1 = {death="7795.1.1"}
}
1029213 = {
	name="Ghazdor"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1023213
	mother=4023213

	7724.1.1 = {birth="7724.1.1"}
	7740.1.1 = {add_trait=poor_warrior}
	7742.1.1 = {add_spouse=4029213}
	7772.1.1 = {death="7772.1.1"}
}
4029213 = {
	name="Ezzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7724.1.1 = {birth="7724.1.1"}
	7772.1.1 = {death="7772.1.1"}
}
1030213 = {
	name="Sezqa"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1023213
	mother=4023213

	7726.1.1 = {birth="7726.1.1"}
	7800.1.1 = {death="7800.1.1"}
}
1031213 = {
	name="Paezhar"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1029213
	mother=4029213

	7746.1.1 = {birth="7746.1.1"}
	7762.1.1 = {add_trait=poor_warrior}
	7764.1.1 = {add_spouse=4031213}
	7789.1.1 = {death="7789.1.1"}
}
4031213 = {
	name="Qezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7746.1.1 = {birth="7746.1.1"}
	7789.1.1 = {death="7789.1.1"}
}
1032213 = {
	name="Selezzqa"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1031213
	mother=4031213

	7764.1.1 = {birth="7764.1.1"}
	7840.1.1 = {death="7840.1.1"}
}
1033213 = {
	name="Skahaz"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1031213
	mother=4031213

	7765.1.1 = {birth="7765.1.1"}
	7781.1.1 = {add_trait=skilled_warrior}
	7783.1.1 = {add_spouse=4033213}
	7824.1.1 = {death="7824.1.1"}
}
4033213 = {
	name="Mezzala"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7765.1.1 = {birth="7765.1.1"}
	7824.1.1 = {death="7824.1.1"}
}
1034213 = {
	name="Ellezzara"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1031213
	mother=4031213

	7768.1.1 = {birth="7768.1.1"}
	7837.1.1 = {death="7837.1.1"}
}
1035213 = {
	name="Barghaz"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1031213
	mother=4031213

	7771.1.1 = {birth="7771.1.1"}
	7787.1.1 = {add_trait=trained_warrior}
	7795.1.1 = {death="7795.1.1"}
}
1036213 = {
	name="Reznak"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1033213
	mother=4033213

	7785.1.1 = {birth="7785.1.1"}
	7801.1.1 = {add_trait=poor_warrior}
	7841.1.1 = {death="7841.1.1"}
}
1037213 = {
	name="Galazza"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1033213
	mother=4033213

	7787.1.1 = {birth="7787.1.1"}
	7790.1.1 = {death="7790.1.1"}
}


######################  House na Yhezher

1000220 = {
	name="Paezhar"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	7480.1.1 = {birth="7480.1.1"}
	7496.1.1 = {add_trait=poor_warrior}
	7498.1.1 = {add_spouse=4000220}
	7535.1.1 = {death="7535.1.1"}
}
4000220 = {
	name="Yezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7480.1.1 = {birth="7480.1.1"}
	7535.1.1 = {death="7535.1.1"}
}
1001220 = {
	name="Haznak"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1000220
	mother=4000220

	7506.1.1 = {birth="7506.1.1"}
	7522.1.1 = {add_trait=poor_warrior}
	7524.1.1 = {add_spouse=4001220}
	7574.1.1 = {death="7574.1.1"}
}
4001220 = {
	name="Zellazara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7506.1.1 = {birth="7506.1.1"}
	7574.1.1 = {death="7574.1.1"}
}
1002220 = {
	name="Fendahl"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1001220
	mother=4001220

	7530.1.1 = {birth="7530.1.1"}
	7546.1.1 = {add_trait=trained_warrior}
	7555.1.1 = {death="7555.1.1"}
}
1003220 = {
	name="Gezzella"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1001220
	mother=4001220

	7532.1.1 = {birth="7532.1.1"}
	7585.1.1 = {death="7585.1.1"}
}
1004220 = {
	name="Skahaz"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1001220
	mother=4001220

	7533.1.1 = {birth="7533.1.1"}
	7549.1.1 = {add_trait=poor_warrior}
	7551.1.1 = {add_spouse=4004220}
	7591.1.1 = {death="7591.1.1"}
}
4004220 = {
	name="Hazzea"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7533.1.1 = {birth="7533.1.1"}
	7591.1.1 = {death="7591.1.1"}
}
1005220 = {
	name="Skahaz"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1001220
	mother=4001220

	7535.1.1 = {birth="7535.1.1"}
	7551.1.1 = {add_trait=trained_warrior}
	7590.1.1 = {death="7590.1.1"}
}
1006220 = {
	name="Zaraq"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1004220
	mother=4004220

	7560.1.1 = {birth="7560.1.1"}
	7576.1.1 = {add_trait=poor_warrior}
	7578.1.1 = {add_spouse=4006220}
	7626.1.1 = {death="7626.1.1"}
}
4006220 = {
	name="Qazella"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7560.1.1 = {birth="7560.1.1"}
	7626.1.1 = {death="7626.1.1"}
}
1007220 = {
	name="Yezza"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1006220
	mother=4006220

	7591.1.1 = {birth="7591.1.1"}
	7655.1.1 = {death="7655.1.1"}
}
1008220 = {
	name="Gorzhak"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1006220
	mother=4006220

	7592.1.1 = {birth="7592.1.1"}
	7608.1.1 = {add_trait=trained_warrior}
	7610.1.1 = {add_spouse=4008220}
	7658.1.1 = {death="7658.1.1"}
}
4008220 = {
	name="Tezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7592.1.1 = {birth="7592.1.1"}
	7658.1.1 = {death="7658.1.1"}
}
1009220 = {
	name="Reznak"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1006220
	mother=4006220

	7593.1.1 = {birth="7593.1.1"}
	7609.1.1 = {add_trait=poor_warrior}
	7622.1.1 = {death="7622.1.1"}
}
1010220 = {
	name="Morghaz"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1006220
	mother=4006220

	7595.1.1 = {birth="7595.1.1"}
	7611.1.1 = {add_trait=trained_warrior}
	7667.1.1 = {death="7667.1.1"}
}
1011220 = {
	name="Mezzala"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1008220
	mother=4008220

	7626.1.1 = {birth="7626.1.1"}
	7675.1.1 = {death="7675.1.1"}
}
1012220 = {
	name="Ghazdor"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1008220
	mother=4008220

	7629.1.1 = {birth="7629.1.1"}
	7645.1.1 = {add_trait=poor_warrior}
	7647.1.1 = {add_spouse=4012220}
	7690.1.1 = {death="7690.1.1"}
}
4012220 = {
	name="Azzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7629.1.1 = {birth="7629.1.1"}
	7690.1.1 = {death="7690.1.1"}
}
1013220 = {
	name="Azzara"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1008220
	mother=4008220

	7632.1.1 = {birth="7632.1.1"}
	7664.1.1 = {death="7664.1.1"}
}
1014220 = {
	name="Barkhaz"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1008220
	mother=4008220

	7633.1.1 = {birth="7633.1.1"}
	7649.1.1 = {add_trait=trained_warrior}
	7710.1.1 = {death="7710.1.1"}
}
1015220 = {
	name="Yurkhaz"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1012220
	mother=4012220

	7655.1.1 = {birth="7655.1.1"}
	7671.1.1 = {add_trait=skilled_warrior}
	7673.1.1 = {add_spouse=4015220}
	7703.1.1 = {death="7703.1.1"}
}
4015220 = {
	name="Dhazzar"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7655.1.1 = {birth="7655.1.1"}
	7703.1.1 = {death="7703.1.1"}
}
1016220 = {
	name="Hazzea"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1012220
	mother=4012220

	7658.1.1 = {birth="7658.1.1"}
	7711.1.1 = {death="7711.1.1"}
}
1017220 = {
	name="Grazdhan"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1015220
	mother=4015220

	7674.1.1 = {birth="7674.1.1"}
	7690.1.1 = {add_trait=trained_warrior}
	7692.1.1 = {add_spouse=4017220}
	7724.1.1 = {death="7724.1.1"}
}
4017220 = {
	name="Dhazzar"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7674.1.1 = {birth="7674.1.1"}
	7724.1.1 = {death="7724.1.1"}
}
1018220 = {
	name="Barkhaz"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1015220
	mother=4015220

	7675.1.1 = {birth="7675.1.1"}
	7691.1.1 = {add_trait=poor_warrior}
	7744.1.1 = {death="7744.1.1"}
}
1019220 = {
	name="Bharkaz"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1017220
	mother=4017220

	7697.1.1 = {birth="7697.1.1"}
	7713.1.1 = {add_trait=skilled_warrior}
	7715.1.1 = {add_spouse=4019220}
	7750.1.1 = {death="7750.1.1"}
}
4019220 = {
	name="Jezhane"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7697.1.1 = {birth="7697.1.1"}
	7750.1.1 = {death="7750.1.1"}
}
1020220 = {
	name="Mellezzara"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1017220
	mother=4017220

	7698.1.1 = {birth="7698.1.1"}
	7728.1.1 = {death="7728.1.1"}
}
1021220 = {
	name="Skahaz"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1017220
	mother=4017220

	7700.1.1 = {birth="7700.1.1"}
	7716.1.1 = {add_trait=skilled_warrior}
	7738.1.1 = {death="7738.1.1"}
}
1022220 = {
	name="Azzara"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1017220
	mother=4017220

	7701.1.1 = {birth="7701.1.1"}
	7772.1.1 = {death="7772.1.1"}
}
1023220 = {
	name="Yarkhaz"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1019220
	mother=4019220

	7716.1.1 = {birth="7716.1.1"}
	7732.1.1 = {add_trait=trained_warrior}
	7734.1.1 = {add_spouse=4023220}
	7782.1.1 = {death="7782.1.1"}
}
4023220 = {
	name="Gezzella"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7716.1.1 = {birth="7716.1.1"}
	7782.1.1 = {death="7782.1.1"}
}
1024220 = {
	name="Tezza"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1019220
	mother=4019220

	7718.1.1 = {birth="7718.1.1"}
	7762.1.1 = {death="7762.1.1"}
}
1025220 = {
	name="Yezza"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1019220
	mother=4019220

	7720.1.1 = {birth="7720.1.1"}
	7773.1.1 = {death="7773.1.1"}
}
1026220 = {
	name="Yezzan"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1019220
	mother=4019220

	7721.1.1 = {birth="7721.1.1"}
	7737.1.1 = {add_trait=trained_warrior}
	7796.1.1 = {death="7796.1.1"}
}
1027220 = {
	name="Barkhaz"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1023220
	mother=4023220

	7751.1.1 = {birth="7751.1.1"}
	7767.1.1 = {add_trait=poor_warrior}
	7769.1.1 = {add_spouse=4027220}
	7805.1.1 = {death="7805.1.1"}
}
4027220 = {
	name="Tezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7751.1.1 = {birth="7751.1.1"}
	7805.1.1 = {death="7805.1.1"}
}
1028220 = {
	name="Mezzala"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1023220
	mother=4023220

	7752.1.1 = {birth="7752.1.1"}
	7802.1.1 = {death="7802.1.1"}
}
1029220 = {
	name="Tezza"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1023220
	mother=4023220

	7755.1.1 = {birth="7755.1.1"}
	7813.1.1 = {death="7813.1.1"}
}
1030220 = {
	name="Gezzella"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1023220
	mother=4023220

	7758.1.1 = {birth="7758.1.1"}
	7816.1.1 = {death="7816.1.1"}
}
1031220 = {
	name="Hazzaara"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1023220
	mother=4023220

	7761.1.1 = {birth="7761.1.1"}
	7833.1.1 = {death="7833.1.1"}
}
1032220 = {
	name="Azzara"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1027220
	mother=4027220

	7773.1.1 = {birth="7773.1.1"}
	7840.1.1 = {death="7840.1.1"}
}
1033220 = {
	name="Hizdhar"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1027220
	mother=4027220

	7774.1.1 = {birth="7774.1.1"}
	7790.1.1 = {add_trait=trained_warrior}
	7792.1.1 = {add_spouse=4033220}
	7818.1.1 = {death="7818.1.1"}
}
4033220 = {
	name="Hazzaara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7774.1.1 = {birth="7774.1.1"}
	7818.1.1 = {death="7818.1.1"}
}
1034220 = {
	name="Kraznys"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1027220
	mother=4027220

	7777.1.1 = {birth="7777.1.1"}
	7793.1.1 = {add_trait=poor_warrior}
	7836.1.1 = {death="7836.1.1"}
}
1035220 = {
	name="Faezdhar"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1033220
	mother=4033220

	7797.1.1 = {birth="7797.1.1"}
	7813.1.1 = {add_trait=poor_warrior}
	7857.1.1 = {death="7857.1.1"}
}
1036220 = {
	name="Jezhane"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1033220
	mother=4033220

	7798.1.1 = {birth="7798.1.1"}
	7855.1.1 = {death="7855.1.1"}
}


