7300.1.1={ # About 700 years before the events of the books Nymeria landed.
	law = succ_primogeniture
	law = true_cognatic_succession
	law = centralization_2
	law = investiture_law_2
	law = first_night_1
	holder=2100001 #Mors
}
7309.1.1 = {
	holder = 2000001 #Nymeria
}
7336.1.1 = { holder=6055001} # Nymeria II (nc)
7358.1.1 = { holder=6056001} # Mors II (C)
7390.1.1 = { holder=60570001} # Myriah (nc)
7418.1.1 = { holder=60840001} # Ellaria (nc)
7445.1.1 = { holder=6058001 } # Garin (nc)
7470.1.1 = { holder=6059001 } # Nymeria (nc)
7496.1.1 = { holder=6060001 } # Mors (nc)
7504.1.1 = { holder=6061001 } # Chroyana (nc)
7532.1.1 = { holder=6062001 } # Anders (nc)
7567.1.1 = { holder=6064001 } # Arron (nc)
7596.1.1 = { holder=6065001 } # Dorea (nc)
7598.1.1 = { holder=6066001 } # Nymella (nc)
7635.1.1 = { holder=6069001 } # Theodan (nc)
7664.1.1 = { holder=6070001 } # Arron (nc)
7712.1.1 = { holder=6071001 } # Nymerios (nc)
7748.1.1 = { holder=6073001 } # Oberyn (nc)
7763.1.1 = { holder=6074001 } # Nyeron (nc)
7779.1.1 = { holder=6075001 } # Nysterica (nc)
7790.1.1 = { holder=6076001 } # Doran (nc)
7835.1.1 = { holder=6077001 } # Nymeria (nc)
