1.1.1={ law=succ_primogeniture law=cognatic_succession
	liege="k_oldtownTK"
}
25.1.1={holder=6000285} # Uthor of the High tower (C)
94.1.1={holder=6100285} # Urrigon (C)
120.1.1={holder=6200285} # Otho I (C)
140.1.1={holder=6300285} # Uthor II
175.1.1={holder=6400285} # Otho II (C)
195.1.1={holder=0}
2670.1.1={holder=6700285} # Lymond the Sea Lion # Last king of the Oldtown (C)
2700.1.1={liege="e_reach"}
2723.1.1={holder=0}

6350.1.1={holder=6900285} # Jeremy (C)
6370.1.1={holder=61000285} # Jason (C)
6390.1.1={holder=61100285} # Jared
6445.1.1={holder=61200285} # Jarett
6465.1.1={holder=61300285} # Lymond II
6515.1.1={holder=61400285} # John
6555.1.1={holder=61500285} # Otto
6560.1.1={holder=61600285} # Gareth
6580.1.1={holder=61700285} # Dorian (C)
6638.1.1={holder=61800285} # Ormund
6650.1.1={holder=61900285} # Damon the Devout (C) # First to convert to the seven
6665.1.1={holder=62000285} # Triston (C)
6715.1.1={holder=62100285} # Barris (C)
6760.1.1={holder=0}
7450.1.1={holder=62300285} # Orton
7470.1.1={holder=63200285} # Runcel
7490.1.1={holder=62400285} # Lymond III
7540.1.1={holder=62600285} # Oscar
7557.1.1={holder=63100285} # Morgan
7560.1.1={
	holder= 21285 # Bandred
}
7594.1.1={
	holder= 17285 # Manfred Hightower
}

7620.1.1={
	holder= 16285 # Martyn Hightower 	
}
7642.1.1={
	holder= 177285 # Ben Hightower 
}
7680.1.1={
	holder= 14285 # Runcel Hightower 
}
7705.1.1={
	holder= 11285 # Lyonel Hightower 
}
7720.1.1={
	holder= 12285 # Ormund Hightower 
}
7730.8.1={
	holder= 175285 # Manfred Hightower 
}
7775.1.1={
	holder= 10285 # Jon Hightower 
}

7815.1.1={
	holder= 9285 # Abelar Hightower 
}
7826.1.1={
	holder= 88017 # Quenton Hightower - Died in 8232
}
7838.5.5={
	holder=285 # Leyton Hightower
}


