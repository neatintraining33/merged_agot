#House Xhaxos
7060.1.1 = { 
	holder=100304008 
	law = succ_seniority
	law = cognatic_succession
} # Adeyemu (nc)
7103.1.1 = { holder=101304008 } # Odingaxho (nc)
7120.1.1 = { holder=103304008 } # Bapato (nc)
7142.1.1 = { holder=107304008 } # Doshuru (nc)
7202.1.1 = { holder=108304008 } # Rudo (nc)
7217.1.1 = { holder=112304008 } # Odingaxho (nc)
7244.1.1 = { holder=114304008 } # Quhura (nc)

#House Qo Unifies the Islands
7258.1.1 = { holder=114304005 } # Xanda (C)
7278.1.1 = { holder=118304005 } # Isingodo (nc)
7303.1.1 = { holder=121304005 } # Thabo (nc)
7321.1.1 = { holder=122304005 } # Xhelimo (nc)
7335.1.1 = { holder=124304005 } # Jalahuru (nc)

#House Xho usurps the Islands
7358.1.1 = { holder=122304000 } # Udo (nc)
7387.1.1 = { holder=126304000 } # Qubhar (nc)

7389.6.1 = { holder=127304005 } #Usurped by House Ko
