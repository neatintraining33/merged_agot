3.1.1={
	law = succ_primogeniture
	law = cognatic_succession
	law = centralization_1
	law = investiture_law_2
	law = first_night_1
}
20.1.1={holder=88151} # Garth I
80.1.1={holder=65101524} # Garth II # Post Long Night
98.1.1={holder=65201524} # Gwayne I
110.1.1={holder=65301524} # Gareth I
123.1.1={holder=65401524} # Gordan I Grey-Eyes
167.1.1={holder=59501524} # Mervyn I
180.1.1={holder=0}

2550.1.1={holder=68901524} # Garth III the Great # Old-Gods era
2602.1.1={holder=65501524} # Gareth II
2604.1.1={holder=65601524} # Gyles I the Woe
2632.1.1={holder=65701524} # Garland I
2645.1.1={holder=59601524} # Mervyn II
2650.1.1={holder=65801524} # John I
2660.1.1={holder=65901524} # Garland II
2705.1.1={holder=67001524} # Gwayne II
2720.1.1={holder=67101524} # Gwayne III
2760.1.1={holder=67201524} # John II the Tall
2780.1.1={holder=67301524} # Garth IV
2790.1.1={holder=67401524} # Garth V the Hammer
2798.1.1={holder=67501524} # Mern I
2860.1.1={holder=65001524} # Garse I
2880.1.1={holder=64001524} # Garse II
2901.1.1={holder=67601524} # Garth VI the Morningstar
2904.1.1={holder=0}
4340.1.1={holder=65501599}
4380.1.1={holder=65509599}
4400.1.1={holder=0}
6397.1.1={holder=67701524} # Garth VII the Goldenhand
6483.1.1={holder=63001524} # Garse III
6500.1.1={holder=59801524} # Gareth III
6503.1.1={holder=68201524} # Garth VIII
6506.1.1={holder=68001524} # Gareth IV
6530.1.1={holder=68101524} # Mern II the Mason
6580.1.1={holder=68301524} # Mern III the Madling
6590.1.1={holder=68401524} # Garth IX
6604.1.1={holder=68501524} # Merle the Meek
6612.1.1={holder=68701524} # Gwayne V the Andal 

6635.1.1={holder=0} # 

7050.1.1={holder=59701524} # Mervyn III
7089.1.1={holder=60001524} # Garse IV # Post-Andal era
7119.1.1={holder=61001524} # Garse V
7150.1.1={holder=62001524} # Garla I ruling Queen
7201.1.1={holder=88152} # Mern IV
7212.1.1={holder=88153} # Mern V
7235.1.1={holder=88154} # Gyles II
7237.1.1={holder=88155} # Garth X Greybeard. Where the direct line ends and goes to his 2nd cousin Mern VI
7326.1.1={holder=88156} # Mern VI
7330.1.1={holder=88157} # Garth XI the Painter
7340.1.1={holder=599001254} # Greydon I
7341.1.1={holder=88158} # Garse VI
7364.1.1={holder=88159} # Garland III
7379.1.1={holder=56201254} # Perceon III. Exiled the Manderly's
7434.1.1={holder=88160} # Gareth VI
