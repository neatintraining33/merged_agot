1.1.1 = { effect = { location = { set_province_flag = pre_harren_harrenhal } } }

###House Justman
6939.1.1 = { holder=600174432 } # Benedict (C) # House Jusman
6962.1.1 = { holder=602174432 } # Benedict (C) 
7022.1.1 = { holder=606174432 } # Bernarr (C)
7029.1.1 = { holder=607174432 } # Edwyn (nc)
7038.1.1 = { holder=609174432 } # Brynden (nc)
7086.1.1 = { holder=615174432 } # Bernarr (C)

7101.1.1 = { holder=0 } # Justman royal line extinct

###House Teague
7200.1.1 = { holder=600174435 } # Torrence (C) # House Teague
7250.1.1 = { holder=601174435 } # Brynden (nc)
7251.1.1 = { holder=602174435 } # Petyr (nc)
7275.1.1 = { holder=604174435 } # Theo (C)
7290.1.1 = { holder=607174435 } # Willem (nC)
7326.1.1 = { holder=608174435 } # Harold (nC)
7354.1.1 = { holder=609174435 } # Randyl (nC)