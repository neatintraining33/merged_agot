623.1.1={ 
	liege="c_norvos"
	law = succ_appointment	
	effect = {
		set_title_flag = military_command
		holder_scope = {
			if = {
				limit = { primary_title = { title = PREVPREV } }
				set_government_type = military_command_government_city 
				PREV = { succession = appointment }
				recalc_succession = yes
			}
		}
	}
}
# 7069.1.1 = { holder=100300562 } # Cossomo (nc)
# 7110.1.1 = { holder=101300562 } # Ordello (nc)
# 7123.1.1 = { holder=103300562 } # Beqqo (nc)
# 7139.1.1 = { holder=106300562 } # Areo (nc)
# 7162.1.1 = { holder=107300562 } # Narbo (nc)
# 7191.1.1 = { holder=110300562 } # Allaquo (nc)
# 7197.1.1 = { holder=112300562 } # Syrio (nc)
# 7206.1.1 = { holder=113300562 } # Collio (nc)
# 7226.1.1 = { holder=117300562 } # Cossomo (nc)
# 7246.1.1 = { holder=118300562 } # Daario (nc)
# 7287.1.1 = { holder=120300562 } # Luco (nc)
# 7288.1.1 = { holder=123300562 } # Yorko (nc)
# 7294.1.1 = { holder=126300562 } # Illyrio (nc)
# 7335.1.1 = { holder=128300562 } # Illyrio (nc)
# 7351.1.1 = { holder=129300562 } # Myrmello (nc)
# 7368.1.1 = { holder=133300562 } # Moreo (nc)
# 7381.1.1 = { holder=137300562 } # Byan (nc)



