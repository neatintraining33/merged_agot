5559.1.1={ 
	holder=901174999 #Qarlton the great
} 
5579.1.1={ holder = 0 }
7055.1.1 = { 
	holder=77000500 # Areo (nc)
	law = slavery_2
	law = centralization_1 
	law = succ_open_elective
	effect = {
		holder_scope = { 
			e_new_valyria = {
				holder_scope = {
					make_tributary = { who = PREVPREV tributary_type = valyrian_tributary }
				}
			}
		}	
	}
} 
7125.1.1 = { holder=77000501 } # Ferrego (nc)
7131.1.1 = { holder=77000502 } # Luco (nc)
7145.1.1 = { holder=77000503 } # Beqqo (nc)
7200.1.1 = { holder=77000504 } # Areo (nc)
7225.1.1 = { holder=77000505 } # Lotho (nc)
7263.1.1 = { holder=77000506 } # Moredo (nc)
7293.1.1 = { holder=77000507 } # Luco (nc)
7321.1.1 = { holder=77000508 } # Meralyn (nc)
7340.1.1 = { holder=77000509 } # Groleo (nc)
7353.1.1 = { holder=77000510 } # Ordello (nc)
7385.1.1 = { holder=77000511 } # Allaquo (nc)
