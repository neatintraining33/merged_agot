20.1.1 = {
	holder = 41544 #Durran Godsgrief
	liege="d_shipbreaker"
	law = succ_primogeniture
	law = cognatic_succession
}
90.1.1 = { holder = 1501544 } #Durran II Durrandon
122.1.1 = { holder = 1511544 } #Durran III Durrandon (NC)
149.1.1 = { holder = 1521544 } #Durran IV Durrandon
162.1.1 = { holder = 1531544 } #Durran V Durrandon
189.1.1 = { holder = 1541544 } #Durran VI Durrandon
219.1.1 = { holder = 1551544 } #Erich I Durrandon (NC)
230.1.1 = { holder = 1561544 } #Durran VII Durrandon
282.1.1 = { holder = 0 }

3050.1.1 = { holder = 1591544 } #Durran VIII Durrandon
3078.1.1 = { holder = 1601544 } #Erich II Durrandon (NC)
3090.1.1 = { holder = 1611544 } #Erich III Durrandon
3126.1.1 = { holder = 1621544 } #Maldon I Durrandon (NC)
3145.1.1 = { holder = 1631544 } #Maldon II Durrandon (NC)
3156.1.1 = { holder = 1641544 } #Ormund I Durrandon (NC)
3198.1.1 = { holder = 1651544 } #Durran IX Durrandon (NC)
3205.1.1 = { holder = 1661544 } #Morden I Durrandon (NC)
3238.1.1 = { holder = 1671544 } #Erich IV Durrandon (NC)
3249.1.1 = { holder = 1681544 } #Durran X Durrandon
3275.1.1 = { holder = 1691544 } #Monfryd I Durrandon
3299.1.1 = { holder = 1701544 } #Durran XI Durrandon
3331.1.1 = { holder = 1711544 } #Barron  Durrandon
3365.1.1 = { holder = 1721544 } #Monfryd II Durrandon (NC)
3380.1.1 = { holder = 1731544 } #Durran XII Durrandon (NC)
3391.1.1 = { holder = 1741544 } #Erich V Durrandon (NC)
3419.1.1 = { holder = 1751544 } #Qarlton I Durrandon (NC)
3451.1.1 = { holder = 1761544 } #Durran XIII Durrandon (NC)
3463.1.1 = { holder = 1771544 } #Durran XIV Durrandon (NC)
3475.1.1 = { holder = 1781544 } #Durwald I Durrandon
3534.1.1 = { holder = 1791544 } #Monfryd III Durrandon (NC)
3549.1.1 = { holder = 1801544 } #Erich VI Durrandon (NC)
3568.1.1 = { holder = 1811544 } #Durran XV Durrandon (NC)
3588.1.1 = { holder = 1821544 } #Durran XVI Durrandon (NC)
3603.1.1 = { holder = 1831544 } #Morden II Durrandon
3608.1.1 = { holder = 1841544 } #Ronard  Durrandon
3638.1.1 = { holder = 0 }

6339.1.1 = { holder = 1861544 } #Maldon III Durrandon (NC)
6363.1.1 = { holder = 1871544 } #Ormund II Durrandon (NC)
6393.1.1 = { holder = 1881544 } #Durran XVII Durrandon (NC)
6423.1.1 = { holder = 1891544 } #Durran XVIII Durrandon (NC)
6445.1.1 = { holder = 1901544 } #Monfryd IV Durrandon (NC)
6468.1.1 = { holder = 1911544 } #Erich VII Durrandon
6501.1.1 = { holder = 1921544 } #Durran IXX Durrandon (NC)
6515.1.1 = { holder = 1941544 } #Qarlton II Durrandon
6532.1.1 = { holder = 1931544 } #Qarlton III Durrandon
6555.1.1 = { holder = 1951544 } #Monfryd V Durrandon
6567.1.1 = { holder = 1961544 } #Durran XX Durrandon (NC)
6582.1.1 = { holder = 1971544 } #Baldric I Durrandon
6617.1.1 = { holder = 1981544 } #Durran XXI Durrandon
6632.1.1 = { holder = 1991544 } #Cleoden I Durrandon
6647.1.1 = { holder = 2021544 } #Maldon IV Durrandon
6678.1.1 = { holder = 2031544 } #Durran XXIV Durrandon
6690.1.1 = { holder = 2041544 } #Ormund III Durrandon

6723.1.1 = { holder = 0 }

7095.1.1 = { holder=10001544 } # Torghold (nc)
7127.1.1 = { holder=10011544 } # Willem (nc)
7140.1.1 = { holder=10041544 } # Steffon (nc)
7175.1.1 = { holder=10051544 } # Durrhold (nc)
7193.1.1 = { holder=10071544 } # Durin (nc)
7206.1.1 = { holder=10081544 } # Balder (nc)

7267.1.1 = { holder = 22001544 } #Erich Durrandon (NC)
7277.1.1 = { holder = 2001544 } #Durran XXII Durrandon (NC)
7298.1.1 = { holder = 2011544 } #Durran XXIII Durrandon (NC)
7326.1.1 = { holder = 21011544 } #Baldric Durrandon (NC)

