
#Dragon of Yi Ti Empress
77990108 = {
	name="Vaelax"
	dynasty = 497007
	female = yes
	
	religion="dragon_rel"
	culture="dragon_culture"
	
	martial = 34
	
	add_trait="dragon"
	add_trait="misguided_warrior"
	
	disallow_random_traits = yes
	
	father = 77990109
	
	7045.1.1={
		birth="7045.1.1"
		effect = { 
			set_character_flag = tamed_dragon 			
			character_event = { id = dragon.0 }
			c_40065962 = { add_friend = ROOT }
			e_yi_ti = {
				holder_scope = {					
					ROOT = { move_character = PREV }
				}	
			}
		}
	}
	7165.1.1={
		death="7165.1.1"
	}
}
77990109 = {
	name="Unknown Dragon"

	religion="dragon_rel"
	culture="dragon_culture"
	
	martial = 50
	
	add_trait="dragon"
	add_trait="misguided_warrior"
	
	disallow_random_traits = yes
	
	6950.1.1={
		birth="6950.1.1"
	}	
	7045.1.1={
		death="7045.1.1"
	}
}

#Nagga the Sea Dragon
1836582 = {
	name = "Nagga"
	dynasty = 1836582
	
	religion = dragon_rel
	culture="dragon_culture"
	
	martial = 120
	
	add_trait = dragon
	add_trait = misguided_warrior
	add_trait = dragon_12
	add_trait = strong_dragon
	add_trait = wroth_dragon
	
	disallow_random_traits = yes
	
	father = 750066
	
	1.1.3 = {
		birth="1.1.3"
		give_nickname = nick_sea_dragon
		effect = { 			
			character_event = { id = dragon.0 }		
		}
	}
	300.1.1 = {
		death = {
			# Killed by the Grey King
			death_reason = death_duel
			killer = 750066
		}
	}
}