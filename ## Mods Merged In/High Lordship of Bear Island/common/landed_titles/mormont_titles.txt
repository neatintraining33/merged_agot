d_bearisland = {
	color={ 172 185 172 }
	color2={ 255 255 255 }

	capital = 33 #Bear Island

	allow = {
		dynasty = 102
		has_landed_title = c_bearisland
	}
}