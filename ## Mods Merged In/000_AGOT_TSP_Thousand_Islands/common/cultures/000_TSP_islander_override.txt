islander_group = {
	graphical_cultures = { drownedgodgfx } # buildings/holdings
	
	islander = {
		graphical_cultures = { islandergfx } # Portraits

		used_for_random = no
		alternate_start = { 
			OR = {
				has_alternate_start_parameter = { key = special_culture value = mythological } 
				has_alternate_start_parameter = { key = special_culture value = all } 
			}
		}
		
		color = { 0.45 0.45 0.55 }
		
		male_names = {
			Bodger Brogg
			Caloth Clargg Colosh Corffmana Cull
			Deh Depp Doth Draloth Drogg Durr
			Eleogg Erroth
			Green Grog Grok Groo Grugg Gurn
			Haggon Holger Howd
			Innsmowth
			Jogg Joth Jarr
			Korf Krull
			Marrlinos Mikkel Morr Moggoth
			Nemos
			Oba Ogg Oog
			Phlips
			Quort
			Skoodge Skrogg Skugg Slugg Sogg Sogoloth
			Tumosh Tugg Togg Torr Teleoth
			Ugg Ulf Umar
			Verd Verdeos Vuh
			Ygon
			Zed
		}
		
		female_names = {
			Alred Assadora
			Bellegere Bess Betharios Blasey Brea Brella
			Chatana Coralos Cossoma
			Denia Doryos
			Ferny Frynne
			Glucka Greeni Grisel Gu Gug Guh Gylenios Gylora
			Harra Hotep
			Indrawattie Izembara
			Jaqea
			Kamala
			Lanna Luff Lum Luv
			Mellario Mera Meralyia Mordea
			Nyalhotep
			Oscaio
			Rampersaud Rogga Rowan
			Slurry
			Tagganaria Tak Torma
			Umma
			Vada Vadda Ved Vat
			Yna Ynys Yorka
			Zia Zoe  
		}

		male_patronym = "deep"
		female_patronym = "deep"

		from_dynasty_suffix = "feesh"
		bastard_dynasty_prefix = "Thulu"
		
		founder_named_dynasties = yes
		disinherit_from_blinding = yes
		allow_looting = yes
		seafarer = yes
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 60
		mat_grf_name_chance = 10
		father_name_chance = 30
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 50
		
#		modifier = backward_culture_modifier
	}
}